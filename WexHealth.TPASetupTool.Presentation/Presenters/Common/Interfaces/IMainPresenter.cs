﻿using System;
using WexHealth.TPASetupTool.Presentation.Views;

namespace WexHealth.TPASetupTool.Presentation.Presenters.Common.Interfaces
{
    public delegate IMainPresenter MainPresenterFactory(IMainView view);

    public interface IMainPresenter
    {
        Guid AdministratorId { set; }

        void InitView(bool isPostBack);

        void LoadAdministrator();
    }
}
