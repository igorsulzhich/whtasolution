﻿using System;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;
using WexHealth.TPASetupTool.Presentation.Views;

namespace WexHealth.TPASetupTool.Presentation.Presenters.Common.Interfaces
{
    public delegate IManageProfilePresenter ManageProfilePresenterFactory(IManageProfileView view);

    public interface IManageProfilePresenter
    {
        Guid AdministratorId { set; }

        void InitView(bool isPostBack);

        IResult CreateAdministrator();

        IResult UpdateAdministrator();
    }
}
