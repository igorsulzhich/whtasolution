﻿using WexHealth.TPASetupTool.Presentation.Views;

namespace WexHealth.TPASetupTool.Presentation.Presenters.Common.Interfaces
{
    public delegate ILoginPresenter LoginPresenterFactory(ILoginView view);

    public interface ILoginPresenter
    {
        void InitView(bool isPostBack);

        void LoadAdministrators();
    }
}
