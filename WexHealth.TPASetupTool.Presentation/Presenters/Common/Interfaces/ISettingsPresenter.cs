﻿using System;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;
using WexHealth.Presentation.Views;

namespace WexHealth.TPASetupTool.Presentation.Presenters.Common.Interfaces
{
    public delegate ISettingsPresenter SettingsPresenterFactory(ISettingView view);

    public interface ISettingsPresenter
    {
        Guid AdministratorId { set; }

        void InitView(bool isPostBack);

        IResult SaveSettings();
    }
}
