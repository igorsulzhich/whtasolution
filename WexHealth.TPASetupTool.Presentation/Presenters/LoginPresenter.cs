﻿using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.TPASetupTool.Presentation.Views;
using WexHealth.TPASetupTool.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.TPASetupTool.Presentation.Presenters
{
    public class LoginPresenter : ViewPresenterBase<ILoginView>, ILoginPresenter
    {
        private readonly IAdministratorService _administratorService;

        public LoginPresenter(ILoginView view, IAdministratorService administratorService) : base(view, false)
        {
            _administratorService = administratorService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            LoadAdministrators();
        }

        public void LoadAdministrators()
        {
            _view.Administrators = _administratorService.GetAdministrators();
        }
    }
}
