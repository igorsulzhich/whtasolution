﻿using System;
using Autofac.Extras.Attributed;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Models;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;
using WexHealth.TPASetupTool.Presentation.Views;
using WexHealth.TPASetupTool.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.TPASetupTool.Presentation.Presenters
{
    public class ManageProfilePresenter : ViewPresenterBase<IManageProfileView>, IManageProfilePresenter
    {
        private readonly IAdministratorService _administratorService;
        private readonly IDomainService _domainService;

        private Guid _administratorId;

        public Guid AdministratorId
        {
            set
            {
                _administratorId = value;
            }
        }

        public ManageProfilePresenter(IManageProfileView view, IAdministratorService administratorService, IDomainService domainService) :
            base(view, false)
        {
            _administratorService = administratorService;
            _domainService = domainService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            LoadStates();
            LoadAdministrator();
        }

        private void LoadStates()
        {
            _view.States = _domainService.GetStates();
        }

        private void LoadAdministrator()
        {
            if (!Guid.Empty.Equals(_administratorId))
            {
                Administrator administrator = _administratorService.GetAdministratorById(_administratorId);
                if (null == administrator)
                {
                    _view.ShowErrorMessage("Administrator doesn't exist");
                }

                Map(administrator);
            }
        }

        public IResult CreateAdministrator()
        {
            IResult result = null;

            var model = new AdministratorInputModel();
            Map(model);

            try
            {
                Guid id = _administratorService.CreateAdministrator(model);

                result = new OperationResult(OperationStatus.Success);
            }
            catch (Exception exception)
            {
                result = new OperationResult(OperationStatus.Failed, exception.Message);
            }

            return result;
        }

        public IResult UpdateAdministrator()
        {
            IResult result = null;

            var model = new AdministratorInputModel();
            Map(model);

            try
            {
                _administratorService.UpdateAdministrator(model);

                result = new OperationResult(OperationStatus.Success);
            }
            catch (Exception exception)
            {
                result = new OperationResult(OperationStatus.Failed, exception.Message);
            }

            return result;
        }

        //Domain entity -> View
        private void Map(Administrator administrator)
        {
            _view.AdministratorName = administrator.Name;
            _view.Alias = administrator.Alias;

            if (null != administrator.Address)
            {
                _view.Street = administrator.Address.Street;
                _view.City = administrator.Address.City;
                _view.SelectedState = administrator.Address.State;
                _view.ZipCode = administrator.Address.ZipCode;
            }

            if (null != administrator.Phone)
            {
                _view.Phone = administrator.Phone.PhoneNumber;
            }
        }

        //View -> BLL model
        private void Map(AdministratorInputModel model)
        {
            model.Id = _administratorId;
            model.Name = _view.AdministratorName;
            model.Alias = _view.Alias;

            model.Address = new AddressInputModel();
            Map(model.Address);

            model.Phone = new PhoneInputModel();
            Map(model.Phone);
        }

        private void Map(AddressInputModel model)
        {
            model.Street = _view.Street;
            model.City = _view.City;
            model.State = _view.SelectedState;
            model.ZipCode = _view.ZipCode;
        }

        private void Map(PhoneInputModel model)
        {
            model.Phone = _view.Phone;
        }
    }
}
