﻿using System;
using Autofac.Extras.Attributed;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;
using WexHealth.Presentation.Views;
using WexHealth.TPASetupTool.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.TPASetupTool.Presentation.Presenters
{
    public class SettingsPresenter : ViewPresenterBase<ISettingView>, ISettingsPresenter
    {
        private readonly ISettingService _settingService;

        private Guid _administratorId;

        public Guid AdministratorId
        {
            set
            {
                _administratorId = value;
            }
        }

        public SettingsPresenter(ISettingView view, [WithKey("Administrator")] ISettingService settingService) :
            base(view, false)
        {
            _settingService = settingService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            _settingService.InitService(null, _administratorId);

            LoadSettings();
        }

        private void LoadSettings()
        {
            _view.SettingList = _settingService.GetSettings();
        }

        public IResult SaveSettings()
        {
            IResult result = null;

            try
            {
                _settingService.SaveSettings(_view.SelectedSettings);
                result = new OperationResult(OperationStatus.Success);
            }
            catch (Exception exception)
            {
                result = new OperationResult(OperationStatus.Failed, exception.Message);
            }

            return result;
        }
    }
}
