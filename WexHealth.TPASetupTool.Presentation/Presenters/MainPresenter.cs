﻿using System;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.TPASetupTool.Presentation.Views;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.TPASetupTool.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.TPASetupTool.Presentation.Presenters
{
    public class MainPresenter : ViewPresenterBase<IMainView>, IMainPresenter
    {
        private readonly IAdministratorService _administratorService;

        private Guid _administratorId;

        public Guid AdministratorId
        {
            set
            {
                _administratorId = value;
            }
        }

        public MainPresenter(IMainView view, IAdministratorService administratorService) :
            base(view, false)
        {
            _administratorService = administratorService;
        }

        protected override void InternalInitView(bool isPostBack)
        { }

        public void LoadAdministrator()
        {
            Administrator dbAdministrator = _administratorService.GetAdministratorById(_administratorId);
            if (null == dbAdministrator)
            {
                _view.ShowErrorMessage("Administrator doesn't exist");
            }
            else
            {
                _view.CurrentAdministrator = dbAdministrator;
            }
        }
    }
}
