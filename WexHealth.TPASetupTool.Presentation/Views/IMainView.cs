﻿using System;
using WexHealth.Domain.Entities;


namespace WexHealth.TPASetupTool.Presentation.Views
{
    public interface IMainView
    {
        Administrator CurrentAdministrator { set; }

        void ShowErrorMessage(string errorMessage);
    }
}
