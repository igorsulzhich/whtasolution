﻿using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.TPASetupTool.Presentation.Views
{
    public interface IManageProfileView
    {
        string AdministratorName { get; set; }

        string Alias { get; set; }

        string Street { get; set; }

        string City { get; set; }

        IEnumerable<State> States { set; }

        string SelectedState { get; set; }

        string ZipCode { get; set; }

        string Phone { get; set; }

        void ShowErrorMessage(string errorMessage);
    }
}
