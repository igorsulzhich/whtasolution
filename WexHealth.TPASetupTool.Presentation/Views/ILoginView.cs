﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.TPASetupTool.Presentation.Views
{
    public interface ILoginView
    {
        Guid SelectedAdministrator { get; set; }

        IEnumerable<Administrator> Administrators { set; }

        void ShowErrorMessage(string errorMessage);
    }
}
