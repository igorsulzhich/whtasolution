﻿using System.Reflection;
using System.ComponentModel.Composition;
using Autofac;
using Autofac.Builder;
using Autofac.Extras.Attributed;
using WexHealth.Common.Modules.Common.Interfaces;
using WexHealth.TPASetupTool.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.TPASetupTool.Presentation
{
    [Export(typeof(IModule))]
    public class ModuleInit : IModule
    {
        public void Initialize(ContainerBuilder builder)
        {
            var assembly = Assembly.GetExecutingAssembly();

            builder.RegisterAssemblyTypes(assembly)
                   .Where(t => t.Name.EndsWith("Presenter"))
                   .AsImplementedInterfaces().WithAttributeFilter();

            builder.RegisterGeneratedFactory<LoginPresenterFactory>();
            builder.RegisterGeneratedFactory<MainPresenterFactory>();
            builder.RegisterGeneratedFactory<ManageProfilePresenterFactory>();
            builder.RegisterGeneratedFactory<SettingsPresenterFactory>();
        }
    }
}
