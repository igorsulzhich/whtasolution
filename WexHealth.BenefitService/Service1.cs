﻿using System;
using System.Linq;
using System.ServiceProcess;
using System.Timers;
using System.Collections.Generic;
using WexHealth.Domain.Entities;
using WexHealth.DAL.Repositories.Common.Interfaces;
using WexHealth.BLL.Exceptions;

namespace WexHealth.BenefitService
{
    public partial class Service1 : ServiceBase
    {
        private Timer _timer;
        private DateTime _prevRun;

        private readonly IBenefitsPackageRepository _benefitsPackageRepository;
        private readonly IEnrollmentRepository _enrollmentRepository;
        private readonly IContributionRepository _contributionRepository;

        public Service1(
            IBenefitsPackageRepository benefitsPackageRepository,
            IEnrollmentRepository enrollmentRepository,
            IContributionRepository contributionRepository)
        {
            InitializeComponent();

            _benefitsPackageRepository = benefitsPackageRepository;
            _enrollmentRepository = enrollmentRepository;
            _contributionRepository = contributionRepository;
        }

        protected override void OnStart(string[] args)
        {
            _timer = new Timer(30 * 1000);
            _timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
            _timer.Start();
        }

        protected override void OnStop()
        {
        }

        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _timer.Stop();
            try
            {
                IEnumerable<Enrollment> enrollments = _enrollmentRepository.GetEnrollmentsByPackage("Initialized", DateTime.Now);
                eventLog1.WriteEntry($"Get {enrollments.Count()} active enrollments for checking");

                foreach (var i in enrollments)
                {
                    int packageId = i.BenefitsOffering.BenefitsPackageId;
                    BenefitsPackage dbPackage = _benefitsPackageRepository.GetPackageById(packageId);
                    if (null == dbPackage)
                    {
                        throw new EntityNotFoundException("BenefitPackage", "id", packageId.ToString());
                    }

                    i.Contributions = _contributionRepository.GetListByEnrollment(i.Id);

                    TimeSpan periodForPay;

                    Contribution dbContribution = i.Contributions.OrderByDescending(x => x.DateOfContribute).FirstOrDefault();
                    if (null == dbContribution)
                    {
                        periodForPay = DateTime.Now - dbPackage.DateOfStart;
                    }
                    else
                    {
                        periodForPay = DateTime.Now - dbContribution.DateOfContribute;
                    }

                    if (periodForPay.Days == dbPackage.Frequency)
                    {
                        eventLog1.WriteEntry($"Enrollment with id: {i.Id}, has {periodForPay.Days} days from last contribution");

                        TimeSpan totalPeriod = dbPackage.DateOfEnd - dbPackage.DateOfStart;

                        int restDays = totalPeriod.Days % dbPackage.Frequency;
                        if (restDays != 0)
                        {
                            totalPeriod = totalPeriod.Add(-TimeSpan.FromDays(restDays));
                        }

                        decimal amountForPay = i.BenefitsOffering.ContributionAmount * dbPackage.Frequency / totalPeriod.Days;

                        var contribution = new Contribution
                        {
                            Amount = amountForPay,
                            DateOfContribute = DateTime.Now,
                            EnrollmentId = i.Id
                        };

                        _contributionRepository.Save(contribution);

                        eventLog1.WriteEntry($"Contribute ${contribution.Amount} for enrollment with id: {contribution.EnrollmentId}");
                    }
                }
            }
            catch (Exception ex)
            {
                eventLog1.WriteEntry(ex.Message);
            }

            _prevRun = DateTime.Now;
            _timer.Start();
        }
    }
}
