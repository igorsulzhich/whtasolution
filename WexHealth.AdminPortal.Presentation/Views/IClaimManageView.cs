﻿using System;

namespace WexHealth.AdminPortal.Presentation.Views
{
    public interface IClaimManageView
    {
        string ClaimId { get; }

        Guid ConsumerId { get; set; }

        string FirstName { get; set; }

        string LastName { get; set; }

        string PhoneNumber { get; set; }

        DateTime DateOfService { get; set; }

        string PlanType { get; set; }

        decimal Amount { get; set; }

        string ImageFile { get; set; }

        string StatusName { set; }
    }
}
