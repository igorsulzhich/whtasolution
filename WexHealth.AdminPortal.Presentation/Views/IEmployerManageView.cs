﻿
namespace WexHealth.AdminPortal.Presentation.Views
{
    public interface IEmployerManageView
    {
        string Code { get; set; }

        bool IsSettingAllow(string settinkKey);
    }
}
