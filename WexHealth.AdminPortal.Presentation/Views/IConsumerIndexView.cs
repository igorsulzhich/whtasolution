﻿using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.AdminPortal.Presentation.Views
{
    public interface IConsumerIndexView
    {
        string ConsumerFirstName { get; }

        string ConsumerLastName { get; }

        IEnumerable<Employer> Employers { set; }

        string SelectedEmployer { get; }

        IEnumerable<Consumer> Consumers { set; }
    }
}
