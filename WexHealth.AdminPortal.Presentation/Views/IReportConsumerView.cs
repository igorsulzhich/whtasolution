﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.AdminPortal.Presentation.Views
{
    public interface IReportConsumerView
    {
        IEnumerable<ObjectField> Fields { set; }

        IEnumerable<ObjectField> SelectedFields { get; }

        IEnumerable<Employer> Employers { set; }

        Guid SelectedEmployer { get; }
    }
}
