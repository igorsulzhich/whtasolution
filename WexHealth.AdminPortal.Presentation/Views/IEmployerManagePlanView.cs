﻿
namespace WexHealth.AdminPortal.Presentation.Views
{
    public interface IEmployerManagePlanView
    {
        int Id { get; set; }

        string Name { get; set; }

        string Type { get; set; }

        decimal Contribution { get; set; }

        string Identity { get; set; }

        int BenefitsPackageId { get; set; }

        string PlanType { get; set; }

        int PlanId { get; set; }

        bool IsStart { get; set; }
    }
}
