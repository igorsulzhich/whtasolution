﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.AdminPortal.Presentation.Views
{
    public interface IClaimEditView
    {
        string ClaimId { get; }

        Guid ConsumerId { get; set; }

        string FirstName { get; set; }

        string LastName { get; set; }

        string PhoneNumber { get; set; }

        DateTime DateOfService { get; set; }

        IEnumerable<Enrollment> Enrollments { set; }

        int SelectedEnrollment { get;  set; }

        decimal Amount { get; set; }
    }
}
