﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.AdminPortal.Presentation.Views
{
    public interface IConsumerManageProfileView
    {
        Guid ConsumerId { get; set; }

        string EmployerCode { get; }

        string FirstName { get; set; }

        string LastName { get; set; }

        string SSN { get; set; }

        string Phone { get; set; }

        string Street { get; set; }

        string City { get; set; }

        string Email { get; set; }

        string SelectState { get; set; }

        IEnumerable<State> States { set; }

        string ZipCode { get; set; }

        DateTime DateOfBirth { get; set; }

        string UserName { get; set; }

        string Password { get; set; }

        string Identity { get; set; }

    }
}
