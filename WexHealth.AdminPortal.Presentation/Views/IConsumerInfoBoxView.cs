﻿
namespace WexHealth.AdminPortal.Presentation.Views
{
    public interface IConsumerInfoBoxView
    {
        string FirstName { get; set; }

        string LastName { get; set; }

        string EmployerName { get; set; }

        string EmployerCode { get; set; }

        string SSN { get; set; }

        string Identity { get; set; }
    }
}
