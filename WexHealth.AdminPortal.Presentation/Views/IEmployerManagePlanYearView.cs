﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.AdminPortal.Presentation.Views
{
    public interface IEmployerManagePlanYearView
    {
        int Id { get; set; }

        string Name { get; set; }

        DateTime StartDate { get; set; }

        DateTime EndDate { get; set; }

        IEnumerable<BenefitsOffering> BenefitsOfferings { set; }

        IEnumerable<PayrollFrequency> PayrollFrequencies { set; }

        int SelectedPayrollFrequency { get; set; }

        IEnumerable<PlanType> PlanTypes { set; }

        string SelectedPlanType { get; set; }

        string Identity { get; set; }

        string Status { set; }

        int PlanId { get; set; }
    }
}
