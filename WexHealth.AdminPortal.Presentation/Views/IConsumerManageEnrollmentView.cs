﻿using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.AdminPortal.Presentation.Views
{
    public interface IConsumerManageEnrollmentView
    {
        string ConsumerId { get; }

        IEnumerable<Enrollment> Enrollments { set; }

        int EnrollmentId { get; set; }

        IEnumerable<BenefitsOffering> Plans { set; }

        int SelectedPlan { get; set; }

        decimal Election { get; set; }

        decimal Contribution { set; }
    }
}
