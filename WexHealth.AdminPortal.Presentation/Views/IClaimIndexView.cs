﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.AdminPortal.Presentation.Views
{
    public interface IClaimIndexView
    {
        string ClaimNumber { get; }

        IEnumerable<Employer> Employers { set; }

        string SelectedEmployer { get; }

        IEnumerable<ClaimStatus> ClaimStatuses { set; }

        string SelectedClaimStatus { get; }

        Guid ConsumerId { get; }

        IEnumerable<Claim> Claims { set; }
    }
}
