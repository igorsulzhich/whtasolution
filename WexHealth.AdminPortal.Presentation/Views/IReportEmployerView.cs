﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.AdminPortal.Presentation.Views
{
    public interface IReportEmployerView
    {
        IEnumerable<ObjectField> Fields { set; }

        IEnumerable<ObjectField> SelectedFields { get; }
    }
}
