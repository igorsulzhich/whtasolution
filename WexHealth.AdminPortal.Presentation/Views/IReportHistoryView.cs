﻿using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.AdminPortal.Presentation.Views
{
    public interface IReportHistoryView
    {
        IEnumerable<ReportRequest> ReportRequests { set; }
    }
}
