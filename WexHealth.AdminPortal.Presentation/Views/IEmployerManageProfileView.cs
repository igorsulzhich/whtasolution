﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.AdminPortal.Presentation.Views
{
    public interface IEmployerManageProfileView
    {
        Guid Id { get; set; }

        string Name { get; set; }

        string Code { get; set; }

        string Street { get; set; }

        string City { get; set; }

        string Phone { get; set; }

        string SelectState { get; set; }

        IEnumerable<State> States { set; }

        string ZipCode { get; set; }

        string Identity { get; set; }

        byte[] Logo { get; }
    }
}
