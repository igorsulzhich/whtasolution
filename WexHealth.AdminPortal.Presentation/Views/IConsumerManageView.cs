﻿
namespace WexHealth.AdminPortal.Presentation.Views
{
    public interface IConsumerManageView
    {
        string Identity { get; set; }

        string EmployerCode { get; set; }

        bool IsSettingAllow(string settingKey);
    }
}
