﻿
namespace WexHealth.AdminPortal.Presentation.Views
{
    public interface IEmployerInfoBoxView
    {
        string Name { get; set; }

        string Code { get; set; }

        string City { get; set; }

        string Zip { get; set; }

        string Phone { get; set; }

        string Logo { get; set; }
    }
}
