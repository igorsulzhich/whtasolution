﻿using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.AdminPortal.Presentation.Views
{
    public interface IEmployerIndexView
    {
        string EmployerName { get; }

        string EmployerCode { get; }

        IEnumerable<Employer> Employers { set; }
    }
}
