﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.AdminPortal.Presentation.Views
{
    public interface IBenefitsPackageView
    {
        IEnumerable<BenefitsPackage> BenefitsPackages { set; }

        string Identity { get; set; }
    }
}
