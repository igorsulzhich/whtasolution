﻿using System.Collections.Generic;
using WexHealth.BLL.Models;
using WexHealth.Domain.Entities;

namespace WexHealth.AdminPortal.Presentation.Views
{
    public interface IEmployerSetupView
    {
        IEnumerable<Setting> SettingList { set; }

        IEnumerable<SettingInputModel> SelectedSettings { get; }

        string Identity { get; }
    }
}
