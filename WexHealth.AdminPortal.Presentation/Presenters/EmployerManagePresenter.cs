﻿using System.Linq;
using System.Security.Principal;
using Autofac.Extras.Attributed;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Security.Infrastructure;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.Presentation.Presenters.Common;

namespace WexHealth.AdminPortal.Presentation.Presenters
{
    public class EmployerManagePresenter : ViewPresenterBase<IEmployerManageView>, IEmployerManagePresenter
    {
        private readonly ISettingReadService _settingService;

        public EmployerManagePresenter(IEmployerManageView view, IPrincipal principal, [WithKey("Administrator")] ISettingReadService settingService) :
            base(view, principal)
        {
            _settingService = settingService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            User user = ((WexHealthIdentity)_principal.Identity).User;
            _settingService.InitService(user);
        }

        public bool IsSettingAllow(string settingKey)
        {
            return _settingService.GetSettings().Any(x => x.Name.Equals(settingKey));
        }
    }
}


