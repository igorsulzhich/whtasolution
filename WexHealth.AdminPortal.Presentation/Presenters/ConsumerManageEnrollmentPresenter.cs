﻿using System;
using System.Linq;
using System.Security.Principal;
using FluentValidation;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Exceptions;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Security.Infrastructure;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.AdminPortal.Presentation.Presenters
{
    public class ConsumerManageEnrollmentPresenter : ViewPresenterBase<IConsumerManageEnrollmentView>, IConsumerManageEnrollmentPresenter
    {
        private readonly IEnrollmentReadService _enrollmentReadService;
        private readonly IEnrollmentManageService _enrollmentManageService;
        private readonly IBenefitsOfferingReadService _benefitService;

        public ConsumerManageEnrollmentPresenter(IConsumerManageEnrollmentView view, IPrincipal principal, IEnrollmentReadService enrollmentReadService,
            IEnrollmentManageService enrollmentManageService, IBenefitsOfferingReadService benefitService) :
            base(view, principal)
        {
            _enrollmentReadService = enrollmentReadService;
            _enrollmentManageService = enrollmentManageService;
            _benefitService = benefitService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            User user = ((WexHealthIdentity)_principal.Identity).User;
            _enrollmentReadService.InitService(user, Guid.Parse(_view.ConsumerId));
            _enrollmentManageService.InitService(user, Guid.Parse(_view.ConsumerId));
            _benefitService.InitService(user, Guid.Parse(_view.ConsumerId));

            if (!isPostBack)
            {
                LoadPlans();
                LoadEnrollments();
            }
        }

        public void LoadEnrollment(int id)
        {
            if (id > 0)
            {
                Enrollment enrollment = _enrollmentReadService.GetEnrollment(id);
                MapFromModel(enrollment);
            }
        }

        public void LoadPlanContribution()
        {
            if (_view.SelectedPlan > 0)
            {
                BenefitsOffering plan = _benefitService.GetBenefitsOfferingById(_view.SelectedPlan);
                if (null != plan)
                {
                    _view.Contribution = plan.ContributionAmount;
                }
            }
            else
            {
                _view.Contribution = 0;
            }
        }

        private void LoadPlans()
        {
            _view.Plans = _benefitService.GetBenefitsOfferings().OrderBy(x => x.Name);
        }

        private void LoadEnrollments()
        {
            _view.Enrollments = _enrollmentReadService.GetEnrollments();
        }

        public IResult CreateEnrollment()
        {
            IResult result = null;

            try
            {
                if (_view.EnrollmentId == 0)
                {
                    var enrollment = new Enrollment();
                    MapFromView(enrollment);

                    _enrollmentManageService.CreateEnrollment(enrollment);

                    result = new OperationResult(OperationStatus.Success);

                    LoadEnrollments();
                }
            }
            catch (EntityOperationFailedException e) when (
                e.InnerException != null &&
                e.InnerException is EntityNotFoundException ||
                e.InnerException is InvalidOperationException ||
                e.InnerException is ValidationException
            )
            {
                result = new OperationResult(OperationStatus.Failed, e.InnerException.Message);
            }
            catch (Exception)
            {
                result = new OperationResult(OperationStatus.Failed, "Server error");
            }

            return result;
        }

        public IResult UpdateEnrollment()
        {
            IResult result = null;

            try
            {
                if (_view.EnrollmentId > 0)
                {
                    var enrollment = new Enrollment();
                    MapFromView(enrollment);

                    _enrollmentManageService.UpdateEnrollment(enrollment);

                    result = new OperationResult(OperationStatus.Success);

                    LoadEnrollments();
                }
            }
            catch (EntityOperationFailedException e) when (
                e.InnerException != null &&
                e.InnerException is EntityNotFoundException ||
                e.InnerException is InvalidOperationException ||
                e.InnerException is ValidationException
            )
            {
                result = new OperationResult(OperationStatus.Failed, e.InnerException.Message);
            }
            catch (Exception)
            {
                result = new OperationResult(OperationStatus.Failed, "Server error");
            }

            return result;
        }

        private void MapFromModel(Enrollment enrollment)
        {
            _view.SelectedPlan = enrollment.BenefitsOfferingId;
            _view.Election = enrollment.ElectionAmount;
            _view.Contribution = enrollment.BenefitsOffering.ContributionAmount;
        }

        private void MapFromView(Enrollment enrollment)
        {
            enrollment.Id = _view.EnrollmentId;
            enrollment.BenefitsOfferingId = _view.SelectedPlan;
            enrollment.ElectionAmount = _view.Election;
        }
    }
}
