﻿using System;
using System.Security.Principal;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Models;
using WexHealth.BLL.Exceptions;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Security.Infrastructure;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.AdminPortal.Presentation.Presenters
{
    public class EmployerManageProfilePresenter : ViewPresenterBase<IEmployerManageProfileView>, IEmployerManageProfilePresenter
    {
        private readonly IAdministratorService _administratorService;
        private readonly IEmployerService _employerService;
        private readonly IDomainService _domainService;

        private readonly Guid _administratorId;
        private readonly Guid _userId;

        public EmployerManageProfilePresenter(IEmployerManageProfileView view, IPrincipal principal, IAdministratorService administratorService,
            IEmployerService employerService, IDomainService domainService)
            : base(view, principal)
        {
            _administratorService = administratorService;
            _employerService = employerService;
            _domainService = domainService;

            User user = ((WexHealthIdentity)_principal.Identity).User;

            _administratorId = _administratorService.GetAdministratorBySecurityDomain(user.SecurityDomainId).Id;
            _userId = user.Id;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            if (!isPostBack)
            {
                LoadStates();

                if (!String.IsNullOrEmpty(_view.Identity))
                {
                    LoadEmployer();
                }
            }
        }

        public IResult SaveEmployer()
        {
            IResult result = null;

            try
            {
                var model = new EmployerInputModel();
                Map(model);

                if (Guid.Empty.Equals(model.Id))
                {
                    Guid employerId = _employerService.CreateEmployer(model, _administratorId);
                }
                else
                {
                    _employerService.UpdateEmployer(model, _administratorId);
                }

                result = new OperationResult(OperationStatus.Success);
            }
            catch (EntityAlreadyExistsException e)
            {
                result = new OperationResult(OperationStatus.Failed, e.Message);
            }
            catch (Exception)
            {
                result = new OperationResult(OperationStatus.Failed, "Server error");
            }

            return result;
        }

        private void LoadEmployer()
        {
            var model = new Employer();

            model = _employerService.GetEmployerByCode(_view.Identity, _userId);
            Map(model);
        }

        private void LoadStates()
        {
            _view.States = _domainService.GetStates();
        }

        private void Map(EmployerInputModel model)
        {
            if (!Guid.Empty.Equals(_view.Id))
            {
                model.Id = _view.Id;
            }

            model.Name = _view.Name;
            model.Code = _view.Code;
            model.City = _view.City;
            model.Street = _view.Street;
            model.State = _view.SelectState;
            model.Phone = _view.Phone;
            model.ZipCode = _view.ZipCode;
            model.Logo = _view.Logo;
        }

        private void Map(Employer model)
        {
            _view.Id = model.Id;
            _view.Name = model.Name;
            _view.Code = model.Code;
            _view.SelectState = model.Address.State;
            _view.Street = model.Address.Street;
            _view.ZipCode = model.Address.ZipCode;
            _view.City = model.Address.City;
            _view.Phone = model.Phones.PhoneNumber;
        }
    }
}
