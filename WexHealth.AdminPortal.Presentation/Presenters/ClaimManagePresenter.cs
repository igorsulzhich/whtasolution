﻿using System;
using System.Security.Principal;
using WexHealth.Domain.Entities;
using WexHealth.Security.Infrastructure;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;

namespace WexHealth.AdminPortal.Presentation.Presenters
{
    public class ClaimManagePresenter : ViewPresenterBase<IClaimManageView>, IClaimManagePresenter
    {
        private readonly IClaimManageService _claimService;
        private readonly IConsumerService _consumerService;

        public ClaimManagePresenter(IClaimManageView view, IPrincipal principal, IClaimManageService claimService, IConsumerService consumerService) :
            base(view, principal)
        {
            _claimService = claimService;
            _consumerService = consumerService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            User user = ((WexHealthIdentity)_principal.Identity).User;
            _claimService.InitService(user);
            _consumerService.ObjectUser = user;

            if (!isPostBack)
            {
                LoadClaim();
            }
        }

        private void LoadClaim()
        {
            Claim claim = _claimService.GetClaim(_view.ClaimId);
            Map(claim);

            Consumer consumer = _consumerService.GetConsumerById(claim.Enrollment.ConsumerId);
            Map(consumer);
        }

        public IResult ApproveClaim()
        {
            IResult result = null;

            try
            {
                _claimService.ApproveClaim(_view.ClaimId);
                result = new OperationResult(OperationStatus.Success);
            }
            catch (Exception e)
            {
                result = new OperationResult(OperationStatus.Failed, e.Message);
            }

            return result;
        }

        public IResult DenyClaim()
        {
            IResult result = null;

            try
            {
                _claimService.DenyClaim(_view.ClaimId);
                result = new OperationResult(OperationStatus.Success);
            }
            catch (Exception e)
            {
                result = new OperationResult(OperationStatus.Failed, e.Message);
            }

            return result;
        }

        private string byteArrayToImage(byte[] byteArrayIn)
        {
            string imageBase64Data = byteArrayIn != null ? Convert.ToBase64String(byteArrayIn) : String.Empty;
            return imageBase64Data.Equals(String.Empty) ? imageBase64Data : string.Format($"data:image;base64,{imageBase64Data}");
        }

        private void Map(Claim claim)
        {
            _view.DateOfService = claim.DateOfService;
            _view.PlanType = claim.Enrollment.BenefitsOffering.PlanType.Name;
            _view.Amount = claim.Amount;
            _view.StatusName = claim.Status.Name;
            _view.ImageFile = byteArrayToImage(claim.File);
        }

        private void Map(Consumer consumer)
        {
            _view.ConsumerId = consumer.Id;
            _view.FirstName = consumer.Individual.FirstName;
            _view.LastName = consumer.Individual.LastName;
            _view.PhoneNumber = consumer.Phone.PhoneNumber;
        }
    }
}
