﻿using System.Linq;
using System.Security.Principal;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Security.Infrastructure;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.AdminPortal.Presentation.Presenters
{
    public class EmployerIndexPresenter : ViewPresenterBase<IEmployerIndexView>, IEmployerIndexPresenter
    {
        private readonly IEmployerService _employerService;

        public EmployerIndexPresenter(IEmployerIndexView view, IPrincipal principal, IEmployerService employerService) :
            base(view, principal)
        {
            _employerService = employerService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            User user = ((WexHealthIdentity)_principal.Identity).User;
            _employerService.ObjectUser = user;

            if (!isPostBack)
            {
                LoadEmployers();
            }
        }

        private void LoadEmployers()
        {
            _view.Employers = _employerService.SearchEmployers(_view.EmployerName, _view.EmployerCode).OrderBy(x => x.Name);
        }
    }
}
