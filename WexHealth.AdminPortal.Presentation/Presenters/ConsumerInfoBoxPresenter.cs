﻿using System;
using System.Security.Principal;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Security.Infrastructure;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.AdminPortal.Presentation.Presenters
{
    public class ConsumerInfoBoxPresenter : ViewPresenterBase<IConsumerInfoBoxView>, IConsumerInfoBoxPresenter
    {
        private readonly IAdministratorService _administratorService;
        private readonly IConsumerService _consumerService;

        public ConsumerInfoBoxPresenter(IConsumerInfoBoxView view, IPrincipal principal, IAdministratorService administratorService, IConsumerService consumerService)
            : base(view, principal)
        {
            _administratorService = administratorService;
            _consumerService = consumerService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            User user = ((WexHealthIdentity)_principal.Identity).User;
            _consumerService.ObjectUser = user;

            Guid result = Guid.Empty;

            try
            {
                result = Guid.Parse(_view.Identity);
            }

            catch (Exception)
            {
                result = Guid.Empty;
            }

            var consumer = _consumerService.GetConsumerById(result);

            _view.FirstName = consumer.Individual.FirstName;
            _view.LastName = consumer.Individual.LastName;
            _view.SSN = consumer.SSN;
            _view.EmployerName = consumer.Employer.Name;
            _view.EmployerCode = consumer.Employer.Code;
        }
    }
}
