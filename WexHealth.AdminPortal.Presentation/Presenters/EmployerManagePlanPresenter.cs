﻿using System;
using System.Security.Principal;
using WexHealth.Domain.Entities;
using WexHealth.Security.Infrastructure;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;

namespace WexHealth.AdminPortal.Presentation.Presenters
{
    public class EmployerManagePlanPresenter : ViewPresenterBase<IEmployerManagePlanView>, IEmployerManagePlanPresenter
    {
        private readonly IBenefitsOfferingReadService _benefitsOfferingReadService;
        private readonly IBenefitsOfferingManageService _benefitsOfferingManageService;
        private readonly IBenefitsPackageService _benefitsPackageService;

        private int _planTypeId;

        public EmployerManagePlanPresenter(IEmployerManagePlanView view, IPrincipal principal, IBenefitsOfferingReadService benefitsOfferingReadService,
            IBenefitsOfferingManageService benefitsOfferingManageService, IBenefitsPackageService benefitsPackageService, IEmployerService employerService) :
            base(view, principal)
        {
            _benefitsOfferingReadService = benefitsOfferingReadService;
            _benefitsOfferingManageService = benefitsOfferingManageService;
            _benefitsPackageService = benefitsPackageService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            User user = ((WexHealthIdentity)_principal.Identity).User;
            _benefitsOfferingReadService.InitService(user, _view.Identity);
            _benefitsOfferingManageService.InitService(user, _view.Identity); //TODO
            _benefitsPackageService.InitService(user, _view.Identity);

            if (!isPostBack)
            {
                LoadPlan();
            }
        }

        public IResult LoadPlan()
        {
            IResult result = null;
            
            try
            {
                if (_view.PlanId != 0)
                {
                    BenefitsOffering dbOffering = _benefitsOfferingReadService.GetBenefitsOfferingById(_view.PlanId);
                    BenefitsPackage dbPackage = _benefitsPackageService.GetPackageById(dbOffering.BenefitsPackageId);

                    _view.IsStart = dbPackage.DateOfStart >= DateTime.Now;
                    Map(dbOffering);
                }

                if (!_view.PlanType.Equals(String.Empty))
                {
                    PlanType dbPlan = _benefitsOfferingReadService.GetPlanTypeByName(_view.PlanType);
                    _planTypeId = dbPlan.Id;
                    _view.Type = dbPlan.Name;
                }

                result = new OperationResult(OperationStatus.Success);
            }
            catch (Exception exception)
            {
                result = new OperationResult(OperationStatus.Failed, exception.Message);
            }

            return result;
        }

        public IResult SaveBenefitsOffering()
        {
            IResult result = null;

            try
            {
                if (_view.Id == 0)
                {
                    _benefitsOfferingManageService.CreateBenefitsOffering(Map());
                }
                else
                {
                    _benefitsOfferingManageService.UpdateBenefitsOffering(Map());
                }

                result = new OperationResult(OperationStatus.Success);
            }
            catch (Exception exception)
            {
                result = new OperationResult(OperationStatus.Failed, exception.Message);
            }

            return result;
        }

        private BenefitsOffering Map()
        {
            BenefitsOffering inputPackage = new BenefitsOffering
            {
                Id = _view.Id,
                Name = _view.Name,
                ContributionAmount = _view.Contribution,
                BenefitsPackageId = _view.BenefitsPackageId,
                PlanType = new PlanType
                {
                    Name = _view.Type
                }
            };

            return inputPackage;
        }

        private void Map(BenefitsOffering package)
        {
            _view.Id = package.Id;
            _view.Name = package.Name;
            _view.Contribution = package.ContributionAmount;
            _view.Type = package.PlanType.Name;
        }
    }
}
