﻿using System.Security.Principal;
using System.Linq;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Security.Infrastructure;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.AdminPortal.Presentation.Presenters
{
    public class ConsumerIndexPresenter : ViewPresenterBase<IConsumerIndexView>, IConsumerIndexPresenter
    {
        private readonly IConsumerService _consumerService;
        private readonly IEmployerService _employerService;

        public ConsumerIndexPresenter(IConsumerIndexView view, IPrincipal principal, IConsumerService consumerService, IEmployerService employerService) :
            base(view, principal)
        {
            _consumerService = consumerService;
            _employerService = employerService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            User user = ((WexHealthIdentity)_principal.Identity).User;
            _employerService.ObjectUser = user;
            _consumerService.ObjectUser = user;

            if (!isPostBack)
            {
                LoadEmployers();
                LoadConsumers();
            }
        }

        private void LoadEmployers()
        {
            _view.Employers = _employerService.GetEmployers().OrderBy(x => x.Name);
        }

        private void LoadConsumers()
        {
            _view.Consumers = _consumerService
                .SearchConsumers(_view.ConsumerFirstName, _view.ConsumerLastName, _view.SelectedEmployer, null)
                .OrderBy(x => x.Individual.LastName).ThenBy(x => x.Individual.FirstName);
        }
    }
}
