﻿using System;
using System.Security.Principal;
using Autofac.Extras.Attributed;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Security.Infrastructure;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;
using WexHealth.AdminPortal.Presentation.Views;

namespace WexHealth.AdminPortal.Presentation.Presenters
{
    public class EmployerSetupPresenter : ViewPresenterBase<IEmployerSetupView>, IEmployerSetupPresenter
    {
        private readonly IEmployerService _employerService;
        private readonly ISettingService _settingService;

        public EmployerSetupPresenter(IEmployerSetupView view, IPrincipal principal, IEmployerService employerService,
            [WithKey("Employer")] ISettingService settingService) :
            base(view, principal)
        {
            _employerService = employerService;
            _settingService = settingService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            User user = ((WexHealthIdentity)_principal.Identity).User;

            Guid _employerId = _employerService.GetEmployerByCode(_view.Identity, user.Id).Id;
            _settingService.InitService(user, _employerId);

            if (!isPostBack)
            {
                LoadSettings();
            }
        }

        private void LoadSettings()
        {
            _view.SettingList = _settingService.GetSettings();
        }

        public IResult SaveSettings()
        {
            IResult result = null;

            try
            {
                _settingService.SaveSettings(_view.SelectedSettings);
                result = new OperationResult(OperationStatus.Success);
            }
            catch (Exception exception)
            {
                result = new OperationResult(OperationStatus.Failed, exception.Message);
            }

            return result;
        }
    }
}
