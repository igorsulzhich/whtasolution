﻿using System;
using System.Linq;
using System.Security.Principal;
using Autofac.Extras.Attributed;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Exceptions;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Security.Infrastructure;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;
using WexHealth.AdminPortal.Presentation.Views;

namespace WexHealth.AdminPortal.Presentation.Presenters
{
    public class ConsumerManagePresenter : ViewPresenterBase<IConsumerManageView>, IConsumerManagePresenter
    {
        private readonly IConsumerService _consumerService;
        private readonly ISettingReadService _settingService;

        public ConsumerManagePresenter(IConsumerManageView view, IPrincipal principal, IConsumerService consumerService,
            [WithKey("Administrator")] ISettingReadService settingService) :
            base(view, principal)
        {
            _consumerService = consumerService;
            _settingService = settingService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            User user = ((WexHealthIdentity)_principal.Identity).User;
            _consumerService.ObjectUser = user;
            _settingService.InitService(user);

            if (!isPostBack)
            {
                LoadConsumer();
            }
        }

        private void LoadConsumer()
        {
            Consumer consumer = _consumerService.GetConsumerById(Guid.Parse(_view.Identity));

            if (null != consumer)
            {
                _view.EmployerCode = consumer.Employer.Code;
            }
        }

        public bool IsSettingAllow(string settingKey)
        {
            return _settingService.GetSettings().Any(x => x.Name.Equals(settingKey));
        }
    }
}
