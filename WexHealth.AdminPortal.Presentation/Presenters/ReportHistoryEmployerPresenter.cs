﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using log4net;
using Autofac.Extras.Attributed;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.Domain.Entities;
using WexHealth.Security.Infrastructure;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;
using WexHealth.Presentation.Presenters.Results;

namespace WexHealth.AdminPortal.Presentation.Presenters
{
    public class ReportHistoryEmployerPresenter : ViewPresenterBase<IReportHistoryView>, IReportHistoryEmployerPresenter
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ReportConsumerPresenter));

        private const string _settingKey = "Reports";

        private readonly IReportService _reportEmployerService;
        private readonly ISettingReadService _settingService;

        public ReportHistoryEmployerPresenter(
            IReportHistoryView view, 
            IPrincipal principal, 
            [WithKey("Employer")]IReportService reportEmployerService,
            [WithKey("Administrator")] ISettingReadService settingService) :
            base(view, principal)
        {
            _reportEmployerService = reportEmployerService;
            _settingService = settingService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            User user = ((WexHealthIdentity)_principal.Identity).User;
            _reportEmployerService.InitService(user);
            _settingService.InitService(user);

            if (IsSettingAllow(_settingKey))
            {
                _view.ReportRequests = GetReports();
            }
            else
            {
                throw new InvalidOperationException("Access denied");
            }
        }

        public IResult RejectRequest(int id)
        {
            IResult result = null;

            try
            {
                if (_reportEmployerService.RejectRequest(id, ReportStatus.Rejected))
                {
                    result = new OperationResult(OperationStatus.Success);
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(OperationStatus.Failed, exception.Message);
            }

            return result;
        }

        public IResult DeleteRequest(int id)
        {
            IResult result = null;

            try
            {
                if(_reportEmployerService.RemoveRequest(id))
                {
                    result = new OperationResult(OperationStatus.Success);
                }                
            }
            catch (Exception exception)
            {
                result = new OperationResult(OperationStatus.Failed, exception.Message);
            }

            return result;
        }

        private IEnumerable<ReportRequest> GetReports()
        {
            return _reportEmployerService.GetReportRequests();
        }

        private bool IsSettingAllow(string settingKey)
        {
            return _settingService.GetSettings().Any(x => x.Name.Equals(settingKey));
        }

    }
}
