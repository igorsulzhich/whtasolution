﻿using System;
using System.Security.Principal;
using FluentValidation;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Exceptions;
using WexHealth.Security.Infrastructure;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;

namespace WexHealth.AdminPortal.Presentation.Presenters
{
    public class ClaimEditPresenter : ViewPresenterBase<IClaimEditView>, IClaimEditPresenter
    {
        private readonly IClaimManageService _claimService;
        private readonly IEnrollmentReadService _enrollmentService;
        private readonly IConsumerService _consumerService;

        public ClaimEditPresenter(IClaimEditView view, IPrincipal principal, IClaimManageService claimService, IEnrollmentReadService enrollmentService,
            IConsumerService consumerService) :
            base(view, principal)
        {
            _claimService = claimService;
            _enrollmentService = enrollmentService;
            _consumerService = consumerService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            User user = ((WexHealthIdentity)_principal.Identity).User;
            _claimService.InitService(user);
            _consumerService.ObjectUser = user;

            if (!isPostBack)
            {
                LoadClaim();
                _enrollmentService.InitService(user, _view.ConsumerId);

                LoadPlans();
            }
        }

        private void LoadPlans()
        {
            _view.Enrollments = _enrollmentService.GetActiveEnrollments();
        }

        private void LoadClaim()
        {
            Claim claim = _claimService.GetClaim(_view.ClaimId);
            MapFromModel(claim);

            Consumer consumer = _consumerService.GetConsumerById(claim.Enrollment.ConsumerId);
            MapFromModel(consumer);
        }

        public IResult EditClaim()
        {
            IResult result = null;

            try
            {
                var claim = new Claim();
                MapFromView(claim);

                _claimService.SaveClaim(claim);
                result = new OperationResult(OperationStatus.Success);
            }
            catch (EntityOperationFailedException e) when (
                e.InnerException is EntityNotFoundException ||
                e.InnerException is InvalidOperationException ||
                e.InnerException is ValidationException
            )
            {
                result = new OperationResult(OperationStatus.Failed, e.InnerException.Message);
                LoadClaim();
            }
            catch (InvalidOperationException e)
            {
                result = new OperationResult(OperationStatus.Failed, e.Message);
                LoadClaim();
            }
            catch (Exception)
            {
                result = new OperationResult(OperationStatus.Failed, "Server error");
                LoadClaim();
            }

            return result;
        }

        private void MapFromModel(Claim claim)
        {
            _view.SelectedEnrollment = claim.EnrollmentId;
            _view.DateOfService = claim.DateOfService;
            _view.Amount = claim.Amount;
        }

        private void MapFromModel(Consumer consumer)
        {
            _view.ConsumerId = consumer.Id;
            _view.FirstName = consumer.Individual.FirstName;
            _view.LastName = consumer.Individual.LastName;
            _view.PhoneNumber = consumer.Phone.PhoneNumber;
        }

        private void MapFromView(Claim claim)
        {
            claim.Id = _view.ClaimId;
            claim.DateOfService = _view.DateOfService;
            claim.EnrollmentId = _view.SelectedEnrollment;
            claim.Amount = _view.Amount;
        }
    }
}
