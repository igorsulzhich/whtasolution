﻿using System;
using System.Linq;
using System.Security.Principal;
using Autofac.Extras.Attributed;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Models;
using WexHealth.BLL.Exceptions;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Security.Infrastructure;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.AdminPortal.Presentation.Presenters
{
    public class ConsumerManageProfilePresenter : ViewPresenterBase<IConsumerManageProfileView>, IConsumerManageProfilePresenter
    {
        private const string _settingKey = "Create consumers";

        private readonly IAdministratorService _administratorService;
        private readonly IConsumerService _consumerService;
        private readonly IEmployerService _employerService;
        private readonly IDomainService _domainService;
        private readonly ConsumerUserServiceFactory _userServiceFactory;
        private IDomainUserService _userService;
        private ISettingReadService _settingService;

        private Guid _administratorId;

        public ConsumerManageProfilePresenter(IConsumerManageProfileView view, IPrincipal principal, IAdministratorService administratorService,
            IConsumerService consumerService, IEmployerService employerService, IDomainService domainService,
            ConsumerUserServiceFactory userServiceFactory, [WithKey("Administrator")] ISettingReadService settingService) :
            base(view, principal)
        {
            _administratorService = administratorService;
            _consumerService = consumerService;
            _employerService = employerService;
            _domainService = domainService;
            _userServiceFactory = userServiceFactory;
            _settingService = settingService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            User user = ((WexHealthIdentity)_principal.Identity).User;
            _settingService.InitService(user);
            _consumerService.ObjectUser = user;

            if (isSettingAllow(_settingKey))
            {
                _administratorId = InitAdministrator();

                if (!isPostBack)
                {
                    LoadStates();
                }
            }
            else
            {
                throw new InvalidOperationException("Access denied");
            }
        }

        private Guid InitAdministrator()
        {
            Guid securityDomainId = ((WexHealthIdentity)_principal.Identity).User.SecurityDomainId;
            Guid administratorId = _administratorService.GetAdministratorBySecurityDomain(securityDomainId).Id;

            return administratorId;
        }

        public void LoadConsumer()
        {
            Consumer consumer = _consumerService.GetConsumerById(Guid.Parse(_view.Identity));
            Map(consumer);

            _userService = _userServiceFactory(_view.ConsumerId, consumer.EmployerId, _administratorId);
            User user = _userService.GetUsers().FirstOrDefault();
            if (null != user)
            {
                Map(user);
            }
        }

        private void LoadStates()
        {
            _view.States = _domainService.GetStates();
        }

        public IResult SaveConsumer()
        {
            IResult result = null;

            try
            {
                var consumerModel = new ConsumerInputModel();
                Map(consumerModel);

                var individualModel = new IndividualInputModel();
                Map(individualModel);

                var userModel = new UserInputModel();
                Map(userModel);

                if (Guid.Empty.Equals(consumerModel.Id))
                {
                    Guid individualId = _domainService.CreateIndividual(individualModel);

                    Employer employer = _employerService.GetEmployerByCode(_view.EmployerCode,
                        ((WexHealthIdentity)_principal.Identity).User.Id);

                    Guid consumerId = _consumerService.CreateConsumer(consumerModel, individualId, employer.Id);

                    if (!string.IsNullOrEmpty(userModel.UserName) && !string.IsNullOrEmpty(userModel.Password))
                    {
                        _userService = _userServiceFactory(consumerId, employer.Id, _administratorId);
                        _userService.CreateUser(userModel, individualId);
                    }
                }
                else
                {
                    Consumer consumer = _consumerService.GetConsumerById(_view.ConsumerId);
                    if (null == consumer || Guid.Empty.Equals(consumer.IndividualId))
                    {
                        result = new OperationResult(OperationStatus.Failed, $"Consumer with id: {_view.ConsumerId}, doesn't exist");
                    }

                    individualModel.Id = consumer.IndividualId;
                    _domainService.UpdateIndividual(individualModel);

                    _consumerService.UpdateConsumer(consumerModel, consumer.EmployerId, _administratorId);

                    if (!string.IsNullOrEmpty(userModel.UserName) || !string.IsNullOrEmpty(userModel.Password))
                    {
                        _userService = _userServiceFactory(_view.ConsumerId, consumer.EmployerId, _administratorId);
                        User user = _userService.GetUsers().FirstOrDefault();
                        if (null == user)
                        {
                            _userService.CreateUser(userModel, consumer.IndividualId);
                        }
                        else
                        {
                            userModel.Id = user.Id;
                            _userService.UpdateUser(userModel);
                        }
                    }
                }

                result = new OperationResult(OperationStatus.Success);
            }
            catch (Exception e) when (
            e is InvalidOperationException ||
            e is EntityAlreadyExistsException)
            {
                result = new OperationResult(OperationStatus.Failed, e.Message);
            }
            catch (Exception)
            {
                result = new OperationResult(OperationStatus.Failed, "Server error");
            }

            return result;
        }

        private bool isSettingAllow(string settingKey)
        {
            return _settingService.GetSettings().Any(x => x.Name.Equals(settingKey));
        }

        //View -> BLL Models
        private void Map(ConsumerInputModel model)
        {
            if (!Guid.Empty.Equals(_view.ConsumerId))
            {
                model.Id = _view.ConsumerId;
            }

            model.SSN = _view.SSN;

            model.Address = new AddressInputModel
            {
                City = _view.City,
                Street = _view.Street,
                State = _view.SelectState,
                ZipCode = _view.ZipCode
            };

            model.Phone = new PhoneInputModel
            {
                Phone = _view.Phone
            };
        }

        private void Map(UserInputModel model)
        {
            model.UserName = _view.UserName;
            model.Password = _view.Password;
        }

        private void Map(IndividualInputModel model)
        {
            model.FirstName = _view.FirstName;
            model.LastName = _view.LastName;
            model.Email = _view.Email;
            model.DateBirth = _view.DateOfBirth;
        }

        //BLL Models -> View
        private void Map(Consumer model)
        {
            _view.ConsumerId = model.Id;
            _view.FirstName = model.Individual.FirstName;
            _view.LastName = model.Individual.LastName;
            _view.Email = model.Individual.Email;
            _view.SSN = model.SSN;
            _view.Phone = model.Phone.PhoneNumber;
            _view.Street = model.Address.Street;
            _view.City = model.Address.City;
            _view.SelectState = model.Address.State;
            _view.ZipCode = model.Address.ZipCode;
            _view.DateOfBirth = model.Individual.DateBirth;
        }

        private void Map(User model)
        {
            _view.UserName = model.UserName;
        }
    }
}
