﻿using System;
using System.Security.Principal;
using WexHealth.Domain.Entities;
using WexHealth.Security.Infrastructure;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;

namespace WexHealth.AdminPortal.Presentation.Presenters
{
    public class EmployerManagePlanYearPresenter : ViewPresenterBase<IEmployerManagePlanYearView>, IEmployerManagePlanYearPresenter
    {
        private readonly IBenefitsOfferingReadService _benefitsOfferingReadService;
        private readonly IBenefitsPackageService _benefitsPackageService;

        public EmployerManagePlanYearPresenter(IEmployerManagePlanYearView view, IPrincipal principal, IBenefitsOfferingReadService benefitsOfferingReadService,
            IBenefitsPackageService benefitsPackageService, IEmployerService employerService) :
            base(view, principal)
        {
            _benefitsOfferingReadService = benefitsOfferingReadService;
            _benefitsPackageService = benefitsPackageService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            User user = ((WexHealthIdentity)_principal.Identity).User;
            _benefitsOfferingReadService.InitService(user, _view.Identity);
            _benefitsPackageService.InitService(user, _view.Identity);

            if (!isPostBack)
            {          
                GetPlanTypes();              
                GetPayrollFrequencies();

                if (_view.PlanId != 0)
                {
                    LoadBenefitsPackage();
                }

                GetBenefitsOffering();
            }          
        }

        public IResult SaveBenefitsPackage()
        {
            IResult result = null;

            try
            {
                if (_view.PlanId == 0)
                {
                    _benefitsPackageService.CreatePackage(Map());
                }
                else
                {
                    _benefitsPackageService.UpdatePackage(Map());
                }
                result = new OperationResult(OperationStatus.Success);
            }
            catch (Exception exception)
            {
                result = new OperationResult(OperationStatus.Failed, exception.Message);
            }

            return result;
        }

        private void GetPayrollFrequencies()
        {
            _view.PayrollFrequencies = _benefitsPackageService.GetPayrollFrequencies();
        }

        private void GetBenefitsOffering()
        {
            if (_view.Id != 0)
            {
                _view.BenefitsOfferings = _benefitsOfferingReadService.GetBenefitsOfferingByPackageId(_view.Id);
            }
        }

        private void GetPlanTypes()
        {
            _view.PlanTypes = _benefitsOfferingReadService.GetPlanTypes();
        }

        private void LoadBenefitsPackage()
        {
            BenefitsPackage dbPackage = _benefitsPackageService.GetPackageById(_view.PlanId);
            if (null != dbPackage)
            {
                Map(dbPackage);
            }
        }

        private BenefitsPackage Map()
        {
            BenefitsPackage inputPackage = new BenefitsPackage
            {
                Id = _view.Id,
                Name = _view.Name,
                DateOfStart = _view.StartDate,
                DateOfEnd = _view.EndDate,
                Frequency = _view.SelectedPayrollFrequency
            };

            return inputPackage;
        }

        private void Map(BenefitsPackage package)
        {
            _view.Id = package.Id;
            _view.Name = package.Name;
            _view.StartDate = package.DateOfStart;
            _view.EndDate = package.DateOfEnd;
            _view.SelectedPayrollFrequency = package.Frequency;
            _view.Status = package.Status.Name;
        }
    }
}
