﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Security.Principal;
using Autofac.Extras.Attributed;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Domain.Entities;
using WexHealth.Security.Infrastructure;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;

namespace WexHealth.AdminPortal.Presentation.Presenters
{
    public class ReportHistoryConsumerPresenter : ViewPresenterBase<IReportHistoryView>, IReportHistoryConsumerPresenter
    {
        private const string _settingKey = "Reports";

        private readonly IReportService _reportConsumerService;
        private readonly ISettingReadService _settingService;

        public ReportHistoryConsumerPresenter(
            IReportHistoryView view,
            IPrincipal principal,
            [WithKey("Consumer")]IReportService reportConsumerService,
            [WithKey("Administrator")] ISettingReadService settingService) :
            base(view, principal)
        {
            _reportConsumerService = reportConsumerService;
            _settingService = settingService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            User user = ((WexHealthIdentity)_principal.Identity).User;
            _reportConsumerService.InitService(user);
            _settingService.InitService(user);

            if (IsSettingAllow(_settingKey))
            {
                _view.ReportRequests = GetReports();
            }
            else
            {
                throw new InvalidOperationException("Access denied");
            }
        }

        public IResult RejectRequest(int id)
        {
            IResult result = null;

            try
            {
                if (_reportConsumerService.RejectRequest(id, ReportStatus.Rejected))
                {
                    result = new OperationResult(OperationStatus.Success);
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(OperationStatus.Failed, exception.Message);
            }

            return result;
        }

        public IResult DeleteRequest(int id)
        {
            IResult result = null;

            try
            {
                if (_reportConsumerService.RemoveRequest(id))
                {
                    result = new OperationResult(OperationStatus.Success);
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(OperationStatus.Failed, exception.Message);
            }

            return result;
        }

        private IEnumerable<ReportRequest> GetReports()
        {
            return _reportConsumerService.GetReportRequests();
        }

        private bool IsSettingAllow(string settingKey)
        {
            return _settingService.GetSettings().Any(x => x.Name.Equals(settingKey));
        }
    }
}
