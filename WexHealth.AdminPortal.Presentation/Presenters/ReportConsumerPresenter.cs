﻿using System;
using System.Linq;
using System.Security.Principal;
using log4net;
using Autofac.Extras.Attributed;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Domain.Entities;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;
using WexHealth.Security.Infrastructure;

namespace WexHealth.AdminPortal.Presentation.Presenters
{
    public class ReportConsumerPresenter : ViewPresenterBase<IReportConsumerView>, IReportConsumerPresenter
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ReportConsumerPresenter));

        private const string _settingKey = "Reports";

        private readonly IReportService _reportConsumerService;
        private readonly IEmployerService _employerService;
        private readonly ISettingReadService _settingService;

        public ReportConsumerPresenter(
            IReportConsumerView view, 
            IPrincipal principal, 
            [WithKey("Consumer")]IReportService reportConsumerService, 
            IEmployerService employerService, 
            [WithKey("Administrator")] ISettingReadService settingService) :
            base(view, principal)
        {
            _reportConsumerService = reportConsumerService;
            _employerService = employerService;
            _settingService = settingService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            User user = ((WexHealthIdentity)_principal.Identity).User;
            _reportConsumerService.InitService(user);
            _employerService.ObjectUser = user;
            _settingService.InitService(user);

            if (IsSettingAllow(_settingKey))
            {
                if (!isPostBack)
                {
                    _view.Fields = _reportConsumerService.GetObjectFields();
                    _view.Employers = _employerService.GetEmployers();
                }
            }
            else
            {
                throw new InvalidOperationException("Access denied");
            }
        }

        public IResult SendRequest()
        {
            IResult result = null;

            try
            {
                int report = _reportConsumerService.CreateReportRequest(_view.SelectedEmployer);

                _reportConsumerService.SetObjectFields(_view.SelectedFields, report);

                result = new OperationResult(OperationStatus.Success);
            }
            catch (Exception e) when (
            e is InvalidOperationException ||
            e is InvalidOperationException)
            {
                result = new OperationResult(OperationStatus.Failed, e.Message);
            }
            catch (Exception)
            {
                result = new OperationResult(OperationStatus.Failed, "Server error");
            }

            return result;
        }

        private bool IsSettingAllow(string settingKey)
        {
            return _settingService.GetSettings().Any(x => x.Name.Equals(settingKey));
        }
    }
}
