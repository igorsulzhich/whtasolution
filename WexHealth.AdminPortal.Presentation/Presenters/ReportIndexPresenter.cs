﻿using System;
using System.Linq;
using System.Security.Principal;
using Autofac.Extras.Attributed;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Security.Infrastructure;
using WexHealth.Presentation.Views;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.AdminPortal.Presentation.Presenters
{
    public class ReportIndexPresenter : ViewPresenterBase<IIndexView>, IReportIndexPresenter
    {
        private const string _settingKey = "Reports";

        private readonly ISettingReadService _settingService;

        public ReportIndexPresenter(
            IIndexView view, 
            IPrincipal principal,
            [WithKey("Administrator")] ISettingReadService settingService) :
            base(view, principal)
        {
            _settingService = settingService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            User user = ((WexHealthIdentity)_principal.Identity).User;
            _settingService.InitService(user);

            if (!IsSettingAllow(_settingKey))
            {
                throw new InvalidOperationException("Access denied");
            }
        }

        private bool IsSettingAllow(string settingKey)
        {
            return _settingService.GetSettings().Any(x => x.Name.Equals(settingKey));
        }
    }
}
