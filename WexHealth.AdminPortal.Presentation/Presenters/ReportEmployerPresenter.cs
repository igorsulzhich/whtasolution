﻿using System;
using System.Linq;
using System.Security.Principal;
using Autofac.Extras.Attributed;
using log4net;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Domain.Entities;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;
using WexHealth.Security.Infrastructure;

namespace WexHealth.AdminPortal.Presentation.Presenters
{
    public class ReportEmployerPresenter : ViewPresenterBase<IReportEmployerView>, IReportEmployerPresenter
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ReportEmployerPresenter));

        private const string _settingKey = "Reports";

        private readonly IReportService _reportEmployerService;
        private readonly IEmployerService _employerService;
        private readonly ISettingReadService _settingService;

        public ReportEmployerPresenter(
            IReportEmployerView view, 
            IPrincipal principal, 
            [WithKey("Employer")]IReportService reportEmployerService,
            IEmployerService employerService,
            [WithKey("Administrator")] ISettingReadService settingService) :
            base(view, principal)
        {
            _reportEmployerService = reportEmployerService;
            _employerService = employerService;
            _settingService = settingService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            User user = ((WexHealthIdentity)_principal.Identity).User;
            _employerService.ObjectUser = user;
            _reportEmployerService.InitService(user);
            _settingService.InitService(user);

            if (IsSettingAllow(_settingKey))
            {
                if (!isPostBack)
                {
                    _view.Fields = _reportEmployerService.GetObjectFields();
                }
            }
            else
            {
                throw new InvalidOperationException("Access denied");
            }
        }

        public IResult SendRequest()
        {
            IResult result = null;

            try
            {
                int reportId = _reportEmployerService.CreateReportRequest(Guid.Empty);

                _reportEmployerService.SetObjectFields(_view.SelectedFields, reportId);

                result = new OperationResult(OperationStatus.Success);
            }
            catch (Exception e) when (
            e is InvalidOperationException ||
            e is InvalidOperationException)
            {
                result = new OperationResult(OperationStatus.Failed, e.Message);
            }
            catch (Exception)
            {
                result = new OperationResult(OperationStatus.Failed, "Server error");
            }

            return result;
        }

        private bool IsSettingAllow(string settingKey)
        {
            return _settingService.GetSettings().Any(x => x.Name.Equals(settingKey));
        }
    }
}
