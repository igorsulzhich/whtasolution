﻿using System.Security.Principal;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;

namespace WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces
{
    public delegate IEmployerManagePlanPresenter EmployerManagePlanPresenterFactory(IEmployerManagePlanView view, IPrincipal principal);

    public interface IEmployerManagePlanPresenter
    {
        IResult SaveBenefitsOffering();

        void InitView(bool isPostBack);
    }
}
