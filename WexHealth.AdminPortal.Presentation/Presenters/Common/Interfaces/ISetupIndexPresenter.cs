﻿using System.Security.Principal;
using WexHealth.AdminPortal.Presentation.Views;

namespace WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces
{
    public delegate ISetupIndexPresenter SetupIndexPresenterFactory(ISetupIndexView view, IPrincipal principal);

    public interface ISetupIndexPresenter
    {
        void InitView(bool isPostBack);
    }
}
