﻿using System.Security.Principal;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;

namespace WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces
{
    public delegate IReportHistoryConsumerPresenter ReportHistoryConsumerPresenterFactory(IReportHistoryView view, IPrincipal principal);

    public interface IReportHistoryConsumerPresenter
    {
        void InitView(bool isPostBack);

        IResult RejectRequest(int id);

        IResult DeleteRequest(int id);
    }
}
