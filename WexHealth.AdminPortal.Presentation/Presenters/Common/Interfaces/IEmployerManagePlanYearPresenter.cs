﻿using System.Security.Principal;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;

namespace WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces
{
    public delegate IEmployerManagePlanYearPresenter EmployerManagePlanYearPresenterFactory(IEmployerManagePlanYearView view, IPrincipal principal);

    public interface IEmployerManagePlanYearPresenter
    {
        IResult SaveBenefitsPackage();

        void InitView(bool isPostBack);
    }
}
