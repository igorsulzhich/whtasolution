﻿using System.Security.Principal;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;

namespace WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces
{
    public delegate IClaimManagePresenter ClaimManagePresenterFactory(IClaimManageView view, IPrincipal principal);

    public interface IClaimManagePresenter
    {
        void InitView(bool isPostBack);

        IResult ApproveClaim();

        IResult DenyClaim();
    }
}
