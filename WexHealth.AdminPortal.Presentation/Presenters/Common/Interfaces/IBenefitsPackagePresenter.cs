﻿using System.Security.Principal;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;

namespace WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces
{
    public delegate IBenefitsPackagePresenter BenefitsPackagePresenterFactory(IBenefitsPackageView view, IPrincipal principal);

    public interface IBenefitsPackagePresenter
    {
        IResult Initialize(int id);

        IResult Delete(int id);

        bool HaveEnroll(int packageId);

        void InitView(bool isPostBack);
    }
}
