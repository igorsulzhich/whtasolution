﻿using System.Security.Principal;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;

namespace WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces
{
    public delegate IReportConsumerPresenter ReportConsumerPresenterFactory(IReportConsumerView view, IPrincipal principal);

    public interface IReportConsumerPresenter
    {
        void InitView(bool isPostBack);

        IResult SendRequest();       
    }
}
