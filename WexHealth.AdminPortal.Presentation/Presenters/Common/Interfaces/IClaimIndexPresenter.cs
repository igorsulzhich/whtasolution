﻿using System.Security.Principal;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;

namespace WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces
{
    public delegate IClaimIndexPresenter ClaimIndexPresenterFactory(IClaimIndexView view, IPrincipal principal);

    public interface IClaimIndexPresenter
    {
        void InitView(bool isPostBack);
    }
}
