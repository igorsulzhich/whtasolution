﻿using System.Security.Principal;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;

namespace WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces
{
    public delegate IClaimEditPresenter ClaimEditPresenterFactory(IClaimEditView view, IPrincipal principal);

    public interface IClaimEditPresenter
    {
        void InitView(bool isPostBack);

        IResult EditClaim();
    }
}
