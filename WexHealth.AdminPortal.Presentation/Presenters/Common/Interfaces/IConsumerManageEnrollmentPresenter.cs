﻿using System.Security.Principal;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.Domain.Entities;

namespace WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces
{
    public delegate IConsumerManageEnrollmentPresenter ConsumerManageEnrollmentPresenterFactory(IConsumerManageEnrollmentView view, IPrincipal principal);

    public interface IConsumerManageEnrollmentPresenter
    {
        void InitView(bool isPostBack);

        void LoadEnrollment(int id);

        void LoadPlanContribution();

        IResult CreateEnrollment();

        IResult UpdateEnrollment();
    }
}
