﻿using System.Security.Principal;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;

namespace WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces
{
    public delegate IEmployerSetupPresenter EmployerSetupPresenterFactory(IEmployerSetupView view, IPrincipal principal);

    public interface IEmployerSetupPresenter
    {
        void InitView(bool isPostBack);

        IResult SaveSettings();
    }
}
