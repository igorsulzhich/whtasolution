﻿using System.Security.Principal;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;
using WexHealth.AdminPortal.Presentation.Views;

namespace WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces
{
    public delegate IConsumerIndexPresenter ConsumerIndexPresenterFactory(IConsumerIndexView view, IPrincipal principal);

    public interface IConsumerIndexPresenter
    {
        void InitView(bool isPostBack);
    }
}
