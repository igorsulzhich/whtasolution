﻿using System.Security.Principal;
using WexHealth.AdminPortal.Presentation.Views;

namespace WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces
{
    public delegate IConsumerInfoBoxPresenter ConsumerInfoBoxPresenterFactory(IConsumerInfoBoxView view, IPrincipal principal);

    public interface IConsumerInfoBoxPresenter
    {
        void InitView(bool isPostBack);
    }
}
