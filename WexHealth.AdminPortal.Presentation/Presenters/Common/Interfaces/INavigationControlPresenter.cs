﻿using System.Security.Principal;
using WexHealth.Presentation.Views;

namespace WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces
{
    public delegate INavigationControlPresenter NavigationControlPresenterFactory(IIndexView view, IPrincipal principal);

    public interface INavigationControlPresenter
    {
        void InitView(bool isPostBack);

        bool IsSettingAllow(string settingKey);
    }
}
