﻿using System.Security.Principal;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;
using WexHealth.AdminPortal.Presentation.Views;

namespace WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces
{
    public delegate IConsumerManageProfilePresenter ConsumerManageProfilePresenterFactory(IConsumerManageProfileView view, IPrincipal principal);

    public interface IConsumerManageProfilePresenter
    {
        void InitView(bool isPostBack);

        void LoadConsumer();

        IResult SaveConsumer();
    }
}
