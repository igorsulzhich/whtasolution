﻿using System.Security.Principal;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;

namespace WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces
{
    public delegate IReportEmployerPresenter ReportEmployerPresenterFactory(IReportEmployerView view, IPrincipal principal);

    public interface IReportEmployerPresenter
    {
        void InitView(bool isPostBack);

        IResult SendRequest();
    }
}
