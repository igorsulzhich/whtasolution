﻿using System.Security.Principal;
using WexHealth.AdminPortal.Presentation.Views;

namespace WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces
{
    public delegate IConsumerManagePresenter ConsumerManagePresenterFactory(IConsumerManageView view, IPrincipal principal);

    public interface IConsumerManagePresenter
    {
        void InitView(bool isPostBack);

        bool IsSettingAllow(string settingKey);
    }
}
