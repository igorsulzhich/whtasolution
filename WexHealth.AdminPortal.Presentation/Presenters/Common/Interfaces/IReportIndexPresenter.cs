﻿using System.Security.Principal;
using WexHealth.Presentation.Views;

namespace WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces
{
    public delegate IReportIndexPresenter ReportIndexPresenterFactory(IIndexView view, IPrincipal principal);

    public interface IReportIndexPresenter
    {
        void InitView(bool isPostBack);
    }
}
