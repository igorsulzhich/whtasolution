﻿using System.Security.Principal;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;
using WexHealth.AdminPortal.Presentation.Views;

namespace WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces
{
    public delegate IEmployerManageProfilePresenter EmployerManageProfilePresenterFactory(IEmployerManageProfileView view, IPrincipal principal);

    public interface IEmployerManageProfilePresenter
    {
        void InitView(bool isPostBack);

        IResult SaveEmployer();
    }
}
