﻿using System.Security.Principal;
using WexHealth.AdminPortal.Presentation.Views;

namespace WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces
{
    public delegate IEmployerInfoBoxPresenter EmployerInfoBoxPresenterFactory(IEmployerInfoBoxView view, IPrincipal principal);

    public interface IEmployerInfoBoxPresenter
    {
        void InitView(bool isPostBack);
    }
}
