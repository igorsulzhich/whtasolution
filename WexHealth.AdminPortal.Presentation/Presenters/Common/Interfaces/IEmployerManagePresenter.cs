﻿using System.Security.Principal;
using WexHealth.AdminPortal.Presentation.Views;

namespace WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces
{
    public delegate IEmployerManagePresenter EmployerManagePresenterFactory(IEmployerManageView view, IPrincipal principal);

    public interface IEmployerManagePresenter
    {
        void InitView(bool isPostBack);

        bool IsSettingAllow(string settingKey);
    }
}
