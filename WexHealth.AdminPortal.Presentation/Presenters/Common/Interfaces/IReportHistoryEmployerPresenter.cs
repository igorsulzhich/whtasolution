﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.Domain.Entities;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;

namespace WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces
{
    public delegate IReportHistoryEmployerPresenter ReportHistoryEmployerPresenterFactory(IReportHistoryView view, IPrincipal principal);

    public interface IReportHistoryEmployerPresenter
    {
        void InitView(bool isPostBack);

        IResult RejectRequest(int id);

        IResult DeleteRequest(int id);
    }
}
