﻿using System.Security.Principal;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;
using WexHealth.AdminPortal.Presentation.Views;

namespace WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces
{
    public delegate IEmployerIndexPresenter EmployerIndexPresenterFactory(IEmployerIndexView view, IPrincipal principal);

    public interface IEmployerIndexPresenter
    {
        void InitView(bool isPostBack);
    }
}
