﻿using System.Security.Principal;
using WexHealth.Presentation.Views;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;

namespace WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces
{
    public delegate ISetupManagePresenter SetupManagePresenterFactory(ISettingView view, IPrincipal principal);

    public interface ISetupManagePresenter
    {
        void InitView(bool isPostBack);

        IResult SaveSettings();
    }
}
