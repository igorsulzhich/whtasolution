﻿using System.Linq;
using System.Security.Principal;
using log4net;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Security.Infrastructure;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;
using WexHealth.AdminPortal.Presentation.Views;

namespace WexHealth.AdminPortal.Presentation.Presenters
{
    public class ClaimIndexPresenter : ViewPresenterBase<IClaimIndexView>, IClaimIndexPresenter
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ClaimIndexPresenter));

        private readonly IClaimReadService _claimService;
        private readonly IEmployerService _employerService;

        public ClaimIndexPresenter(IClaimIndexView view, IPrincipal principal, IClaimReadService claimService, IEmployerService employerService) :
            base(view, principal)
        {
            _claimService = claimService;
            _employerService = employerService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            User user = ((WexHealthIdentity)_principal.Identity).User;
            _claimService.InitService(user);
            _employerService.ObjectUser = user;

            if (!isPostBack)
            {
                LoadEmployers();
                LoadClaims();
                LoadClaimStatuses();
            }
        }

        private void LoadEmployers()
        {
            _view.Employers = _employerService.GetEmployers().OrderBy(x => x.Name);
        }

        private void LoadClaimStatuses()
        {
            _view.ClaimStatuses = _claimService.GetClaimStatuses();
        }

        private void LoadClaims()
        {
            _view.Claims = _claimService.SearchClaims(_view.ClaimNumber, _view.SelectedClaimStatus, _view.ConsumerId, _view.SelectedEmployer);
        }
    }
}
