﻿using System.Linq;
using System.Security.Principal;
using Autofac.Extras.Attributed;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Security.Infrastructure;
using WexHealth.Presentation.Views;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.AdminPortal.Presentation.Presenters
{
    public class NavigationControlPresenter : ViewPresenterBase<IIndexView>, INavigationControlPresenter
    {
        private const string _settingKey = "Reports";

        private readonly ISettingReadService _settingService;

        public NavigationControlPresenter(
            IIndexView view,
            IPrincipal principal,
            [WithKey("Administrator")] ISettingReadService settingService) :
            base(view, principal)
        {
            _settingService = settingService;
        }

        protected override void InternalInitView(bool isPostBack)
        { }

        public bool IsSettingAllow(string settingKey)
        {
            User user = ((WexHealthIdentity)_principal.Identity).User;
            _settingService.InitService(user);
            return _settingService.GetSettings().Any(x => x.Name.Equals(settingKey));
        }

    }
}
