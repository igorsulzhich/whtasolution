﻿using System.Security.Principal;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.AdminPortal.Presentation.Presenters
{
    public class SetupIndexPresenter : ViewPresenterBase<ISetupIndexView>, ISetupIndexPresenter
    {
        public SetupIndexPresenter(ISetupIndexView view, IPrincipal principal, IAdministratorService administratorService) :
            base(view, principal)
        {
        }

        protected override void InternalInitView(bool isPostBack)
        {
        }
    }
}
