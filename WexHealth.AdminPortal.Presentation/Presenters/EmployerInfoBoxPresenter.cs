﻿using System;
using System.Security.Principal;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Security.Infrastructure;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.AdminPortal.Presentation.Presenters
{
    public class EmployerInfoBoxPresenter : ViewPresenterBase<IEmployerInfoBoxView>, IEmployerInfoBoxPresenter
    {
        private readonly IAdministratorService _administratorService;
        private readonly IEmployerService _employerService;

        private Guid _userId;

        public EmployerInfoBoxPresenter(IEmployerInfoBoxView view, IPrincipal principal, IAdministratorService administratorService, IEmployerService employerService)
            : base(view, principal)
        {
            _employerService = employerService;
            _administratorService = administratorService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            _userId = ((WexHealthIdentity)_principal.Identity).User.Id;

            Employer _employer = _employerService.GetEmployerByCode(_view.Code, _userId);

            _view.Name = _employer.Name;
            _view.Code = _employer.Code;
            _view.City = _employer.Address.City;
            _view.Zip = _employer.Address.ZipCode;
            _view.Phone = _employer.Phones.PhoneNumber;
            _view.Logo = byteArrayToImage(_employer.Logo);
        }

        private string byteArrayToImage(byte[] byteArrayIn)
        {
            string imageBase64Data = byteArrayIn != null ? Convert.ToBase64String(byteArrayIn) : String.Empty;
            return imageBase64Data.Equals(String.Empty) ? imageBase64Data : string.Format($"data:image;base64,{imageBase64Data}");
        }
    }
}
