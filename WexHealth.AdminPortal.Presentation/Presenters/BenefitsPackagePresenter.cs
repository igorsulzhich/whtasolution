﻿using System;
using System.Linq;
using System.Security.Principal;
using System.Collections.Generic;
using WexHealth.Security.Infrastructure;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;
using WexHealth.Presentation.Presenters.Results;

namespace WexHealth.AdminPortal.Presentation.Presenters
{
    public class BenefitsPackagePresenter : ViewPresenterBase<IBenefitsPackageView>, IBenefitsPackagePresenter
    {
        private readonly IBenefitsPackageService _benefitsPackageService;
        private readonly IEnrollmentReadService _enrollmentReadService;

        public BenefitsPackagePresenter(IBenefitsPackageView view, IPrincipal principal, IBenefitsPackageService benefitsPackageService,
            IEnrollmentReadService enrollmentReadService)
            :base(view, principal)
        {
            _benefitsPackageService = benefitsPackageService;
            _enrollmentReadService = enrollmentReadService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            InitAdministrator();

            if (!isPostBack)
            {
                _view.BenefitsPackages = _benefitsPackageService.GetPackages();
            }
        }

        private void InitAdministrator()
        {
            _benefitsPackageService.InitService(((WexHealthIdentity)_principal.Identity).User, _view.Identity);
        }

        public IResult Initialize(int id)
        {
            IResult result = null;

            try
            {
                _benefitsPackageService.Initialize(id);
                result = new OperationResult(OperationStatus.Success);
            }
            catch (Exception exception)
            {
                result = new OperationResult(OperationStatus.Failed, exception.Message);
            }

            return result;
        }

        public IResult Delete(int id)
        {
            IResult result = null;

            try
            {
                _benefitsPackageService.DeletePackage(id);
                result = new OperationResult(OperationStatus.Success);
            }
            catch (Exception exception)
            {
                result = new OperationResult(OperationStatus.Failed, exception.Message);
            }

            return result;
        }

        public bool HaveEnroll(int packageId)
        {
            bool result = false;

            IEnumerable<Enrollment> dbEnrollment = _enrollmentReadService.GetEnrollmentsByPackageId(packageId);

            if (dbEnrollment.Any())
            {
                result = true;
            }

            return result;
        }
    }
}
