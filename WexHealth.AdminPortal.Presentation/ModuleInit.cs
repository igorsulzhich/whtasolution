﻿using System.Reflection;
using System.ComponentModel.Composition;
using Autofac;
using Autofac.Builder;
using Autofac.Extras.Attributed;
using WexHealth.Common.Modules.Common.Interfaces;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.AdminPortal.Presentation
{
    [Export(typeof(IModule))]
    public class ModuleInit : IModule
    {
        public void Initialize(ContainerBuilder builder)
        {
            var assembly = Assembly.GetExecutingAssembly();

            builder.RegisterAssemblyTypes(assembly)
                   .Where(t => t.Name.EndsWith("Presenter"))
                   .AsImplementedInterfaces().WithAttributeFilter();

            builder.RegisterGeneratedFactory<SetupManagePresenterFactory>();
            builder.RegisterGeneratedFactory<BenefitsPackagePresenterFactory>();
            builder.RegisterGeneratedFactory<ClaimEditPresenterFactory>();
            builder.RegisterGeneratedFactory<ClaimIndexPresenterFactory>();
            builder.RegisterGeneratedFactory<ClaimManagePresenterFactory>();
            builder.RegisterGeneratedFactory<ConsumerInfoBoxPresenterFactory>();
            builder.RegisterGeneratedFactory<ConsumerManageProfilePresenterFactory>();
            builder.RegisterGeneratedFactory<ConsumerIndexPresenterFactory>();
            builder.RegisterGeneratedFactory<ConsumerManageEnrollmentPresenterFactory>();
            builder.RegisterGeneratedFactory<ConsumerManagePresenterFactory>();
            builder.RegisterGeneratedFactory<EmployerManageProfilePresenterFactory>();
            builder.RegisterGeneratedFactory<EmployerIndexPresenterFactory>();
            builder.RegisterGeneratedFactory<EmployerInfoBoxPresenterFactory>();
            builder.RegisterGeneratedFactory<EmployerManagePresenterFactory>();
            builder.RegisterGeneratedFactory<EmployerManagePlanPresenterFactory>();
            builder.RegisterGeneratedFactory<EmployerSetupPresenterFactory>();
            builder.RegisterGeneratedFactory<EmployerManagePlanYearPresenterFactory>();
            builder.RegisterGeneratedFactory<NavigationControlPresenterFactory>();
            builder.RegisterGeneratedFactory<ReportIndexPresenterFactory>();
            builder.RegisterGeneratedFactory<ReportEmployerPresenterFactory>();
            builder.RegisterGeneratedFactory<ReportConsumerPresenterFactory>();
            builder.RegisterGeneratedFactory<ReportHistoryEmployerPresenterFactory>();
            builder.RegisterGeneratedFactory<ReportHistoryConsumerPresenterFactory>();
            builder.RegisterGeneratedFactory<SetupIndexPresenterFactory>();
            builder.RegisterGeneratedFactory<SetupManagePresenterFactory>();

        }
    }
}
