﻿using System;
using Moq;
using NUnit.Framework;
using WexHealth.Domain.Entities;
using WexHealth.DAL.Repositories.Common.Interfaces;
using WexHealth.BLL.Services;
using WexHealth.Presentation.Presenters;
using WexHealth.Presentation.Views;
using WexHealth.Presentation.Presenters.Results;
using System.Collections.Generic;

namespace WexHealth.EmployerPortal.Tests
{
    [TestFixture]
    public class LoginPresenterTest
    {
        //private User testUser = new User
        //{
        //    Id = Guid.Parse("64bd8ce3-4a68-e611-9bee-408d5cc8a737"),
        //    UserName = "test",
        //    PasswordHash = "6933cb900ce4300d10bee607da957381b3b987c9d7f92ea1f2aef1c74c9ce639",
        //    PasswordSalt = "954fd626821b4aa044511f5583f2d5343cf982a3b12eff9406826cf278e01316",
        //    LoginType = "Employer"
        //};
        //private UserService userService;

        //[OneTimeSetUp]
        //public void Initialize()
        //{
        //    //Arrange
        //    Mock<IUserRepositoryMock> userRepositoryMock = new Mock<IUserRepositoryMock>();
        //    GetPasswordHashResponse userRepositoryResponse = new GetPasswordHashResponse
        //    {
        //        PasswordHash = testUser.PasswordHash,
        //        PasswordSalt = testUser.PasswordSalt
        //    };
        //    string passwordHash = null;
        //    string passwordSalt = null;
        //    userRepositoryMock
        //        .Setup(x => x.GetPasswordHash(testUser.UserName, passwordHash, passwordSalt))
        //        .Returns(userRepositoryResponse);
        //    IUserRepository userRepository = new UserRepositoryStub(userRepositoryMock.Object);

        //    Mock<ISecurityDomainRepository> securityDomainRepository = new Mock<ISecurityDomainRepository>();

        //    userService = new UserService(userRepository, securityDomainRepository.Object);
        //}
        //[Test]
        //public void Login_WhenUsernameAndPasswordValid_LoginSuccess()
        //{
        //    //Arrange
        //    Mock<ILoginView> loginView = new Mock<ILoginView>();
        //    loginView.Setup(x => x.UserName).Returns("test");
        //    loginView.Setup(x => x.Password).Returns("test");

        //    LoginPresenter presenter = new LoginPresenter(loginView.Object, userService);

        //    //Act
        //    LoginResult loginResult = (LoginResult)presenter.Login();

        //    //Assert
        //    Assert.AreEqual(LoginStatus.Success, loginResult.Status);
        //}

        //[Test]
        //public void Login_WhenusernameAndPasswordInvalid_LoginFailed()
        //{
        //    //Arrange
        //    Mock<ILoginView> loginView = new Mock<ILoginView>();
        //    loginView.Setup(x => x.UserName).Returns("test");
        //    loginView.Setup(x => x.Password).Returns("test2");

        //    LoginPresenter presenter = new LoginPresenter(loginView.Object, userService);

        //    //Act
        //    LoginResult loginResult = (LoginResult)presenter.Login();

        //    //Assert
        //    Assert.AreEqual(LoginStatus.Failed, loginResult.Status);
        //}



        ////Stub
        //public interface IUserRepositoryMock
        //{
        //    GetPasswordHashResponse GetPasswordHash(string username, string passwordHash, string passwordSalt);
        //}
        //public class GetPasswordHashResponse
        //{
        //    public string PasswordHash { get; set; }
        //    public string PasswordSalt { get; set; }
        //}

        //public class UserRepositoryStub : IUserRepository
        //{
        //    private readonly IUserRepositoryMock _userRepositoryMock;
        //    public UserRepositoryStub(IUserRepositoryMock userRepositoryMock)
        //    {
        //        _userRepositoryMock = userRepositoryMock;
        //    }

        //    public Guid CreateUser(string username, string passwordHash, string passwordSalt, string loginType, string alias)
        //    {
        //        throw new NotImplementedException();
        //    }

        //    public bool DeleteUser(Guid id)
        //    {
        //        throw new NotImplementedException();
        //    }

        //    public void GetPasswordHash(string username, ref string passwordHash, ref string passwordSalt)
        //    {
        //        GetPasswordHashResponse response = _userRepositoryMock.GetPasswordHash(username, passwordHash, passwordSalt);
        //        if (response == null) return;
        //        passwordHash = response.PasswordHash;
        //        passwordSalt = response.PasswordSalt;
        //    }

        //    public User GetUserById(Guid id)
        //    {
        //        throw new NotImplementedException();
        //    }

        //    public User GetUserByUsername(string username)
        //    {
        //        throw new NotImplementedException();
        //    }

        //    public IEnumerable<User> GetUsersByLoginType(string loginType)
        //    {
        //        throw new NotImplementedException();
        //    }

        //    public IEnumerable<User> GetUsersBySecurityDomain(Guid id)
        //    {
        //        throw new NotImplementedException();
        //    }

        //    public IEnumerable<User> GetUsersBySecurityDomain(Guid id, string loginType)
        //    {
        //        throw new NotImplementedException();
        //    }
        //}


    }
}
