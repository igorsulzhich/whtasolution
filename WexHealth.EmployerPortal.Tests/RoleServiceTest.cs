﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration.Provider;
using Moq;
using NUnit.Framework;
using WexHealth.BLL.Services;
using WexHealth.DAL.Repositories.Common.Interfaces;
using WexHealth.Domain.Entities;

namespace WexHealth.EmployerPortal.Tests
{
    [TestFixture]
    class RoleServiceTest
    {
        private Role testRoleNew = new Role
        {
            Id = Guid.Parse("64bd8ce3-4a68-e611-9bee-408d5cc8a737"),
            Name = "new_role"         
        };

        private List<Role> testRoleList = new List<Role>()
        {
            new Role
            {
                Id = Guid.Parse("11bd8ce1-4a68-e611-1bee-111d5cc8a111"),
                Name = "old_role"
            }
        };

        Mock<IUserRepository> userRepositoryMock = new Mock<IUserRepository>();
        Mock<IRoleRepository> roleRepositoryMock = new Mock<IRoleRepository>();

        [OneTimeSetUp]
        public void Initialize()
        {
            roleRepositoryMock
               .Setup(x => x.RoleExists("new_role")).Returns(false);
            roleRepositoryMock
               .Setup(x => x.RoleExists("old_role")).Returns(true);

            roleRepositoryMock
                .Setup(x => x.SaveRole(testRoleNew.Name))
                .Callback(() => testRoleList.Add(testRoleNew))
                .Returns(testRoleNew.Id);
        }

        [Test]
        public void CreateRole_WhenCreateRoleValid()
        {
            //Arrange
            RoleService roleService = new RoleService(roleRepositoryMock.Object, userRepositoryMock.Object);

            //Act
            Guid createRoleResult = (Guid)roleService.CreateRole(testRoleNew.Name);

            //Assert
            Assert.AreEqual(testRoleList.LastOrDefault().Id, createRoleResult);
        }

        [Test]
        public void CreateRole_WhenCreateRoleInalid()
        {
            //Arrange
            RoleService roleService = new RoleService(roleRepositoryMock.Object, userRepositoryMock.Object);

            //Act & Assert
            Assert.That(() => (Guid)roleService.CreateRole("old_role"), Throws.TypeOf<ProviderException>());
        }
    }
}
