﻿<%@ Page Language="C#" Title="Login" MasterPageFile="~/Authentication.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WexHealth.EmployerPortal.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderLogin" runat="server">
    <div class="page__content page__content_centered">
        <div class="form__wrapper form__wrapper_centered">
            <asp:ValidationSummary runat="server" ValidationGroup="validationGroupLogin" DisplayMode="BulletList" ShowMessageBox="False" ShowSummary="True" CssClass="message message_type_error" />
            <asp:CustomValidator runat="server" ID="validateLogin" ValidationGroup="validationGroupLogin" Display="None" EnableClientScript="false" />
            <div class="form">
                <div class="form__row inlinefix">
                    <div class="form__label">Login</div>
                    <div class="form__input-wrapper">
                        <input type="text" runat="server" id="textboxUserName" class="input input_type_text" autocomplete="off" />
                        <asp:RequiredFieldValidator runat="server" ValidationGroup="validationGroupLogin" ControlToValidate="textboxUserName" Display="None" ErrorMessage="Enter username" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Password</div>
                    <div class="form__input-wrapper">
                        <input type="password" runat="server" id="textboxPassword" class="input input_type_text" autocomplete="off" />
                        <asp:RequiredFieldValidator runat="server" ValidationGroup="validationGroupLogin" ControlToValidate="textboxPassword" Display="None" ErrorMessage="Enter password" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Domain</div>
                    <div class="form__input-wrapper">
                        <asp:DropDownList runat="server" ID="dropdownSecurityDomains" CssClass="input input_type_dropdown" AppendDataBoundItems="true">
                            <asp:ListItem Text="- Select domain -" Value="" />
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator runat="server" ValidationGroup="validationGroupLogin" ControlToValidate="dropdownSecurityDomains" Display="None" InitialValue="" ErrorMessage="Select security domain" />
                    </div>
                </div>
                <div class="form__btns">
                    <asp:Button ID="SubmitLogin" ValidationGroup="validationGroupLogin" OnClick="Login_Click" CssClass="btn btn_priority_high" Text="Login" runat="server" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>

