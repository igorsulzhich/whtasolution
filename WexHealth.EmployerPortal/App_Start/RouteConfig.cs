﻿using System.Web.Routing;

namespace WexHealth.EmployerPortal
{
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.Ignore("{resource}.axd/{*pathInfo}");
            routes.Ignore("bundles/{*catch}");
            routes.Ignore("Content/{*catch}");

            routes.MapPageRoute("Default", "", "~/Index.aspx");

            routes.MapPageRoute(null, "{page}/", "~/Pages/{page}/Index.aspx");
            routes.MapPageRoute(null, "{page}/{action}/", "~/Pages/{page}/{action}.aspx");
        }
    }
}