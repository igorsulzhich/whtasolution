﻿<%@ Page Language="C#" Title="Home" AutoEventWireup="true" CodeBehind="Index.aspx.cs" MasterPageFile="~/Employer.Master" Inherits="WexHealth.EmployerPortal.Index" %>
<%@ Register TagPrefix="UC" TagName="NavBar" Src="~/Controls/NavigationControl.ascx" %>

<asp:Content ID="ContentEmployer" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <UC:NavBar runat="server" CurrentPageName="Home"/>

    <div class="page__content">
    </div>
    
</asp:Content>