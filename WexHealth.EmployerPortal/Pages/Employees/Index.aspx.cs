﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Collections.Specialized;
using Microsoft.Practices.ServiceLocation;
using WexHealth.Domain.Entities;
using WexHealth.Web.Helpers;
using WexHealth.Security.Infrastructure;
using WexHealth.EmployerPortal.Presentation.Views;
using WexHealth.EmployerPortal.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.ConsumerPortal.Employer
{
    public partial class Employees_Index : System.Web.UI.Page, IEmployeesView
    {
        private readonly IEmployeesPresenter _presenter;

        public string FirstName
        {
            get
            {
                return Request.QueryString["fn"];
            }
        }

        public string LastName
        {
            get
            {
                return Request.QueryString["ln"];
            }
        }

        public string SSN
        {
            get
            {
                return Request.QueryString["ssn"];
            }
        }

        public IEnumerable<Consumer> Consumers
        {
            set
            {
                listConsumers.DataSource = value;
                listConsumers.DataBind();
            }
        }

        public Employees_Index()
        {
            var factory = ServiceLocator.Current.GetInstance<EmployeesPresenterFactory>();
            var principal = (WexHealthPrincipal)HttpContext.Current.User;
            _presenter = factory(this, principal);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.InitView(Page.IsPostBack);

            if (!Page.IsPostBack)
            {
                InitFirstName();
                InitLastName();
                InitSSN();
            }
        }

        private void InitFirstName()
        {
            if (!string.IsNullOrEmpty(FirstName))
            {
                textboxFirstName.Value = FirstName;
            }
        }

        private void InitLastName()
        {
            if (!string.IsNullOrEmpty(LastName))
            {
                textboxLastName.Value = LastName;
            }
        }

        private void InitSSN()
        {
            if (!string.IsNullOrEmpty(SSN))
            {
                textboxSSN.Value = SSN;
            }
        }

        protected void Search_Consumer_Click(object sender, EventArgs e)
        {
            var paramList = new NameValueCollection()
            {
                { "fn", textboxFirstName.Value },
                { "ln", textboxLastName.Value },
                { "ssn", textboxSSN.Value }
            };

            Response.Redirect(UrlHelper.AddParamsUrl(paramList));
        }
    }
}