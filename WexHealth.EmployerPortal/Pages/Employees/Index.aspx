﻿<%@ Page Title="Employees" Language="C#" MasterPageFile="~/Employer.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WexHealth.ConsumerPortal.Employer.Employees_Index" %>

<%@ Register TagPrefix="UC" TagName="NavBar" Src="~/Controls/NavigationControl.ascx" %>
<%@ Import Namespace="System.Globalization" %>

<asp:Content ID="ContentEmployer" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <UC:NavBar runat="server" CurrentPageName="Employees" />

    <div class="page__content">

        <div class="table__wrapper">

            <div class="table__search">
                <div class="form__wrapper">
                    <div class="form form_orientation_horizontal inlinefix">
                        <div class="form__row form_orientation_horizontal__row">
                            <div class="form__label form_orientation_horizontal__label">First Name</div>
                            <div class="form__input-wrapper form_orientation_horizontal__input-wrapper">
                                <input type="text" class="input input_type_text" id="textboxFirstName" runat="server" autocomplete="off" />
                            </div>
                        </div>
                        <div class="form__row form_orientation_horizontal__row">
                            <div class="form__label form_orientation_horizontal__label">Last Name</div>
                            <div class="form__input-wrapper form_orientation_horizontal__input-wrapper">
                                <input type="text" class="input input_type_text" id="textboxLastName" runat="server" autocomplete="off" />
                            </div>
                        </div>
                        <div class="form__row form_orientation_horizontal__row">
                            <div class="form__label form_orientation_horizontal__label">SSN</div>
                            <div class="form__input-wrapper form_orientation_horizontal__input-wrapper">
                                <input type="text" class="input input_type_text" id="textboxSSN" runat="server" autocomplete="off" />
                            </div>
                        </div>
                        <div class="form__btns form_orientation_horizontal__row__btns">
                            <asp:Button ID="Search_Consumer" OnClick="Search_Consumer_Click" CssClass="btn btn_priority_high" Text="Search" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="table__title-wrapper clearfix">
                <div class="table__title">Employees</div>
            </div>
            <table class="table">
                <thead class="table__thead">
                    <tr class="table__head">
                        <th class="table__column">
                            <div class="column__text">First Name</div>
                        </th>
                        <th class="table__column table__column_filter">
                            <div class="column__text">Last Name</div>
                        </th>
                        <th class="table__column">
                            <div class="column__text table__column_type_numeric">SSN</div>
                        </th>
                        <th class="table__column table__column_type_numeric">
                            <div class="column__text">Date of Birth</div>
                        </th>
                    </tr>
                </thead>
                <tbody class="table__tbody">
                    <asp:Repeater runat="server" ID="listConsumers" ItemType="WexHealth.Domain.Entities.Consumer">
                        <ItemTemplate>
                            <tr class="table__row">
                                <td class="table__cell"><%#: Item.Individual.FirstName %></td>
                                <td class="table__cell"><%#: Item.Individual.LastName %></td>
                                <td class="table__cell table__cell_type_numeric"><%#: Item.SSN %></td>
                                <td class="table__cell table__cell_type_numeric">
                                    <div runat="server" visible='<%# !DateTime.MinValue.Equals(Item.Individual.DateBirth) %>'>
                                        <%#: Item.Individual.DateBirth.ToString("MM/d/yyyy", CultureInfo.InvariantCulture) %>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
