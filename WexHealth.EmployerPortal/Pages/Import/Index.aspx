﻿<%@ Page Language="C#" Title="Import" MasterPageFile="~/Employer.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WexHealth.ConsumerPortal.Employer.Import_Index" %>

<%@ Register TagPrefix="UC" TagName="NavBar" Src="~/Controls/NavigationControl.ascx" %>

<asp:Content ID="ContentEmployer" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <UC:NavBar runat="server" CurrentPageName="Import" />

    <div class="page__content">
        <div runat="server" class="message message_type_success" id="successImport" visible="false">
        </div>
        <div class="form__wrapper">
            <asp:ValidationSummary runat="server" ValidationGroup="validationGroupImport" DisplayMode="BulletList" ShowMessageBox="False" ShowSummary="True" CssClass="message message_type_error" />
            <asp:CustomValidator runat="server" ValidationGroup="validationGroupImport" ID="validateImport" Display="None" EnableClientScript="false" />
            <div class="form">
                <div class="form__head">
                    <div class="form__title">Import employees</div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Upload file</div>
                    <div class="form__input-wrapper">
                        <asp:FileUpload class="input input_type_file" runat="server" ID="fileConsumers" />
                    </div>
                    <div class="form__comment">Locate file you want to download. <a href="<%= Page.ResolveUrl("~/Content/data/import-template.csv") %>" class="link">Download template</a></div>
                    <asp:RequiredFieldValidator runat="server" ValidationGroup="validationGroupImport" ControlToValidate="fileConsumers" ErrorMessage="Select file to import" />
                    <asp:RegularExpressionValidator runat="server" ValidationGroup="validationGroupImport" ControlToValidate="fileConsumers" ValidationExpression="(.*\.(csv)$)" ErrorMessage="Only .csv files are allowed" />
                </div>
                <div class="form__btns">
                    <asp:Button runat="server" ValidationGroup="groupImport" ID="validationGroupImport" CssClass="btn btn_priority_high" Text="Import" OnClick="sumbitImport_Click" />
                    <a href="<%= Page.ResolveUrl("~/") %>" class="btn btn_priority_normal"><span class="btn__text">Cancel</span></a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
