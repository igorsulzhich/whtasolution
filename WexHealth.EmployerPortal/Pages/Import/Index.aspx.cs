﻿using System;
using System.Web;
using System.IO;
using Microsoft.Practices.ServiceLocation;
using WexHealth.Security.Infrastructure;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.EmployerPortal.Presentation.Views;
using WexHealth.EmployerPortal.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.ConsumerPortal.Employer
{
    public partial class Import_Index : System.Web.UI.Page, IImportView
    {
        private readonly IImportPresenter _presenter;

        public string FileName
        {
            get
            {
                return fileConsumers.HasFile ? fileConsumers.FileName : null;
            }
        }

        public Import_Index()
        {
            var factory = ServiceLocator.Current.GetInstance<ImportPresenterFactory>();
            var principal = (WexHealthPrincipal)HttpContext.Current.User;
            _presenter = factory(this, principal);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.InitView(Page.IsPostBack);

            if (!Page.IsPostBack)
            {
                successImport.Visible = false;
            }
        }

        protected void sumbitImport_Click(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                Page.Validate("validationGroupImport");

                if (Page.IsValid)
                {
                    OperationResult result = (OperationResult)_presenter.CreateRequest();
                    if (result.Status == OperationStatus.Success)
                    {
                        if (!string.IsNullOrEmpty(result.Message))
                        {
                            successImport.InnerText = result.Message;
                            successImport.Visible = true;
                        }
                    }
                    else
                    {
                        validateImport.IsValid = false;
                        validateImport.ErrorMessage = result.Message;
                    }
                }
            }
        }

        public string SaveFile(string filename, string extension)
        {
            string path = Server.MapPath("~/Content/data/") + filename + extension;

            fileConsumers.SaveAs(path);
            if (!File.Exists(path))
            {
                throw new InvalidOperationException("Imported file can not be saved");
            }

            return path;
        }
    }
}