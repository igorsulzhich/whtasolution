﻿<%@ Page Language="C#" Title="Setup" MasterPageFile="~/Employer.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WexHealth.ConsumerPortal.Employer.Setup_Index" %>

<%@ Register TagPrefix="UC" TagName="NavBar" Src="~/Controls/NavigationControl.ascx" %>

<asp:Content ID="ContentEmployer" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <UC:NavBar runat="server" CurrentPageName="Setup" />

    <div class="page__content">
        <div class="form__wrapper">
            <div class="form__head">
                <div class="form__title">Consumer management</div>
            </div>
            <div class="form">
                <asp:Repeater runat="server" ID="tableSettings" ItemType="WexHealth.Domain.Entities.Setting">
                    <ItemTemplate>
                        <div class="form__row inlinefix">
                            <div class="form__label"><%#: Item.Name %></div>
                            <div class="form__input-wrapper">
                                <input type="hidden" runat="server" value='<%#: Item.Id %>' id="settingId" name="settingId" />
                                <input class="input input_type_radiobutton" type="radio" id="defaultSetting" name="radioSetting" runat="server" />
                                <div class="input_type_radiobutton__label">Administrator Default</div>
                                <input class="input input_type_radiobutton" type="radio" id="allowSetting" name="radioSetting" runat="server" />
                                <div class="input_type_radiobutton__label">Allow</div>
                                <input class="input input_type_radiobutton" type="radio" id="disallowSetting" name="radioSetting" runat="server" />
                                <div class="input_type_radiobutton__label">Prevent</div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>

                <div class="form__btns">
                    <asp:Button ID="Button" runat="server" Text="Submit" OnClick="Setting_Click" CssClass="btn btn_priority_high" />
                    <a href="<%= Page.ResolveUrl("~/") %>" class="btn btn_priority_normal"><span class="btn__text">Cancel</span></a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
