﻿using System;

namespace WexHealth.EmployerPortal.Controls
{
    public partial class NavigationControl : System.Web.UI.UserControl
    {
        public string CurrentPageName { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public string IsCurrentPage(string itemName)
        {
            return CurrentPageName == itemName ? "tabs__link_active" : string.Empty;
        }
    }
}