﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NavigationControl.ascx.cs" Inherits="WexHealth.EmployerPortal.Controls.NavigationControl" %>

    <nav class="tabs">
        <ul class="tabs__list inlinefix">
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/") %>" class="tabs__link <%= IsCurrentPage("Home")  %>">Home</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/employees") %>" class="tabs__link <%= IsCurrentPage("Employees") %>">Employees</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/import") %>" class="tabs__link <%= IsCurrentPage("Import") %>">Import</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/setup") %>" class="tabs__link <%= IsCurrentPage("Setup") %>">Setup</a></li>
        </ul>
    </nav>