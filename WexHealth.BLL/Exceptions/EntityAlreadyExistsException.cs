﻿using System;

namespace WexHealth.BLL.Exceptions
{
    /// <summary>
    /// An exception that occurs when the entity with specified properties exists
    /// </summary>
    public sealed class EntityAlreadyExistsException : Exception
    {
        private const string _msgFormat = "Entity already exists";
        private const string _msgEntityFormat = "{0} already exists";
        private const string _mshEntityParamsFormat = "{0} with {1}: {2} already exists";

        /// <summary>
        /// Initialize an instance of <seealso cref="EntityAlreadyExistsException"/>
        /// </summary>
        public EntityAlreadyExistsException() :
            base(_msgFormat)
        { }

        /// <summary>
        /// Initialize an instance of <seealso cref="EntityAlreadyExistsException"/>
        /// </summary>
        /// <param name="entityName">Entity name that affects the exception</param>
        public EntityAlreadyExistsException(string entityName) :
            base(string.Format(_msgEntityFormat, entityName))
        { }

        /// <summary>
        /// Initialize an instance of <seealso cref="EntityAlreadyExistsException"/>
        /// </summary>
        /// <param name="entityName">Entity name that affects the exception</param>
        /// <param name="innerException">Inner exception that caused current exception</param>
        public EntityAlreadyExistsException(string entityName, Exception innerException) :
            base(string.Format(_msgEntityFormat, entityName), innerException)
        { }

        /// <summary>
        /// Initialize an instance of <seealso cref="EntityAlreadyExistsException"/>
        /// </summary>
        /// <param name="entityName">Entity name that affects the exception</param>
        /// <param name="paramKey">Name of entity property that effects the exception</param>
        /// <param name="paramValue">Value of entity property that effects the exception</param>
        public EntityAlreadyExistsException(string entityName, string paramKey, string paramValue) :
            base(string.Format(_mshEntityParamsFormat, entityName, paramKey, paramValue))
        { }

        /// <summary>
        /// Initialize an instance of <seealso cref="EntityAlreadyExistsException"/>
        /// </summary>
        /// <param name="entityName">Entity name that affects the exception</param>
        /// <param name="paramKey">Name of entity property that effects the exception</param>
        /// <param name="paramValue">Value of entity property that effects the exception</param>
        /// <param name="innerException">Inner exception that caused current exception</param>
        public EntityAlreadyExistsException(string entityName, string paramKey, string paramValue, Exception innerException) :
            base(string.Format(_mshEntityParamsFormat, entityName, paramKey, paramValue), innerException)
        { }
    }
}
