﻿using System;

namespace WexHealth.BLL.Exceptions
{
    /// <summary>
    /// An exception that occurs when operation with entity failed
    /// </summary>
    public class EntityOperationFailedException : Exception
    {
        private const string _msgFormat = "Failed operation with entity";
        private const string _msgEntityFormat = "Failed operation with {0} entity";
        private const string _msgEntityOperationFormat = "Failed {0} operation with {1} entity";

        /// <summary>
        /// Initialize an instance of <seealso cref="EntityOperationFailedException"/>
        /// </summary>
        public EntityOperationFailedException() :
            base(_msgFormat)
        { }

        /// <summary>
        /// Initialize an instance of <seealso cref="EntityOperationFailedException"/>
        /// </summary>
        /// <param name="entityName">Entity name that affects the current exception</param>
        public EntityOperationFailedException(string entityName) :
            base(string.Format(_msgEntityFormat, entityName))
        { }

        /// <summary>
        /// Initialize an instance of <seealso cref="EntityOperationFailedException"/>
        /// </summary>
        /// <param name="entityName">Entity name that affects the current exception</param>
        /// <param name="innerException">Inner exception that caused current exception</param>
        public EntityOperationFailedException(string entityName, Exception innerException) :
            base(string.Format(_msgEntityFormat, entityName), innerException)
        { }

        /// <summary>
        /// Initialize an instance of <seealso cref="EntityOperationFailedException"/>
        /// </summary>
        /// <param name="entityName">Entity name that affects the current exception</param>
        /// <param name="operationName">Operation name that affects the current exception</param>
        public EntityOperationFailedException(string entityName, string operationName)
            : base(string.Format(_msgEntityOperationFormat, operationName, entityName))
        { }

        /// <summary>
        /// Initialize an instance of <seealso cref="EntityOperationFailedException"/>
        /// </summary>
        /// <param name="entityName">Entity name that affects the current exception</param>
        /// <param name="operationName">Operation name that affects the current exception</param>
        /// <param name="innerException">Inner exception that caused current exception</param>
        public EntityOperationFailedException(string entityName, string operationName, Exception innerException)
            : base(string.Format(_msgEntityOperationFormat, operationName, entityName), innerException)
        { }
    }
}
