﻿using System;

namespace WexHealth.BLL.Exceptions
{
    /// <summary>
    /// An exception that occurs when the entity with specified properties doesn't exist
    /// </summary>
    public sealed class EntityNotFoundException : Exception
    {
        private const string _msgFormat = "Entity doesn't exist";
        private const string _msgEntityFormat = "{0} doesn't exist";
        private const string _msgEntityParamsFormat = "{0} with {1}: {2} doesn't exist";

        /// <summary>
        /// Initialize an instance of <seealso cref="EntityNotFoundException"/>
        /// </summary>
        public EntityNotFoundException() :
            base(_msgFormat)
        { }

        /// <summary>
        /// Initialize an instance of <seealso cref="EntityNotFoundException"/>
        /// </summary>
        /// <param name="entityName">Entity name that affects the exception</param>
        public EntityNotFoundException(string entityName) :
            base(string.Format(_msgEntityFormat, entityName))
        { }

        /// <summary>
        /// Initialize an instance of <seealso cref="EntityNotFoundException"/>
        /// </summary>
        /// <param name="entityName">Entity name that affects the exception</param>
        /// <param name="innerException">Inner exception instance that caused current exception</param>
        public EntityNotFoundException(string entityName, Exception innerException) :
            base(string.Format(_msgEntityFormat, entityName), innerException)
        { }

        /// <summary>
        /// Initialize an instance of <seealso cref="EntityNotFoundException"/>
        /// </summary>
        /// <param name="entityName">Entity name that affects the exception</param>
        /// <param name="paramKey">Name of entity property that affects the exception</param>
        /// <param name="paramValue">Value of entity property that affects the exception</param>
        public EntityNotFoundException(string entityName, string paramKey, string paramValue) :
            base(string.Format(_msgEntityParamsFormat, entityName, paramKey, paramValue))
        { }

        /// <summary>
        /// Initialize an instance of <seealso cref="EntityNotFoundException"/>
        /// </summary>
        /// <param name="entityName">Entity name that affects the exception</param>
        /// <param name="paramKey">Name of entity property that affects the exception</param>
        /// <param name="entityKeyValue">Value of entity property that affects the exception</param>
        /// <param name="paramValue">Inner exception instance that caused current exception</param>
        public EntityNotFoundException(string entityName, string paramKey, string paramValue, Exception innerException) :
            base(string.Format(_msgEntityParamsFormat, entityName, paramKey, paramValue), innerException)
        { }
    }
}
