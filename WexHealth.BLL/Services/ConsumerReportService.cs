﻿using WexHealth.BLL.Services.Common;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.BLL.Services
{
    public class ConsumerReportService : ReportServiceBase, IReportService
    {
        public ConsumerReportService(IObjectFieldRepository objectFieldRepository, IReportRequestRepository reportRequestRepository, 
            IReportRequestStatusRepository reportRequestStatusRepository, IReportFieldsRepository reportFieldsRepository, IAdministratorService administratorService,
            IEmployerRepository employerRepository) : 
            base(objectFieldRepository, reportRequestRepository, reportRequestStatusRepository, reportFieldsRepository, administratorService, employerRepository)
        {
        }

        protected override string ObjectType
        {
            get
            {
                return "Consumer";
            }
        }
    }
}
