﻿using System;
using System.Collections.Generic;
using System.Configuration.Provider;
using log4net;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.BLL.Services
{
    /// <summary>
    /// Manage roles
    /// </summary>
    public class RoleService : IRoleService
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(RoleService));

        private readonly IUserRepository _userRepository;
        private readonly IRoleRepository _roleRepository;

        public RoleService(IRoleRepository roleRepository, IUserRepository userRepository)
        {
            _userRepository = userRepository;
            _roleRepository = roleRepository;
        }

        /// <summary>
        /// Add users to roles
        /// </summary>
        /// <param name="usernames">Array names of users</param>
        /// <param name="roleNames">Array names of roles</param>
        public void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            foreach (string rolename in roleNames)
            {
                if (rolename == null || rolename == "")
                    throw new ProviderException("Role name cannot be empty or null.");
                if (!RoleExists(rolename))
                    throw new ProviderException("Role name not found.");
            }

            foreach (string username in usernames)
            {
                if (username == null || username == "")
                    throw new ProviderException("User name cannot be empty or null.");
                if (username.Contains(","))
                    throw new ArgumentException("User names cannot contain commas.");
                if (_userRepository.GetUserByUserName(username) == null)
                    throw new ArgumentException("User name not found.");

                foreach (string rolename in roleNames)
                {
                    if (IsUserInRole(username, rolename))
                        throw new ProviderException("User is already in role.");
                }
            }

            _roleRepository.AddUsersToRoles(usernames, roleNames);
        }

        /// <summary>
        /// Create new role
        /// </summary>
        /// <param name="roleName">New role name</param>
        /// <returns></returns>
        public Guid CreateRole(string roleName)
        {
            if (RoleExists(roleName))
                throw new ProviderException("Role is already exists.");

            return _roleRepository.SaveRole(roleName);
        }

        /// <summary>
        /// Get all roles
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Role> GetAllRoles()
        {
            return _roleRepository.GetAllRoles();
        }

        /// <summary>
        /// Get all roles for current user
        /// </summary>
        /// <param name="username">User name</param>
        /// <returns></returns>
        public IEnumerable<Role> GetRolesForUser(string username)
        {
            if (username == null || username == "")
                throw new ProviderException("User name cannot be empty or null.");

            return _roleRepository.GetRolesForUser(username);
        }

        /// <summary>
        /// Check if user is in the current role
        /// </summary>
        /// <param name="username">User name</param>
        /// <param name="roleName">Role name</param>
        /// <returns></returns>
        public bool IsUserInRole(string username, string roleName)
        {
            if (username == null || username == "")
                throw new ProviderException("User name cannot be empty or null.");
            if (roleName == null || roleName == "")
                throw new ProviderException("Role name cannot be empty or null.");

            return _roleRepository.IsInRole(username, roleName);
        }

        /// <summary>
        /// Check if role exists
        /// </summary>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public bool RoleExists(string roleName)
        {
            if (roleName == null || roleName == "")
                throw new ProviderException("Role name cannot be empty or null.");

            return _roleRepository.RoleExists(roleName);
        }
    }
}
