﻿using System;
using log4net;
using WexHealth.BLL.Exceptions;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.DAL.Repositories.Common.Interfaces;
using WexHealth.DAL.Models;

namespace WexHealth.BLL.Services
{
    public class EmployerValidateService : IEmployerValidateService
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(EmployerValidateService));

        private readonly IAdministratorRepository _administratorRepository;
        private readonly IEmployerRepository _employerRepository;

        public EmployerValidateService(IAdministratorRepository administratorRepository, IEmployerRepository employerRepository)
        {
            _administratorRepository = administratorRepository;
            _employerRepository = employerRepository;
        }

        public bool ValidateByAdministrator(Guid employerId, Guid administratorId)
        {
            bool result = false;

            if (Guid.Empty.Equals(employerId)) throw new ArgumentNullException(nameof(employerId));

            if (Guid.Empty.Equals(administratorId)) throw new ArgumentNullException(nameof(administratorId));

            try
            {
                EmployerDAL dbEmployer = _employerRepository.GetEmployerById(employerId);
                if (null == dbEmployer)
                {
                    throw new EntityNotFoundException("Employer", "id", employerId.ToString());
                }

                AdministratorDAL dbAdministrator = _administratorRepository.GetAdministratorById(administratorId);
                if (null == dbAdministrator)
                {
                    throw new EntityNotFoundException("Administrator", "id", administratorId.ToString());
                }

                result = dbAdministrator.Id.Equals(dbEmployer.AdministratorId);
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("Employer", "validate", e);
            }

            return result;
        }

        public void TryValidateByAdministrator(Guid employerId, Guid administratorId)
        {
            bool result = ValidateByAdministrator(employerId, administratorId);

            if (!result)
            {
                throw new InvalidOperationException($"Employer with id: {employerId}, doesn't relate to administrator with id: {administratorId}");
            }
        }
    }
}
