﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WexHealth.BLL.Services.Common;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.BLL.Services
{
    public class EmployerReportService : ReportServiceBase, IReportService
    {
        public EmployerReportService(IObjectFieldRepository objectFieldRepository, IReportRequestRepository reportRequestRepository,
            IReportRequestStatusRepository reportRequestStatusRepository, IReportFieldsRepository reportFieldsRepository, 
            IAdministratorService administratorService, IEmployerRepository employerRepository) : 
            base(objectFieldRepository, reportRequestRepository, reportRequestStatusRepository, reportFieldsRepository, administratorService, employerRepository)
        {          
        }

        protected override string ObjectType
        {
            get
            {
                return "Employer";
            }
        }
    }
}
