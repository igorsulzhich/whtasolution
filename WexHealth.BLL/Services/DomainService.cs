﻿using System;
using System.Linq;
using System.Collections.Generic;
using PostSharp.Patterns.Contracts;
using log4net;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Models;
using WexHealth.BLL.Exceptions;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.DAL.Models;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.BLL.Services
{
    /// <summary>
    /// Manage common administrator, employer and consumer properties
    /// </summary>
    public class DomainService : IDomainService
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(DomainService));

        private readonly IPhoneRepository _phoneRepository;
        private readonly IPhoneConnectionRepository _phoneConnectionRepository;
        private readonly IAddressRepository _addressRepository;
        private readonly IAddressConnectionRepository _addressConnectionRepository;
        private readonly IIndividualRepository _individualRepository;
        private readonly IStateRepository _stateRepository;

        public DomainService(IPhoneRepository phoneRepository, IPhoneConnectionRepository phoneConnectionRepository,
            IAddressRepository addressRepository, IAddressConnectionRepository addressConnectionRepository, IIndividualRepository individualRepository,
            IStateRepository stateRepository)
        {
            _phoneRepository = phoneRepository;
            _phoneConnectionRepository = phoneConnectionRepository;
            _addressRepository = addressRepository;
            _addressConnectionRepository = addressConnectionRepository;
            _individualRepository = individualRepository;
            _stateRepository = stateRepository;
        }

        /// <summary>
        /// Get object's phones
        /// </summary>
        /// <param name="objectId">Object id</param>
        /// <param name="objectType">Type of object</param>
        /// <returns>List of object's phones</returns>
        public IEnumerable<Phone> GetPhonesByObject(Guid objectId, [Required] string objectType)
        {
            IEnumerable<Phone> result = null;

            if (Guid.Empty.Equals(objectId)) throw new ArgumentNullException(nameof(objectId));

            try
            {
                IEnumerable<PhoneConnectionDAL> phoneConnections = _phoneConnectionRepository.GetPhoneConnectionsByObject(objectId, objectType);
                result = Map(phoneConnections);
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("Phone", "get", exception);
            }

            return result;
        }

        /// <summary>
        /// Set phone to object
        /// </summary>
        /// <param name="model">Phone input model</param>
        /// <param name="objectId">Object id</param>
        /// <param name="objectType">Type of object</param>
        /// <returns>Id of inserted object's phone</returns>
        public Guid SetPhoneToObject([NotNull] PhoneInputModel model, Guid objectId, string objectType)
        {
            Guid result = Guid.Empty;

            if (Guid.Empty.Equals(objectId)) throw new ArgumentNullException(nameof(objectId));

            if (string.IsNullOrEmpty(objectType)) throw new ArgumentNullException(nameof(objectType));

            try
            {
                System.ComponentModel.DataAnnotations.Validator.ValidateObject(model,
                    new System.ComponentModel.DataAnnotations.ValidationContext(model, null, null), true);

                var phone = new PhoneDAL();
                Map(model, phone);

                var phoneConnection = new PhoneConnectionDAL
                {
                    ObjectId = objectId,
                    ObjectType = objectType
                };

                phoneConnection.PhoneId = _phoneRepository.SavePhone(phone);
                result = _phoneConnectionRepository.SavePhoneConnection(phoneConnection);

                log.Info(string.Format("Set phone: {0}, to object with id: {1}, and objectType: {2}",
                    model.Phone, objectId, objectType));
            }
            catch (Exception exception) when (exception is System.ComponentModel.DataAnnotations.ValidationException)
            {
                throw;
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("Phone", "setToObject", exception);
            }

            return result;
        }

        /// <summary>
        /// Update object's phone
        /// </summary>
        /// <param name="model">Phone input model</param>
        /// <param name="objectId">Object id</param>
        /// <param name="objectType">Type of object</param>
        /// <returns>Id of updated object's phone</returns>
        public Guid UpdatePhoneToObject([NotNull] PhoneInputModel model, Guid objectId, [Required] string objectType)
        {
            Guid result = Guid.Empty;

            if (Guid.Empty.Equals(objectId)) throw new ArgumentNullException(nameof(objectId));

            try
            {
                System.ComponentModel.DataAnnotations.Validator.ValidateObject(model,
                    new System.ComponentModel.DataAnnotations.ValidationContext(model, null, null), true);

                IEnumerable<PhoneConnectionDAL> dbPhoneConnections = _phoneConnectionRepository.GetPhoneConnectionsByObject(objectId, objectType);

                if (null == dbPhoneConnections)
                {
                    throw new EntityNotFoundException("PhoneConnections");
                }

                PhoneConnectionDAL dbPhoneConnection = dbPhoneConnections.FirstOrDefault();
                if (null == dbPhoneConnection)
                {
                    throw new EntityNotFoundException("PhoneConnection");
                }

                PhoneDAL dbPhone = dbPhoneConnection.Phone;
                if (null == dbPhone)
                {
                    throw new EntityNotFoundException("PhoneConnection", "id", dbPhoneConnection.PhoneId.ToString());
                }

                if (!string.Equals(dbPhone.PhoneNumber, model.Phone))
                {
                    Map(model, dbPhone);

                    result = _phoneRepository.SavePhone(dbPhone);

                    log.Info(string.Format("Update phone: {0} (phoneId: {1}), to object with id: {2}, and objectType: {3}",
                        model.Phone, dbPhone.Id, objectId, objectType));
                }
            }
            catch (Exception exception) when (exception is EntityNotFoundException || exception is System.ComponentModel.DataAnnotations.ValidationException)
            {
                throw;
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("Phone", "updateToObject", exception);
            }

            return result;
        }

        /// <summary>
        /// Get object's addresses
        /// </summary>
        /// <param name="objectId">Object id</param>
        /// <param name="objectType">Type of object</param>
        /// <returns>List of object's addresses</returns>
        public IEnumerable<Address> GetAddressesByObject(Guid objectId, [Required] string objectType)
        {
            IEnumerable<Address> result = null;

            if (Guid.Empty.Equals(objectId)) throw new ArgumentNullException(nameof(objectId));

            try
            {
                IEnumerable<AddressConnectionDAL> addressConnections = _addressConnectionRepository.GetAddressConnectionsByObject(objectId, objectType);
                result = Map(addressConnections);
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("Addresses", "getByObject", exception);
            }

            return result;
        }

        /// <summary>
        /// Set address to object
        /// </summary>
        /// <param name="model">Address input model</param>
        /// <param name="objectId">Object id</param>
        /// <param name="objectType">Type of object</param>
        /// <returns>Id of inserted object's address</returns>
        public Guid SetAddressToObject([NotNull] AddressInputModel model, Guid objectId, [Required] string objectType)
        {
            Guid result = Guid.Empty;

            if (Guid.Empty.Equals(objectId)) throw new ArgumentNullException(nameof(objectId));

            if (string.IsNullOrEmpty(objectType)) throw new ArgumentNullException(nameof(objectType));

            try
            {
                System.ComponentModel.DataAnnotations.Validator.ValidateObject(model,
                    new System.ComponentModel.DataAnnotations.ValidationContext(model, null, null), true);

                StateDAL dbState = _stateRepository.GetStateByName(model.State);
                if (null == dbState)
                {
                    throw new EntityNotFoundException("State", "name", model.State);
                }

                var address = new AddressDAL();
                Map(model, address);

                var addressConnection = new AddressConnectionDAL
                {
                    ObjectId = objectId,
                    ObjectType = objectType
                };

                addressConnection.AddressId = _addressRepository.SaveAddress(address);
                result = _addressConnectionRepository.SaveAddressConnection(addressConnection);

                log.Info(string.Format("Set address (addressId: {0}) to object with id: {1}, and objectType: {2}",
                    addressConnection.AddressId, objectId, objectType));
            }
            catch (Exception exception) when (exception is EntityNotFoundException || exception is System.ComponentModel.DataAnnotations.ValidationException)
            {
                throw;
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("Address", "setToObject", exception);
            }

            return result;
        }

        /// <summary>
        /// Update object's address
        /// </summary>
        /// <param name="model">Address input model</param>
        /// <param name="objectId">Object id</param>
        /// <param name="objectType">Type of object</param>
        /// <returns>Id of updated address</returns>
        public Guid UpdateAddressToObject([NotNull] AddressInputModel model, Guid objectId, [Required] string objectType)
        {
            Guid result = Guid.Empty;

            if (Guid.Empty.Equals(objectId)) throw new ArgumentNullException(nameof(objectId));

            try
            {
                System.ComponentModel.DataAnnotations.Validator.ValidateObject(model,
                    new System.ComponentModel.DataAnnotations.ValidationContext(model, null, null), true);

                IEnumerable<AddressConnectionDAL> dbAddressConnections = _addressConnectionRepository.GetAddressConnectionsByObject(objectId, objectType);
                if (null == dbAddressConnections)
                {
                    throw new EntityNotFoundException("AddressConnections");
                }

                AddressConnectionDAL dbAddressConnection = dbAddressConnections.FirstOrDefault();
                if (null == dbAddressConnection)
                {
                    throw new EntityNotFoundException("AddressConnection");
                }

                AddressDAL dbAddress = dbAddressConnection.Address;
                if (null == dbAddress)
                {
                    throw new EntityNotFoundException("Address", "id", dbAddressConnection.AddressId.ToString());
                }

                StateDAL dbState = _stateRepository.GetStateByName(model.State);
                if (null == dbState)
                {
                    throw new EntityNotFoundException("State", "name", model.State);
                }

                if (!string.Equals(dbAddress.Street, model.Street) || !string.Equals(dbAddress.City, model.City) ||
                    !string.Equals(dbAddress.ZipCode, model.ZipCode) || !string.Equals(dbAddress.State, model.State))
                {
                    Map(model, dbAddress);

                    result = _addressRepository.SaveAddress(dbAddress);

                    log.Info(string.Format("Update address (addressId: {0}) to object with id: {1}, and objectType: {2}",
                       dbAddress.Id, objectId, objectType));
                }
            }
            catch (Exception exception) when (exception is EntityNotFoundException || exception is System.ComponentModel.DataAnnotations.ValidationException)
            {
                throw;
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("Address", "updateToObject", exception);
            }

            return result;
        }

        /// <summary>
        /// Create individual
        /// </summary>
        /// <param name="model">Idividual model</param>
        /// <returns>Id of inserted individual</returns>
        public Guid CreateIndividual([NotNull] IndividualInputModel model)
        {
            Guid result = Guid.Empty;

            try
            {
                System.ComponentModel.DataAnnotations.Validator.ValidateObject(model,
                    new System.ComponentModel.DataAnnotations.ValidationContext(model, null, null), true);

                var individual = new Individual();
                Map(model, individual);

                result = _individualRepository.SaveIndividual(individual);

                log.Info(string.Format("Create individual with id: {0}", result));
            }
            catch (Exception exception) when (exception is System.ComponentModel.DataAnnotations.ValidationException)
            {
                throw;
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("Individual", "create", exception);
            }

            return result;
        }

        /// <summary>
        /// Update individual
        /// </summary>
        /// <param name="model">Individual model</param>
        public void UpdateIndividual([NotNull] IndividualInputModel model)
        {
            try
            {
                System.ComponentModel.DataAnnotations.Validator.ValidateObject(model,
                    new System.ComponentModel.DataAnnotations.ValidationContext(model, null, null), true);

                Individual dbIndividual = _individualRepository.GetIndividualById(model.Id);
                if (null == dbIndividual)
                {
                    throw new EntityNotFoundException("Individual", "id", model.Id.ToString());
                }

                Map(model, dbIndividual);

                _individualRepository.SaveIndividual(dbIndividual);

                log.Info(string.Format("Update individual with id: {0}", model.Id));
            }
            catch (Exception exception) when (exception is EntityNotFoundException || exception is System.ComponentModel.DataAnnotations.ValidationException)
            {
                throw;
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("Individual", "update", exception);
            }
        }

        /// <summary>
        /// Get states for address setup
        /// </summary>
        /// <returns>List of allowed states</returns>
        public IEnumerable<State> GetStates()
        {
            IEnumerable<State> result = null;

            try
            {
                IEnumerable<StateDAL> states = _stateRepository.GetStates();
                result = Map(states);
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("States", "get", exception);
            }

            return result;
        }

        //BLL Model -> DAL Model
        private void Map([NotNull] PhoneInputModel model, [NotNull] PhoneDAL phoneDAL)
        {
            phoneDAL.PhoneNumber = model.Phone;
        }

        private void Map([NotNull] AddressInputModel model, [NotNull] AddressDAL addressDAL)
        {
            addressDAL.Street = model.Street;
            addressDAL.City = model.City;
            addressDAL.State = model.State;
            addressDAL.ZipCode = model.ZipCode;
        }

        private void Map([NotNull] IndividualInputModel model, [NotNull] Individual individualDAL)
        {
            individualDAL.FirstName = model.FirstName;
            individualDAL.LastName = model.LastName;
            individualDAL.Email = model.Email;
            individualDAL.DateBirth = model.DateBirth;
        }

        //DAL Model -> Domain Entity
        private IEnumerable<Phone> Map([NotNull] IEnumerable<PhoneConnectionDAL> phoneConnections)
        {
            return Map(phoneConnections.Select(x => x.Phone));
        }

        private IEnumerable<Phone> Map([NotNull] IEnumerable<PhoneDAL> phones)
        {
            foreach (var phoneDAL in phones)
            {
                var phone = new Phone();
                Map(phoneDAL, phone);

                yield return phone;
            }
        }

        private void Map([NotNull] PhoneDAL phoneDAL, [NotNull] Phone phone)
        {
            phone.Id = phoneDAL.Id;
            phone.PhoneNumber = phoneDAL.PhoneNumber;
        }

        private IEnumerable<Address> Map([NotNull] IEnumerable<AddressConnectionDAL> addressConnections)
        {
            return Map(addressConnections.Select(x => x.Address));
        }

        private IEnumerable<Address> Map([NotNull] IEnumerable<AddressDAL> addresses)
        {
            foreach (var addressDAL in addresses)
            {
                var address = new Address();
                Map(addressDAL, address);

                yield return address;
            }
        }

        private void Map([NotNull] AddressDAL addressDAL, [NotNull] Address address)
        {
            address.Id = addressDAL.Id;
            address.Street = addressDAL.Street;
            address.City = addressDAL.City;
            address.State = addressDAL.State;
            address.ZipCode = addressDAL.ZipCode;
        }

        private IEnumerable<State> Map([NotNull] IEnumerable<StateDAL> states)
        {
            foreach (var stateDAL in states)
            {
                State state = new State();
                Map(stateDAL, state);

                yield return state;
            }
        }

        private void Map([NotNull] StateDAL stateDAL, [NotNull] State state)
        {
            state.Name = stateDAL.Name;
        }
    }
}
