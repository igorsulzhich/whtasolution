﻿using System;
using System.Linq;
using System.Collections.Generic;
using PostSharp.Patterns.Contracts;
using log4net;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Models;
using WexHealth.BLL.Exceptions;
using WexHealth.BLL.Services.Common;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.DAL.Models;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.BLL.Services
{
    /// <summary>
    /// Manage administrator's users
    /// </summary>
    public class AdministratorUserService : UserService, IDomainUserService, ILoginUserService
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(AdministratorUserService));

        private const string _loginType = "Administrator";
        private readonly Guid _administratorId;

        private readonly IAdministratorRepository _administratorRepository;
        private readonly IAdministratorUserRepository _administratorUserRepository;

        protected override Guid SecurityDomainId
        {
            get
            {
                Guid result = Guid.Empty;

                AdministratorDAL dbAdministrator = _administratorRepository.GetAdministratorById(_administratorId);
                result = dbAdministrator.SecurityDomainId;

                return result;
            }
        }

        protected override string LoginType
        {
            get
            {
                return _loginType;
            }
        }

        public AdministratorUserService(IUserServiceContext context):
            base(context)
        { }

        public AdministratorUserService(Guid administratorId, IAdministratorRepository administratorRepository,
            IAdministratorUserRepository administratorUserRepository, IUserServiceContext context) :
            base(context)
        {
            _administratorRepository = administratorRepository;
            _administratorUserRepository = administratorUserRepository;

            _administratorId = administratorId;
        }

        /// <summary>
        /// Vadlidate if current administrator exist
        /// </summary>
        protected override void ValidateDomainRelation()
        {
            AdministratorDAL dbAdministrator = _administratorRepository.GetAdministratorById(_administratorId);
            if (null == dbAdministrator)
            {
                throw new EntityNotFoundException("Administrator", "id", _administratorId.ToString());
            }
        }

        /// <summary>
        /// Validate user relation to current administrator
        /// </summary>
        /// <param name="user">User for validation</param>
        protected override void ValidateUserRelation([NotNull] User user)
        {
            AdministratorUserDAL dbAdministratorUser = _administratorUserRepository.GetUser(user.Id, _administratorId);
            if (null == dbAdministratorUser)
            {
                throw new InvalidOperationException($"User with id: {user.Id}, doesn't relate to administrator with id: {_administratorId}");
            }
        }

        /// <summary>
        /// Get all users of current administrator
        /// </summary>
        /// <returns>List of users</returns>
        protected override IEnumerable<User> GetDomainUsers()
        {
            return _administratorUserRepository.GetUsers(_administratorId).Select(x => x.User);
        }

        /// <summary>
        /// Create new user and connect him with current administrator
        /// </summary>
        /// <param name="model">User input model</param>
        /// <returns>Id of inserted user</returns>
        public override Guid CreateUser([NotNull] UserInputModel model, Guid individualId)
        {
            if (Guid.Empty.Equals(individualId)) throw new ArgumentNullException(nameof(individualId));

            Guid result = base.CreateUser(model, individualId);

            try
            {
                var administratorUser = new AdministratorUserDAL
                {
                    UserId = result,
                    AdministratorId = _administratorId
                };

                _administratorUserRepository.SaveUser(administratorUser);
            }
            catch (Exception exception)
            {
                _log.Error("BLL error", exception);
                throw new EntityOperationFailedException("AdministratorUser", "create", exception);
            }

            return result;
        }
    }
}
