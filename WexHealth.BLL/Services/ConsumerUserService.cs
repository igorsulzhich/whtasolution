﻿using System;
using System.Collections.Generic;
using PostSharp.Patterns.Contracts;
using log4net;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Exceptions;
using WexHealth.BLL.Services.Common;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.DAL.Models;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.BLL.Services
{
    public class ConsumerUserService : UserService, IDomainUserService, ILoginUserService
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(ConsumerUserService));

        private const string _loginType = "Consumer";
        private readonly Guid _consumerId;
        private readonly Guid _employerId;
        private readonly Guid _administratorId;

        private readonly IConsumerRepository _consumerRepository;
        private readonly IEmployerRepository _employerRepository;
        private readonly IAdministratorRepository _administratorRepository;
        private readonly IUserRepository _userRepository;

        protected override Guid SecurityDomainId
        {
            get
            {
                Guid result = Guid.Empty;

                AdministratorDAL dbAdministrator = _administratorRepository.GetAdministratorById(_administratorId);
                result = dbAdministrator.SecurityDomainId;

                return result;
            }
        }

        protected override string LoginType
        {
            get
            {
                return _loginType;
            }
        }

        public ConsumerUserService(IUserServiceContext context)
            : base(context)
        { }

        public ConsumerUserService(Guid consumerId, Guid employerId, Guid administratorId, IConsumerRepository consumerRepository,
            IEmployerRepository employerRepository, IAdministratorRepository administratorRepository, IUserRepository userRepository,
            IUserServiceContext context) :
            base(context)
        {
            _consumerRepository = consumerRepository;
            _employerRepository = employerRepository;
            _administratorRepository = administratorRepository;
            _userRepository = userRepository;

            _consumerId = consumerId;
            _employerId = employerId;
            _administratorId = administratorId;
        }

        /// <summary>
        /// Validate if current administrator, employer and consumer exist and have relationship
        /// </summary>
        protected override void ValidateDomainRelation()
        {
            ConsumerDAL dbConsumer = _consumerRepository.GetConsumerById(_consumerId);
            if (null == dbConsumer)
            {
                throw new EntityNotFoundException("Consumer", "id", _consumerId.ToString());
            }

            EmployerDAL dbEmployer = _employerRepository.GetEmployerById(_employerId);
            if (null == dbEmployer)
            {
                throw new EntityNotFoundException("Employer", "id", _employerId.ToString());
            }

            if (!dbEmployer.Id.Equals(dbConsumer.EmployerId))
            {
                throw new Exception($"Consumer with id: {_consumerId}, doesn't relate to employer with id: {_employerId}");
            }

            AdministratorDAL dbAdministrator = _administratorRepository.GetAdministratorById(_administratorId);
            if (null == dbAdministrator)
            {
                throw new EntityNotFoundException("Administrator", "id", _administratorId.ToString());
            }

            if (!dbAdministrator.Id.Equals(dbEmployer.AdministratorId))
            {
                throw new Exception($"Employer with id: {_employerId}, doesn't relate to administrator with id: {_administratorId}");
            }
        }

        /// <summary>
        /// Validate user relation to current consumer
        /// </summary>
        /// <param name="id">User id</param>
        protected override void ValidateUserRelation([NotNull] User user)
        {
            ConsumerDAL dbConsumer = _consumerRepository.GetConsumerById(_consumerId);
            if (!dbConsumer.IndividualId.Equals(user.IndividualId))
            {
                throw new InvalidOperationException($"User with id: {user.Id}, doesn't relate to consumer with id: {_consumerId}");
            }
        }

        /// <summary>
        /// Get all users of current consumer. Note that consumer can have only zero/one user
        /// </summary>
        /// <returns>List of users consists of one user</returns>
        protected override IEnumerable<User> GetDomainUsers()
        {
            User result = null;

            ConsumerDAL dbConsumer = _consumerRepository.GetConsumerById(_consumerId);
            if (null != dbConsumer)
            {
                result = _userRepository.GetUserByIndividualId(dbConsumer.IndividualId);
            }

            yield return result;
        }
    }
}

