﻿using System;
using System.Linq;
using System.Collections.Generic;
using PostSharp.Patterns.Contracts;
using log4net;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Models;
using WexHealth.BLL.Exceptions;
using WexHealth.BLL.Services.Common;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.DAL.Models;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.BLL.Services
{
    /// <summary>
    /// Manage employer's users
    /// </summary>
    public class EmployerUserService : UserService, IDomainUserService, ILoginUserService
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(EmployerUserService));

        private const string _loginType = "Employer";
        private readonly Guid _employerId;
        private readonly Guid _administratorId;

        private readonly IEmployerRepository _employerRepository;
        private readonly IEmployerUserRepository _employerUserRepository;
        private readonly IAdministratorRepository _administratorRepository;
        private readonly IAdministratorUserRepository _administratorUserRepository;

        protected override Guid SecurityDomainId
        {
            get
            {
                Guid result = Guid.Empty;

                AdministratorDAL dbAdministrator = _administratorRepository.GetAdministratorById(_administratorId);
                result = dbAdministrator.SecurityDomainId;

                return result;
            }
        }

        protected override string LoginType
        {
            get
            {
                return _loginType;
            }
        }

        public EmployerUserService(IUserServiceContext context) :
            base(context)
        { }

        public EmployerUserService([Required] string employerCode, Guid administratorUserId, IEmployerRepository employerRepository,
            IEmployerUserRepository employerUserRepository, IAdministratorRepository administratorRepository,
            IAdministratorUserRepository administratorUserRepository, IUserServiceContext context) :
            base(context)
        {
            _employerRepository = employerRepository;
            _employerUserRepository = employerUserRepository;
            _administratorRepository = administratorRepository;
            _administratorUserRepository = administratorUserRepository;

            AdministratorUserDAL dbAdministratorUser = _administratorUserRepository.GetUser(administratorUserId);
            if (null == dbAdministratorUser)
            {
                throw new EntityNotFoundException("AdministratorUser", "userId", administratorUserId.ToString());
            }

            _administratorId = dbAdministratorUser.AdministratorId;

            EmployerDAL dbEmployer = _employerRepository.GetEmployerByCode(employerCode);
            if (null == dbEmployer)
            {
                throw new EntityNotFoundException("Employer", "code", employerCode);
            }

            _employerId = dbEmployer.Id;
        }

        /// <summary>
        /// Validate if current administrator and employer exist and have relationship
        /// </summary>
        protected override void ValidateDomainRelation()
        {
            EmployerDAL dbEmployer = _employerRepository.GetEmployerById(_employerId);
            if (null == dbEmployer)
            {
                throw new EntityNotFoundException("Employer", "id", _employerId.ToString());
            }

            AdministratorDAL dbAdministrator = _administratorRepository.GetAdministratorById(_administratorId);
            if (null == dbAdministrator)
            {
                throw new EntityNotFoundException("Administrator", "id", _administratorId.ToString());
            }

            if (!dbEmployer.AdministratorId.Equals(_administratorId))
            {
                throw new Exception($"Employer with id: {_employerId}, doesn't relate to administrator with id: {_administratorId}");
            }
        }

        /// <summary>
        /// Validate if user relates to current employer
        /// </summary>
        /// <param name="id">User id</param>
        protected override void ValidateUserRelation([NotNull] User user)
        {
            EmployerUserDAL dbEmployerUser = _employerUserRepository.GetUser(user.Id, _employerId);
            if (null == dbEmployerUser)
            {
                throw new InvalidOperationException($"User with id: {user.Id}, doesn't relate to employer with id: {_employerId}");
            }
        }

        /// <summary>
        /// Get all users of current employer
        /// </summary>
        /// <returns>List of users</returns>
        protected override IEnumerable<User> GetDomainUsers()
        {
            return _employerUserRepository.GetUsers(_employerId).Select(x => x.User);
        }

        /// <summary>
        /// Create user and connect him to current employer
        /// </summary>
        /// <param name="model">Employer user input model</param>
        /// <returns>Id of inserted user</returns>
        public override Guid CreateUser([NotNull] UserInputModel model, Guid individualId)
        {
            if (Guid.Empty.Equals(individualId)) throw new ArgumentNullException(nameof(individualId));

            Guid result = base.CreateUser(model, individualId);

            try
            {
                var employerUser = new EmployerUserDAL
                {
                    UserId = result,
                    EmployerId = _employerId
                };

                _employerUserRepository.SaveUser(employerUser);
            }
            catch (Exception exception)
            {
                _log.Error("BLL error", exception);
                throw new EntityOperationFailedException("EmployerUser", "create", exception);
            }

            return result;
        }
    }
}
