﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WexHealth.BLL.Exceptions;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.DAL.Models;
using WexHealth.DAL.Repositories.Common.Interfaces;
using WexHealth.Domain.Entities;

namespace WexHealth.BLL.Services.Common
{
    public abstract class ReportServiceBase : IReportServiceBase
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ReportServiceBase));

        private readonly IAdministratorService _administratorService;
        private readonly IObjectFieldRepository _objectFieldRepository;
        private readonly IReportRequestRepository _reportRequestRepository;
        private readonly IReportRequestStatusRepository _reportRequestStatusRepository;
        private readonly IReportFieldsRepository _reportFieldsRepository;
        private readonly IEmployerRepository _employerRepository;

        private Guid _employerId;
        private Administrator _administrator;
        private string _loginType;

        protected abstract string ObjectType { get; }

        protected ReportServiceBase(IObjectFieldRepository objectFieldRepository, IReportRequestRepository reportRequestRepository,
            IReportRequestStatusRepository reportRequestStatusRepository, IReportFieldsRepository reportFieldsRepository,
            IAdministratorService administratorService, IEmployerRepository employerRepository)
        {
            _objectFieldRepository = objectFieldRepository;
            _reportRequestRepository = reportRequestRepository;
            _reportRequestStatusRepository = reportRequestStatusRepository;
            _administratorService = administratorService;
            _reportFieldsRepository = reportFieldsRepository;
            _employerRepository = employerRepository;
        }

        public void InitService(User user)
        {
            if (null == user) throw new ArgumentNullException(nameof(user));

            _loginType = user.LoginType;

            if (_loginType.Equals("Administrator"))
            {
                _administrator = _administratorService.GetAdministratorByUser(user);
            }
        }

        public IEnumerable<ObjectField> GetObjectFields()
        {
            IEnumerable<ObjectField> result = null;
            try
            {
                result = _objectFieldRepository.GetObjectFields(ObjectType);
                log.Info($"Get object fields with type: {ObjectType}");
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("ReportServiceBase", "GetObjectFields", exception);
            }

            return result;
        }

        public void SetObjectFields(IEnumerable<ObjectField> checkedFields, int reportId)
        {
            if (null == checkedFields) throw new ArgumentNullException(nameof(checkedFields));

            if (reportId <= 0) throw new ArgumentOutOfRangeException(nameof(reportId));

            try
            {
                foreach (var item in checkedFields)
                {
                    ReportField reportField = new ReportField
                    {
                        ObjectFieldsId = item.Id,
                        ReportRequestId = reportId
                    };

                    _reportFieldsRepository.SaveReportFields(reportField);
                }

                log.Info($"Set object fields with type: {ObjectType}");
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("ReportServiceBase", "SetObjectFields", exception);
            }
        }

        public IEnumerable<ReportField> GetReportFieldsByRequestId(int requestId)
        {
            if (requestId == 0) throw new ArgumentNullException(nameof(requestId));

            IEnumerable<ReportField> result = null;

            try
            {
                result = _reportFieldsRepository.GetReportFieldByRequestId(requestId);
                log.Info($"Get fields for request with id: {requestId}");
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("ReportServiceBase", "GetReportFieldsByRequestId", exception);
            }

            return result;
        }

        public int CreateReportRequest(Guid employerId)
        {
            int result = 0;

            try
            {
                ReportRequestStatus status = _reportRequestStatusRepository.GetStatusByName(ReportStatus.Requested.ToString());
                if (null == status)
                {
                    throw new EntityNotFoundException("ReportRequestStatus", "name", ReportStatus.Requested.ToString());
                }

                ReportRequest request = new ReportRequest
                {
                    DateOfRequest = DateTime.Now,
                    AdministratorId = _administrator.Id,
                    RequestedBy = _administrator.Alias + "/" + _administrator.Name,
                    FormatReport = "CSV",
                    RequestType = ObjectType,
                    StatusId = status.Id
                };

                if (!Guid.Empty.Equals(employerId))
                {
                    EmployerDAL dbEmployer = _employerRepository.GetEmployerById(employerId);
                    if (null == dbEmployer)
                    {
                        throw new EntityNotFoundException("EmployerDAL", "Id", dbEmployer.ToString());
                    }

                    if (!_administrator.Id.Equals(dbEmployer.AdministratorId))
                    {
                        throw new InvalidOperationException("Employer doesn't related to current administrator");
                    }

                    request.EmployerId = employerId;
                }

                result = _reportRequestRepository.SaveRequest(request);

                log.Info($"Create report request with id: {result}");
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("ReportServiceBase", "CreateReportRequest", exception);
            }

            return result;
        }

        public bool RejectRequest(int id, ReportStatus status)
        {
            bool result = false;

            try
            {
                ReportRequest request = _reportRequestRepository.GetRequestById(id);
                if (null == request)
                {
                    throw new EntityNotFoundException("ReportRequestStatus", "id", id.ToString());
                }

                if (request.Status.Name == ReportStatus.Requested.ToString())
                {
                    ChangeStatus(request, status);
                    log.Info($"The report request with id: {request.Id} cancelled");

                    result = true;
                }
                else
                {
                    log.Error($"This request with id: {id} cannot be cancelled. Status: {request.Status.Name}");
                    throw new InvalidOperationException($"This request cannot be cancelled. Status: {request.Status.Name}");
                }
            }
            catch (InvalidOperationException)
            {
                throw;
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("ReportServiceBase", "RejectRequest", exception);
            }

            return result;
        }

        public bool RemoveRequest(int id)
        {
            bool result = false;

            try
            {
                ReportRequest request = _reportRequestRepository.GetRequestById(id);
                if (null == request)
                {
                    throw new EntityNotFoundException("ReportRequestStatus", "id", id.ToString());
                }

                if (request.Status.Name == ReportStatus.Rejected.ToString() || request.Status.Name == ReportStatus.Completed.ToString())
                {
                    _reportFieldsRepository.DeleteReportFieldByRequest(request.Id);

                    if (!string.IsNullOrEmpty(request.FilePath) && File.Exists(request.FilePath))
                    {
                        File.Delete(request.FilePath);
                    }

                    _reportRequestRepository.DeleteRequest(request.Id);
                    log.Info($"The report request with id: {request.Id} removed");

                    result = true;
                }
                else
                {
                    log.Error($"This request with id: {id} cannot be removed. Status: {request.Status.Name}");
                    throw new InvalidOperationException($"This request cannot be removed. Status: {request.Status.Name}");
                }
            }
            catch (InvalidOperationException)
            {
                throw;
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("ReportServiceBase", "RemoveRequest", exception);
            }

            return result;
        }

        private void ChangeStatus(ReportRequest request, ReportStatus status)
        {
            if (null == request) throw new ArgumentOutOfRangeException(nameof(request));

            int result = 0;

            try
            {
                ReportRequestStatus dbStatus = _reportRequestStatusRepository.GetStatusByName(status.ToString());
                if (null == dbStatus)
                {
                    throw new EntityNotFoundException("ReportRequestStatus", "name", "Requested");
                }

                request.StatusId = dbStatus.Id;

                result = _reportRequestRepository.SaveRequest(request);

                if (result > 0)
                {
                    log.Info($"Updated status for the report request with id: {result}");
                }
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("ReportServiceBase", "GetObjectFields", exception);
            }
        }

        public ReportRequest GetFirstReportRequest()
        {
            ReportRequest result = null;

            try
            {
                ReportRequestStatus status = _reportRequestStatusRepository.GetStatusByName(ReportStatus.Requested.ToString());
                if (null == status)
                {
                    throw new EntityNotFoundException("ReportRequestStatus", "name", ReportStatus.Requested.ToString());
                }

                result = _reportRequestRepository.GetRequestFirstStatus(status.Id);

                log.Info($"Get report requests with status: {status.Name} and object type: {ObjectType}");
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("ReportServiceBase", "GetFirstReportRequest", exception);
            }

            return result;
        }

        public IEnumerable<ReportRequest> GetReportRequests()
        {
            IEnumerable<ReportRequest> result = null;

            try
            {
                ReportRequestStatus status = _reportRequestStatusRepository.GetStatusByName(ReportStatus.Requested.ToString());
                if (null == status)
                {
                    throw new EntityNotFoundException("ReportRequestStatus", "name", ReportStatus.Requested.ToString());
                }

                result = _reportRequestRepository.GetRequestByAdministrator(_administrator.Id, ObjectType);

                log.Info($"Get report requests with status: {status.Name} and object type: {ObjectType}");
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("ReportServiceBase", "GetReportRequests", exception);
            }

            return result;
        }
    }
}
