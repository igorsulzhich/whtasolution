﻿using System;
using System.Linq;
using System.Collections.Generic;
using PostSharp.Patterns.Contracts;
using log4net;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Models;
using WexHealth.BLL.Exceptions;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.DAL.Models;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.BLL.Services.Common
{
    /// <summary>
    /// Types of domain levels ordered by descending pripority
    /// </summary>
    public enum LevelType
    {
        TPA, Administrator, Employer, Consumer
    };

    /// <summary>
    /// Manage settings
    /// </summary>
    public abstract class SettingServiceBase : ISettingServiceBase
    {
        public interface ISettingServiceContext
        {
            ISettingRepository SettingRepository { get; }

            ISettingConnectionRepository SettingConnectionRepository { get; }
        }

        private static readonly ILog log = LogManager.GetLogger(typeof(SettingServiceBase));

        private readonly ISettingServiceContext _context;

        protected abstract LevelType ObjectType { get; }

        protected abstract IEnumerable<KeyValuePair<Guid, string>> ObjectHierarchy { get; }

        protected SettingServiceBase(ISettingServiceContext context)
        {
            _context = context;
        }

        public void FillSettings([NotNull] IEnumerable<Setting> settings, Guid objectId, bool isOverride)
        {
            try
            {
                IEnumerable<SettingConnectionDAL> dbSettingConnections =
                    _context.SettingConnectionRepository.GetSettingConnectionsByObject(objectId, ObjectType.ToString());

                foreach (var i in settings)
                {
                    SettingConnectionDAL dbSettingConnection = dbSettingConnections.Where(x => x.SettingId.Equals(i.Id)).SingleOrDefault();
                    if (null != dbSettingConnection)
                    {
                        var buffSetting = new Setting();
                        Map(dbSettingConnection, buffSetting);

                        if (buffSetting.Id.Equals(i.Id))
                        {
                            i.isAllowed = buffSetting.isAllowed;
                        }
                    }
                    else
                    {
                        if (isOverride)
                        {
                            i.isAllowed = GetOverrideSetting(i.Id, ObjectHierarchy.Skip(1));
                        }
                        else
                        {
                            i.isAllowed = null;
                        }
                    }
                }

                //log.Info($"Fill setting for object with id: {objectId}, and type: {ObjectType}");
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("Settings", "getObjectSettings", exception);
            }
        }

        /// <summary>
        /// Set object settings
        /// </summary>
        /// <param name="models">Setting input models</param>
        /// <param name="objectId">Object id</param>
        /// <returns>Ids of inserted settings</returns>
        public IEnumerable<Guid> SetObjectSettings(IEnumerable<SettingInputModel> models, Guid objectId)
        {
            var result = new List<Guid>();

            if (Guid.Empty.Equals(objectId)) throw new ArgumentNullException(nameof(objectId));

            try
            {
                foreach (var model in models.Where(x => x.isAllowed.HasValue))
                {
                    Guid settingId = SetObjectSetting(model, objectId);
                    result.Add(settingId);
                }

                log.Info($"Set settings for object with id: {objectId}, and type: {ObjectType}");
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("Settings", "setObjectSettings", exception);
            }

            return result;
        }

        /// <summary>
        /// Set object settings
        /// </summary>
        /// <param name="model">Setting input model</param>
        /// <param name="objectId">Object id</param>
        /// <returns>Id of inserted setting</returns>
        private Guid SetObjectSetting([NotNull] SettingInputModel model, Guid objectId)
        {
            Guid result = Guid.Empty;

            if (Guid.Empty.Equals(objectId)) throw new ArgumentNullException(nameof(objectId));

            try
            {
                if (model.isAllowed.HasValue)
                {

                    SettingConnectionDAL dbSettingConnection =
                        _context.SettingConnectionRepository.GetSettingConnectionByObjectSetting(objectId, ObjectType.ToString(), model.Id);
                    if (null != dbSettingConnection)
                    {
                        throw new EntityAlreadyExistsException("SettingConnection");
                    }

                    var settingConnection = new SettingConnectionDAL();
                    Map(model, objectId, ObjectType.ToString(), settingConnection);

                    result = _context.SettingConnectionRepository.SaveSettingConnection(settingConnection);
                }
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("Setting", "setObjectSetting", exception);
            }

            return result;
        }

        /// <summary>
        /// Update object settings
        /// </summary>
        /// <param name="models">Setting input model</param>
        /// <param name="objectId">Object id</param>
        public void UpdateObjectSettings([NotNull] IEnumerable<SettingInputModel> models, Guid objectId)
        {
            if (Guid.Empty.Equals(objectId)) throw new ArgumentNullException(nameof(objectId));

            try
            {
                foreach (var model in models)
                {
                    UpdateObjectSetting(model, objectId);
                }

                log.Info($"Update settings for object with id: {objectId}, and type: {ObjectType}");
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("Settings", "updateObjectSettings", exception);
            }
        }

        /// <summary>
        /// Update object setting
        /// </summary>
        /// <param name="model">Setting input model</param>
        /// <param name="objectId">Object id</param>
        private void UpdateObjectSetting([NotNull] SettingInputModel model, Guid objectId)
        {
            if (Guid.Empty.Equals(objectId)) throw new ArgumentNullException(nameof(objectId));

            try
            {
                SettingConnectionDAL dbSettingConnection = _context.SettingConnectionRepository.GetSettingConnectionByObjectSetting(objectId, ObjectType.ToString(), model.Id);

                if (null == dbSettingConnection)
                {
                    dbSettingConnection = new SettingConnectionDAL();
                }

                if (model.isAllowed.HasValue)
                {
                    Map(model, objectId, ObjectType.ToString(), dbSettingConnection);

                    _context.SettingConnectionRepository.SaveSettingConnection(dbSettingConnection);
                }
                else
                {
                    if (!Guid.Empty.Equals(dbSettingConnection.Id))
                    {
                        _context.SettingConnectionRepository.DeleteSettingConnection(dbSettingConnection.Id);
                    }
                }
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("Setting", "updateObjectSetting", exception);
            }
        }

        protected virtual bool GetOverrideSetting(Guid settingId, IEnumerable<KeyValuePair<Guid, string>> hierarchy)
        {
            bool result = false;

            SettingConnectionDAL dbSettingConnection = null;

            SettingDAL dbSetting = _context.SettingRepository.GetSettingById(settingId);
            if (null == dbSetting)
            {
                throw new EntityNotFoundException("Setting", "id", settingId.ToString());
            }

            foreach (var i in hierarchy)
            {
                dbSettingConnection =
                    _context.SettingConnectionRepository.GetSettingConnectionByObjectSetting(i.Key, i.Value, settingId);

                if (null != dbSettingConnection)
                {
                    break;
                }
            }

            var setting = new Setting();
            Map(dbSettingConnection, setting);

            if (setting.isAllowed.HasValue)
            {
                result = setting.isAllowed.Value;
            }

            return result;
        }

        //DAL Model -> Domain Entity
        private IEnumerable<Setting> Map([NotNull] IEnumerable<SettingConnectionDAL> settingConnections)
        {
            foreach (var settingConnectionDAL in settingConnections)
            {
                var setting = new Setting();
                Map(settingConnectionDAL, setting);

                yield return setting;
            }
        }

        protected void Map([NotNull] SettingConnectionDAL settingConnectionDAL, [NotNull] Setting setting)
        {
            bool result;

            if (Boolean.TryParse(settingConnectionDAL.Value, out result))
            {
                setting.isAllowed = result;
            }
            else
            {
                setting.isAllowed = null;
            }

            if (null != settingConnectionDAL.Setting)
            {
                Map(settingConnectionDAL.Setting, setting);
            }
        }

        protected IEnumerable<Setting> Map([NotNull] IEnumerable<SettingDAL> settings)
        {
            foreach (var settingDAL in settings)
            {
                var setting = new Setting();
                Map(settingDAL, setting);

                yield return setting;
            }
        }

        private void Map([NotNull] SettingDAL settingDAL, [NotNull] Setting setting)
        {
            setting.Id = settingDAL.Id;
            setting.Name = settingDAL.Key;
        }

        //BLL Model -> DAL Model
        private void Map([NotNull] SettingInputModel model, Guid objectId, string ObjectType, [NotNull] SettingConnectionDAL settingConnectionDAL)
        {
            if (Guid.Empty.Equals(objectId)) throw new ArgumentNullException(nameof(objectId));

            SettingDAL dbSetting = _context.SettingRepository.GetSettingById(model.Id);
            if (null == dbSetting)
            {
                throw new EntityNotFoundException("Setting", "id", model.Id.ToString());
            }

            if (Guid.Empty.Equals(settingConnectionDAL.Id))
            {
                settingConnectionDAL.ObjectId = objectId;
                settingConnectionDAL.ObjectType = ObjectType;
                settingConnectionDAL.SettingId = dbSetting.Id;
            }

            if (model.isAllowed.HasValue)
            {
                settingConnectionDAL.Value = model.isAllowed.Value.ToString();
            }
        }
    }
}
