﻿using System;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using WexHealth.BLL.Services.Common.Interfaces;

namespace WexHealth.BLL.Services.Common
{
    public abstract class UserServiceBase : IUserServiceBase
    {
        protected const int passwordSaltLength = 32;

        /// <summary>
        /// Encrypt user password using salt
        /// </summary>
        /// <param name="password">User typed password</param>
        /// <param name="passwordSalt">Generated salt</param>
        /// <returns></returns>
        public string EncryptPassword(string password, string passwordSalt)
        {
            if (string.IsNullOrEmpty(password)) throw new ArgumentNullException(nameof(password));

            if (string.IsNullOrEmpty(passwordSalt)) throw new ArgumentNullException(nameof(passwordSalt));

            byte[] passwordBytes = Encoding.ASCII.GetBytes(password);
            byte[] passwordSaltBytes = GetBytesFromHex(passwordSalt);

            byte[] encryptedPasswordBytes = EncryptPassword(passwordBytes, passwordSaltBytes);
            string encryptedPassword = GetHexFromBytes(encryptedPasswordBytes);

            return encryptedPassword;
        }

        /// <summary>
        /// Encrypt user password using salt
        /// </summary>
        /// <param name="password">User typed password converted to bytes array</param>
        /// <param name="passwordSalt">Generated 32-bytes salt</param>
        /// <returns></returns>
        private byte[] EncryptPassword(byte[] password, byte[] passwordSalt)
        {
            if (null == password) throw new ArgumentNullException(nameof(password));

            if (null == passwordSalt) throw new ArgumentNullException(nameof(passwordSalt));

            var sha256 = SHA256Managed.Create();

            byte[] saltedPassword = SaltPassword(password, passwordSalt);
            byte[] encryptedPassword = sha256.ComputeHash(saltedPassword);

            return encryptedPassword;
        }

        /// <summary>
        /// Generate new password salt
        /// </summary>
        /// <returns></returns>
        public string GeneratePasswordSalt()
        {
            byte[] passwordSaltBytes = new byte[passwordSaltLength];

            using (var provider = new RNGCryptoServiceProvider())
            {
                provider.GetNonZeroBytes(passwordSaltBytes);
            }

            string passwordSalt = GetHexFromBytes(passwordSaltBytes);

            return passwordSalt;
        }

        /// <summary>
        /// Combine password with generated salt
        /// </summary>
        /// <param name="byteList">User password and 32-bytes salt</param>
        /// <returns></returns>
        private byte[] SaltPassword(params byte[][] byteList)
        {
            byte[] result = new byte[byteList.Sum(x => x.Length)];
            int offset = 0;

            foreach (byte[] array in byteList)
            {
                Buffer.BlockCopy(array, 0, result, offset, array.Length);
                offset += array.Length;
            }

            return result;
        }

        /// <summary>
        /// Convert bytes array to HEX format
        /// </summary>
        /// <param name="bytes">Bytes array</param>
        /// <returns></returns>
        private string GetHexFromBytes(byte[] bytes)
        {
            if (null == bytes) throw new ArgumentNullException(nameof(bytes));

            var stringBuilder = new StringBuilder(bytes.Length * 2);

            foreach (byte i in bytes)
            {
                stringBuilder.AppendFormat("{0:x2}", i);
            }

            return stringBuilder.ToString();
        }

        /// <summary>
        /// Convert string in HEX format to bytes array
        /// </summary>
        /// <param name="hex">String in HEX format</param>
        /// <returns></returns>
        private byte[] GetBytesFromHex(string hex)
        {
            if (string.IsNullOrEmpty(hex)) throw new ArgumentNullException(nameof(hex));

            byte[] result = new byte[hex.Length / 2];

            for (int i = 0; i < hex.Length; i += 2)
            {
                result[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            }

            return result;
        }

        /// <summary>
        /// Generate password for new user
        /// </summary>
        /// <param name="length">Password legth</param>
        /// <returns></returns>
        public virtual string GeneratePassword(int length)
        {
            if (length < 0) throw new ArgumentOutOfRangeException(nameof(length));

            string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
            var chars = new char[length];

            Random random = new Random();

            for (int i = 0; i < length; i++)
            {
                chars[i] = allowedChars[random.Next(0, allowedChars.Length)];
            }

            return new string(chars);
        }
    }
}
