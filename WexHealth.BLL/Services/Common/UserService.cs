﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using log4net;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Models;
using WexHealth.BLL.Exceptions;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.DAL.Models;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.BLL.Services.Common
{
    /// <summary>
    /// Manage users
    /// </summary>
    public abstract class UserService : UserServiceBase, IUserService
    {
        public interface IUserServiceContext
        {
            IDomainService DomainService { get; }

            IUserRepository UserRepository { get; }

            ISecurityDomainRepository SecurityDomainRepository { get; }

            IIndividualRepository IndividualRepository { get; }
        }

        private static readonly ILog _log = LogManager.GetLogger(typeof(UserService));

        private const int _generatedPasswordLegth = 8;

        private readonly IUserServiceContext _context;

        protected abstract Guid SecurityDomainId { get; }

        protected abstract string LoginType { get; }

        public UserService(IUserServiceContext context)
        {
            _context = context;
        }

        protected abstract void ValidateDomainRelation();

        protected abstract void ValidateUserRelation(User user);

        /// <summary>
        /// Get user by id with relation validation
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns>User entity</returns>
        public virtual User GetUser(Guid id)
        {
            User result = null;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            try
            {
                ValidateDomainRelation();

                result = _context.UserRepository.GetUserById(id);

                ValidateUserRelation(result);
            }
            catch (Exception exception)
            {
                _log.Error("BLL error", exception);
                throw new EntityOperationFailedException("User", "get", exception);
            }

            return result;
        }

        protected abstract IEnumerable<User> GetDomainUsers();

        public virtual IEnumerable<User> GetUsers()
        {
            IEnumerable<User> result = null;

            try
            {
                ValidateDomainRelation();

                result = GetDomainUsers();
            }
            catch (Exception exception)
            {
                _log.Error("BLL error", exception);
                throw new EntityOperationFailedException("Users", "get", exception);
            }

            return result;
        }

        /// <summary>
        /// Create new user
        /// </summary>
        /// <param name="model">Input model of user</param>
        /// <returns>Id of inserted user</returns>
        public virtual Guid CreateUser(UserInputModel model, Guid individualId)
        {
            Guid result = Guid.Empty;

            if (null == model) throw new ArgumentNullException(nameof(model));

            if (Guid.Empty.Equals(individualId)) throw new ArgumentNullException(nameof(individualId));

            try
            {
                ValidateDomainRelation();

                Validator.ValidateObject(model, new ValidationContext(model, null, null), true);

                string passwordSalt = GeneratePasswordSalt();
                string encryptedPassword = EncryptPassword(model.Password, passwordSalt);

                User dbUser = _context.UserRepository.GetUserByUserName(model.UserName);
                if (null != dbUser)
                {
                    throw new EntityAlreadyExistsException("User", "username", model.UserName);
                }

                SecurityDomainDAL dbSecurityDomain = _context.SecurityDomainRepository.GetSecurityDomainById(SecurityDomainId);
                if (null == dbSecurityDomain)
                {
                    throw new EntityNotFoundException("Security domain", "id", SecurityDomainId.ToString());
                }

                var user = new User
                {
                    PasswordHash = encryptedPassword,
                    PasswordSalt = passwordSalt,
                    LoginType = LoginType,
                    SecurityDomainId = SecurityDomainId,
                    IndividualId = individualId
                };
                Map(model, user);

                result = _context.UserRepository.SaveUser(user);

                _log.Info(string.Format("Create new user: {0} (id: {1}, securityDomaiId: {2})", model.UserName, result, SecurityDomainId));
            }
            catch (Exception exception) when (exception is EntityNotFoundException || exception is EntityAlreadyExistsException || exception is ValidationException)
            {
                throw;
            }
            catch (Exception exception)
            {
                _log.Error("BLL error", exception);
                throw new EntityOperationFailedException("User", "create", exception);
            }

            return result;
        }

        /// <summary>
        /// Update user
        /// </summary>
        /// <param name="model">Input model of user</param>
        public void UpdateUser(UserInputModel model)
        {
            if (null == model) throw new ArgumentNullException(nameof(model));

            try
            {
                ValidateDomainRelation();

                Validator.ValidateObject(model, new ValidationContext(model, null, null));

                User dbUser = _context.UserRepository.GetUserById(model.Id);
                if (null == dbUser)
                {
                    throw new EntityNotFoundException("User", "id", model.Id.ToString());
                }

                ValidateUserRelation(dbUser);

                if (!dbUser.UserName.Equals(model.UserName))
                {
                    User dbUserByUsername = _context.UserRepository.GetUserByUserName(model.UserName);
                    if (null != dbUserByUsername)
                    {
                        throw new EntityAlreadyExistsException("User", "name", model.UserName);
                    }
                }

                if (!string.IsNullOrEmpty(model.Password))
                {
                    string passwordHash = null, passwordSalt = null;
                    _context.UserRepository.GetPasswordHash(dbUser.UserName, ref passwordHash, ref passwordSalt);
                    if (!string.IsNullOrEmpty(passwordSalt))
                    {
                        string encryptedPassword = EncryptPassword(model.Password, passwordSalt);
                        dbUser.PasswordHash = encryptedPassword;
                    }
                }

                Map(model, dbUser);

                _context.UserRepository.SaveUser(dbUser);

                _log.Info(string.Format("Update user: {0} (id: {1})", model.UserName, model.Id));
            }
            catch (Exception exception) when (exception is EntityNotFoundException || exception is EntityAlreadyExistsException ||
            exception is ValidationException)
            {
                throw;
            }
            catch (Exception exception)
            {
                _log.Error("BLL error", exception);
                throw new EntityOperationFailedException("User", "update", exception);
            }
        }


        /// <summary>
        /// Authenticate user using his credentials
        /// </summary>
        /// <param name="userName">User name</param>
        /// <param name="password">User password</param>
        /// <param name="securityDomainId">Selected security domain id</param>
        /// <returns>Validation boolean result</returns>
        public virtual bool ValidateUser(string userName, string password, Guid securityDomainId)
        {
            bool result = false;

            string passwordHash = null, passwordSalt = null;

            if (string.IsNullOrEmpty(userName)) throw new ArgumentNullException(nameof(userName));

            if (string.IsNullOrEmpty(password)) throw new ArgumentNullException(nameof(password));

            if (Guid.Empty.Equals(securityDomainId)) throw new ArgumentNullException(nameof(securityDomainId));

            try
            {
                User dbUser = _context.UserRepository.GetUserByUserName(userName);

                if (null != dbUser && securityDomainId.Equals(dbUser.SecurityDomainId) && string.Equals(LoginType, dbUser.LoginType))
                {
                    SecurityDomainDAL dbSecurityDomain = _context.SecurityDomainRepository.GetSecurityDomainById(securityDomainId);

                    if (null != dbSecurityDomain && dbSecurityDomain.Id.Equals(dbUser.SecurityDomainId))
                    {
                        _context.UserRepository.GetPasswordHash(userName, ref passwordHash, ref passwordSalt);
                        if (!string.IsNullOrEmpty(passwordHash) && !string.IsNullOrEmpty(passwordSalt))
                        {
                            string encryptedPassword = EncryptPassword(password, passwordSalt);
                            result = string.Equals(passwordHash, encryptedPassword);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                _log.Error("BLL error", exception);
                throw new EntityOperationFailedException("User", "validate", exception);
            }

            _log.Info(string.Format("Validate user: {0} (securityDomainId: {1}). Validation result: {2}",
                userName, securityDomainId, result));

            return result;
        }

        /// <summary>
        /// Get all security domains for authentication
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SecurityDomain> GetSecurityDomains()
        {
            IEnumerable<SecurityDomain> result = null;

            try
            {
                IEnumerable<SecurityDomainDAL> securityDomains = _context.SecurityDomainRepository.GetSecurityDomains();
                result = Map(securityDomains);
            }
            catch (Exception exception)
            {
                _log.Error("BLL error", exception);
                throw new EntityOperationFailedException("SecurityDomains", "get", exception);
            }

            return result;
        }

        /// <summary>
        /// Generate random password for new user
        /// </summary>
        /// <returns></returns>
        public string GeneratePassword()
        {
            string result = null;

            try
            {
                result = GeneratePassword(_generatedPasswordLegth);
            }
            catch (Exception exception)
            {
                _log.Error("BLL error", exception);
                throw new InvalidOperationException("Password generation process failed");
            }

            return result;
        }

        //BLL Model -> DAL Model
        private void Map(UserInputModel model, User userDAL)
        {
            if (null == model) throw new ArgumentNullException(nameof(model));

            if (null == userDAL) throw new ArgumentNullException(nameof(userDAL));

            userDAL.UserName = model.UserName;
        }

        private void Map(IndividualInputModel model, Individual individualDAL)
        {
            if (null == model) throw new ArgumentNullException(nameof(model));

            if (null == individualDAL) throw new ArgumentNullException(nameof(individualDAL));

            individualDAL.FirstName = model.FirstName;
            individualDAL.LastName = model.LastName;
            individualDAL.Email = model.Email;
            individualDAL.DateBirth = model.DateBirth;
        }

        //DAL Model -> Domain Entity
        private IEnumerable<SecurityDomain> Map(IEnumerable<SecurityDomainDAL> securityDomains)
        {
            if (null == securityDomains) throw new ArgumentNullException(nameof(securityDomains));

            foreach (var securityDomainDAL in securityDomains)
            {
                SecurityDomain securityDomain = new SecurityDomain();
                Map(securityDomainDAL, securityDomain);

                yield return securityDomain;
            }
        }

        private void Map(SecurityDomainDAL securityDomainDAL, SecurityDomain securityDomain)
        {
            if (null == securityDomainDAL) throw new ArgumentNullException(nameof(securityDomainDAL));

            if (null == securityDomain) throw new ArgumentNullException(nameof(securityDomain));

            securityDomain.Id = securityDomainDAL.Id;
            securityDomain.Alias = securityDomainDAL.Alias;
        }
    }
}
