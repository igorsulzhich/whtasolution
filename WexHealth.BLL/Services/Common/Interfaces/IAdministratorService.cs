﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Models;

namespace WexHealth.BLL.Services.Common.Interfaces
{
    public interface IAdministratorService
    {
        Administrator GetAdministratorById(Guid id);

        Administrator GetAdministratorByName(string name);

        Administrator GetAdministratorBySecurityDomain(Guid id);

        Administrator GetAdministratorByUser(User user);

        IEnumerable<Administrator> GetAdministrators();

        Guid CreateAdministrator(AdministratorInputModel model);

        void UpdateAdministrator(AdministratorInputModel model);
    }
}
