﻿using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.BLL.Services.Common.Interfaces
{
    public interface ISettingReadService
    {
        void InitService(User user);

        IEnumerable<Setting> GetSettings();
    }
}
