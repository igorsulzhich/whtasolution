﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.BLL.Services.Common.Interfaces
{
    public interface IRoleService
    {
        void AddUsersToRoles(string[] usernames, string[] roleNames);

        Guid CreateRole(string roleName);

        IEnumerable<Role> GetAllRoles();

        IEnumerable<Role> GetRolesForUser(string username);

        bool IsUserInRole(string username, string roleName);

        bool RoleExists(string roleName);
    }
}
