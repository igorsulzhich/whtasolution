﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.BLL.Services.Common.Interfaces
{
    public interface IBenefitsPackageService
    {
        void InitService(User user);

        void InitService(User user, string employerCode);

        BenefitsPackage GetPackageById(int id);

        IEnumerable<BenefitsPackage> GetPackages();

        IEnumerable<PayrollFrequency> GetPayrollFrequencies();

        int CreatePackage(BenefitsPackage inputPackage);

        int UpdatePackage(BenefitsPackage inputPackage);

        bool DeletePackage(int id);

        int Initialize(int id);
    }
}
