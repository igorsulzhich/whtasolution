﻿using System;

namespace WexHealth.BLL.Services.Common.Interfaces
{
    public interface IConsumerValidateService
    {
        bool ValidateByEmployer(Guid consumerId, Guid employerId);

        bool ValidateByAdministrator(Guid consumerId, Guid administratorId);

        void TryValidateByEmployer(Guid consumerId, Guid employerId);

        void TryValidateByAdministrator(Guid consumerId, Guid administratorId);
    }
}
