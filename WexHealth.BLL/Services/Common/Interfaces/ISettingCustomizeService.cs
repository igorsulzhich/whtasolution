﻿using System.Collections.Generic;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Models;

namespace WexHealth.BLL.Services.Common.Interfaces
{
    public interface ISettingCustomizeService
    {
        //User ObjectUser { set; }

        void InitService(User user);

        IEnumerable<Setting> GetSettings();

        void SaveSettings(IEnumerable<SettingInputModel> model);
    }
}
