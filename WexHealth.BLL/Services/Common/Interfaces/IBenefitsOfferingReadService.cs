﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.BLL.Services.Common.Interfaces
{
    public interface IBenefitsOfferingReadService
    {
        void InitService(User user);

        void InitService(User user, string employerCode);

        void InitService(User user, Guid consumerId);

        IEnumerable<BenefitsOffering> GetBenefitsOfferings();

        BenefitsOffering GetBenefitsOfferingById(int id);

        IEnumerable<BenefitsOffering> GetBenefitsOfferingByPackageId(int benefitsPackageId);

        IEnumerable<PlanType> GetPlanTypes();

        PlanType GetPlanTypeByName(string name);
    }
}
