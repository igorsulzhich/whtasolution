﻿using System;

namespace WexHealth.BLL.Services.Common.Interfaces
{
    public interface IEmployerValidateService
    {
        bool ValidateByAdministrator(Guid employerId, Guid administratorId);

        void TryValidateByAdministrator(Guid employerId, Guid administratorId);
    }
}
