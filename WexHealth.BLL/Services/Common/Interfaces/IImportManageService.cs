﻿using WexHealth.Domain.Entities;

namespace WexHealth.BLL.Services.Common.Interfaces
{
    public interface IImportManageService
    {
        Import GetAvailableRequest();

        void ChangeStatus(int id, ImportStatus status);
    }
}
