﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.BLL.Services.Common.Interfaces
{
    public interface IClaimFileService
    {
        void InitService(User user);

        string SaveClaim(Claim claim);
    }
}
