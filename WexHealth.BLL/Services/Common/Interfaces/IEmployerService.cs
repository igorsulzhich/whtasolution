﻿using System;
using System.Collections.Generic;
using WexHealth.BLL.Models;
using WexHealth.Domain.Entities;

namespace WexHealth.BLL.Services.Common.Interfaces
{
    public interface IEmployerService
    {
        User ObjectUser { set; }

        IEnumerable<Employer> SearchEmployers(string name, string code);

        IEnumerable<Employer> GetEmployersByAdmin(Guid id);

        Employer GetEmployerByName(string name);

        Employer GetEmployerById(Guid id);

        Employer GetEmployerByCode(string code, Guid userId);

        IEnumerable<Employer> GetEmployers();

        Employer GetEmployerByUser(User user);

        Guid CreateEmployer(EmployerInputModel employer, Guid administratorId);

        Guid UpdateEmployer(EmployerInputModel inputEmployer, Guid administratorId);
    }
}
