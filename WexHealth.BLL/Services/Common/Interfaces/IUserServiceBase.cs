﻿namespace WexHealth.BLL.Services.Common.Interfaces
{
    public interface IUserServiceBase
    {
        string EncryptPassword(string password, string passwordSalt);

        string GeneratePasswordSalt();

        string GeneratePassword(int length);
    }
}
