﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.BLL.Services.Common.Interfaces
{
    public interface IEnrollmentReadService
    {
        void InitService(User user);

        void InitService(User user, Guid consumerId);

        Enrollment GetEnrollment(int id);

        IEnumerable<Enrollment> GetEnrollments();

        IEnumerable<Enrollment> GetEnrollmentsByPackageId(int packageId);

        IEnumerable<Enrollment> GetActiveEnrollments();
    }
}
