﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Models;

namespace WexHealth.BLL.Services.Common.Interfaces
{
    public interface IUserService
    {
        Guid CreateUser(UserInputModel model, Guid individualId);

        void UpdateUser(UserInputModel model);

        bool ValidateUser(string userName, string password, Guid securityDomainId);

        IEnumerable<SecurityDomain> GetSecurityDomains();

        string GeneratePassword();
    }
}
