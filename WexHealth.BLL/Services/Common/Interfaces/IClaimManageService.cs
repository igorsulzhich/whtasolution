﻿using System;
using WexHealth.Domain.Entities;

namespace WexHealth.BLL.Services.Common.Interfaces
{
    public interface IClaimManageService
    {
        void InitService(User user);

        Claim GetClaim(string claimId);

        void ApproveClaim(string claimId);

        void DenyClaim(string claimId);

        void SaveClaim(Claim claim);
    }
}
