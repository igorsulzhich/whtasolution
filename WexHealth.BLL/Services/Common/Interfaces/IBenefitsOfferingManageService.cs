﻿using System;
using WexHealth.Domain.Entities;

namespace WexHealth.BLL.Services.Common.Interfaces
{
    public interface IBenefitsOfferingManageService
    {
        void InitService(User user, string employerCode);

        int CreateBenefitsOffering(BenefitsOffering benefitsOffering);

        int UpdateBenefitsOffering(BenefitsOffering benefitsOffering);
    }
}
