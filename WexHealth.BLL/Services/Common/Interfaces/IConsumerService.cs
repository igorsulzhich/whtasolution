﻿using System;
using System.Collections.Generic;
using WexHealth.BLL.Models;
using WexHealth.Domain.Entities;

namespace WexHealth.BLL.Services.Common.Interfaces
{
    public interface IConsumerService
    {
        User ObjectUser { set; }

        Consumer GetConsumerById(Guid id);

        IEnumerable<Consumer> GetConsumerByEmployer(Guid id, Guid administratorId);

        IEnumerable<Consumer> SearchConsumers(string firstName, string lastName, string employerCode, string ssn);

        IEnumerable<Consumer> SearchConsumers(string firstName, string lastName, string ssn);

        IEnumerable<Consumer> SearchConsumers(string firstName, string lastName, Guid employerId);

        Consumer GetConsumerByUser(User user);

        Guid CreateConsumer(ConsumerInputModel modelConsumer, Guid individualId, Guid employerId);

        void UpdateConsumer(ConsumerInputModel modelConsumer, Guid employerId, Guid administratorId);
    }
}
