﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Models;

namespace WexHealth.BLL.Services.Common.Interfaces
{
    public delegate IDomainUserService AdministratorUserServiceFactory(Guid administratorId);

    public delegate IDomainUserService EmployerUserServiceFactory(string employerCode, Guid administratorUserId);

    public delegate IDomainUserService ConsumerUserServiceFactory(Guid consumerId, Guid employerId, Guid administratorId);

    public interface IDomainUserService
    {
        User GetUser(Guid id);

        IEnumerable<User> GetUsers();

        Guid CreateUser(UserInputModel model, Guid individualId);

        void UpdateUser(UserInputModel model);

        string GeneratePassword();
    }
}
