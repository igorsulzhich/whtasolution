﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.BLL.Services.Common.Interfaces
{
    public interface IReportService
    {
        IEnumerable<ObjectField> GetObjectFields();

        void SetObjectFields(IEnumerable<ObjectField> checkedFields, int requestId);

        IEnumerable<ReportField> GetReportFieldsByRequestId(int requestId);

        IEnumerable<ReportRequest> GetReportRequests();

        int CreateReportRequest(Guid employerId);

        bool RejectRequest(int id, ReportStatus status);

        ReportRequest GetFirstReportRequest();

        bool RemoveRequest(int id);

        void InitService(User user);
    }
}
