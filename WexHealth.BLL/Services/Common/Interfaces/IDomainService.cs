﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Models;

namespace WexHealth.BLL.Services.Common.Interfaces
{
    public interface IDomainService
    {
        IEnumerable<Phone> GetPhonesByObject(Guid objectId, string objectType);

        Guid SetPhoneToObject(PhoneInputModel model, Guid objectId, string objectType);

        IEnumerable<Address> GetAddressesByObject(Guid objectId, string objectType);

        Guid SetAddressToObject(AddressInputModel model, Guid objectId, string objectType);

        Guid UpdateAddressToObject(AddressInputModel model, Guid objectId, string objectType);

        Guid UpdatePhoneToObject(PhoneInputModel model, Guid objectId, string objectType);

        Guid CreateIndividual(IndividualInputModel model);

        void UpdateIndividual(IndividualInputModel model);

        IEnumerable<State> GetStates();
    }
}
