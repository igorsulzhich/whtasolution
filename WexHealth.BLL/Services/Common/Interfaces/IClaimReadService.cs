﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.BLL.Services.Common.Interfaces
{
    public interface IClaimReadService
    {
        void InitService(User user);

        void InitService(User user, Guid consumerId);

        IEnumerable<Claim> GetClaims();

        IEnumerable<Claim> SearchClaims(string number, string status, Guid consumerId, string employerCode);

        IEnumerable<ClaimStatus> GetClaimStatuses();
    }
}
