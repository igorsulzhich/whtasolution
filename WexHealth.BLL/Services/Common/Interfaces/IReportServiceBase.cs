﻿using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.BLL.Services.Common.Interfaces
{
    public interface IReportServiceBase
    {
        IEnumerable<ObjectField> GetObjectFields();
    }
}
