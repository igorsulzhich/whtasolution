﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Models;

namespace WexHealth.BLL.Services.Common.Interfaces
{
    public interface ISettingService
    {
        //Guid ObjectId { set; }

        //User ObjectUser { set; }

        void InitService(User user, Guid objectId);

        IEnumerable<Setting> GetSettings();

        void SaveSettings(IEnumerable<SettingInputModel> model);        
    }
}
