﻿using WexHealth.Domain.Entities;

namespace WexHealth.BLL.Services.Common.Interfaces
{
    public interface IImportFileService
    {
        void InitService(User user);

        int CreateRequest(Import entity);
    }
}
