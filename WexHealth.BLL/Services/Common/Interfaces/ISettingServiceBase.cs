﻿using System;
using System.Collections.Generic;
using WexHealth.BLL.Models;
using WexHealth.DAL.Models;
using WexHealth.Domain.Entities;

namespace WexHealth.BLL.Services.Common.Interfaces
{
    public interface ISettingServiceBase
    {
        void FillSettings(IEnumerable<Setting> settingsDAL, Guid objectId, bool isOverrid);

        IEnumerable<Guid> SetObjectSettings(IEnumerable<SettingInputModel> models, Guid objectId);

        void UpdateObjectSettings(IEnumerable<SettingInputModel> models, Guid objectId);
    }
}
