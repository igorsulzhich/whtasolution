﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.BLL.Services.Common.Interfaces
{
    public interface ILoginUserService
    {
        bool ValidateUser(string userName, string password, Guid securityDomainId);

        IEnumerable<SecurityDomain> GetSecurityDomains();
    }
}
