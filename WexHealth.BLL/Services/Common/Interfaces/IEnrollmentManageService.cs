﻿using System;
using WexHealth.Domain.Entities;

namespace WexHealth.BLL.Services.Common.Interfaces
{
    public interface IEnrollmentManageService
    {
        void InitService(User user, Guid consumerId);

        int CreateEnrollment(Enrollment enrollemnt);

        void UpdateEnrollment(Enrollment enrollment);
    }
}
