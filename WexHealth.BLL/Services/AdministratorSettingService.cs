﻿using System;
using System.Linq;
using System.Collections.Generic;
using log4net;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Models;
using WexHealth.BLL.Exceptions;
using WexHealth.BLL.Services.Common;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.DAL.Models;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.BLL.Services
{
    public class AdministratorSettingService : SettingServiceBase, ISettingService, ISettingReadService, ISettingCustomizeService
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(AdministratorSettingService));

        private Guid _administratorId;
        private string _loginType;

        private readonly IAdministratorService _administratorService;

        private readonly ISettingRepository _settingRepository;
        private readonly ISettingConnectionRepository _settingConnectionRepository;

        protected override LevelType ObjectType
        {
            get
            {
                return LevelType.Administrator;
            }
        }

        protected override IEnumerable<KeyValuePair<Guid, string>> ObjectHierarchy
        {
            get
            {
                return new Dictionary<Guid, string>()
                {
                    { _administratorId, "Administrator"},
                    { Guid.Empty, "Global" }
                };
            }
        }

        public AdministratorSettingService(IAdministratorService administratorService, ISettingRepository settingRepository,
            ISettingConnectionRepository settingconnectionRepository, ISettingServiceContext context) :
            base(context)
        {
            _administratorService = administratorService;

            _settingRepository = settingRepository;
            _settingConnectionRepository = settingconnectionRepository;
        }

        public void InitService(User user)
        {
            if (null == user) throw new ArgumentNullException(nameof(user));

            _loginType = user.LoginType;

            Administrator administrator = _administratorService.GetAdministratorByUser(user);

            _administratorId = administrator.Id;
        }

        void ISettingService.InitService(User user, Guid objectId)
        {
            if (Guid.Empty.Equals(objectId)) throw new ArgumentNullException(nameof(objectId));

            if (null == user)
            {
                _administratorId = objectId;
            }
        }

        /// <summary>
        /// Get administrator's features on TPA level
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Setting> GetSettings()
        {
            IEnumerable<Setting> result = null;

            IEnumerable<SettingDAL> settings = _settingRepository.GetSettingsByParent(Guid.Empty);
            result = Map(settings).ToList();

            base.FillSettings(result, _administratorId, true);

            return result;
        }

        /// <summary>
        /// Get administrator's allow features on Admin level
        /// </summary>
        /// <returns></returns>
        IEnumerable<Setting> ISettingReadService.GetSettings()
        {
            return GetSettings().Where(x => x.isAllowed.Value);
        }

        /// <summary>
        /// Update administrator's features on TPA level
        /// </summary>
        /// <param name="model"></param>
        void ISettingService.SaveSettings(IEnumerable<SettingInputModel> model)
        {
            base.UpdateObjectSettings(model, _administratorId);

            IEnumerable<SettingInputModel> dissallowSettings = model.Where(x => x.isAllowed.HasValue && !x.isAllowed.Value);
            foreach (var setting in dissallowSettings)
            {
                IEnumerable<SettingDAL> childSettings = _settingRepository.GetSettingsByParent(setting.Id);
                foreach (var childSetting in childSettings)
                {
                    SettingConnectionDAL settingConnection =
                        _settingConnectionRepository.GetSettingConnectionByObjectSetting(_administratorId, ObjectType.ToString(), childSetting.Id);

                    if (null != settingConnection)
                    {
                        _settingConnectionRepository.DeleteSettingConnection(settingConnection.Id);
                    }
                }
            }
        }

        /// <summary>
        /// Get settings of allowed features for self-editing on Admin level
        /// </summary>
        /// <returns></returns>
        IEnumerable<Setting> ISettingCustomizeService.GetSettings()
        {
            IEnumerable<Setting> result = null;

            IEnumerable<Setting> allowSettings = GetSettings().Where(x => x.isAllowed.Value);
            var settings = new List<SettingDAL>();
            foreach (var setting in allowSettings)
            {
                IEnumerable<SettingDAL> childSettings = _settingRepository.GetSettingsByParent(setting.Id);
                settings.AddRange(childSettings);
            }

            result = Map(settings).ToList();
            base.FillSettings(result, _administratorId, true);

            return result;
        }

        /// <summary>
        /// Update settings of allowed features on Admin level
        /// </summary>
        /// <param name="model"></param>
        void ISettingCustomizeService.SaveSettings(IEnumerable<SettingInputModel> model)
        {
            base.UpdateObjectSettings(model, _administratorId);
        }
    }
}
