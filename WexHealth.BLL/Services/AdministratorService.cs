﻿using System;
using System.Linq;
using System.Collections.Generic;
using PostSharp.Patterns.Contracts;
using log4net;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Models;
using WexHealth.BLL.Exceptions;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.DAL.Models;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.BLL.Services
{
    /// <summary>
    /// Manage setting on administrator level
    /// </summary>
    public class AdministratorService : IAdministratorService
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(AdministratorService));

        private const string objectType = "Administrator";

        private readonly IDomainService _domainService;

        private readonly ISecurityDomainRepository _securityDomainRepository;
        private readonly IAdministratorRepository _administratorRepository;
        private readonly IAdministratorUserRepository _administratorUserRepository;
        private readonly IEmployerRepository _employerRepository;
        private readonly IEmployerUserRepository _employerUserRepository;
        private readonly IConsumerRepository _consumerRepository;

        public AdministratorService(IDomainService domainService, ISecurityDomainRepository securityDomainRepository, IAdministratorRepository administratorRepository,
            IAdministratorUserRepository administratorUserRepository, IEmployerRepository employerRepository, IEmployerUserRepository employerUserRepository,
            IConsumerRepository consumerRepository)
        {
            _domainService = domainService;

            _securityDomainRepository = securityDomainRepository;
            _administratorRepository = administratorRepository;
            _administratorUserRepository = administratorUserRepository;
            _employerRepository = employerRepository;
            _employerUserRepository = employerUserRepository;
            _consumerRepository = consumerRepository;
        }

        /// <summary>
        /// Get administartor
        /// </summary>
        /// <param name="id">Administrator id</param>
        /// <returns>Administrator entity</returns>
        public Administrator GetAdministratorById(Guid id)
        {
            Administrator result = null;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            try
            {
                AdministratorDAL administratorDAL = _administratorRepository.GetAdministratorById(id);
                result = new Administrator();
                Map(administratorDAL, result);
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("Administrator", "getById", e);
            }

            return result;
        }

        /// <summary>
        /// Get administrator
        /// </summary>
        /// <param name="name">Administrator name</param>
        /// <returns>Administrator entity</returns>
        public Administrator GetAdministratorByName([Required]string name)
        {
            Administrator result = null;

            try
            {
                AdministratorDAL administratorDAL = _administratorRepository.GetAdministratorByName(name);
                result = new Administrator();
                Map(administratorDAL, result);
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("Administrator", "getByName", e);
            }

            return result;
        }

        /// <summary>
        /// Get administrator
        /// </summary>
        /// <param name="id">Administrator security domain id</param>
        /// <returns>Administrator entity</returns>
        public Administrator GetAdministratorBySecurityDomain(Guid id)
        {
            Administrator result = null;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            try
            {
                AdministratorDAL administratorDAL = _administratorRepository.GetAdministratorBySecurityDomain(id);
                result = new Administrator();
                Map(administratorDAL, result);
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("Administrator", "getBySecurityDomain", e);
            }

            return result;
        }

        public Administrator GetAdministratorByUser([NotNull] User user)
        {
            var result = new Administrator();

            try
            {
                Guid administratorId = Guid.Empty;

                if (user.LoginType.Equals("Administrator"))
                {
                    AdministratorUserDAL dbUser = _administratorUserRepository.GetUser(user.Id);
                    if (null == dbUser)
                    {
                        throw new EntityNotFoundException("AdministratorUser", "userId", user.Id.ToString());
                    }

                    administratorId = dbUser.AdministratorId;
                }
                else if (user.LoginType.Equals("Employer"))
                {
                    EmployerUserDAL dbUser = _employerUserRepository.GetUser(user.Id);
                    if (null == dbUser)
                    {
                        throw new EntityNotFoundException("EmployerUser", "userId", user.Id.ToString());
                    }

                    EmployerDAL dbEmployer = _employerRepository.GetEmployerById(dbUser.EmployerId);
                    if (null == dbEmployer)
                    {
                        throw new EntityNotFoundException("Employer", "id", dbUser.EmployerId.ToString());
                    }

                    administratorId = dbEmployer.AdministratorId;
                }
                else if (user.LoginType.Equals("Consumer"))
                {
                    ConsumerDAL dbConsumer = _consumerRepository.GetConsumerByIndividualId(user.IndividualId);
                    if (null == dbConsumer)
                    {
                        throw new EntityNotFoundException("Consumer", "individualId", user.IndividualId.ToString());
                    }

                    EmployerDAL dbEmployer = _employerRepository.GetEmployerById(dbConsumer.EmployerId);
                    if (null == dbEmployer)
                    {
                        throw new EntityNotFoundException("Employer", "id", dbConsumer.EmployerId.ToString());
                    }

                    administratorId = dbEmployer.AdministratorId;
                }

                AdministratorDAL dbAdministrator = _administratorRepository.GetAdministratorById(administratorId);
                if (null == dbAdministrator)
                {
                    throw new EntityNotFoundException("Administrator", "id", administratorId.ToString());
                }

                Map(dbAdministrator, result);
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("Administrator", e);
            }

            return result;
        }

        /// <summary>
        /// Get all administrators
        /// </summary>
        /// <returns>List of administrators</returns>
        public IEnumerable<Administrator> GetAdministrators()
        {
            IEnumerable<Administrator> result = null;

            try
            {
                IEnumerable<AdministratorDAL> administrators = _administratorRepository.GetAdministrators();
                result = Map(administrators);
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("Administrators", "get", e);
            }

            return result;
        }


        /// <summary>
        /// Create new administrator
        /// </summary>
        /// <param name="model">Input model with administrator info</param>
        /// <returns>Id of inserted administrator</returns>
        public Guid CreateAdministrator([NotNull] AdministratorInputModel model)
        {
            Guid result = Guid.Empty;

            try
            {
                System.ComponentModel.DataAnnotations.Validator.ValidateObject(model,
                    new System.ComponentModel.DataAnnotations.ValidationContext(model, null, null), true);

                SecurityDomainDAL dbSecurityDomain = _securityDomainRepository.GetSecurityDomainByAlias(model.Alias);
                if (dbSecurityDomain != null)
                {
                    throw new EntityAlreadyExistsException("Security domain", "alias", model.Alias);
                }

                var securityDomain = new SecurityDomainDAL();
                Map(model, securityDomain);

                Guid securityDomainId = _securityDomainRepository.SaveSecurityDomain(securityDomain);

                var administrator = new AdministratorDAL();
                Map(model, administrator);
                administrator.SecurityDomainId = securityDomainId;

                result = _administratorRepository.SaveAdministrator(administrator);

                _domainService.SetAddressToObject(model.Address, result, objectType);
                _domainService.SetPhoneToObject(model.Phone, result, objectType);

                log.Info(string.Format("Create new administrator: {0} (id: {1})", model.Name, result));
            }
            catch (Exception e) when (e is EntityNotFoundException || e is EntityAlreadyExistsException || e is System.ComponentModel.DataAnnotations.ValidationException)
            {
                throw;
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("Administrator", "create", e);
            }

            return result;
        }

        /// <summary>
        /// Update administrator
        /// </summary>
        /// <param name="model">Administrator input model with filled properties</param>
        public void UpdateAdministrator([NotNull] AdministratorInputModel model)
        {
            try
            {
                System.ComponentModel.DataAnnotations.Validator.ValidateObject(model,
                    new System.ComponentModel.DataAnnotations.ValidationContext(model, null, null), true);

                AdministratorDAL dbAdministrator = _administratorRepository.GetAdministratorById(model.Id);
                if (null == dbAdministrator)
                {
                    throw new EntityNotFoundException("Administrator", "id", model.Id.ToString());
                }

                if (!string.Equals(dbAdministrator.SecurityDomain.Alias, model.Alias))
                {
                    SecurityDomainDAL dbSecurityDomain = _securityDomainRepository.GetSecurityDomainByAlias(model.Alias);
                    if (null != dbSecurityDomain)
                    {
                        throw new EntityAlreadyExistsException("Security domain", "alias", model.Alias);
                    }

                    dbSecurityDomain = _securityDomainRepository.GetSecurityDomainById(dbAdministrator.SecurityDomainId);
                    if (null == dbSecurityDomain)
                    {
                        throw new EntityNotFoundException("Security domain", "id", dbAdministrator.SecurityDomainId.ToString());
                    }

                    Map(model, dbSecurityDomain);
                    _securityDomainRepository.SaveSecurityDomain(dbSecurityDomain);
                }

                _domainService.UpdateAddressToObject(model.Address, dbAdministrator.Id, objectType);
                _domainService.UpdatePhoneToObject(model.Phone, dbAdministrator.Id, objectType);

                Map(model, dbAdministrator);
                _administratorRepository.SaveAdministrator(dbAdministrator);

                log.Info(string.Format("Update administrator: {0} (id: {1})", model.Name, model.Id));
            }
            catch (Exception e) when (e is EntityNotFoundException || e is EntityAlreadyExistsException || e is System.ComponentModel.DataAnnotations.ValidationException)
            {
                throw;
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("Administrator", "update", e);
            }
        }

        //BLL Model -> DAL Model
        private void Map([NotNull] AdministratorInputModel model, [NotNull] AdministratorDAL administratorDAL)
        {
            administratorDAL.Name = model.Name;
        }

        private void Map([NotNull] AdministratorInputModel model, [NotNull] SecurityDomainDAL securityDomainDAL)
        {
            securityDomainDAL.Alias = model.Alias;
        }

        //DAL Model -> Domain Entity
        private IEnumerable<Administrator> Map([NotNull] IEnumerable<AdministratorDAL> administrators)
        {
            foreach (var administratorDAL in administrators)
            {
                Administrator administrator = new Administrator();
                Map(administratorDAL, new List<Address>(), new List<Phone>(), administrator);

                yield return administrator;
            }
        }

        private void Map([NotNull] AdministratorDAL administratorDAL, [NotNull] Administrator administrator)
        {
            IEnumerable<Address> addresses = _domainService.GetAddressesByObject(administratorDAL.Id, objectType);
            IEnumerable<Phone> phones = _domainService.GetPhonesByObject(administratorDAL.Id, objectType);

            Map(administratorDAL, addresses, phones, administrator);
        }

        private void Map([NotNull] AdministratorDAL administratorDAL, [NotNull] IEnumerable<Address> addresses, [NotNull] IEnumerable<Phone> phones,
            [NotNull] Administrator administrator)
        {
            administrator.Id = administratorDAL.Id;
            administrator.Name = administratorDAL.Name;
            administrator.Alias = administratorDAL.SecurityDomain.Alias;

            Address address = addresses.FirstOrDefault();
            if (null != address)
            {
                administrator.Address = new Address
                {
                    Id = address.Id,
                    Street = address.Street,
                    City = address.City,
                    State = address.State,
                    ZipCode = address.ZipCode
                };
            }

            Phone phone = phones.FirstOrDefault();
            if (null != phone)
            {
                administrator.Phone = new Phone
                {
                    Id = phone.Id,
                    PhoneNumber = phone.PhoneNumber
                };
            }
        }
    }
}

