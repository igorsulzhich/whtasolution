﻿using System;
using System.Collections.Generic;
using log4net;
using WexHealth.Domain.Entities;
using WexHealth.Domain.Validators;
using WexHealth.BLL.Exceptions;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.DAL.Models;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.BLL.Services
{
    public class ImportService : IImportFileService, IImportManageService
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ImportService));

        private Guid _employerId;

        private readonly IImportRepository _importRequestRepository;
        private readonly IEmployerService _employerService;

        public ImportService(IImportRepository importRequestRepository, IEmployerService employerService)
        {
            _importRequestRepository = importRequestRepository;
            _employerService = employerService;
        }

        public void InitService(User user)
        {
            if (null == user) throw new ArgumentNullException(nameof(user));

            Employer employer = _employerService.GetEmployerByUser(user);
            _employerId = employer.Id;
        }

        int IImportFileService.CreateRequest(Import entity)
        {
            int result = 0;

            if (null == entity) throw new ArgumentNullException(nameof(entity));

            try
            {
                entity.Id = 0;
                entity.Status = ImportStatus.Requested;
                entity.EmployerId = _employerId;

                result = _importRequestRepository.Save(entity);
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw;
            }

            return result;
        }

        Import IImportManageService.GetAvailableRequest()
        {
            Import result = null;

            try
            {
                result = _importRequestRepository.GetFirstWithStatus(ImportStatus.Requested);
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw;
            }

            return result;
        }

        void IImportManageService.ChangeStatus(int id, ImportStatus status)
        {
            if (id <= 0) throw new ArgumentOutOfRangeException(nameof(id));

            try
            {
                Import dbEntity = _importRequestRepository.GetById(id);
                if (null == dbEntity)
                {
                    throw new EntityNotFoundException("Import", "id", id.ToString());
                }

                if (dbEntity.Status > status)
                {
                    throw new InvalidOperationException(
                        $"Import request status can not be changed from: {dbEntity.Status.ToString()}, to: {status.ToString()}"
                        );
                }

                dbEntity.Status = status;

                _importRequestRepository.Save(dbEntity);
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw;
            }
        }
    }
}
