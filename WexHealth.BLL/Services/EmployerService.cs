﻿using System;
using System.Linq;
using System.Collections.Generic;
using PostSharp.Patterns.Contracts;
using log4net;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Models;
using WexHealth.BLL.Exceptions;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.DAL.Models;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.BLL.Services
{
    /// <summary>
    /// Manage employers
    /// </summary>
    public class EmployerService : IEmployerService
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(EmployerService));

        private readonly IDomainService _domainService;

        private readonly IAdministratorRepository _administratorRepository;
        private readonly IAdministratorUserRepository _administratorUserRepository;
        private readonly IEmployerRepository _employerRepository;
        private readonly IEmployerUserRepository _employerUserRepository;
        private readonly IConsumerRepository _consumerRepository;

        private Guid _administratorId;
        private string _loginType;

        private const string _objectType = "Employer";

        public EmployerService(IDomainService domainService, IAdministratorRepository administratorRepository, IAdministratorUserRepository administratorUserRepository,
            IEmployerRepository employerRepository, IEmployerUserRepository employerUserRepository, IConsumerRepository consumerRepository)
        {
            _domainService = domainService;

            _administratorRepository = administratorRepository;
            _administratorUserRepository = administratorUserRepository;
            _employerRepository = employerRepository;
            _employerUserRepository = employerUserRepository;
            _consumerRepository = consumerRepository;
        }     

        public User ObjectUser
        {
            set
            {
                if (value.LoginType.Equals("Administrator"))
                {
                    AdministratorUserDAL dbUser = _administratorUserRepository.GetUser(value.Id);
                    if (null == dbUser)
                    {
                        throw new EntityNotFoundException("AdministratorUser", "userId", value.Id.ToString());
                    }

                    _administratorId = dbUser.AdministratorId;
                    _loginType = value.LoginType;
                }
            }
        }

        /// <summary>
        /// Search employer by name and code
        /// </summary>
        /// <param name="name">Employer name</param>
        /// <param name="code">Employer code</param>
        /// <returns></returns>
        public IEnumerable<Employer> SearchEmployers(string name, string code)
        {
            IEnumerable<EmployerDAL> dbEmployers = _employerRepository.GetEmployersByNameAndCode(name, code, _administratorId);

            return Map(dbEmployers);
        }

        /// <summary>
        /// Get employers for current administrator in Admin portal
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Employer> GetEmployers()
        {
            IEnumerable<Employer> result = null;

            if (_loginType == "Administrator")
            {
                IEnumerable<EmployerDAL> employers = _employerRepository.GetEmployersByAdministrator(_administratorId);
                result = Map(employers);
            }

            return result;
        }

        /// <summary>
        /// Get employers for current administrator
        /// </summary>
        /// <param name="id">Administrator id</param>
        /// <returns></returns>
        public IEnumerable<Employer> GetEmployersByAdmin(Guid id)
        {
            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            IEnumerable<EmployerDAL> employers = _employerRepository.GetEmployersByAdministrator(id);

            return Map(employers);
        }

        /// <summary>
        /// Get employer entity by name
        /// </summary>
        /// <param name="name">Employer name</param>
        /// <returns></returns>
        public Employer GetEmployerByName([Required] string name)
        {
            Employer result = null;

            EmployerDAL employer = _employerRepository.GetEmployerByName(name);
            if (null == employer)
            {
                throw new EntityNotFoundException("Employer", "employer", name);
            }

            result = new Employer();
            Map(employer, result);

            return result;
        }

        /// <summary>
        /// Get employer entity by Id
        /// </summary>
        /// <param name="id">Employer Id</param>
        /// <returns></returns>
        public Employer GetEmployerById(Guid id)
        {
            Employer result = null;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            EmployerDAL employer = _employerRepository.GetEmployerById(id);
            if (null == employer)
            {
                throw new EntityNotFoundException("Employer", "employer", id.ToString());
            }

            result = new Employer();
            Map(employer, result);

            return result;
        }

        /// <summary>
        /// Get employer entity by Employer code
        /// </summary>
        /// <param name="code">Employer code</param>
        /// <param name="administratorId">Current administrator</param>
        /// <returns></returns>
        public Employer GetEmployerByCode([Required] string code, Guid userId)
        {
            Employer result = null;

            if (Guid.Empty.Equals(userId)) throw new ArgumentNullException(nameof(userId));

            Guid administratorId = _administratorUserRepository.GetUser(userId).AdministratorId;

            if (Guid.Empty.Equals(administratorId)) throw new ArgumentNullException(nameof(administratorId));

            AdministratorDAL dbAdministrator = _administratorRepository.GetAdministratorById(administratorId);
            if (null == dbAdministrator)
            {
                throw new EntityNotFoundException("Administrator", "id", administratorId.ToString());
            }

            EmployerDAL dbEmployer = _employerRepository.GetEmployerByCode(code);
            if (null == dbEmployer)
            {
                throw new EntityNotFoundException("Employer", "code", code.ToString());
            }

            if (!dbEmployer.AdministratorId.Equals(administratorId))
            {
                throw new Exception("Employer doesn't related to current administrator");
            }

            result = new Employer();
            Map(dbEmployer, result);

            return result;
        }

        public Employer GetEmployerByUser([NotNull] User user)
        {
            var result = new Employer();

            try
            {
                Guid employerId = Guid.Empty;

                if (user.LoginType.Equals("Employer"))
                {
                    EmployerUserDAL dbUser = _employerUserRepository.GetUser(user.Id);
                    if (null == dbUser)
                    {
                        throw new EntityNotFoundException("EmployerUser", "userId", user.Id.ToString());
                    }

                    employerId = dbUser.EmployerId;
                }
                else if (user.LoginType.Equals("Consumer"))
                {
                    ConsumerDAL dbConsumer = _consumerRepository.GetConsumerByIndividualId(user.IndividualId);
                    if (null == dbConsumer)
                    {
                        throw new EntityNotFoundException("Consumer", "individualId", user.IndividualId.ToString());
                    }

                    employerId = dbConsumer.EmployerId;
                }

                EmployerDAL dbEmployer = _employerRepository.GetEmployerById(employerId);
                if (null == dbEmployer)
                {
                    throw new EntityNotFoundException("Employer", "id", employerId.ToString());
                }

                Map(dbEmployer, result);
            }
            catch (Exception e)
            {
                log.Error("BLL error");
                throw new EntityOperationFailedException("Employer", e);
            }

            return result;
        }

        /// <summary>
        /// Create new employer
        /// </summary>
        /// <param name="name">New employer name</param>
        /// <param name="code">New employer code</param>
        /// <param name="administratorId">Administrator id for new employer</param>
        /// <param name="logo">Logo for new employer</param>
        /// <returns></returns>
        public Guid CreateEmployer([NotNull] EmployerInputModel model, Guid administratorId)
        {
            Guid result = Guid.Empty;

            if (Guid.Empty.Equals(administratorId)) throw new ArgumentNullException(nameof(administratorId));

            try
            {
                AdministratorDAL dbAdministrator = _administratorRepository.GetAdministratorById(administratorId);
                if (null == dbAdministrator)
                {
                    throw new EntityNotFoundException("Administrator", "id", administratorId.ToString());
                }

                EmployerDAL dbEmployer = _employerRepository.GetEmployerByCode(model.Code);
                if (null != dbEmployer)
                {
                    throw new EntityAlreadyExistsException("Employer", "code", model.Name);
                }

                Administrator administrator = new Administrator
                {
                    Id = administratorId
                };

                var employer = new EmployerDAL();
                Map(model, employer);

                employer.AdministratorId = administratorId;

                result = _employerRepository.SaveEmployer(employer);

                var newAddress = new AddressInputModel();
                Map(model, newAddress);

                var phone = new PhoneInputModel();
                Map(model, phone);

                _domainService.SetAddressToObject(newAddress, result, _objectType);
                _domainService.SetPhoneToObject(phone, result, _objectType);

                log.Info("Employer with Id: " + result + " - successfully created");
            }
            catch (EntityAlreadyExistsException)
            {
                throw;
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("Employer", "create", exception);
            }

            return result;
        }

        /// <summary>
        /// Update employer
        /// </summary>
        /// <param name="id">Employer id</param>
        /// <param name="name">Employer name</param>
        /// <param name="code">Employer code</param>
        /// <param name="administratorId">Employer's administrator id</param>
        /// <param name="logo">Employer logo</param>
        /// <returns></returns>
        public Guid UpdateEmployer([NotNull] EmployerInputModel model, Guid administratorId)
        {
            Guid result = Guid.Empty;

            if (Guid.Empty.Equals(administratorId)) throw new ArgumentNullException(nameof(administratorId));

            try
            {
                EmployerDAL dbEmployer = _employerRepository.GetEmployerById(model.Id);
                if (null == dbEmployer)
                {
                    throw new EntityNotFoundException("Employer", "id", model.Id.ToString());
                }

                if (model.Code == dbEmployer.Code && model.Id != dbEmployer.Id)
                {
                    throw new EntityAlreadyExistsException("Employer", "code", model.Code);
                }

                AdministratorDAL dbAdministrator = _administratorRepository.GetAdministratorById(administratorId);
                if (null == dbAdministrator)
                {
                    throw new EntityNotFoundException("Administrator", "id", administratorId.ToString());
                }

                if (!dbEmployer.AdministratorId.Equals(administratorId))
                {
                    throw new Exception("Employer doesn't related to current administrator");
                }

                Map(model, dbEmployer);

                dbEmployer.AdministratorId = administratorId;

                result = _employerRepository.SaveEmployer(dbEmployer);

                var updateAddress = new AddressInputModel();
                Map(model, updateAddress);

                var phone = new PhoneInputModel();
                Map(model, phone);

                _domainService.UpdateAddressToObject(updateAddress, result, _objectType);
                _domainService.UpdatePhoneToObject(phone, result, _objectType);

                log.Info("Employer with Id: " + result + " - successfully updated");
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("Employer", "update", e);
            }

            return result;
        }

        private IEnumerable<Employer> Map([NotNull] IEnumerable<EmployerDAL> employers)
        {
            foreach (var employerDAL in employers)
            {
                Employer employer = new Employer();
                Map(employerDAL, new List<Address>(), new List<Phone>(), employer);

                yield return employer;
            }
        }

        private void Map([NotNull] EmployerDAL employerDAL, [NotNull] Employer employer)
        {
            IEnumerable<Address> addresses = _domainService.GetAddressesByObject(employerDAL.Id, _objectType);
            IEnumerable<Phone> phones = _domainService.GetPhonesByObject(employerDAL.Id, _objectType);

            Map(employerDAL, addresses, phones, employer);
        }

        private void Map([NotNull] EmployerDAL employerDAL, [NotNull] IEnumerable<Address> addresses, [NotNull] IEnumerable<Phone> phones, [NotNull] Employer employer)
        {
            employer.Id = employerDAL.Id;
            employer.Name = employerDAL.Name;
            employer.Code = employerDAL.Code;
            employer.Logo = employerDAL.Logo;
            employer.AdministratorId = employerDAL.AdministratorId;

            Address address = addresses.FirstOrDefault();
            if (null != address)
            {
                employer.Address = new Address
                {
                    Id = address.Id,
                    Street = address.Street,
                    City = address.City,
                    State = address.State,
                    ZipCode = address.ZipCode
                };
            }

            Phone phone = phones.FirstOrDefault();
            if (null != phone)
            {
                employer.Phones = new Phone
                {
                    Id = phone.Id,
                    PhoneNumber = phone.PhoneNumber
                };
            }
        }

        private void Map([NotNull] EmployerInputModel model, [NotNull] EmployerDAL employerUserModel)
        {
            employerUserModel.Id = model.Id;
            employerUserModel.Name = model.Name;
            employerUserModel.Code = model.Code;

            if (null != model.Logo)
            {
                employerUserModel.Logo = model.Logo;
            }
        }

        private void Map([NotNull] EmployerInputModel model, [NotNull] AddressInputModel addressModel)
        {
            addressModel.Street = model.Street;
            addressModel.City = model.City;
            addressModel.State = model.State;
            addressModel.ZipCode = model.ZipCode;
        }

        private void Map([NotNull] EmployerInputModel model, [NotNull] PhoneInputModel phoneModel)
        {
            phoneModel.Phone = model.Phone;
        }
    }
}
