﻿using System;
using System.Collections.Generic;
using FluentValidation;
using log4net;
using WexHealth.Domain.Entities;
using WexHealth.Domain.Validators;
using WexHealth.BLL.Exceptions;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.BLL.Services
{
    public class EnrollmentService : IEnrollmentReadService, IEnrollmentManageService
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(EnrollmentService));

        private Guid _consumerId;
        private Guid _administratorId;
        private string _loginType;

        private readonly IAdministratorService _administratorService;
        private readonly IConsumerService _consumerService;
        private readonly IConsumerValidateService _consumerValidateService;

        private readonly IEnrollmentRepository _enrollmentRepository;
        private readonly IBenefitsOfferingRepository _benefitOfferingRepository;
        private readonly IContributionRepository _contributionRepository;

        public EnrollmentService(
            IAdministratorService administratorService,
            IConsumerService consumerService,
            IConsumerValidateService consumerValidateService,
            IEnrollmentRepository enrollmentRepository,
            IBenefitsOfferingRepository benefitOfferingRepository,
            IContributionRepository contributionRepository)
        {
            _administratorService = administratorService;
            _consumerService = consumerService;
            _consumerValidateService = consumerValidateService;

            _enrollmentRepository = enrollmentRepository;
            _benefitOfferingRepository = benefitOfferingRepository;
            _contributionRepository = contributionRepository;
        }

        public void InitService(User user)
        {
            if (null == user) throw new ArgumentNullException(nameof(user));

            _loginType = user.LoginType;

            if (_loginType.Equals("Consumer"))
            {
                Consumer consumer = _consumerService.GetConsumerByUser(user);
                _consumerId = consumer.Id;
            }
            else if (_loginType.Equals("Administrator"))
            {
                Administrator administrator = _administratorService.GetAdministratorByUser(user);
                _administratorId = administrator.Id;
            }
        }

        public void InitService(User user, Guid consumerId)
        {
            if (Guid.Empty.Equals(consumerId)) throw new ArgumentNullException(nameof(consumerId));

            InitService(user);

            if (_loginType.Equals("Administrator"))
            {
                _consumerValidateService.TryValidateByAdministrator(consumerId, _administratorId);
                _consumerId = consumerId;
            }
        }

        public Enrollment GetEnrollment(int id)
        {
            Enrollment result = null;

            if (id < 0) throw new ArgumentOutOfRangeException(nameof(id));

            try
            {
                result = _enrollmentRepository.GetEnrollment(id);
                if (null == result)
                {
                    throw new EntityNotFoundException("Enrollment", "id", id.ToString());
                }

                if (!result.ConsumerId.Equals(_consumerId))
                {
                    throw new InvalidOperationException($"Enrollment with id: {id}, doesn't relate to consumer with id: {_consumerId}");
                }
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("Enrollment", "Get", e);
            }

            return result;
        }

        IEnumerable<Enrollment> IEnrollmentReadService.GetEnrollments()
        {
            IEnumerable<Enrollment> result = null;

            try
            {
                result = _enrollmentRepository.GetEnrollments(_consumerId);
                JoinContibutions(result);
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("Enrollment", "Get", e);
            }

            return result;
        }

        IEnumerable<Enrollment> IEnrollmentReadService.GetEnrollmentsByPackageId(int packageId)
        {
            IEnumerable<Enrollment> result = null;

            try
            {
                result = _enrollmentRepository.GetEnrollmentsByBenefitsPackageId(packageId);
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("Enrollment", "GetEnrollmentsByPackageId", e);
            }

            return result;
        }

        IEnumerable<Enrollment> IEnrollmentReadService.GetActiveEnrollments()
        {
            IEnumerable<Enrollment> result = null;

            try
            {
                result = _enrollmentRepository.GetActiveEnrollments(_consumerId, "Initialized");
                JoinContibutions(result);
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("Enrollment", "Get", e);
            }

            return result;
        }

        int IEnrollmentManageService.CreateEnrollment(Enrollment enrollment)
        {
            int result = 0;

            if (null == enrollment) throw new ArgumentNullException(nameof(enrollment));

            try
            {
                BenefitsOffering dbPlan = _benefitOfferingRepository.GetBenefitsOfferingById(enrollment.BenefitsOfferingId);
                if (null == dbPlan)
                {
                    throw new EntityNotFoundException("BenefitOffering", "id", enrollment.BenefitsOfferingId.ToString());
                }

                Enrollment dbEnrollment = _enrollmentRepository.GetEnrollment(dbPlan.Id, _consumerId);
                if (null != dbEnrollment)
                {
                    throw new InvalidOperationException("Consumer have been already enrolled to this plan");
                }

                enrollment.Id = 0;
                enrollment.ConsumerId = _consumerId;

                var validator = new EnrollmentValidator();
                validator.ValidateAndThrow(enrollment);

                result = _enrollmentRepository.SaveEnrollment(enrollment);
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("Enrollment", "Create", e);
            }

            return result;
        }

        void IEnrollmentManageService.UpdateEnrollment(Enrollment enrollment)
        {
            if (null == enrollment) throw new ArgumentNullException(nameof(enrollment));

            try
            {
                Enrollment dbEnrollment = _enrollmentRepository.GetEnrollment(enrollment.Id);
                if (null == dbEnrollment)
                {
                    throw new EntityNotFoundException("Enrollment", "id", enrollment.Id.ToString());
                }

                if (!dbEnrollment.ConsumerId.Equals(_consumerId))
                {
                    throw new InvalidOperationException("Enrollment doesn't relate to current consumer");
                }

                BenefitsOffering dbPlan = _benefitOfferingRepository.GetBenefitsOfferingById(enrollment.BenefitsOfferingId);
                if (null == dbPlan)
                {
                    throw new EntityNotFoundException("BenefitOffering", "id", enrollment.BenefitsOfferingId.ToString());
                }

                if (!dbEnrollment.BenefitsOfferingId.Equals(dbPlan.Id))
                {
                    Enrollment dbEntity = _enrollmentRepository.GetEnrollment(dbPlan.Id, _consumerId);
                    if (null != dbEntity)
                    {
                        throw new InvalidOperationException("Consumer have been already enrolled to this plan");
                    }
                }

                dbEnrollment.BenefitsOfferingId = enrollment.BenefitsOfferingId;
                dbEnrollment.ElectionAmount = enrollment.ElectionAmount;

                var validator = new EnrollmentValidator();
                validator.ValidateAndThrow(dbEnrollment);

                _enrollmentRepository.SaveEnrollment(dbEnrollment);
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("Enrollment", "Update", e);
            }
        }

        private void JoinContibutions(IEnumerable<Enrollment> enrollments)
        {
            foreach (var i in enrollments)
            {
                i.Contributions = _contributionRepository.GetListByEnrollment(i.Id);
            }
        }
    }
}
