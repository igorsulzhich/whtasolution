﻿using System;
using System.Linq;
using System.Collections.Generic;
using PostSharp.Patterns.Contracts;
using log4net;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Models;
using WexHealth.BLL.Exceptions;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.DAL.Repositories.Common.Interfaces;
using WexHealth.DAL.Models;

namespace WexHealth.BLL.Services
{
    /// <summary>
    /// Manage consumers and their users
    /// </summary>
    public class ConsumerService : IConsumerService
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ConsumerService));

        private const string _loginType = "Consumer";
        private Guid _employerId;
        private Guid _administratorId;

        private readonly IDomainService _domainService;

        private readonly IConsumerRepository _consumerRepository;
        private readonly IEmployerRepository _employerRepository;
        private readonly IEmployerUserRepository _employerUserRepository;
        private readonly IAdministratorRepository _administratorRepository;
        private readonly IAdministratorUserRepository _administratorUserRepository;

        public User ObjectUser
        {
            set
            {
                switch (value.LoginType)
                {
                    case "Administrator":
                        InitAdministrator(value.Id);
                        break;
                    case "Employer":
                        InitEmployer(value.Id);
                        break;
                }
            }
        }

        public ConsumerService(IDomainService domainService, IConsumerRepository consumerRepository, IEmployerRepository employerRepository,
            IEmployerUserRepository employerUserRepository, IAdministratorRepository administratorRepository, IAdministratorUserRepository administratorUserRepository)
        {
            _domainService = domainService;

            _consumerRepository = consumerRepository;
            _employerRepository = employerRepository;
            _employerUserRepository = employerUserRepository;
            _administratorRepository = administratorRepository;
            _administratorUserRepository = administratorUserRepository;
        }

        private void InitAdministrator(Guid userId)
        {
            AdministratorUserDAL dbUser = _administratorUserRepository.GetUser(userId);
            if (null == dbUser)
            {
                throw new InvalidOperationException($"User with id: {userId}, doesn't relate to administrator user");
            }

            _administratorId = dbUser.AdministratorId;
        }

        private void InitEmployer(Guid userId)
        {
            EmployerUserDAL dbUser = _employerUserRepository.GetUser(userId);
            if (null == dbUser)
            {
                throw new InvalidOperationException($"User with id: {userId}, doesn't relate to employer user");
            }

            EmployerDAL dbEmployer = _employerRepository.GetEmployerById(dbUser.EmployerId);
            if (null == dbEmployer)
            {
                throw new EntityNotFoundException("Employer", "id", dbUser.EmployerId.ToString());
            }

            _employerId = dbEmployer.Id;
            _administratorId = dbEmployer.AdministratorId;
        }

        /// <summary>
        /// Get consumer related to employers of current administrator
        /// </summary>
        /// <param name="id">Consumer id</param>
        /// <param name="administratorId">Current administrator id</param>
        /// <returns>Consumer entity</returns>
        public Consumer GetConsumerById(Guid id)
        {
            Consumer result = null;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            try
            {
                ConsumerDAL dbConsumer = _consumerRepository.GetConsumerById(id);
                if (null == dbConsumer)
                {
                    throw new EntityNotFoundException("Consumer", "id", id.ToString());
                }

                EmployerDAL dbEmployer = _employerRepository.GetEmployerById(dbConsumer.EmployerId);
                if (null == dbConsumer)
                {
                    throw new EntityNotFoundException("Employer", "id", dbConsumer.EmployerId.ToString());
                }

                AdministratorDAL dbAdministrator = _administratorRepository.GetAdministratorById(_administratorId);
                if (null == dbAdministrator)
                {
                    throw new EntityNotFoundException("Administrator", "id", _administratorId.ToString());
                }

                if (!dbAdministrator.Id.Equals(dbEmployer.AdministratorId))
                {
                    throw new InvalidOperationException("Consumer's employer doesn't related to current administrator");
                }

                result = new Consumer();
                Map(dbConsumer, result);
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("Consumer", "getById", exception);
            }

            return result;
        }

        /// <summary>
        /// Get consumers by current employer id
        /// </summary>
        /// <param name="id">Current employer id</param>
        /// <returns></returns>
        public IEnumerable<Consumer> GetConsumerByEmployer(Guid id, Guid administratorId)
        {
            List<Consumer> result = null;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            if (Guid.Empty.Equals(administratorId)) throw new ArgumentNullException(nameof(administratorId));

            try
            {
                EmployerDAL dbEmployer = _employerRepository.GetEmployerById(id);
                if (null == dbEmployer)
                {
                    throw new EntityNotFoundException("Employer", "id", id.ToString());
                }

                AdministratorDAL dbAdministrator = _administratorRepository.GetAdministratorById(administratorId);
                if (null == dbAdministrator)
                {
                    throw new EntityNotFoundException("Administrator", "id", administratorId.ToString());
                }

                if (!dbAdministrator.Id.Equals(dbEmployer.AdministratorId))
                {
                    throw new InvalidOperationException("Employer doesn't related to current administrator");
                }

                IEnumerable<ConsumerDAL> dbConsumers = _consumerRepository.GetConsumerByEmployer(dbEmployer.Id);
                if (null == dbConsumers)
                {
                    throw new EntityNotFoundException("Consumer", "EmployerId", dbEmployer.Id.ToString());
                }

                result = new List<Consumer>();
                Consumer consumer = null;

                foreach (var item in dbConsumers)
                {
                    consumer = new Consumer();
                    Map(item, consumer);
                    result.Add(consumer);
                }
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("Consumer", "GetConsumerByEmployer", exception);
            }

            return result;
        }

        public IEnumerable<Consumer> SearchConsumers(string firstName, string lastName, string employerCode, string ssn)
        {
            if (!string.IsNullOrEmpty(employerCode))
            {
                EmployerDAL dbEmployer = _employerRepository.GetEmployerByCode(employerCode);
                if (null == dbEmployer)
                {
                    throw new EntityNotFoundException("Employer", "code", employerCode);
                }

                return SearchConsumers(firstName, lastName, dbEmployer.Id);
            }

            return SearchConsumers(firstName, lastName, Guid.Empty);
        }

        public IEnumerable<Consumer> SearchConsumers(string firstName, string lastName, string ssn)
        {
            return SearchConsumers(firstName, lastName, ssn, _employerId);
        }

        public IEnumerable<Consumer> SearchConsumers(string firstName, string lastName, Guid employerId)
        {
            return SearchConsumers(firstName, lastName, null, employerId);
        }

        private IEnumerable<Consumer> SearchConsumers(string firstName, string lastName, string ssn, Guid employerId)
        {
            IEnumerable<Consumer> result = null;

            AdministratorDAL dbAdministrator = _administratorRepository.GetAdministratorById(_administratorId);
            if (null == dbAdministrator)
            {
                throw new EntityNotFoundException("Administrator", "id", _administratorId.ToString());
            }

            try
            {
                IEnumerable<ConsumerDAL> dbConsumers = _consumerRepository.GetConsumersByIndividualData(firstName, lastName, ssn, employerId, _administratorId);
                result = Map(dbConsumers);
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("Consumers", "search", exception);
            }

            return result;
        }

        public Consumer GetConsumerByUser([NotNull] User user)
        {
            Consumer result = null;

            try
            {
                if (user.LoginType.Equals("Consumer"))
                {
                    ConsumerDAL dbConsumer = _consumerRepository.GetConsumerByIndividualId(user.IndividualId);
                    if (null == dbConsumer)
                    {
                        throw new EntityNotFoundException("Consumer", "individualId", user.IndividualId.ToString());
                    }

                    result = new Consumer();
                    Map(dbConsumer, result);
                }
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("Consumer", "Get", e);
            }

            return result;
        }

        /// <summary>
        /// Create consumer to employer by current administrator
        /// </summary>
        /// <param name="model">Consumer model</param>
        /// <param name="employerId">Employer id</param>
        /// <param name="administratorId">Current administrator id</param>
        /// <returns>Id of inserted consumer</returns>
        public Guid CreateConsumer([NotNull] ConsumerInputModel model, Guid individualId, Guid employerId)
        {
            Guid result = Guid.Empty;

            if (Guid.Empty.Equals(individualId)) throw new ArgumentNullException(nameof(individualId));

            if (Guid.Empty.Equals(employerId)) throw new ArgumentNullException(nameof(employerId));

            try
            {
                System.ComponentModel.DataAnnotations.Validator.ValidateObject(model,
                    new System.ComponentModel.DataAnnotations.ValidationContext(model, null, null));


                EmployerDAL dbEmployer = _employerRepository.GetEmployerById(employerId);
                if (null == dbEmployer)
                {
                    throw new EntityNotFoundException("Employer", "id", employerId.ToString());
                }

                ConsumerDAL dbConsumer = _consumerRepository.GetConsumerBySSN(model.SSN);
                if (null != dbConsumer)
                {
                    throw new EntityAlreadyExistsException("Consumer", "SSN", model.SSN);
                }

                var consumer = new ConsumerDAL
                {
                    SSN = model.SSN,
                    EmployerId = dbEmployer.Id,
                    IndividualId = individualId
                };

                result = _consumerRepository.SaveConsumer(consumer);

                _domainService.SetAddressToObject(model.Address, result, _loginType);
                _domainService.SetPhoneToObject(model.Phone, result, _loginType);

                log.Info($"Create new consumer with id: {result}, to employer with id: {employerId}");
            }
            catch (Exception exception) when (exception is EntityNotFoundException || exception is EntityAlreadyExistsException ||
            exception is System.ComponentModel.DataAnnotations.ValidationException)
            {
                throw;
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("Consumer", "create", exception);
            }

            return result;
        }

        /// <summary>
        /// Update consumer to employer by current administrator
        /// </summary>
        /// <param name="model">Consumer model</param>
        /// <param name="employerId">Employer id</param>
        /// <param name="administratorId">Current administrator id</param>
        /// <returns></returns>
        public void UpdateConsumer([NotNull] ConsumerInputModel model, Guid employerId, Guid administratorId)
        {
            if (Guid.Empty.Equals(employerId)) throw new ArgumentNullException(nameof(employerId));

            if (Guid.Empty.Equals(administratorId)) throw new ArgumentNullException(nameof(administratorId));

            try
            {
                System.ComponentModel.DataAnnotations.Validator.ValidateObject(model,
                    new System.ComponentModel.DataAnnotations.ValidationContext(model, null, null));

                AdministratorDAL dbAdministrator = _administratorRepository.GetAdministratorById(administratorId);
                if (null == dbAdministrator)
                {
                    throw new EntityNotFoundException("Administrator", "id", administratorId.ToString());
                }

                EmployerDAL dbEmployer = _employerRepository.GetEmployerById(employerId);
                if (null == dbEmployer)
                {
                    throw new EntityNotFoundException("Employer", "id", employerId.ToString());
                }

                if (!dbAdministrator.Id.Equals(dbEmployer.AdministratorId))
                {
                    throw new InvalidOperationException("Employer doesn't related to current administrator");
                }

                ConsumerDAL dbConsumer = _consumerRepository.GetConsumerById(model.Id);
                if (null == dbConsumer)
                {
                    throw new EntityNotFoundException("Consumer", "id", model.Id.ToString());
                }

                if (!dbEmployer.Id.Equals(dbConsumer.EmployerId))
                {
                    throw new InvalidOperationException("Consumer doesn't related to current employer");
                }

                ConsumerDAL checkEntity = _consumerRepository.GetConsumerBySSN(model.SSN);
                if (null != checkEntity && !dbConsumer.Id.Equals(checkEntity.Id))
                {
                    throw new EntityAlreadyExistsException("Consumer", "SSN", model.SSN);
                }

                dbConsumer.SSN = model.SSN;

                _domainService.UpdateAddressToObject(model.Address, dbConsumer.Id, _loginType);
                _domainService.UpdatePhoneToObject(model.Phone, dbConsumer.Id, _loginType);

                _consumerRepository.SaveConsumer(dbConsumer);

                log.Info($"Update consumer with id: {model.Id}, to employer with id: {employerId}, by administrator with id: {administratorId}");
            }
            catch (Exception exception) when (exception is EntityNotFoundException || exception is EntityAlreadyExistsException ||
            exception is System.ComponentModel.DataAnnotations.ValidationException || exception is InvalidOperationException)
            {
                throw;
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("Consumer", "update", exception);
            }
        }


        //DAL Model -> Domain Entity
        private IEnumerable<Consumer> Map([NotNull] IEnumerable<ConsumerDAL> consumers)
        {
            foreach (var consumerDAL in consumers)
            {
                Consumer consumer = new Consumer();
                Map(consumerDAL, new List<Address>(), new List<Phone>(), consumer);

                yield return consumer;
            }
        }

        private void Map([NotNull] ConsumerDAL consumerDAL, [NotNull] Consumer consumer)
        {
            IEnumerable<Address> addresses = _domainService.GetAddressesByObject(consumerDAL.Id, _loginType);
            IEnumerable<Phone> phones = _domainService.GetPhonesByObject(consumerDAL.Id, _loginType);

            Map(consumerDAL, addresses, phones, consumer);
        }

        private void Map([NotNull] ConsumerDAL consumerDAL, [NotNull] IEnumerable<Address> addresses, [NotNull] IEnumerable<Phone> phones, [NotNull] Consumer consumer)
        {
            consumer.Id = consumerDAL.Id;
            consumer.SSN = consumerDAL.SSN;
            consumer.IndividualId = consumerDAL.IndividualId;
            consumer.EmployerId = consumerDAL.EmployerId;

            if (null != consumerDAL.Individual)
            {
                consumer.Individual = new Individual
                {
                    Id = consumerDAL.Individual.Id,
                    Email = consumerDAL.Individual.Email,
                    DateBirth = consumerDAL.Individual.DateBirth,
                    FirstName = consumerDAL.Individual.FirstName,
                    LastName = consumerDAL.Individual.LastName
                };
            }

            if (null != consumerDAL.Employer)
            {
                consumer.Employer = new Employer
                {
                    Id = consumerDAL.Employer.Id,
                    Name = consumerDAL.Employer.Name,
                    Code = consumerDAL.Employer.Code
                };
            }

            Address address = addresses.FirstOrDefault();
            if (null != address)
            {
                consumer.Address = new Address
                {
                    Id = address.Id,
                    Street = address.Street,
                    City = address.City,
                    State = address.State,
                    ZipCode = address.ZipCode
                };
            }

            Phone phone = phones.FirstOrDefault();
            if (null != phone)
            {
                consumer.Phone = new Phone
                {
                    Id = phone.Id,
                    PhoneNumber = phone.PhoneNumber
                };
            }
        }
    }
}
