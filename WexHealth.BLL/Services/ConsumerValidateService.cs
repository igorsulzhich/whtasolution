﻿using System;
using System.Linq;
using System.Collections.Generic;
using log4net;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Models;
using WexHealth.BLL.Exceptions;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.DAL.Repositories.Common.Interfaces;
using WexHealth.DAL.Models;

namespace WexHealth.BLL.Services
{
    public class ConsumerValidateService : IConsumerValidateService
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ConsumerValidateService));

        private readonly IAdministratorRepository _administratorRepository;
        private readonly IEmployerRepository _employerRepository;
        private readonly IConsumerRepository _consumerRepository;

        public ConsumerValidateService(IAdministratorRepository administratorRepository, IEmployerRepository employerRepository,
            IConsumerRepository consumerRepository)
        {
            _administratorRepository = administratorRepository;
            _employerRepository = employerRepository;
            _consumerRepository = consumerRepository;
        }

        public bool ValidateByEmployer(Guid consumerId, Guid employerId)
        {
            bool result = false;

            if (Guid.Empty.Equals(consumerId)) throw new ArgumentNullException(nameof(consumerId));

            if (Guid.Empty.Equals(employerId)) throw new ArgumentNullException(nameof(employerId));

            try
            {
                ConsumerDAL dbConsumer = _consumerRepository.GetConsumerById(consumerId);
                if (null == dbConsumer)
                {
                    throw new EntityNotFoundException("Consumer", "id", consumerId.ToString());
                }

                EmployerDAL dbEmployer = _employerRepository.GetEmployerById(employerId);
                if (null == dbEmployer)
                {
                    throw new EntityNotFoundException("Employer", "id", employerId.ToString());
                }

                result = dbEmployer.Id.Equals(dbConsumer.EmployerId);
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("Consumer", "Validate", e);
            }

            return result;
        }

        public bool ValidateByAdministrator(Guid consumerId, Guid administratorId)
        {
            bool result = false;

            if (Guid.Empty.Equals(consumerId)) throw new ArgumentNullException(nameof(consumerId));

            if (Guid.Empty.Equals(administratorId)) throw new ArgumentNullException(nameof(administratorId));

            try
            {
                ConsumerDAL dbConsumer = _consumerRepository.GetConsumerById(consumerId);
                if (null == dbConsumer)
                {
                    throw new EntityNotFoundException("Consumer", "id", consumerId.ToString());
                }

                AdministratorDAL dbAdministrator = _administratorRepository.GetAdministratorById(administratorId);
                if (null == dbAdministrator)
                {
                    throw new EntityNotFoundException("Administrator", "id", administratorId.ToString());
                }

                EmployerDAL dbEmployer = _employerRepository.GetEmployerById(dbConsumer.EmployerId);
                if (null == dbEmployer)
                {
                    throw new EntityNotFoundException("Employer", "id", dbConsumer.EmployerId.ToString());
                }

                result = dbAdministrator.Id.Equals(dbEmployer.AdministratorId);
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("Consumer", "Validate", e);
            }

            return result;
        }

        public void TryValidateByEmployer(Guid consumerId, Guid employerId)
        {
            bool result = ValidateByEmployer(consumerId, employerId);

            if (!result)
            {
                throw new InvalidOperationException($"Consumer with id: {consumerId}, doesn't relate to employer with id: {employerId}");
            }
        }

        public void TryValidateByAdministrator(Guid consumerId, Guid administratorId)
        {
            bool result = ValidateByAdministrator(consumerId, administratorId);

            if (!result)
            {
                throw new InvalidOperationException($"Consumer with id: {consumerId}, doesn't relate to administrator with id: {administratorId}");
            }
        }
    }
}
