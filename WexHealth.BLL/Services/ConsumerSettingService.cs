﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac.Extras.Attributed;
using WexHealth.BLL.Exceptions;
using WexHealth.BLL.Services.Common;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.DAL.Models;
using WexHealth.DAL.Repositories.Common.Interfaces;
using WexHealth.Domain.Entities;

namespace WexHealth.BLL.Services
{
    public class ConsumerSettingService : SettingServiceBase, ISettingReadService
    {
        private readonly ISettingReadService _administratorSettingService;
        private readonly IEmployerService _employerService;
        private readonly IConsumerService _consumerService;

        private readonly ISettingRepository _settingRepository;

        private Guid _consumerId;
        private Guid _employerId;
        private Guid _administratorId;
        private string _loginType;

        protected override LevelType ObjectType
        {
            get
            {
                return LevelType.Consumer;
            }
        }

        protected override IEnumerable<KeyValuePair<Guid, string>> ObjectHierarchy
        {
            get
            {
                return new Dictionary<Guid, string>()
                {
                    { _consumerId, "Consumer" },
                    { _employerId, "Employer" },
                    { _administratorId, "Administrator"},
                    { Guid.Empty, "Global" }
                };
            }
        }

        public ConsumerSettingService([WithKey("Administrator")] ISettingReadService administratorSettingService, IEmployerService employerService,
            IConsumerService consumerService, ISettingRepository settingRepository, ISettingServiceContext context) : base(context)
        {
            _administratorSettingService = administratorSettingService;
            _employerService = employerService;
            _consumerService = consumerService;

            _settingRepository = settingRepository;
        }

        public void InitService(User user)
        {
            if (null == user) throw new ArgumentNullException(nameof(user));

            _loginType = user.LoginType;

            _administratorSettingService.InitService(user);

            if (_loginType.Equals("Consumer"))
            {
                Consumer consumer = _consumerService.GetConsumerByUser(user);
                _consumerId = consumer.Id;
                _employerId = consumer.EmployerId;

                Employer employer = _employerService.GetEmployerByUser(user);
                _administratorId = employer.AdministratorId;
            }
        }

        /// <summary>
        /// Get consumer's allow settings on Consumer level
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Setting> GetSettings()
        {
            IEnumerable<Setting> result = null;

            IEnumerable<Setting> allowSettings = _administratorSettingService.GetSettings();

            var settings = new List<SettingDAL>();
            foreach (var setting in allowSettings)
            {
                IEnumerable<SettingDAL> childSettings = _settingRepository.GetSettingsByParent(setting.Id);
                settings.AddRange(childSettings);
            }

            result = Map(settings).ToList();
            base.FillSettings(result, _consumerId, true);

            return result.Where(x => x.isAllowed.Value);
        }
    }
}
