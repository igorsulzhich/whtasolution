﻿using System;
using System.Collections.Generic;
using log4net;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Exceptions;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.DAL.Models;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.BLL.Services
{
    public class BenefitsOfferingService : IBenefitsOfferingReadService, IBenefitsOfferingManageService
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(BenefitsOfferingService));

        private Guid _consumerId;
        private Guid _employerId;
        private Guid _administratorId;
        private string _loginType;

        private readonly IAdministratorService _administratorService;
        private readonly IEmployerValidateService _employerValidateService;
        private readonly IConsumerService _consumerService;
        private readonly IConsumerValidateService _consumerValidateService;

        private readonly IBenefitsOfferingRepository _benefitsOfferingRepository;
        private readonly IBenefitsPackageRepository _benefitsPakageRepository;
        private readonly IPlanTypeRepository _planTypeRepository;
        private readonly IEmployerRepository _employerRepository;
        private readonly IConsumerRepository _consumerRepository;

        public BenefitsOfferingService(IAdministratorService administratorService, IEmployerValidateService employerValidateService, IConsumerService consumerService,
            IConsumerValidateService consumerValidateService, IBenefitsOfferingRepository benefitsOfferingRepository, IBenefitsPackageRepository benefitsPakageRepository,
            IPlanTypeRepository planTypeRepository, IEmployerRepository employerRepository, IConsumerRepository consumerRepository)
        {
            _administratorService = administratorService;
            _employerValidateService = employerValidateService;
            _consumerService = consumerService;
            _consumerValidateService = consumerValidateService;

            _benefitsOfferingRepository = benefitsOfferingRepository;
            _benefitsPakageRepository = benefitsPakageRepository;
            _planTypeRepository = planTypeRepository;
            _employerRepository = employerRepository;
            _consumerRepository = consumerRepository;
        }

        public void InitService(User user)
        {
            if (null == user) throw new ArgumentNullException(nameof(user));

            _loginType = user.LoginType;

            if (_loginType.Equals("Consumer"))
            {
                Consumer consumer = _consumerService.GetConsumerByUser(user);
                _consumerId = consumer.Id;
                _employerId = consumer.EmployerId;
            }
            else if (_loginType.Equals("Administrator"))
            {
                Administrator administrator = _administratorService.GetAdministratorByUser(user);
                _administratorId = administrator.Id;
            }
        }

        public void InitService(User user, string employerCode)
        {
            if (string.Empty.Equals(employerCode)) throw new ArgumentNullException(nameof(employerCode));

            InitService(user);

            if (_loginType.Equals("Administrator"))
            {
                EmployerDAL dbEmployer = _employerRepository.GetEmployerByCode(employerCode);
                if (null == dbEmployer)
                {
                    throw new EntityNotFoundException("Employer", "code", employerCode);
                }

                _employerValidateService.TryValidateByAdministrator(dbEmployer.Id, _administratorId);
                _employerId = dbEmployer.Id;
            }
        }

        public void InitService(User user, Guid consumerId)
        {
            if (Guid.Empty.Equals(consumerId)) throw new ArgumentNullException(nameof(consumerId));

            InitService(user);

            if (_loginType.Equals("Administrator"))
            {
                ConsumerDAL dbConsumer = _consumerRepository.GetConsumerById(consumerId);
                _consumerValidateService.TryValidateByAdministrator(consumerId, _administratorId);
                _consumerId = dbConsumer.Id;
                _employerId = dbConsumer.EmployerId;
            }
        }

        IEnumerable<BenefitsOffering> IBenefitsOfferingReadService.GetBenefitsOfferings()
        {
            IEnumerable<BenefitsOffering> result = null;

            try
            {
                result = _benefitsOfferingRepository.GetBenefitsOfferingByEmployer(_employerId);
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("BenefitsOffering", "GetBenefitsOffering", e);
            }

            return result;
        }

        BenefitsOffering IBenefitsOfferingReadService.GetBenefitsOfferingById(int id)
        {
            if (id == 0) throw new ArgumentNullException(nameof(id));

            BenefitsOffering result = null;

            try
            {
                result = _benefitsOfferingRepository.GetBenefitsOfferingById(id);
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("BenefitsOffering", "GetBenefitsOfferingById", e);
            }

            return result;
        }

        IEnumerable<BenefitsOffering> IBenefitsOfferingReadService.GetBenefitsOfferingByPackageId(int benefitsPackageId)
        {
            IEnumerable<BenefitsOffering> result = null;

            BenefitsPackage dbPackage = _benefitsPakageRepository.GetPackageById(benefitsPackageId);
            if (null == dbPackage)
            {
                throw new EntityNotFoundException("BenefitsPackage", "id", benefitsPackageId.ToString());
            }

            try
            {
                result = _benefitsOfferingRepository.GetBenefitsOfferingByBenefitsPackageId(benefitsPackageId);
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("BenefitsOffering", "GetBenefitsOfferingByPackageId", e);
            }

            return result;
        }


        int IBenefitsOfferingManageService.CreateBenefitsOffering(BenefitsOffering benefitsOffering)
        {
            int result = 0;

            if (null == benefitsOffering) throw new ArgumentNullException(nameof(benefitsOffering));

            BenefitsPackage dbBenefitsPackage = _benefitsPakageRepository.GetPackageById(benefitsOffering.BenefitsPackageId);
            if (null == dbBenefitsPackage)
            {
                throw new EntityNotFoundException("BenefitsPackage", "id", benefitsOffering.BenefitsPackageId.ToString());
            }

            if (!dbBenefitsPackage.EmployerId.Equals(_employerId))
            {
                throw new InvalidOperationException($"BenefitsPackage with id: {dbBenefitsPackage.Id}, doesn't relate to employer with id: {_employerId}");
            }

            if (dbBenefitsPackage.Status.Name.Equals("Initialized"))
            {
                throw new InvalidOperationException($"BenefitsPackage with id: {dbBenefitsPackage.Id} initialized on {dbBenefitsPackage.DateOfInitialized}");
            }

            try
            {
                PlanType dbPlan = _planTypeRepository.GetPlanTypeByName(benefitsOffering.PlanType.Name);

                if (null == dbPlan)
                {
                    throw new EntityNotFoundException("PlanType", "Name", benefitsOffering.PlanType.Name);
                }

                benefitsOffering.PlanTypeId = dbPlan.Id;
                result = _benefitsOfferingRepository.SaveBenefitsOffering(benefitsOffering);
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("BenefitsOffering", "SaveBenefitsOffering", e);
            }

            return result;
        }

        int IBenefitsOfferingManageService.UpdateBenefitsOffering(BenefitsOffering inputOffering)
        {
            int result = 0;

            if (null == inputOffering) throw new ArgumentNullException(nameof(inputOffering));

            if (inputOffering.Id == 0) throw new ArgumentNullException(nameof(inputOffering.Id));

            BenefitsOffering dbBenefitsOffering = _benefitsOfferingRepository.GetBenefitsOfferingById(inputOffering.Id);
            if (null == dbBenefitsOffering)
            {
                throw new EntityNotFoundException("BenefitsOffering", "id", inputOffering.Id.ToString());
            }

            BenefitsPackage dbBenefitsPackage = _benefitsPakageRepository.GetPackageById(inputOffering.BenefitsPackageId);
            if (null == dbBenefitsPackage)
            {
                throw new EntityNotFoundException("BenefitsPackage", "id", inputOffering.BenefitsPackageId.ToString());
            }

            if (!dbBenefitsPackage.EmployerId.Equals(_employerId))
            {
                throw new InvalidOperationException($"BenefitsPackage with id: {dbBenefitsPackage.Id}, doesn't relate to employer with id: {_employerId}");
            }

            if (!inputOffering.ContributionAmount.Equals(dbBenefitsOffering.ContributionAmount) && dbBenefitsPackage.DateOfStart <= DateTime.Now)
            {
                throw new InvalidOperationException($"Field {nameof(dbBenefitsOffering.ContributionAmount)} of id {dbBenefitsPackage.Id} cannot be changed for the plan year which started");
            }

            try
            {
                dbBenefitsOffering.Name = inputOffering.Name;
                dbBenefitsOffering.ContributionAmount = inputOffering.ContributionAmount;

                result = _benefitsOfferingRepository.SaveBenefitsOffering(dbBenefitsOffering);
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("BenefitsOffering", "SaveBenefitsOffering", e);
            }

            return result;
        }

        IEnumerable<PlanType> IBenefitsOfferingReadService.GetPlanTypes()
        {
            IEnumerable<PlanType> result = null;

            try
            {
                result = _planTypeRepository.GetPlanTypes();
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("PlanType", "Get", exception);
            }

            return result;
        }

        PlanType IBenefitsOfferingReadService.GetPlanTypeByName(string name)
        {
            if (String.Empty.Equals(nameof(name))) throw new ArgumentNullException(nameof(name));

            PlanType result = null;

            try
            {
                result = _planTypeRepository.GetPlanTypeByName(name);
                if (null == result)
                {
                    throw new EntityNotFoundException("PlanType", "name", name);
                }

                log.Info($"Get plan type with name: {name}");
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("PlanType", "Get", exception);
            }

            return result;
        }
    }
}
