﻿using System;
using System.Linq;
using System.Collections.Generic;
using Autofac.Extras.Attributed;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Exceptions;
using WexHealth.BLL.Models;
using WexHealth.BLL.Services.Common;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.DAL.Models;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.BLL.Services
{
    public class EmployerSettingService : SettingServiceBase, ISettingService, ISettingCustomizeService
    {
        private readonly ISettingReadService _administratorSettingService;
        private readonly IAdministratorService _administratorService;
        private readonly IEmployerService _employerService;
        private readonly IEmployerValidateService _employerValidateService;

        private readonly ISettingRepository _settingRepository;

        private Guid _employerId;
        private Guid _administratorId;
        private string _loginType;

        protected override LevelType ObjectType
        {
            get
            {
                return LevelType.Employer;
            }
        }

        protected override IEnumerable<KeyValuePair<Guid, string>> ObjectHierarchy
        {
            get
            {
                return new Dictionary<Guid, string>()
                {
                    { _employerId, "Employer" },
                    { _administratorId, "Administrator"},
                    { Guid.Empty, "Global" }
                };
            }
        }

        public EmployerSettingService([WithKey("Administrator")] ISettingReadService administratorSettingService, IAdministratorService administratorService,
            IEmployerService employerService, IEmployerValidateService employerValidateService, ISettingRepository settingRepository, ISettingServiceContext context) :
            base(context)
        {
            _administratorSettingService = administratorSettingService;
            _administratorService = administratorService;
            _employerService = employerService;
            _employerValidateService = employerValidateService;

            _settingRepository = settingRepository;
        }

        public void InitService(User user)
        {
            if (null == user) throw new ArgumentNullException(nameof(user));

            _loginType = user.LoginType;

            _administratorSettingService.InitService(user);

            Administrator administrator = _administratorService.GetAdministratorByUser(user);
            _administratorId = administrator.Id;

            if (_loginType.Equals("Employer"))
            {
                Employer employer = _employerService.GetEmployerByUser(user);
                _employerId = employer.Id;
            }
        }

        public void InitService(User user, Guid objectId)
        {
            if (Guid.Empty.Equals(objectId)) throw new ArgumentNullException(nameof(objectId));

            InitService(user);

            if (_loginType.Equals("Administrator"))
            {
                _employerValidateService.TryValidateByAdministrator(objectId, _administratorId);
                _employerId = objectId;
            }
        }

        /// <summary>
        /// Get employer's settigns for editing on Admin & Employer level
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Setting> GetSettings()
        {
            IEnumerable<Setting> result = null;

            IEnumerable<Setting> allowSettings = _administratorSettingService.GetSettings();

            var settings = new List<SettingDAL>();
            foreach (var setting in allowSettings)
            {
                IEnumerable<SettingDAL> childSettings = _settingRepository.GetSettingsByParent(setting.Id);
                settings.AddRange(childSettings);
            }

            result = Map(settings).ToList();
            base.FillSettings(result, _employerId, false);

            return result;
        }

        /// <summary>
        /// Update employer's settings on Admin & Employer level
        /// </summary>
        /// <param name="model"></param>
        public void SaveSettings(IEnumerable<SettingInputModel> model)
        {
            base.UpdateObjectSettings(model, _employerId);
        }
    }
}
