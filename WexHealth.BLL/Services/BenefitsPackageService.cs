﻿using System;
using System.Linq;
using System.Collections.Generic;
using log4net;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Exceptions;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.DAL.Models;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.BLL.Services
{
    public class BenefitsPackageService : IBenefitsPackageService
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(BenefitsPackageService));

        private Guid _employerId;
        private Guid _administratorId;
        private string _loginType;

        private readonly IAdministratorService _administratorService;
        private readonly IEmployerValidateService _employerValidateService;

        private readonly IEmployerRepository _employerRepository;
        private readonly IBenefitsPackageRepository _benefitsPackageRepository;
        private readonly IBenefitsPackageStatusRepository _benefitsPackageStatusRepository;
        private readonly IPayrollFrequencyRepository _payrollFrequencyRepository;
        private readonly IPlanTypeRepository _planTypeRepository;
        private readonly IEnrollmentRepository _enrollmentRepository;

        public BenefitsPackageService(IAdministratorService administratorService, IEmployerValidateService employerValidateService,
            IBenefitsPackageRepository benefitsPackageRepository, IBenefitsPackageStatusRepository benefitsPackageStatusRepository, 
            IPayrollFrequencyRepository payrollFrequencyRepository, IEmployerRepository employerRepository, 
            IPlanTypeRepository planTypeRepository, IEnrollmentRepository enrollmentRepository)
        {
            _administratorService = administratorService;
            _employerValidateService = employerValidateService;

            _employerRepository = employerRepository;
            _benefitsPackageRepository = benefitsPackageRepository;
            _benefitsPackageStatusRepository = benefitsPackageStatusRepository;
            _payrollFrequencyRepository = payrollFrequencyRepository;
            _planTypeRepository = planTypeRepository;
            _enrollmentRepository = enrollmentRepository;
        }

        public void InitService(User user)
        {
            if (null == user) throw new ArgumentNullException(nameof(user));

            _loginType = user.LoginType;

            if (_loginType.Equals("Administrator"))
            {
                Administrator administrator = _administratorService.GetAdministratorByUser(user);
                _administratorId = administrator.Id;
            }
        }

        public void InitService(User user, string employerCode)
        {
            InitService(user);

            if (_loginType.Equals("Administrator"))
            {
                EmployerDAL dbEmployer = _employerRepository.GetEmployerByCode(employerCode);
                if (null == dbEmployer)
                {
                    throw new EntityNotFoundException("Employer", "code", employerCode);
                }

                _employerValidateService.TryValidateByAdministrator(dbEmployer.Id, _administratorId);
                _employerId = dbEmployer.Id;
            }
        }

        public IEnumerable<BenefitsPackage> GetPackages()
        {
            IEnumerable<BenefitsPackage> result = null;

            try
            {
                result = _benefitsPackageRepository.GetPackages(_employerId);
                log.Info($"Get packages for employer with id: {_employerId}");
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("BenefitsPackage", "Get", exception);
            }

            return result;
        }

        public BenefitsPackage GetPackageById(int id)
        {
            BenefitsPackage result = null;

            if (id == 0) throw new ArgumentNullException(nameof(id));

            try
            {
                result = _benefitsPackageRepository.GetPackageById(id);

                if (null == result)
                {
                    throw new EntityNotFoundException("BenefitsPackage", "Id", id.ToString());
                }

                if (!result.EmployerId.Equals(_employerId))
                {
                    throw new InvalidOperationException($"BenefitsPackage with id: {id}, doesn't relate to employer with id: {_employerId}");
                }

                log.Info($"Get package with id: {id}");
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("BenefitsPackage", "Get", exception);
            }

            return result;
        }

        public IEnumerable<PlanType> GetPlanTypes()
        {
            IEnumerable<PlanType> result = null;

            try
            {
                result = _planTypeRepository.GetPlanTypes();
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("PlanType", "Get", exception);
            }

            return result;
        }

        public PlanType GetPlanTypeByName(string name)
        {
            if (String.Empty.Equals(nameof(name))) throw new ArgumentNullException(nameof(name));

            PlanType result = null;

            try
            {
                result = _planTypeRepository.GetPlanTypeByName(name);
                if (null == result)
                {
                    throw new EntityNotFoundException("PlanType", "name", name);
                }

                log.Info($"Get plan type with name: {name}");
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("PlanType", "Get", exception);
            }

            return result;
        }

        public IEnumerable<PayrollFrequency> GetPayrollFrequencies()
        {
            IEnumerable<PayrollFrequency> result = null;

            try
            {
                result = _payrollFrequencyRepository.GetPayrollFrequencies();
                log.Info($"Get payroll frequencies");
            }
            catch (Exception exception)
            {
                log.Error("BLL error", exception);
                throw new EntityOperationFailedException("PayrollFrequency", "Get", exception);
            }

            return result;
        }

        public int Initialize(int id)
        {
            int result = 0;

            if (id == 0) throw new ArgumentNullException(nameof(id));

            try
            {
                BenefitsPackage dbBenefitsPackage = _benefitsPackageRepository.GetPackageById(id);
                if (null == dbBenefitsPackage)
                {
                    throw new EntityNotFoundException("BenefitsPackage", "Id", id.ToString());
                }

                if (dbBenefitsPackage.Status.Name != "Not initialized")
                {
                    throw new InvalidOperationException($"BenefitsPackage with id: {dbBenefitsPackage.Id} already initialized");
                }

                BenefitsPackageStatus status = _benefitsPackageStatusRepository.GetStatusByName("Initialized");
                if (null == status)
                {
                    throw new EntityNotFoundException("BenefitsPackageStatus", "name", "Initialized");
                }

                dbBenefitsPackage.StatusId = status.Id;
                dbBenefitsPackage.DateOfInitialized = DateTime.Now;

                result = _benefitsPackageRepository.SavePackage(dbBenefitsPackage);
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("BenefitsPackage", "Initialize", e);
            }

            return result;
        }

        public int CreatePackage(BenefitsPackage inputPackage)
        {
            int result = 0;

            if (null == inputPackage) throw new ArgumentNullException(nameof(inputPackage));

            try
            {
                BenefitsPackageStatus status = _benefitsPackageStatusRepository.GetStatusByName("Not initialized");

                inputPackage.EmployerId = _employerId;
                inputPackage.StatusId = status.Id;

                result = _benefitsPackageRepository.SavePackage(inputPackage);
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("BenefitsPackage", "Create", e);
            }

            return result;
        }

        public int UpdatePackage(BenefitsPackage inputPackage)
        {
            int result = 0;

            if (inputPackage.Id == 0) throw new ArgumentNullException(nameof(inputPackage.Id));

            try
            {
                BenefitsPackage dbBenefitsPackage = _benefitsPackageRepository.GetPackageById(inputPackage.Id);
                if (null == dbBenefitsPackage)
                {
                    throw new EntityNotFoundException("BenefitsPackage", "Id", inputPackage.Id.ToString());
                }

                if (!dbBenefitsPackage.EmployerId.Equals(_employerId))
                {
                    throw new InvalidOperationException($"BenefitsPackage with id: {dbBenefitsPackage.Id}, doesn't relate to employer with: {_employerId}");
                }

                if (dbBenefitsPackage.Status.Name == "Initialized")
                {
                    if (!dbBenefitsPackage.DateOfStart.Equals(inputPackage.DateOfStart))
                    {
                        throw new InvalidOperationException($"Field {nameof(dbBenefitsPackage.DateOfStart)} of id {dbBenefitsPackage.Id} cannot be changed for the initialized plan year");
                    }

                    if (!dbBenefitsPackage.Frequency.Equals(inputPackage.Frequency))
                    {
                        throw new InvalidOperationException($"Field {nameof(dbBenefitsPackage.Frequency)} of id {dbBenefitsPackage.Id} cannot be changed for the initialized plan year");
                    }

                    if (dbBenefitsPackage.DateOfStart <= DateTime.Now && !dbBenefitsPackage.DateOfEnd.Equals(inputPackage.DateOfEnd))
                    {
                        throw new InvalidOperationException($"Field {nameof(dbBenefitsPackage.DateOfEnd)} of id {dbBenefitsPackage.Id} cannot be changed for the initialized plan year which started");
                    }
                }

                dbBenefitsPackage.Name = inputPackage.Name;
                dbBenefitsPackage.DateOfStart = inputPackage.DateOfStart;
                dbBenefitsPackage.DateOfEnd = inputPackage.DateOfEnd;
                dbBenefitsPackage.Frequency = inputPackage.Frequency;

                result = _benefitsPackageRepository.SavePackage(dbBenefitsPackage);
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("BenefitsPackage", "Create", e);
            }

            return result;
        }

        public bool DeletePackage(int id)
        {
            bool result = false;

            if (id == 0) throw new ArgumentNullException(nameof(id));

            try
            {
                BenefitsPackage dbBenefitsPackage = _benefitsPackageRepository.GetPackageById(id);
                if (null == dbBenefitsPackage)
                {
                    throw new EntityNotFoundException("BenefitsPackage", "Id", id.ToString());
                }

                if (dbBenefitsPackage.DateOfStart <= DateTime.Now)
                {
                    throw new InvalidOperationException($"Plan year was starting on {dbBenefitsPackage.DateOfStart}");
                }

                IEnumerable<Enrollment> dbEnrollments = _enrollmentRepository.GetEnrollmentsByBenefitsPackageId(dbBenefitsPackage.Id);
                if (dbEnrollments.Any())
                {
                    throw new InvalidOperationException($"Plan year with id: {dbBenefitsPackage.Id} have enrollments");
                }

                result = _benefitsPackageRepository.DeletePackage(id);
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("BenefitsPackage", "Initialize", e);
            }

            return result;
        }
    }
}
