﻿using System;
using System.Collections.Generic;
using FluentValidation;
using log4net;
using WexHealth.Domain.Entities;
using WexHealth.Domain.Validators;
using WexHealth.BLL.Exceptions;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.DAL.Models;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.BLL.Services
{
    public class ClaimService : IClaimReadService, IClaimFileService, IClaimManageService
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ClaimService));

        private Guid _consumerId;
        private Guid _administratorId;
        private string _loginType;

        private readonly IAdministratorService _administratorService;
        private readonly IConsumerService _consumerService;
        private readonly IConsumerValidateService _consumerValidateService;

        private readonly IClaimRepository _claimRepository;
        private readonly IClaimStatusRepository _claimStatusRepository;
        private readonly IEnrollmentRepository _enrollmentRepostiory;
        private readonly IBenefitsPackageRepository _benefitsPackageRepository;
        private readonly IEmployerRepository _employerRepository;
        private readonly IConsumerRepository _consumerRepository;

        public ClaimService(IAdministratorService administratorService, IConsumerService consumerService, IConsumerValidateService consumerValidateService,
            IClaimRepository claimRepository, IClaimStatusRepository claimStatusRepository, IEnrollmentRepository enrollmentRepository,
            IBenefitsPackageRepository benefitsPackageRepository, IEmployerRepository employerRepository, IConsumerRepository consumerRepository)
        {
            _administratorService = administratorService;
            _consumerService = consumerService;
            _consumerValidateService = consumerValidateService;

            _claimRepository = claimRepository;
            _claimStatusRepository = claimStatusRepository;
            _enrollmentRepostiory = enrollmentRepository;
            _benefitsPackageRepository = benefitsPackageRepository;
            _employerRepository = employerRepository;
            _consumerRepository = consumerRepository;
        }

        public void InitService(User user)
        {
            if (null == user) throw new ArgumentNullException(nameof(user));

            _loginType = user.LoginType;

            if (_loginType.Equals("Consumer"))
            {
                Consumer consumer = _consumerService.GetConsumerByUser(user);
                _consumerId = consumer.Id;
            }
            else if (_loginType.Equals("Administrator"))
            {
                Administrator administrator = _administratorService.GetAdministratorByUser(user);
                _administratorId = administrator.Id;
            }
        }

        public void InitService(User user, Guid consumerId)
        {
            InitService(user);

            if (_loginType.Equals("Administrator"))
            {
                _consumerValidateService.TryValidateByAdministrator(consumerId, _administratorId);
                _consumerId = consumerId;
            }
        }

        public Claim GetClaim(string claimId)
        {
            Claim result = null;

            if (string.Empty.Equals(claimId)) throw new ArgumentOutOfRangeException(nameof(claimId));

            try
            {
                result = _claimRepository.GetClaim(claimId);
                if (null == result)
                {
                    throw new EntityNotFoundException("Claim", "id", claimId.ToString());
                }

                _consumerValidateService.TryValidateByAdministrator(result.Enrollment.ConsumerId, _administratorId);

                log.Info($"Get claim with id: {claimId}");
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("Claim", "Get", e);
            }

            return result;
        }

        IEnumerable<Claim> IClaimReadService.GetClaims()
        {
            IEnumerable<Claim> result = null;

            try
            {
                switch (_loginType)
                {
                    case "Administrator":
                        result = _claimRepository.GetClaimsByAdministrator(_administratorId);
                        break;
                    case "Consumer":
                        result = _claimRepository.GetClaimsByConsumer(_consumerId);
                        break;
                }
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("Claim", "GetClaims", e);
            }

            return result;
        }

        IEnumerable<ClaimStatus> IClaimReadService.GetClaimStatuses()
        {
            IEnumerable<ClaimStatus> result = null;

            try
            {
                result = _claimStatusRepository.GetClaimStatuses();
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("Claim", "Get", e);
            }

            return result;
        }

        IEnumerable<Claim> IClaimReadService.SearchClaims(string number, string status, Guid consumerId, string employerCode)
        {
            IEnumerable<Claim> result = null;

            try
            {
                Guid employerId = Guid.Empty;

                if (!Guid.Empty.Equals(consumerId))
                {
                    ConsumerDAL dbConsumer = _consumerRepository.GetConsumerById(consumerId);
                    if (null == dbConsumer)
                    {
                        throw new EntityNotFoundException("Consumer", "id", consumerId.ToString());
                    }
                }

                if (!string.IsNullOrEmpty(employerCode))
                {
                    EmployerDAL dbEmployer = _employerRepository.GetEmployerByCode(employerCode);
                    if (null == dbEmployer)
                    {
                        throw new EntityNotFoundException("Employer", "code", employerCode);
                    }

                    employerId = dbEmployer.Id;
                }

                result = _claimRepository.SearchClaims(number, status, consumerId, employerId, _administratorId);
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("Claim", "Search", e);
            }

            return result;
        }

        string IClaimFileService.SaveClaim(Claim claim)
        {
            string result;

            if (null == claim) throw new ArgumentNullException(nameof(claim));

            try
            {
                var validator = new ClaimValidator();
                validator.ValidateAndThrow(claim);

                ValidateClaimEnrollment(claim);

                claim.ClaimStatusId = _claimStatusRepository.GetClaimStatus("Pending approval").Id;

                result = _claimRepository.SaveClaim(claim);

                log.Info($"Create claim for consumer with id: {_consumerId}");
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("Claim", "Save", e);
            }

            return result;
        }

        void IClaimManageService.ApproveClaim(string claimId)
        {
            if (string.Empty.Equals(claimId)) throw new ArgumentOutOfRangeException(nameof(claimId));

            try
            {
                Claim claim = GetClaim(claimId);
                if (claim.Status.Name.Equals("Pending approval") || claim.Status.Name.Equals("Denied"))
                {
                    _consumerValidateService.TryValidateByAdministrator(claim.Enrollment.ConsumerId, _administratorId);

                    ClaimStatus status = _claimStatusRepository.GetClaimStatus("Approved");
                    claim.ClaimStatusId = status.Id;

                    _claimRepository.SaveClaim(claim);
                }
                else
                {
                    throw new InvalidOperationException($"Claim with id: {claimId}, cannot be approved because it has status: {claim.Status.Name}");
                }

                log.Info($"Approve claim with id: {claimId}");
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("Claim", "Approve", e);
            }
        }

        void IClaimManageService.DenyClaim(string claimId)
        {
            if (string.Empty.Equals(claimId)) throw new ArgumentOutOfRangeException(nameof(claimId));

            try
            {
                Claim claim = GetClaim(claimId);
                if (claim.Status.Name.Equals("Pending approval") || claim.Status.Name.Equals("Approved"))
                {
                    _consumerValidateService.TryValidateByAdministrator(claim.Enrollment.ConsumerId, _administratorId);

                    ClaimStatus status = _claimStatusRepository.GetClaimStatus("Denied");
                    claim.ClaimStatusId = status.Id;

                    _claimRepository.SaveClaim(claim);
                }
                else
                {
                    throw new InvalidOperationException($"Claim with id: {claimId}, cannot be denied because it has status: {claim.Status.Name}");
                }

                log.Info($"Deny claim with id: {claimId}");
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("Claim", "Deny", e);
            }
        }

        void IClaimManageService.SaveClaim(Claim claim)
        {
            if (null == claim) throw new ArgumentNullException(nameof(claim));

            try
            {
                Claim dbClaim = _claimRepository.GetClaim(claim.Id);
                if (null == dbClaim)
                {
                    throw new EntityNotFoundException("Claim", "id", claim.Id.ToString());
                }

                _consumerValidateService.TryValidateByAdministrator(dbClaim.Enrollment.ConsumerId, _administratorId);

                dbClaim.EnrollmentId = claim.EnrollmentId;
                dbClaim.DateOfService = claim.DateOfService;
                dbClaim.Amount = claim.Amount;

                var validator = new ClaimValidator();
                validator.ValidateAndThrow(claim);

                ValidateClaimEnrollment(dbClaim);

                _claimRepository.SaveClaim(dbClaim);

                log.Info($"Update claim with id: {claim.Id}");
            }
            catch (Exception e)
            {
                log.Error("BLL error", e);
                throw new EntityOperationFailedException("Claim", "Save", e);
            }
        }

        private void ValidateClaimEnrollment(Claim claim)
        {
            Enrollment dbEnrollment = _enrollmentRepostiory.GetEnrollment(claim.EnrollmentId);
            if (null == dbEnrollment)
            {
                throw new EntityNotFoundException("Enrollment", "id", claim.EnrollmentId.ToString());
            }

            int packageId = dbEnrollment.BenefitsOffering.BenefitsPackageId;
            BenefitsPackage dbBenefitPackage = _benefitsPackageRepository.GetPackageById(packageId);
            if (null == dbBenefitPackage)
            {
                throw new EntityNotFoundException("BenefitsPackage", "id", packageId.ToString());
            }

            if (!dbBenefitPackage.Status.Name.Equals("Initialized"))
            {
                throw new InvalidOperationException("Claim can not be filed to no-initialized benefit package");
            }

            if (claim.DateOfService.CompareTo(dbBenefitPackage.DateOfStart) < 0 || claim.DateOfService.CompareTo(dbBenefitPackage.DateOfEnd) > 0)
            {
                throw new InvalidOperationException("Claim can not be filed because date of service is out of benefit package duration");
            }

            if(claim.Amount > dbEnrollment.BenefitsOffering.ContributionAmount)
            {
                throw new InvalidOperationException("Claim amount can not be greater than plan contribution amount");
            }
        }
    }
}
