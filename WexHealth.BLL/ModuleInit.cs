﻿using System.ComponentModel.Composition;
using Autofac;
using Autofac.Core;
using Autofac.Builder;
using Autofac.Extras.AggregateService;
using Autofac.Extras.Attributed;
using WexHealth.BLL.Services;
using WexHealth.BLL.Services.Common;
using WexHealth.BLL.Services.Common.Interfaces;

namespace WexHealth.BLL
{
    [Export(typeof(Common.Modules.Common.Interfaces.IModule))]
    public class ModuleInit : Common.Modules.Common.Interfaces.IModule
    {
        public void Initialize(ContainerBuilder builder)
        {
            builder.RegisterType<AdministratorService>().As<IAdministratorService>();

            builder.RegisterType<AdministratorSettingService>().Keyed<ISettingReadService>("Administrator");
            builder.RegisterType<AdministratorSettingService>().Keyed<ISettingService>("Administrator");
            builder.RegisterType<AdministratorSettingService>().Keyed<ISettingCustomizeService>("Administrator");

            builder.RegisterType<AdministratorUserService>().Keyed<IDomainUserService>("Administrator");
            builder.RegisterType<AdministratorUserService>().Keyed<ILoginUserService>("Administrator");
            builder.RegisterGeneratedFactory<AdministratorUserServiceFactory>(new KeyedService("Administrator", typeof(IDomainUserService)));

            builder.RegisterType<BenefitsOfferingService>().As<IBenefitsOfferingReadService>();
            builder.RegisterType<BenefitsOfferingService>().As<IBenefitsOfferingManageService>();

            builder.RegisterType<ClaimService>().As<IClaimReadService>();
            builder.RegisterType<ClaimService>().As<IClaimFileService>();
            builder.RegisterType<ClaimService>().As<IClaimManageService>();

            builder.RegisterType<BenefitsPackageService>().As<IBenefitsPackageService>();

            builder.RegisterType<ConsumerService>().As<IConsumerService>();

            builder.RegisterType<ConsumerSettingService>().Keyed<ISettingReadService>("Consumer").WithAttributeFilter();

            builder.RegisterType<ConsumerUserService>().Keyed<IDomainUserService>("Consumer");
            builder.RegisterType<ConsumerUserService>().Keyed<ILoginUserService>("Consumer");
            builder.RegisterGeneratedFactory<ConsumerUserServiceFactory>(new KeyedService("Consumer", typeof(IDomainUserService)));

            builder.RegisterType<ConsumerValidateService>().As<IConsumerValidateService>();

            builder.RegisterType<DomainService>().As<IDomainService>();

            builder.RegisterType<EmployerService>().As<IEmployerService>();

            builder.RegisterType<EmployerSettingService>().Keyed<ISettingService>("Employer").WithAttributeFilter();
            builder.RegisterType<EmployerSettingService>().Keyed<ISettingCustomizeService>("Employer").WithAttributeFilter();

            builder.RegisterType<EmployerUserService>().Keyed<IDomainUserService>("Employer");
            builder.RegisterType<EmployerUserService>().Keyed<ILoginUserService>("Employer");
            builder.RegisterGeneratedFactory<EmployerUserServiceFactory>(new KeyedService("Employer", typeof(IDomainUserService)));

            builder.RegisterType<EmployerReportService>().As<IReportService>();

            builder.RegisterType<EmployerValidateService>().As<IEmployerValidateService>();

            builder.RegisterType<EnrollmentService>().As<IEnrollmentReadService>();
            builder.RegisterType<EnrollmentService>().As<IEnrollmentManageService>();

            builder.RegisterType<ImportService>().As<IImportFileService>();
            builder.RegisterType<ImportService>().As<IImportManageService>();

            builder.RegisterType<RoleService>().As<IRoleService>();

            builder.RegisterAggregateService<UserService.IUserServiceContext>();

            builder.RegisterType<EmployerReportService>().Keyed<IReportService>("Employer");
            builder.RegisterType<ConsumerReportService>().Keyed<IReportService>("Consumer");

            builder.RegisterAggregateService<SettingServiceBase.ISettingServiceContext>();
        }
    }
}
