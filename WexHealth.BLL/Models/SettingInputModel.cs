﻿using System;

namespace WexHealth.BLL.Models
{
    public class SettingInputModel
    {
        public Guid Id { get; set; }

        public bool? isAllowed { get; set; }
    }
}
