﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WexHealth.BLL.Models
{
    public class ConsumerInputModel
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Enter SSN")]
        [StringLength(256, ErrorMessage = "Enter first name with length under 256 characters")]
        public string SSN { get; set; }

        public AddressInputModel Address { get; set; }

        public PhoneInputModel Phone { get; set; }
    }
}
