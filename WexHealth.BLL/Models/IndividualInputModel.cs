﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WexHealth.BLL.Models
{
    public class IndividualInputModel
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Enter first name")]
        [StringLength(256, ErrorMessage = "Enter first name with length under 256 characters")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Enter last name")]
        [StringLength(256, ErrorMessage = "Enter last name with length under 256 characters")]
        public string LastName { get; set; }

        public DateTime DateBirth { get; set; }

        [Required(ErrorMessage = "Enter email")]
        [StringLength(256, ErrorMessage = "Enter email with length under 256 characters")]
        [EmailAddress(ErrorMessage = "Enter valid email address")]
        public string Email { get; set; }
    }
}
