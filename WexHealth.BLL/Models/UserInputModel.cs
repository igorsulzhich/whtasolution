﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WexHealth.BLL.Models
{
    public class UserInputModel : IValidatableObject
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Enter user name")]
        [StringLength(256, ErrorMessage = "Enter user name with length under 256 characters")]
        public string UserName { get; set; }

        [MinLength(8, ErrorMessage = "Enter password with length above 8 charachters")]
        [MaxLength(256, ErrorMessage = "Enter password with length under 256 characters")]
        public string Password { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var result = new List<ValidationResult>();

            Validator.TryValidateProperty(UserName, new ValidationContext(this, null, null) { MemberName = "UserName" }, result);

            if (!String.IsNullOrEmpty(Password))
            {
                Validator.TryValidateProperty(Password, new ValidationContext(this, null, null) { MemberName = "Password" }, result);
            }

            return result;
        }
    }
}
