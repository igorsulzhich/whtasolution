﻿using System.ComponentModel.DataAnnotations;

namespace WexHealth.BLL.Models
{
    public class PhoneInputModel
    {
        [Required(ErrorMessage = "Enter phone")]
        [StringLength(256, ErrorMessage = "Enter phone with length under 256 characters")]
        public string Phone { get; set; }
    }
}
