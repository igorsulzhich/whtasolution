﻿
using System;

namespace WexHealth.BLL.Models
{
    public class EmployerInputModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public string Street { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Phone { get; set; }

        public string ZipCode { get; set; }

        public byte[] Logo { get; set; }
    }
}
