﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WexHealth.BLL.Models
{
    public class AdministratorInputModel
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Enter name")]
        [StringLength(256, ErrorMessage = "Enter name with length under 256 characters")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Enter alias")]
        [StringLength(256, ErrorMessage = "Enter alias with length under 256 characters")]
        public string Alias { get; set; }

        public AddressInputModel Address { get; set; }

        public PhoneInputModel Phone { get; set; }
    }
}
