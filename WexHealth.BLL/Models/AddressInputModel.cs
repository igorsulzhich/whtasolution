﻿using System.ComponentModel.DataAnnotations;

namespace WexHealth.BLL.Models
{
    public class AddressInputModel
    {
        [Required(ErrorMessage = "Enter street")]
        [StringLength(256, ErrorMessage = "Enter street with length under 256 characters")]
        public string Street { get; set; }

        [Required(ErrorMessage = "Enter city")]
        [StringLength(256, ErrorMessage = "Enter city with length under 256 characters")]
        public string City { get; set; }

        [Required(ErrorMessage = "Enter state")]
        [StringLength(256, ErrorMessage = "Enter state with length under 256 characters")]
        public string State { get; set; }

        [Required(ErrorMessage = "Enter zip code")]
        [StringLength(256, ErrorMessage = "Enter zip code with length under 256 characters")]
        public string ZipCode { get; set; }
    }
}
