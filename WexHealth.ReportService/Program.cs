﻿using System.ServiceProcess;
using Microsoft.Practices.ServiceLocation;
using Autofac;
using Autofac.Extras.CommonServiceLocator;
using WexHealth.Common.Modules;

namespace WexHealth.ReportService
{
    static class Program
    {
        private static IContainer _container;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            InitAutofac();

            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                ServiceLocator.Current.GetInstance<ReportService>()
            };

            ServiceBase.Run(ServicesToRun);
        }
        
        private static void InitAutofac()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<ReportService>();
            ModuleLoader.LoadContainer(builder, ".", "WexHealth.*.dll");

            _container = builder.Build();

            var commonServiceLocator = new AutofacServiceLocator(_container);
            ServiceLocator.SetLocatorProvider(() => commonServiceLocator);
        }
    }
}
