﻿using System;
using System.IO;
using System.Linq;
using System.Timers;
using System.Globalization;
using System.ServiceProcess;
using System.Collections.Generic;
using WexHealth.ReportService.Model;
using WexHealth.Domain.Entities;
using WexHealth.DAL.Models;
using WexHealth.DAL.Repositories.Common.Interfaces;
using WexHealth.BLL.Services.Common.Interfaces;

namespace WexHealth.ReportService
{
    public partial class ReportService : ServiceBase
    {
        private Timer _timer;
        private DateTime _prevRun;

        private readonly IEmployerRepository _employerRepository;       
        private readonly IReportRequestRepository _reportRequestRepository;
        private readonly IReportRequestStatusRepository _reportRequestStatusRepository;
        private readonly IReportFieldsRepository _reportFieldsRepository;
        private readonly IConsumerService _consumerService;

        public ReportService(IEmployerRepository employerRepository, IConsumerService consumerService, IReportRequestRepository reportRequestRepository,
            IReportRequestStatusRepository reportRequestStatusRepository, IReportFieldsRepository reportFieldsRepository)
        {
            InitializeComponent();

            _employerRepository = employerRepository;
            _reportRequestRepository = reportRequestRepository;
            _reportRequestStatusRepository = reportRequestStatusRepository;
            _reportFieldsRepository = reportFieldsRepository;
            _consumerService = consumerService;
        }

        protected override void OnStart(string[] args)
        {
            _timer = new Timer(10 * 1000);
            _timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
            _timer.Start();
        }

        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            _timer.Stop();

            try
            {
                ReportRequestStatus status = null;
                ReportRequest request = null;

                status = _reportRequestStatusRepository.GetStatusByName(ReportStatus.Requested.ToString());
                request = _reportRequestRepository.GetRequestFirstStatus(status.Id);

                try
                {
                    if (null != request && null != status)
                    { 
                        status = _reportRequestStatusRepository.GetStatusByName(ReportStatus.Processed.ToString());
                        request.StatusId = status.Id;

                        _reportRequestRepository.SaveRequest(request);
                        eventsLog.WriteEntry($"Change request (id: {request.Id}) status to: {ReportStatus.Processed}");

                        IEnumerable<ReportField> reportFields = _reportFieldsRepository.GetReportFieldByRequestId(request.Id);

                        string report = $"{request.RequestType}_report_{request.DateOfRequest.ToString("MMdyyyyHmm", CultureInfo.InvariantCulture)}R{request.Id}.csv";
                        string path = Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.Parent.Parent.FullName + @"\WexHealth.AdminPortal\Content\reports\";
                        string pathReport = Path.Combine(path, @report);

                        using (CsvFileWriter writer = new CsvFileWriter(pathReport))
                        {
                            if (request.RequestType == "Employer")
                            {
                                IEnumerable<EmployerDAL> result = _employerRepository.GetEmployersByAdministrator(request.AdministratorId);
                                eventsLog.WriteEntry($"Received {result.Count()} Employer's items");

                                foreach (var item in result)
                                {
                                    CsvRow row = new CsvRow();

                                    if (reportFields.Any(x => x.ObjectFieldName == "Name"))
                                    {
                                        row.Add(item.Name);
                                    }

                                    if (reportFields.Any(x => x.ObjectFieldName == "Code"))
                                    {
                                        row.Add(item.Code);
                                    }

                                    writer.WriteRow(row);
                                }
                            }

                            else
                            {
                                IEnumerable<Consumer> result = _consumerService.GetConsumerByEmployer(request.EmployerId, request.AdministratorId);
                                eventsLog.WriteEntry($"Received {result.Count()} Consumer's items");

                                foreach (var item in result)
                                {
                                    CsvRow row = new CsvRow();

                                    if (reportFields.Any(x => x.ObjectFieldName == "First Name"))
                                    {
                                        row.Add(item.Individual.FirstName);
                                    }

                                    if (reportFields.Any(x => x.ObjectFieldName == "Last Name"))
                                    {
                                        row.Add(item.Individual.LastName);
                                    }

                                    if (reportFields.Any(x => x.ObjectFieldName == "SSN"))
                                    {
                                        row.Add(item.SSN);
                                    }

                                    if (reportFields.Any(x => x.ObjectFieldName == "Phone"))
                                    {
                                        row.Add(item.Phone.PhoneNumber);
                                    }

                                    if (reportFields.Any(x => x.ObjectFieldName == "Street"))
                                    {
                                        row.Add(item.Address.Street);
                                    }

                                    if (reportFields.Any(x => x.ObjectFieldName == "City"))
                                    {
                                        row.Add(item.Address.City);
                                    }

                                    if (reportFields.Any(x => x.ObjectFieldName == "State"))
                                    {
                                        row.Add(item.Address.State);
                                    }

                                    if (reportFields.Any(x => x.ObjectFieldName == "Zip Code"))
                                    {
                                        row.Add(item.Address.ZipCode);
                                    }

                                    writer.WriteRow(row);
                                }
                            }

                            status = _reportRequestStatusRepository.GetStatusByName(ReportStatus.Completed.ToString());
                            request.StatusId = status.Id;
                            request.FilePath = pathReport;

                            _reportRequestRepository.SaveRequest(request);
                            eventsLog.WriteEntry($"Change request (id: {request.Id}) status to: {ReportStatus.Completed}");
                        }
                    }
                }
                catch (Exception)
                {
                    status = _reportRequestStatusRepository.GetStatusByName(ReportStatus.Failed.ToString());
                    request.StatusId = status.Id;

                    _reportRequestRepository.SaveRequest(request);

                    eventsLog.WriteEntry($"Change request (id: {request.Id}) status to: {ReportStatus.Failed}");
                    throw;
                }
            }
            catch (Exception exception)
            {
                eventsLog.WriteEntry(exception.Message);
            }

            _prevRun = DateTime.Now;
            _timer.Start();
        }

        protected override void OnStop()
        {
            eventsLog.WriteEntry("Stop");
        }
    }
}
