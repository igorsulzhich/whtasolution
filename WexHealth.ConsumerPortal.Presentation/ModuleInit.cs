﻿using System.Reflection;
using System.ComponentModel.Composition;
using Autofac;
using Autofac.Builder;
using Autofac.Extras.Attributed;
using WexHealth.Common.Modules.Common.Interfaces;
using WexHealth.ConsumerPortal.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.ConsumerPortal.Presentation
{
    [Export(typeof(IModule))]
    public class ModuleInit : IModule
    {
        public void Initialize(ContainerBuilder builder)
        {
            var assembly = Assembly.GetExecutingAssembly();

            builder.RegisterAssemblyTypes(assembly)
                   .Where(t => t.Name.EndsWith("Presenter"))
                   .AsImplementedInterfaces().WithAttributeFilter();

            builder.RegisterGeneratedFactory<ClaimsFilePresenterFactory>();
            builder.RegisterGeneratedFactory<ClaimsIndexPresenterFactory>();
            builder.RegisterGeneratedFactory<EnrollmentsIndexPresenterFactory>();
        }
    }
}
