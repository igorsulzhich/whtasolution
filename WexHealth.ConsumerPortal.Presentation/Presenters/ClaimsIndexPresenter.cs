﻿using System.Linq;
using System.Security.Principal;
using Autofac.Extras.Attributed;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Security.Infrastructure;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.ConsumerPortal.Presentation.Views;
using WexHealth.ConsumerPortal.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.ConsumerPortal.Presentation.Presenters
{
    public class ClaimsIndexPresenter : ViewPresenterBase<IClaimsIndexView>, IClaimsIndexPresenter
    {
        private readonly IClaimReadService _claimService;
        private readonly ISettingReadService _settingService;

        public ClaimsIndexPresenter(IClaimsIndexView view, IPrincipal principal, IClaimReadService claimService,
            [WithKey("Consumer")] ISettingReadService settingService) :
            base(view, principal)
        {
            _claimService = claimService;
            _settingService = settingService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            User user = ((WexHealthIdentity)_principal.Identity).User;
            _claimService.InitService(user);
            _settingService.InitService(user);

            if (!isPostBack)
            {
                LoadClaims();
            }
        }

        private void LoadClaims()
        {
            _view.Claims = _claimService.GetClaims().Reverse();
        }

        public bool IsSettingAllow(string settingKey)
        {
            return _settingService.GetSettings().Any(x => x.Name.Equals(settingKey));
        }
    }
}
