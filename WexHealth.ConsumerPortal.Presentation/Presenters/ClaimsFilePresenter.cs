﻿using System;
using System.Linq;
using System.Security.Principal;
using FluentValidation;
using Autofac.Extras.Attributed;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Exceptions;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Security.Infrastructure;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;
using WexHealth.ConsumerPortal.Presentation.Views;
using WexHealth.ConsumerPortal.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.ConsumerPortal.Presentation.Presenters
{
    public class ClaimsFilePresenter : ViewPresenterBase<IClaimsFileView>, IClaimsFilePresenter
    {
        private readonly IClaimFileService _claimService;
        private readonly IEnrollmentReadService _enrollmentService;
        private readonly ISettingReadService _settingService;

        public ClaimsFilePresenter(IClaimsFileView view, IPrincipal principal, IClaimFileService claimService, IEnrollmentReadService enrollmentService,
            [WithKey("Consumer")] ISettingReadService settingService) :
            base(view, principal)
        {
            _claimService = claimService;
            _settingService = settingService;
            _enrollmentService = enrollmentService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            User user = ((WexHealthIdentity)_principal.Identity).User;
            _claimService.InitService(user);
            _enrollmentService.InitService(user);
            _settingService.InitService(user);

            if (isSettingAllow("Claim filling"))
            {
                if (!isPostBack)
                {
                    LoadPlans();
                }
            }
            else
            {
                throw new InvalidOperationException("Access denied");
            }
        }

        private void LoadPlans()
        {
            _view.Enrollments = _enrollmentService.GetActiveEnrollments();
        }

        public IResult SaveClaim()
        {
            IResult result = null;

            try
            {
                var claim = new Claim();
                Map(claim);

                string claimNumber = _claimService.SaveClaim(claim);

                result = new OperationResult(OperationStatus.Success, $"Claim {claimNumber} has been successfully submitted");
            }
            catch (EntityOperationFailedException e) when (
                e.InnerException != null &&
                e.InnerException is EntityNotFoundException ||
                e.InnerException is InvalidOperationException ||
                e.InnerException is ValidationException
            )
            {
                result = new OperationResult(OperationStatus.Failed, e.InnerException.Message);
            }
            catch (InvalidOperationException e)
            {
                result = new OperationResult(OperationStatus.Failed, e.Message);
            }
            catch (Exception)
            {
                result = new OperationResult(OperationStatus.Failed, "Server error");
            }

            return result;
        }

        private bool isSettingAllow(string settingKey)
        {
            return _settingService.GetSettings().Any(x => x.Name.Equals(settingKey));
        }

        private void Map(Claim claim)
        {
            claim.EnrollmentId = _view.SelectedEnrollment;
            claim.DateOfService = _view.DateOfService;
            claim.Amount = _view.Amount;
            claim.File = _view.Receipt;
        }
    }
}
