﻿using System.Security.Principal;
using WexHealth.ConsumerPortal.Presentation.Views;

namespace WexHealth.ConsumerPortal.Presentation.Presenters.Common.Interfaces
{
    public delegate IClaimsIndexPresenter ClaimsIndexPresenterFactory(IClaimsIndexView view, IPrincipal principal);

    public interface IClaimsIndexPresenter
    {
        void InitView(bool isPostBack);

        bool IsSettingAllow(string settingKey);
    }
}
