﻿using System.Security.Principal;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;
using WexHealth.ConsumerPortal.Presentation.Views;

namespace WexHealth.ConsumerPortal.Presentation.Presenters.Common.Interfaces
{
    public delegate IClaimsFilePresenter ClaimsFilePresenterFactory(IClaimsFileView view, IPrincipal principal);

    public interface IClaimsFilePresenter
    {
        void InitView(bool isPostBack);

        IResult SaveClaim();
    }
}
