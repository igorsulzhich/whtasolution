﻿using System.Security.Principal;
using WexHealth.ConsumerPortal.Presentation.Views;

namespace WexHealth.ConsumerPortal.Presentation.Presenters.Common.Interfaces
{
    public delegate IEnrollmentsIndexPresenter EnrollmentsIndexPresenterFactory(IEnrollmentsIndexView view, IPrincipal principal);

    public interface IEnrollmentsIndexPresenter
    {
        void InitView(bool isPostBack);

        bool IsSettingAllow(string settingKey);
    }
}
