﻿using System.Linq;
using System.Security.Principal;
using Autofac.Extras.Attributed;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Security.Infrastructure;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.ConsumerPortal.Presentation.Views;
using WexHealth.ConsumerPortal.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.ConsumerPortal.Presentation.Presenters
{
    public class EnrollmentsIndexPresenter : ViewPresenterBase<IEnrollmentsIndexView>, IEnrollmentsIndexPresenter
    {
        private readonly IEnrollmentReadService _enrollmentService;
        private readonly ISettingReadService _settingService;

        public EnrollmentsIndexPresenter(IEnrollmentsIndexView view, IPrincipal principal, IEnrollmentReadService enrollmentService,
            [WithKey("Consumer")] ISettingReadService settingService) :
            base(view, principal)
        {
            _enrollmentService = enrollmentService;
            _settingService = settingService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            User user = ((WexHealthIdentity)_principal.Identity).User;
            _enrollmentService.InitService(user);
            _settingService.InitService(user);

            if (!isPostBack)
            {
                LoadEnrollments();
            }
        }

        private void LoadEnrollments()
        {
            _view.Enrollments = _enrollmentService.GetActiveEnrollments().OrderBy(x => x.BenefitsOffering.Name);
        }

        public bool IsSettingAllow(string settingKey)
        {
            return _settingService.GetSettings().Any(x => x.Name.Equals(settingKey));
        }
    }
}
