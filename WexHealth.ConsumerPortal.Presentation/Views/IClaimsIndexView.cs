﻿using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.ConsumerPortal.Presentation.Views
{
    public interface IClaimsIndexView
    {
        IEnumerable<Claim> Claims { set; }
    }
}
