﻿using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.ConsumerPortal.Presentation.Views
{
    public interface IEnrollmentsIndexView
    {        
        IEnumerable<Enrollment> Enrollments { set; }
    }
}
