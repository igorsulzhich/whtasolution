﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.ConsumerPortal.Presentation.Views
{
    public interface IClaimsFileView
    {
        IEnumerable<Enrollment> Enrollments { set; }

        int SelectedEnrollment { get; }

        DateTime DateOfService { get; }

        decimal Amount { get; }

        byte[] Receipt { get; }
    }
}
