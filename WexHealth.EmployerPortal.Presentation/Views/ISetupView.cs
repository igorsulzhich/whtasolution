﻿using System.Collections.Generic;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Models;

namespace WexHealth.EmployerPortal.Presentation.Views
{
    public interface ISetupView
    {
        IEnumerable<Setting> SettingList { set; }

        IEnumerable<SettingInputModel> SelectedSettings { get; }
    }
}
