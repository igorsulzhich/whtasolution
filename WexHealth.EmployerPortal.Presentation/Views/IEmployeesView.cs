﻿using System.Collections.Generic;
using WexHealth.Domain.Entities;


namespace WexHealth.EmployerPortal.Presentation.Views
{
    public interface IEmployeesView
    {
        string FirstName { get; }

        string LastName { get; }

        string SSN { get; }

        IEnumerable<Consumer> Consumers { set; }
    }
}
