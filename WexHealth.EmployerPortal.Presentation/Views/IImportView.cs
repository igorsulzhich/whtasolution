﻿namespace WexHealth.EmployerPortal.Presentation.Views
{
    public interface IImportView
    {
        string FileName { get; }

        string SaveFile(string filename, string extension);
    }
}
