﻿using System.Linq;
using System.Security.Principal;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Security.Infrastructure;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.EmployerPortal.Presentation.Views;
using WexHealth.EmployerPortal.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.EmployerPortal.Presentation.Presenters
{
    public class EmployeesPresenter : ViewPresenterBase<IEmployeesView>, IEmployeesPresenter
    {
        private readonly IConsumerService _consumerService;

        public EmployeesPresenter(IEmployeesView view, IPrincipal principal, IConsumerService consumerService) :
            base(view, principal)
        {
            _consumerService = consumerService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            _consumerService.ObjectUser = ((WexHealthIdentity)_principal.Identity).User;
            
            if (!isPostBack)
            {
                LoadConsumers();
            }
        }

        private void LoadConsumers()
        {
            _view.Consumers = _consumerService.SearchConsumers(_view.FirstName, _view.LastName, _view.SSN).OrderBy(x => x.Individual.LastName);
        }
    }
}
