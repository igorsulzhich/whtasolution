﻿using System.Security.Principal;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;
using WexHealth.EmployerPortal.Presentation.Views;

namespace WexHealth.EmployerPortal.Presentation.Presenters.Common.Interfaces
{
    public delegate ISetupPresenter SetupPresenterFactory(ISetupView view, IPrincipal principal);

    public interface ISetupPresenter
    {
        void InitView(bool isPostBack);

        IResult SaveSettings();
    }
}
