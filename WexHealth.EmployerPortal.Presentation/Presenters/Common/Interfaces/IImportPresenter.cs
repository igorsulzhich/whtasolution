﻿using System.Security.Principal;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;
using WexHealth.EmployerPortal.Presentation.Views;

namespace WexHealth.EmployerPortal.Presentation.Presenters.Common.Interfaces
{
    public delegate IImportPresenter ImportPresenterFactory(IImportView view, IPrincipal principal);

    public interface IImportPresenter
    {
        void InitView(bool isPostBack);

        IResult CreateRequest();
    }
}
