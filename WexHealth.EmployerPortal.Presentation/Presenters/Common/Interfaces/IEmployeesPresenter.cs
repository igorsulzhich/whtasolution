﻿using System.Security.Principal;
using WexHealth.EmployerPortal.Presentation.Views;

namespace WexHealth.EmployerPortal.Presentation.Presenters.Common.Interfaces
{
    public delegate IEmployeesPresenter EmployeesPresenterFactory(IEmployeesView view, IPrincipal principal);

    public interface IEmployeesPresenter
    {
        void InitView(bool isPostBack);
    }
}
