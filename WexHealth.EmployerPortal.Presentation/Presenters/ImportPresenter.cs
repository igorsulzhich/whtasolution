﻿using System;
using System.IO;
using System.Security.Principal;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Security.Infrastructure;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;
using WexHealth.EmployerPortal.Presentation.Views;
using WexHealth.EmployerPortal.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.EmployerPortal.Presentation.Presenters
{
    public class ImportPresenter : ViewPresenterBase<IImportView>, IImportPresenter
    {
        private readonly IImportFileService _importService;

        public ImportPresenter(IImportView view, IPrincipal principal, IImportFileService importService) :
            base(view, principal)
        {
            _importService = importService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            User user = ((WexHealthIdentity)_principal.Identity).User;

            _importService.InitService(user);
        }

        public IResult CreateRequest()
        {
            IResult result = null;

            try
            {
                string extension = Path.GetExtension(_view.FileName);
                if (!extension.Equals(".csv"))
                {
                    throw new InvalidOperationException("Invalid file format. Supported formats: .csv");
                }

                string filePath = _view.SaveFile(Guid.NewGuid().ToString(), ".csv");
                Import request = new Import
                {
                    FilePath = filePath
                };

                _importService.CreateRequest(request);

                result = new OperationResult(OperationStatus.Success, $"File {_view.FileName} has been successfully imported");
            }
            catch (InvalidOperationException e)
            {
                result = new OperationResult(OperationStatus.Failed, e.Message);
            }
            catch (Exception)
            {
                result = new OperationResult(OperationStatus.Failed, "Failed to import");
            }

            return result;
        }

    }
}
