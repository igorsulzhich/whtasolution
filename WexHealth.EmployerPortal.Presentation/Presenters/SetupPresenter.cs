﻿using System;
using System.Security.Principal;
using Autofac.Extras.Attributed;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Exceptions;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Security.Infrastructure;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;
using WexHealth.EmployerPortal.Presentation.Views;
using WexHealth.EmployerPortal.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.EmployerPortal.Presentation.Presenters
{
    public class SetupPresenter : ViewPresenterBase<ISetupView>, ISetupPresenter
    {
        private readonly ISettingCustomizeService _settingService;

        public SetupPresenter(ISetupView view, IPrincipal principal, [WithKey("Employer")]ISettingCustomizeService settingService) :
            base(view, principal)
        {
            _settingService = settingService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            User user = ((WexHealthIdentity)_principal.Identity).User;

            _settingService.InitService(user);

            if (!isPostBack)
            {
                LoadSettings();
            }
        }


        private void LoadSettings()
        {
            _view.SettingList = _settingService.GetSettings();
        }

        public IResult SaveSettings()
        {
            IResult result = null;

            try
            {
                _settingService.SaveSettings(_view.SelectedSettings);
                result = new OperationResult(OperationStatus.Success);
            }
            catch (Exception exception)
            {
                result = new OperationResult(OperationStatus.Failed, exception.Message);
            }

            return result;
        }
    }
}
