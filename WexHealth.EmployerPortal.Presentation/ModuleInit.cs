﻿using System.Reflection;
using System.ComponentModel.Composition;
using Autofac;
using Autofac.Builder;
using Autofac.Extras.Attributed;
using WexHealth.Common.Modules.Common.Interfaces;
using WexHealth.EmployerPortal.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.EmployerPortal.Presentation
{
    [Export(typeof(IModule))]
    public class ModuleInit : IModule
    {
        public void Initialize(ContainerBuilder builder)
        {
            var assembly = Assembly.GetExecutingAssembly();

            builder.RegisterAssemblyTypes(assembly)
                   .Where(t => t.Name.EndsWith("Presenter"))
                   .AsImplementedInterfaces().WithAttributeFilter();

            builder.RegisterGeneratedFactory<EmployeesPresenterFactory>();
            builder.RegisterGeneratedFactory<ImportPresenterFactory>();
            builder.RegisterGeneratedFactory<SetupPresenterFactory>();
        }
    }
}
