﻿using Autofac;

namespace WexHealth.Common.Modules.Common.Interfaces
{
    public interface IModule
    {
        void Initialize(ContainerBuilder builder);
    }
}
