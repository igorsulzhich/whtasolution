﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WexHealth.Domain.Entities
{
    public class Individual : IValidatableObject
    {
        public Guid Id { get; set; }

        [StringLength(256, ErrorMessage = "Enter the email address with length under 256 characters")]
        [EmailAddress(ErrorMessage = "Enter valid email address")]
        public string Email { get; set; }

        public DateTime DateBirth { get; set; }

        [Required(ErrorMessage = "Enter the first name")]
        [StringLength(256, ErrorMessage = "Enter the first name with length under 256 characters")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Enter the last name")]
        [StringLength(256, ErrorMessage = "Enter the last name with length under 256 characters")]
        public string LastName { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();

            Validator.TryValidateProperty(Email, new ValidationContext(this, null, null) { MemberName = "Email" }, results);
            Validator.TryValidateProperty(FirstName, new ValidationContext(this, null, null) { MemberName = "FirstName" }, results);
            Validator.TryValidateProperty(LastName, new ValidationContext(this, null, null) { MemberName = "LastName" }, results);

            return results;
        }
    }
}
