﻿namespace WexHealth.Domain.Entities
{
    public class ClaimStatus
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
