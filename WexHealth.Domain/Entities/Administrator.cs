﻿using System;
using System.Collections.Generic;

namespace WexHealth.Domain.Entities
{
    public class Administrator
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Alias { get; set; }

        public virtual Address Address { get; set; }

        public virtual Phone Phone { get; set; }

        public virtual ICollection<Employer> Employers { get; set; }

        public virtual ICollection<Setting> Settings { get; set; }
    }
}
