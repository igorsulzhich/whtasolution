﻿namespace WexHealth.Domain.Entities
{
    public class BenefitsOffering
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public decimal ContributionAmount { get; set; }

        public int PlanTypeId { get; set; }

        public int BenefitsPackageId { get; set; }

        public PlanType PlanType { get; set; }
    }
}
