﻿namespace WexHealth.Domain.Entities
{
    public class ReportField
    {
        public int Id { get; set; }

        public int ObjectFieldsId { get; set; }

        public string ObjectFieldName { get; set; }

        public int ReportRequestId { get; set; }
    }
}
