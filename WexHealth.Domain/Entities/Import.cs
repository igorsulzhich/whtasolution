﻿using System;

namespace WexHealth.Domain.Entities
{
    public enum ImportStatus
    {
        Requested, Processed, Completed, Failed
    }

    public class Import
    {
        public int Id { get; set; }

        public string FilePath { get; set; }

        public ImportStatus Status { get; set; }

        public Guid EmployerId { get; set; }
    }
}
