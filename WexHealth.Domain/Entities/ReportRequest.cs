﻿using System;

namespace WexHealth.Domain.Entities
{
    public class ReportRequest
    {
        public int Id { get; set; }

        public string FormatReport { get; set; }

        public DateTime DateOfRequest { get; set; }

        public string RequestedBy { get; set; }        
        
        public int StatusId { get; set; }

        public string RequestType { get; set; }

        public ReportRequestStatus Status { get; set; }

        public string FilePath { get; set; }

        public Guid EmployerId { get; set; }

        public Guid AdministratorId { get; set; }
    }
}
