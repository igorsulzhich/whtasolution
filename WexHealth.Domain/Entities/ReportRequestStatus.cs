﻿using System;
namespace WexHealth.Domain.Entities
{
    public enum ReportStatus
    {
        Requested, Processed, Completed, Rejected, Failed
    }

    public class ReportRequestStatus
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
