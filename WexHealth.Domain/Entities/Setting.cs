﻿using System;

namespace WexHealth.Domain.Entities
{
    public class Setting
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public bool? isAllowed { get; set; }
    }
}
