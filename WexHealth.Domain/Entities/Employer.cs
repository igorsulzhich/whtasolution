﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WexHealth.Domain.Entities
{
    public class Employer
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public byte[] Logo { get; set; }

        public Guid AdministratorId { get; set; }

        public Administrator Administrator { get; set; }

        public Phone Phones { get; set; }

        public Address Address { get; set; }

        public virtual ICollection<Setting> Settings { get; set; }
    }
}
