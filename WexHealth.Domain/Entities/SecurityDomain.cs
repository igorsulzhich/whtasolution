﻿using System;

namespace WexHealth.Domain.Entities
{
    public class SecurityDomain
    {
        public Guid Id { get; set; }

        public string Alias { get; set; }
    }
}
