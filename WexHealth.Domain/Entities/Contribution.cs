﻿using System;

namespace WexHealth.Domain.Entities
{
    public class Contribution
    {
        public int Id { get; set; }

        public decimal Amount { get; set; }

        public DateTime DateOfContribute { get; set; }

        public int EnrollmentId { get; set; }

        public virtual Enrollment Enrollment { get; set; }
    }
}
