﻿namespace WexHealth.Domain.Entities
{
    public class PlanType
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
