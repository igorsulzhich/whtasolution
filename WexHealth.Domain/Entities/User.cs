﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WexHealth.Domain.Entities
{
    public class User : IValidatableObject
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Enter the user name")]
        [StringLength(256, ErrorMessage = "Enter the user name with length under 256 characters")]
        public string UserName { get; set; }

        public string PasswordHash { get; set; }

        public string PasswordSalt { get; set; }

        public string LoginType { get; set; }

        public Guid IndividualId { get; set; }

        public Guid SecurityDomainId { get; set; }

        public Individual Individual { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();

            Validator.TryValidateProperty(UserName, new ValidationContext(this, null, null) { MemberName = "UserName" }, results);

            return results;
        }
    }
}
