﻿using System;

namespace WexHealth.Domain.Entities
{
    public class Claim
    {
        public string Id { get; set; }

        public DateTime DateOfService { get; set; }

        public decimal Amount { get; set; }

        public byte[] File { get; set; }

        public int ClaimStatusId { get; set; }

        public int EnrollmentId { get; set; }

        public virtual Enrollment Enrollment { get; set; }

        public virtual ClaimStatus Status { get; set; }
    }
}
