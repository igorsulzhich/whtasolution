﻿using System;

namespace WexHealth.Domain.Entities
{
    public class BenefitsPackage
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime DateOfStart { get; set; }

        public DateTime DateOfEnd { get; set; }

        public DateTime DateOfInitialized { get; set; }

        public int Frequency { get; set; }

        public int StatusId { get; set; }

        public Guid EmployerId { get; set; }

        public virtual BenefitsPackageStatus Status { get; set; }
    }
}
