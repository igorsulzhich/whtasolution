﻿namespace WexHealth.Domain.Entities
{
    public class BenefitsPackageStatus
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
