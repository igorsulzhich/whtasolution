﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WexHealth.Domain.Entities
{
    public class Role : IValidatableObject
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Enter name")]
        [StringLength(256, ErrorMessage = "Enter name with length under 256 characters")]
        public string Name { get; set; }

        [StringLength(256, ErrorMessage = "Enter notes with length under 256 characters")]
        public string Notes { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var result = new List<ValidationResult>();

            Validator.TryValidateProperty(Name, new ValidationContext(this, null, null) { MemberName = "Name" }, result);
            Validator.TryValidateProperty(Notes, new ValidationContext(this, null, null) { MemberName = "Notes" }, result);

            return result;
        }
    }
}
