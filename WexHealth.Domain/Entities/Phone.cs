﻿using System;

namespace WexHealth.Domain.Entities
{
    public class Phone
    {
        public Guid Id { get; set; }

        public string PhoneNumber { get; set; }
    }
}
