﻿using System;
using System.Collections.Generic;

namespace WexHealth.Domain.Entities
{
    public class Enrollment
    {
        public int Id { get; set; }

        public decimal ElectionAmount { get; set; }

        public int BenefitsOfferingId { get; set; }

        public Guid ConsumerId { get; set; }

        public virtual Consumer Consumer { get; set; }

        public virtual BenefitsOffering BenefitsOffering { get; set; }

        public virtual IEnumerable<Contribution> Contributions { get; set; }
    }
}
