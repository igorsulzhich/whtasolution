﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WexHealth.Domain.Entities
{
    public class ObjectField
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string ObjectType { get; set; }
    }
}
