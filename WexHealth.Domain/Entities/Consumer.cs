﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WexHealth.Domain.Entities
{
    public class Consumer
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Enter SSN")]
        [StringLength(256, ErrorMessage = "Enter SSN with length under 256 characters")]
        public string SSN { get; set; }

        public Guid EmployerId { get; set; }

        public Guid IndividualId { get; set; }

        public Employer Employer { get; set; }

        public Phone Phone { get; set; }

        public Address Address { get; set; }

        public Individual Individual { get; set; }

    }
}
