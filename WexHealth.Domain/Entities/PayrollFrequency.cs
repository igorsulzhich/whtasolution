﻿namespace WexHealth.Domain.Entities
{
    public class PayrollFrequency
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Days { get; set; }
    }
}
