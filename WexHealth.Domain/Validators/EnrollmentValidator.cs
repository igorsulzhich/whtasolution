﻿using System.Text.RegularExpressions;
using FluentValidation;
using WexHealth.Domain.Entities;

namespace WexHealth.Domain.Validators
{
    public class EnrollmentValidator : AbstractValidator<Enrollment>
    {
        public EnrollmentValidator()
        {
            RuleFor(x => x.ElectionAmount).Must(x => Regex.IsMatch(x.ToString(), "^[0-9]{1,10}([.][0-9]{1,2})?$"));
            RuleFor(x => x.BenefitsOfferingId).GreaterThan(0).WithMessage("Ivalid plan id");
        }
    }
}
