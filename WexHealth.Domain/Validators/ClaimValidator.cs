﻿using System.Text.RegularExpressions;
using FluentValidation;
using WexHealth.Domain.Entities;

namespace WexHealth.Domain.Validators
{
    public class ClaimValidator : AbstractValidator<Claim>
    {
        public ClaimValidator()
        {
            RuleFor(x => x.Amount).Must(x => Regex.IsMatch(x.ToString(), "^[0-9]{1,10}([.][0-9]{1,2})?$"));
            RuleFor(x => x.EnrollmentId).GreaterThan(0).WithMessage("Invalid enrollment id");
        }
    }
}
