﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WexHealth.ParticipantPortal.Views
{
    public interface ILoginView
    {
        string UserName { get; set; }
        string Password { get; set; }
        string Domain { get; set; }
    }
}