﻿<%@ Page Title="Enrollments" Language="C#" MasterPageFile="~/Consumer.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WexHealth.ConsumerPortal.Enrollments.Enrollments_Index" %>

<%@ Register TagPrefix="UC" TagName="NavBar" Src="~/Controls/NavigationControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <UC:NavBar runat="server" CurrentPageName="Enrollments" />

    <div class="page__content">
        <div class="table__wrapper">
            <div class="table__title-wrapper clearfix">
                <div class="table__title">Active enrollments</div>
                <div class="table__controls"></div>
            </div>
            <table class="table">
                <thead class="table__thead">
                    <tr class="table__head">
                        <th class="table__column table__column_filter">
                            <div class="column__text">Name</div>
                        </th>
                        <th class="table__column">
                            <div class="column__text">Type</div>
                        </th>
                        <th class="table__column table__column_type_numeric">
                            <div class="column__text">Election</div>
                        </th>
                        <th class="table__column table__column_type_numeric">
                            <div class="column__text">Plan contribution</div>
                        </th>
                        <th class="table__column table__column_type_numeric">
                            <div class="column__text">Balance</div>
                        </th>
                        <th class="table__column"></th>
                    </tr>
                </thead>
                <tbody class="table__tbody">
                    <asp:Repeater runat="server" ID="listEnrollments" ItemType="WexHealth.Domain.Entities.Enrollment">
                        <ItemTemplate>
                            <tr class="table__row">
                                <td class="table__cell"><%#: Item.BenefitsOffering.Name %></td>
                                <td class="table__cell"><%#: Item.BenefitsOffering.PlanType.Name %></td>
                                <td class="table__cell table__cell_type_numeric">$ <%#: Item.ElectionAmount %></td>
                                <td class="table__cell table__cell_type_numeric">$ <%#: Item.BenefitsOffering.ContributionAmount %></td>
                                <td class="table__cell table__cell_type_numeric">$ <%#: Item.Contributions.Sum(x => x.Amount) %></td>
                                <td class="table__cell table__cell_type_action">
                                    <div runat="server" visible='<%# IsSettingAllow("Claim filling") %>'>
                                        <a href="<%#: Page.ResolveUrl("~/claims/file?e=" + Item.Id) %>" class="btn btn_priority_normal"><span class="btn__text">File claim</span></a>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>

