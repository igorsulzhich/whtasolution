﻿using System;
using System.Web;
using System.Collections.Generic;
using Microsoft.Practices.ServiceLocation;
using WexHealth.Domain.Entities;
using WexHealth.Security.Infrastructure;
using WexHealth.ConsumerPortal.Presentation.Views;
using WexHealth.ConsumerPortal.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.ConsumerPortal.Enrollments
{
    public partial class Enrollments_Index : System.Web.UI.Page, IEnrollmentsIndexView
    {
        private readonly IEnrollmentsIndexPresenter _presenter;

        public IEnumerable<Enrollment> Enrollments
        {
            set
            {
                listEnrollments.DataSource = value;
                listEnrollments.DataBind();
            }
        }

        public Enrollments_Index()
        {
            var factory = ServiceLocator.Current.GetInstance<EnrollmentsIndexPresenterFactory>();
            var principal = (WexHealthPrincipal)HttpContext.Current.User;
            _presenter = factory(this, principal);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.InitView(Page.IsPostBack);
        }

        protected bool IsSettingAllow(string settingKey)
        {
            return _presenter.IsSettingAllow(settingKey);
        }
    }
}