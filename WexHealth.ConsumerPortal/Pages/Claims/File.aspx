﻿<%@ Page Title="File a claim" Language="C#" MasterPageFile="~/Consumer.Master" AutoEventWireup="true" CodeBehind="File.aspx.cs" Inherits="WexHealth.ConsumerPortal.Claims.Claims_FileClaim" %>

<%@ Register TagPrefix="UC" TagName="NavBar" Src="~/Controls/NavigationControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <UC:NavBar runat="server" CurrentPageName="Claims" />

    <div class="page__content">
        <div class="form__wrapper">
            <asp:ValidationSummary runat="server" ValidationGroup="validationGroupFileClaim" DisplayMode="BulletList" ShowMessageBox="False" ShowSummary="True" CssClass="message message_type_error" />
            <asp:CustomValidator runat="server" ID="validateFileClaim" ValidationGroup="validationGroupFileClaim" Display="None" EnableClientScript="false" />
            <div class="form__head">
                <div class="form__title">New claim</div>
            </div>
            <div class="form">
                <div class="form__row inlinefix">
                    <div class="form__label">Plan</div>
                    <div class="form__input-wrapper">
                        <asp:DropDownList runat="server" ID="dropdownPlans" CssClass="input input_type_dropdown" AppendDataBoundItems="true">
                            <asp:ListItem Text="- Select plan -" Value=""></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator runat="server" ValidationGroup="validationGroupFileClaim" ControlToValidate="dropdownPlans" Display="None" InitialValue="" ErrorMessage="Select plan" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Date of service</div>
                    <div class="form__input-wrapper">
                        <input type="text" class="input input_type_text js-input_type_date" runat="server" id="textboxDateOfService" autocomplete="off" />
                        <asp:RequiredFieldValidator runat="server" ValidationGroup="validationGroupFileClaim" ControlToValidate="textboxDateOfService" Display="None" ErrorMessage="Enter date of service" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Amount</div>
                    <div class="form__input-wrapper">
                        <input type="text" class="input input_type_text" runat="server" id="textboxAmount" autocomplete="off" />
                        <asp:RequiredFieldValidator runat="server" ValidationGroup="validationGroupFileClaim" ControlToValidate="textboxAmount" ErrorMessage="Enter amount" Display="None" />
                        <asp:RegularExpressionValidator runat="server" ValidationGroup="validationGroupFileClaim" ControlToValidate="textboxAmount" ErrorMessage="Enter valid amount" Display="None" ValidationExpression="^[0-9]{1,10}([.][0-9]{1,2})?$" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Receipt</div>
                    <div class="form__input-wrapper">
                        <asp:FileUpload class="input input_type_file" runat="server" ID="fileReceipt" />
                    </div>
                    <asp:RequiredFieldValidator runat="server" ValidationGroup="validationGroupFileClaim" ControlToValidate="fileReceipt" ErrorMessage="Upload receipt" />
                    <asp:RegularExpressionValidator runat="server" ValidationGroup="validationGroupFileClaim" ControlToValidate="fileReceipt" ValidationExpression="(.*\.(jpg|JPG|jpeg|JPEG|png|PNG)$)" ErrorMessage="Invalid image format. We support: .jpg, .jpeg, .png" />
                </div>
                <div class="form__btns">
                    <asp:Button runat="server" ID="submitFileClaim" ValidationGroup="validationGroupFileClaim" CssClass="btn btn_priority_high" Text="Submit" OnClick="submitFileClaim_Click" />
                    <a href="<%= Page.ResolveUrl("~/claims") %>" class="btn btn_priority_normal"><span class="btn__text">Cancel</span></a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
