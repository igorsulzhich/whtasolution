﻿<%@ Page Title="Claims" Language="C#" MasterPageFile="~/Consumer.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WexHealth.ConsumerPortal.Claims.Claims_Index" %>

<%@ Register TagPrefix="UC" TagName="NavBar" Src="~/Controls/NavigationControl.ascx" %>
<%@ Import Namespace="System.Globalization" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <UC:NavBar runat="server" CurrentPageName="Claims" />

    <div class="page__content">
        <div class="message message_type_success" runat="server" id="messageSuccess" visible="false">
        </div>

        <div class="table__wrapper">
            <div class="table__title-wrapper clearfix">
                <div class="table__title">Claim's history</div>
                <div class="table__controls" runat="server" visible='<%# IsSettingAllow("Claim filling") %>'>
                    <a href="<%= Page.ResolveUrl("~/claims/file") %>" class="btn btn_priority_normal"><span class="btn__text">File claim</span></a>
                </div>
            </div>
            <table class="table">
                <thead class="table__thead">
                    <tr class="table__head">
                        <th class="table__column">
                            <div class="column__text">Claim number</div>
                        </th>
                        <th class="table__column">
                            <div class="column__text">Date of service</div>
                        </th>
                        <th class="table__column">
                            <div class="column__text">Plan</div>
                        </th>
                        <th class="table__column table__column_type_numeric">
                            <div class="column__text">Amount</div>
                        </th>
                        <th class="table__column">
                            <div class="column__text">Status</div>
                        </th>
                    </tr>
                </thead>
                <tbody class="table__tbody">
                    <asp:Repeater runat="server" ID="listClaims" ItemType="WexHealth.Domain.Entities.Claim">
                        <ItemTemplate>
                            <tr class="table__row">
                                <td class="table__cell"><%#: Item.Id %></td>
                                <td class="table__cell"><%#: Item.DateOfService.ToString("MM/d/yyyy", CultureInfo.InvariantCulture) %></td>
                                <td class="table__cell"><%#: Item.Enrollment.BenefitsOffering.PlanType.Name %></td>
                                <td class="table__cell table__cell_type_numeric">$ <%#: Item.Amount %></td>
                                <td class="table__cell"><%#: Item.Status.Name %></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
