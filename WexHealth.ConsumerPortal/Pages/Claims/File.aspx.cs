﻿using System;
using System.Web;
using System.Linq;
using System.Globalization;
using System.Collections.Generic;
using Microsoft.Practices.ServiceLocation;
using WexHealth.Domain.Entities;
using WexHealth.Security.Infrastructure;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.ConsumerPortal.Presentation.Views;
using WexHealth.ConsumerPortal.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.ConsumerPortal.Claims
{
    public partial class Claims_FileClaim : System.Web.UI.Page, IClaimsFileView
    {
        private readonly IClaimsFilePresenter _presenter;

        public IEnumerable<Enrollment> Enrollments
        {
            set
            {
                dropdownPlans.DataSource = value.Select(x => new
                {
                    Id = x.Id,
                    Text = $"{x.BenefitsOffering.Name} ({x.BenefitsOffering.PlanType.Name})"
                });
                dropdownPlans.DataTextField = "Text";
                dropdownPlans.DataValueField = "Id";
                dropdownPlans.DataBind();
            }
        }

        public int SelectedEnrollment
        {
            get
            {
                int result = 0;

                if (!Int32.TryParse(dropdownPlans.SelectedValue, out result))
                {
                    throw new InvalidOperationException("Invalid selected enrollment");
                }

                return result;
            }
            set
            {
                if (value > 0)
                {
                    dropdownPlans.SelectedValue = value.ToString();
                }
            }
        }

        public DateTime DateOfService
        {
            get
            {
                DateTime result;

                if (!DateTime.TryParse(textboxDateOfService.Value, CultureInfo.InvariantCulture, DateTimeStyles.None, out result))
                {
                    throw new InvalidOperationException("Invalid date format");
                }

                return result;
            }
        }

        public decimal Amount
        {
            get
            {
                Decimal result;

                if (!Decimal.TryParse(textboxAmount.Value, out result))
                {
                    throw new InvalidOperationException("Invalid decimal");
                }

                return result;
            }
        }

        public byte[] Receipt
        {
            get
            {
                return fileReceipt.HasFile ? fileReceipt.FileBytes : null;
            }
        }

        public Claims_FileClaim()
        {
            var factory = ServiceLocator.Current.GetInstance<ClaimsFilePresenterFactory>();
            var principal = (WexHealthPrincipal)HttpContext.Current.User;
            _presenter = factory(this, principal);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.InitView(Page.IsPostBack);

            if (!Page.IsPostBack)
            {
                InitSelectedPlan();
            }
        }

        private void InitSelectedPlan()
        {
            string enrollmentId = Request.QueryString["e"];
            if (!string.IsNullOrEmpty(enrollmentId))
            {
                SelectedEnrollment = Int32.Parse(enrollmentId);
            }
        }

        protected void submitFileClaim_Click(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                Page.Validate("validationGroupFileClaim");

                if (Page.IsValid)
                {
                    OperationResult result = (OperationResult)_presenter.SaveClaim();

                    if (result.Status == OperationStatus.Success)
                    {
                        if (!string.IsNullOrEmpty(result.Message))
                        {
                            Session["SuccessMessage"] = result.Message;
                        }

                        Response.Redirect("~/claims");
                    }
                    else
                    {
                        validateFileClaim.IsValid = false;
                        validateFileClaim.ErrorMessage = result.Message;
                    }
                }
            }
        }
    }
}