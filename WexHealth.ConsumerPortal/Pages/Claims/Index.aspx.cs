﻿using System;
using System.Web;
using System.Collections.Generic;
using Microsoft.Practices.ServiceLocation;
using WexHealth.Domain.Entities;
using WexHealth.Security.Infrastructure;
using WexHealth.ConsumerPortal.Presentation.Views;
using WexHealth.ConsumerPortal.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.ConsumerPortal.Claims
{
    public partial class Claims_Index : System.Web.UI.Page, IClaimsIndexView
    {
        private readonly IClaimsIndexPresenter _presenter;

        public IEnumerable<Claim> Claims
        {
            set
            {
                listClaims.DataSource = value;
                listClaims.DataBind();
            }
        }

        public Claims_Index()
        {
            var factory = ServiceLocator.Current.GetInstance<ClaimsIndexPresenterFactory>();
            var principal = (WexHealthPrincipal)HttpContext.Current.User;
            _presenter = factory(this, principal);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.InitView(Page.IsPostBack);

            if (!Page.IsPostBack)
            {
                if (Session["SuccessMessage"] != null)
                {
                    messageSuccess.InnerText = (string)Session["SuccessMessage"];
                    messageSuccess.Visible = true;

                    Session.Remove("SuccessMessage");
                }
            }

            DataBindChildren();
        }

        protected bool IsSettingAllow(string settingKey)
        {
            return _presenter.IsSettingAllow(settingKey);
        }
    }
}