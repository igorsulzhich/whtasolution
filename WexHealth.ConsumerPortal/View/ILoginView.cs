﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WexHealth.ParticipantPortal.Presenters;

namespace WexHealth.ParticipantPortal.View
{
    public interface ILoginView
    {
        string UserName { get; set; }
        string Password { get; set; }
        string Domain { get; set; }
    }
}