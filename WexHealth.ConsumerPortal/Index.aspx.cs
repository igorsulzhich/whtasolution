﻿using System;
using System.Web;
using Microsoft.Practices.ServiceLocation;
using WexHealth.Security.Infrastructure;
using WexHealth.Presentation.Views;
using WexHealth.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.ConsumerPortal
{
    public partial class Index : System.Web.UI.Page, IIndexView
    {
        private readonly IIndexPresenter _presenter;

        public Index()
        {
            var factory = ServiceLocator.Current.GetInstance<IndexPresenterFactory>();
            var principal = (WexHealthPrincipal)HttpContext.Current.User;
            _presenter = factory(this, principal);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.InitView(Page.IsPostBack);
        }
    }
}