﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WexHealth.ParticipantPortal.Model
{
    public class LoginModel
    {
        string UserName { get; set; }
        string Password { get; set; }
        string Domain { get; set; }
    }
}