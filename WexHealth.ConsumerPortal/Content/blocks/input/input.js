﻿; (function ($) {
    $('.js-input_type_date').datepicker({ dateFormat: "mm/d/yy" });
    $('.js-input_type_phone').mask("(99) 999-99-99");
    $('.js-input_type_ssn').mask("999-99-9999");
})(jQuery);