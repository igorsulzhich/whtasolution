﻿using System;
using System.Collections.Generic;
using System.Configuration.Provider;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace WexHealth.ParticipantPortal.Security
{
    public class WexHealthRoleProvider : RoleProvider
    {
        string connectionString = ""; //TODO

        public override string[] GetAllRoles()
        {
            string roleNames = ""; //TODO

            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand requestDB = new SqlCommand("", connection); //TODO
            SqlDataReader reader = null;

            try
            {
                connection.Open();

                reader = requestDB.ExecuteReader();

                while (reader.Read())
                {
                    roleNames += reader.GetString(0) + ",";
                }
            }
            catch (SqlException)
            {
                // TODO exception
            }
            finally
            {
                if (reader != null) { reader.Close(); }
                connection.Close();
            }

            if (roleNames.Length > 0)
            {
                roleNames = roleNames.Substring(0, roleNames.Length - 1);
                return roleNames.Split(',');
            }

            return new string[0];
        }

        public override string[] GetUsersInRole(string roleName)
        {
            if (roleName == null || roleName == "")
                throw new ProviderException("Role name cannot be empty or null.");

            string connectionString = ""; //TODO
            string userNames = ""; //TODO

            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand requestDB = new SqlCommand("", connection); //TODO
            SqlDataReader reader = null;

            try
            {
                connection.Open();

                reader = requestDB.ExecuteReader();

                while (reader.Read())
                {
                    userNames += reader.GetString(0) + ",";
                }
            }
            catch (SqlException)
            {
                // TODO exception
            }
            finally
            {
                if (reader != null) { reader.Close(); }
                connection.Close();
            }

            if (userNames.Length > 0)
            {
                userNames = userNames.Substring(0, userNames.Length - 1);
                return userNames.Split(',');
            }

            return new string[0];
        }
    }
}