﻿using System.Web.Optimization;

namespace WexHealth.ConsumerPortal
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/jquery-ui-{version}.js",
                "~/Scripts/jquery.maskedinput.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/input")
                .IncludeDirectory("~/Content/blocks/input", "*.js", true));

            bundles.Add(new ScriptBundle("~/bundles/table")
                .IncludeDirectory("~/Content/blocks/table", "*.js", true));

            bundles.Add(new StyleBundle("~/Content/index-page")
                .Include("~/Content/blocks/normalize.css")
                .Include("~/Content/blocks/common.css")
                .IncludeDirectory("~/Content/blocks/header", "*.css", true)
                .IncludeDirectory("~/Content/blocks/link", "*.css", true)
                .IncludeDirectory("~/Content/blocks/table", "*.css", true)
                .IncludeDirectory("~/Content/blocks/tabs", "*.css", true)
                .IncludeDirectory("~/Content/blocks/claim", "*.css", true)
                .IncludeDirectory("~/Content/blocks/message", "*.css", true)
                .IncludeDirectory("~/Content/blocks/form", "*.css", true)
                .IncludeDirectory("~/Content/blocks/input", "*.css", true)
                .IncludeDirectory("~/Content/blocks/btn", "*.css", true)
                .IncludeDirectory("~/Content/blocks/filter", "*.css", true));

            bundles.Add(new StyleBundle("~/Content/consumer-page")
                .Include("~/Content/blocks/normalize.css")
                .Include("~/Content/blocks/common.css")
                .IncludeDirectory("~/Content/blocks/header", "*.css", true)
                .IncludeDirectory("~/Content/blocks/link", "*.css", true)
                .IncludeDirectory("~/Content/blocks/table", "*.css", true)
                .IncludeDirectory("~/Content/blocks/tabs", "*.css", true)
                .IncludeDirectory("~/Content/blocks/claim", "*.css", true)
                .IncludeDirectory("~/Content/blocks/message", "*.css", true)
                .IncludeDirectory("~/Content/blocks/form", "*.css", true)
                .IncludeDirectory("~/Content/blocks/input", "*.css", true)
                .IncludeDirectory("~/Content/blocks/btn", "*.css", true)
                .IncludeDirectory("~/Content/blocks/filter", "*.css", true));

            bundles.Add(new StyleBundle("~/Content/themes/base/jquery")
                .IncludeDirectory("~/Content/themes/base", "*.css", true));

            BundleTable.EnableOptimizations = true;
        }
    }
}