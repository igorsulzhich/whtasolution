﻿<%@ Page Title="Home" Language="C#" MasterPageFile="~/Employer/Employer.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WexHealth.ConsumerPortal.Employer.Home" %>

<asp:Content ID="ContentEmployer" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <header class="header clearfix">
        <div class="header__logout">
            <a href="#" class="link">Log out</a>
        </div>
    </header>
    <nav class="tabs">
        <ul class="tabs__list inlinefix">
            <li class="tabs__item"><span class="tabs__link tabs__link_active">Home</span></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Employer/Employees/Index.aspx") %>" class="tabs__link">Employees</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Employer/Import/Index.aspx") %>" class="tabs__link">Import</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Employer/Setup/Index.aspx") %>" class="tabs__link">Setup</a></li>
        </ul>
    </nav>
    <div class="page__content">
    </div>
</asp:Content>
