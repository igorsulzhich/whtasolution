﻿<%@ Page Title="Employees" Language="C#" MasterPageFile="~/Employer/Employer.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WexHealth.ConsumerPortal.Employer.Employees" %>

<asp:Content ID="ContentEmployer" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <header class="header clearfix">
        <div class="header__logout">
            <a href="#" class="link">Log out</a>
        </div>
    </header>
    <nav class="tabs">
        <ul class="tabs__list inlinefix">
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Employer/Index.aspx") %>" class="tabs__link">Home</a></li>
            <li class="tabs__item"><span class="tabs__link tabs__link_active">Employees</span></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Employer/Import/Index.aspx") %>" class="tabs__link">Import</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Employer/Setup/Index.aspx") %>" class="tabs__link">Setup</a></li>
        </ul>
    </nav>
    <div class="page__content">

        <div class="table__wrapper">
            
            <div class="table__search">
                <div class="form__wrapper">
                    <form class="form form_orientation_horizontal inlinefix">
                        <div class="form__row form_orientation_horizontal__row">
                            <div class="form__label form_orientation_horizontal__label">First Name</div>
                            <div class="form__input-wrapper form_orientation_horizontal__input-wrapper">
                                <input type="text" class="input input_type_text" />
                            </div>
                        </div>
                        <div class="form__row form_orientation_horizontal__row">
                            <div class="form__label form_orientation_horizontal__label">Last Name</div>
                            <div class="form__input-wrapper form_orientation_horizontal__input-wrapper">
                                <input type="text" class="input input_type_text" />
                            </div>
                        </div>
                        <div class="form__row form_orientation_horizontal__row">
                            <div class="form__label form_orientation_horizontal__label">SSN</div>
                            <div class="form__input-wrapper form_orientation_horizontal__input-wrapper">
                                <input type="text" class="input input_type_text" />
                            </div>
                        </div>
                        <div class="form__btns form_orientation_horizontal__row__btns">
                            <input type="submit" class="btn btn_priority_high" value="Search" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="table__title-wrapper clearfix">
                <div class="table__title">Employees</div>
            </div>
            <table class="table">
                <thead class="table__thead">
                    <tr class="table__head">
                        <th class="table__column table__column_filter">
                            <div class="column__text">First Name</div>
                        </th>
                        <th class="table__column">
                            <div class="column__text">Last Name</div>
                        </th>
                        <th class="table__column">
                            <div class="column__text table__column_type_numeric">SSN</div>
                        </th>
                        <th class="table__column table__column_type_numeric">
                            <div class="column__text">Date of Birth</div>
                        </th>
                    </tr>
                </thead>
                <tbody class="table__tbody">
                    <tr class="table__row">
                        <td class="table__cell">Sergey</td>
                        <td class="table__cell">Babitski</td>
                        <td class="table__cell table__cell_type_numeric">123456789</td>
                        <td class="table__cell table__cell_type_numeric">22/11/1986</td>
                    </tr>
                    <tr class="table__row">
                        <td class="table__cell">Slava</td>
                        <td class="table__cell">Leventyuev</td>
                        <td class="table__cell table__cell_type_numeric">987654321</td>
                        <td class="table__cell table__cell_type_numeric">1/11/1984</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
