﻿<%@ Page Language="C#" Title="Import" MasterPageFile="~/Employer/Employer.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WexHealth.ConsumerPortal.Employer.Import" %>

<asp:Content ID="ContentEmployer" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <header class="header clearfix">
        <div class="header__logout">
            <a href="#" class="link">Log out</a>
        </div>
    </header>
    <nav class="tabs">
        <ul class="tabs__list inlinefix">
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Employer/Index.aspx") %>" class="tabs__link">Home</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Employer/Employees/Index.aspx") %>" class="tabs__link">Employees</a></li>
            <li class="tabs__item"><span class="tabs__link tabs__link_active">Import</span></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Employer/Setup/Index.aspx") %>" class="tabs__link">Setup</a></li>
        </ul>
    </nav>
    <div class="page__content">
        <div class="message__wrapper">
            <div class="message message_type_success">
                Файл <span class="message__key-word">Список_сотрудников.csv</span> успешно импортирован
            </div>
        </div>
        <div class="form__wrapper">
            <form class="form">
                <div class="form__head">
                    <div class="form__title">Import employees</div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Upload file</div>
                    <div class="form__input-wrapper">
                        <input type="file" class="input input_type_file" />
                    </div>
                    <div class="form__comment">
                        Locate file you want to upload. <a href="#" class="link">Download template</a>
                    </div>
                </div>
                <div class="form__btns">
                    <input type="submit" class="btn btn_priority_high" value="Import" />
                </div>
            </form>
        </div>
    </div>
</asp:Content>
