﻿<%@ Page Title="Logout" Language="C#" MasterPageFile="~/Employer/Authentication.Master" AutoEventWireup="true" CodeBehind="Logout.aspx.cs" Inherits="WexHealth.ConsumerPortal.Employer.Logout" %>

<asp:Content ID="ContentEmployer" ContentPlaceHolderID="ContentPlaceHolderLogin" runat="server">
    <div class="page__content">
        <p>You have been logged out of the application and may now close your browser.</p>
    </div>
</asp:Content>