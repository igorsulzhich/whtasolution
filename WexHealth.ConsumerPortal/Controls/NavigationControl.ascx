﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NavigationControl.ascx.cs" Inherits="WexHealth.ConsumerPortal.Controls.NavigationControl" %>

    <nav class="tabs">
        <ul class="tabs__list inlinefix">
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/") %>" class="tabs__link <%= IsCurrentPage("Home")  %>">Home</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/enrollments") %>" class="tabs__link <%= IsCurrentPage("Enrollments") %>">Enrollments</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/claims") %>" class="tabs__link <%= IsCurrentPage("Claims") %>">Claims</a></li>
        </ul>
    </nav>