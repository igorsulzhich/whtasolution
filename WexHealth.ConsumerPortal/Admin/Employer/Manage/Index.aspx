﻿<%@ Page Title="Manage | Employer" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WexHealth.ConsumerPortal.Admin.EmployerManager" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <header class="header clearfix">
        <div class="header__logout">
            <a href="#" class="link">Log out</a>
        </div>
    </header>
    <nav class="tabs">
        <ul class="tabs__list inlinefix">
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Index.aspx") %>" class="tabs__link">Home</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Consumer/Index.aspx") %>" class="tabs__link">Consumer</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Employer/Index.aspx") %>" class="tabs__link tabs__link_active">Employer</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Claims/Index.aspx") %>" class="tabs__link">Claims</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Reports/Index.aspx") %>" class="tabs__link">Reports</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Setup/Index.aspx") %>" class="tabs__link">Setup</a></li>
        </ul>
    </nav>
    <div class="page__content">
        <div class="active clearfix">
            <div class="active__title-wrapper">
                <div class="active__title">Amato</div>
                <ul class="active__info-list inlinefix">
                   <li class="active__info-item"><span class="active__info-label">Code</span> Amato</li>
                    <li class="active__info-item"><span class="active__info-label">City</span> New York</li>
                    <li class="active__info-item"><span class="active__info-label">Zip code</span> 214</li>
                    <li class="active__info-item"><span class="active__info-label">Phone</span> +375 29 635-21-21</li>              
                </ul>
            </div>
            <div class="active__controls">
                <a href="<%= Page.ResolveUrl("~/Admin/Employer/Index.aspx") %>" class="btn btn_priority_normal"><span class="btn__text">Select a different employer</span></a>
            </div>
        </div>
        <div class="setting-wrapper">
            <div class="setting__groups inlinefix">
                <div class="setting__group-item">
                    <div class="setting__title">Profile</div>
                    <ul class="setting__list">
                        <li class="setting__item"><a href="<%= Page.ResolveUrl("~/Admin/Employer/Manage/Edit-Profile.aspx") %>" class="setting__link link">Edit</a></li>
                    </ul>
                </div>
                <div class="setting__group-item">
                    <div class="setting__title">Setup</div>
                    <ul class="setting__list">
                        <li class="setting__item"><a href="<%= Page.ResolveUrl("~/Admin/Employer/Manage/Setup.aspx") %>" class="setting__link link">Manage consumer portal rules</a></li>
                    </ul>
                </div>
                <div class="setting__group-item">
                    <div class="setting__title">Plans</div>
                    <ul class="setting__list">
                        <li class="setting__item"><a href="<%= Page.ResolveUrl("~/Admin/Employer/Manage/PlanYear/Index.aspx") %>" class="setting__link link">Manage</a></li>
                    </ul>
                </div>
                <div class="setting__group-item">
                    <div class="setting__title">Consumers</div>
                    <ul class="setting__list">
                        <li class="setting__item"><a href="<%= Page.ResolveUrl("~/Admin/Consumer/Index.aspx") %>" class="setting__link link">Manage</a></li>
                        <li class="setting__item"><a href="<%= Page.ResolveUrl("~/Admin/Employer/Manage/Add-Consumer.aspx") %>" class="setting__link link">Add consumer</a></li>
                    </ul>
                </div>
                <div class="setting__group-item">
                    <div class="setting__title">Users</div>
                    <ul class="setting__list">
                        <li class="setting__item"><a href="<%= Page.ResolveUrl("~/Admin/Employer/Manage/Users/Index.aspx") %>" class="setting__link link">Manage</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
