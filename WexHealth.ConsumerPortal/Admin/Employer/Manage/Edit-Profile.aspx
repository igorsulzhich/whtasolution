﻿<%@ Page Title="Edit profile | Employer" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Edit-Profile.aspx.cs" Inherits="WexHealth.ConsumerPortal.Admin.EditEmployerProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <header class="header clearfix">
        <div class="header__logout">
            <a href="#" class="link">Log out</a>
        </div>
    </header>
    <nav class="tabs">
        <ul class="tabs__list inlinefix">
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Index.aspx") %>" class="tabs__link">Home</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Consumer/Index.aspx") %>" class="tabs__link">Consumer</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Employer/Index.aspx") %>" class="tabs__link tabs__link_active">Employer</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Claims/Index.aspx") %>" class="tabs__link">Claims</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Reports/Index.aspx") %>" class="tabs__link">Reports</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Setup/Index.aspx") %>" class="tabs__link">Setup</a></li>
        </ul>
    </nav>
    <div class="page__content">
        <div class="active clearfix">
            <div class="active__title-wrapper">
                <div class="active__title">Amato</div>
                <ul class="active__info-list inlinefix">
                    <li class="active__info-item"><span class="active__info-label">Code</span> Amato</li>
                    <li class="active__info-item"><span class="active__info-label">City</span> New York</li>
                    <li class="active__info-item"><span class="active__info-label">Zip code</span> 214</li>
                    <li class="active__info-item"><span class="active__info-label">Phone</span> +375 29 635-21-21</li>
                </ul>
            </div>
            <div class="active__controls">
                <a href="<%= Page.ResolveUrl("~/Admin/Employer/Index.aspx") %>" class="btn btn_priority_normal"><span class="btn__text">Select a different employer</span></a>
            </div>
        </div>
        <div class="form__wrapper">
            <div class="form__head">
                <div class="form__title">Edit profile</div>
            </div>
            <form class="form">
                <div class="form__row inlinefix">
                    <div class="form__label">Name</div>
                    <div class="form__input-wrapper">
                        <input type="text" class="input input_type_text" value="Starbucks" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Code</div>
                    <div class="form__input-wrapper">
                        <input type="text" class="input input_type_text" value="SB" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Street</div>
                    <div class="form__input-wrapper">
                        <input type="text" class="input input_type_text" value="London Bridge 25" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">City</div>
                    <div class="form__input-wrapper">
                        <input type="text" class="input input_type_text" value="New York" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Zip code</div>
                    <div class="form__input-wrapper">
                        <input type="text" class="input input_type_text" value="124634" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Phone</div>
                    <div class="form__input-wrapper">
                        <input type="text" class="input input_type_text" value="125 123-41-24" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Logo</div>
                    <div class="form__input-wrapper">
                        <input type="file" class="input input_type_file" />
                    </div>
                </div>
                <div class="form__btns">
                    <input type="submit" class="btn btn_priority_high" value="Submit" />
                    <a href="<%= Page.ResolveUrl("~/Admin/Employer/Manage/Index.aspx") %>" class="btn btn_priority_normal"><span class="btn__text">Cancel</span></a>
                </div>
            </form>
        </div>
    </div>
</asp:Content>
