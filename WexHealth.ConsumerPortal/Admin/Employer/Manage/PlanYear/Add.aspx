﻿<%@ Page Title="Add plan year | Employer" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="WexHealth.ConsumerPortal.Admin.Add_Employer_Plan_Year" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <header class="header clearfix">
        <div class="header__logout">
            <a href="#" class="link">Log out</a>
        </div>
    </header>
    <nav class="tabs">
        <ul class="tabs__list inlinefix">
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Index.aspx") %>" class="tabs__link">Home</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Consumer/Index.aspx") %>" class="tabs__link">Consumer</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Employer/Index.aspx") %>" class="tabs__link tabs__link_active">Employer</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Claims/Index.aspx") %>" class="tabs__link">Claims</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Reports/Index.aspx") %>" class="tabs__link">Reports</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Setup/Index.aspx") %>" class="tabs__link">Setup</a></li>
        </ul>
    </nav>
    <div class="page__content">
        <div class="active clearfix">
            <div class="active__title-wrapper">
                <div class="active__title">Amato</div>
                <ul class="active__info-list inlinefix">
                    <li class="active__info-item"><span class="active__info-label">Code</span> Amato</li>
                    <li class="active__info-item"><span class="active__info-label">City</span> New York</li>
                    <li class="active__info-item"><span class="active__info-label">Zip code</span> 214</li>
                    <li class="active__info-item"><span class="active__info-label">Phone</span> +375 29 635-21-21</li>
                </ul>
            </div>
            <div class="active__controls">
                <a href="<%= Page.ResolveUrl("~/Admin/Employer/Index.aspx") %>" class="btn btn_priority_normal"><span class="btn__text">Select a different employer</span></a>
            </div>
        </div>

        <div class="form__wrapper">
            <div class="form__head">
                <div class="form__title">New plan year</div>
            </div>
            <form class="form">
                <div class="form__row inlinefix">
                    <div class="form__label">Start date</div>
                    <div class="form__input-wrapper">
                        <input type="text" class="input input_type_text" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">End date</div>
                    <div class="form__input-wrapper">
                        <input type="text" class="input input_type_text" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Payroll frequency</div>
                    <div class="form__input-wrapper">
                        <select class="input input_type_dropdown">
                            <option value="1">Weekly</option>
                            <option value="2">Monthly</option>
                        </select>
                    </div>
                </div>
                <div class="table__wrapper" style="-moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box; margin: 60px 0 10px 0; padding: 0 30px 0 150px;">
                    <div class="table__title-wrapper">
                        <div class="form__row inlinefix">
                            <div class="form__input-wrapper">
                                <select class="input input_type_dropdown">
                                    <option value="1">Medical</option>
                                    <option value="2">Dental</option>
                                </select>
                            </div>
                            <a href="#" class="btn btn_priority_normal" style="margin: 0 0 0 10px;"><span class="btn__text">Add</span></a>
                        </div>

                    </div>
                    <table class="table">
                        <thead class="table__thead">
                            <tr class="table__head">
                                <th class="table__column table__column_filter">
                                    <div class="column__text">Name</div>
                                </th>
                                <th class="table__column">
                                    <div class="column__text">Type</div>
                                </th>
                                <th class="table__column table__column_type_numeric">
                                    <div class="column__text">Contributions</div>
                                </th>
                            </tr>
                        </thead>
                        <tbody class="table__tbody">
                            <tr class="table__row">
                                <td class="table__cell">General Medical Insurance</td>
                                <td class="table__cell">Medical</td>
                                <td class="table__cell table__cell_type_numeric">$ 2000</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="form__btns">
                    <input type="submit" class="btn btn_priority_high" value="Submit" />
                    <a href="<%= Page.ResolveUrl("~/Admin/Employer/Manage/PlanYear/Index.aspx") %>" class="btn btn_priority_normal"><span class="btn__text">Cancel</span></a>
                </div>
            </form>
        </div>
    </div>
</asp:Content>
