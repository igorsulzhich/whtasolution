﻿<%@ Page Title="Plan years | Employer" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WexHealth.ConsumerPortal.Admin.Manage_Plan_Years" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <header class="header clearfix">
        <div class="header__logout">
            <a href="#" class="link">Log out</a>
        </div>
    </header>
    <nav class="tabs">
        <ul class="tabs__list inlinefix">
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Index.aspx") %>" class="tabs__link">Home</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Consumer/Index.aspx") %>" class="tabs__link">Consumer</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Employer/Index.aspx") %>" class="tabs__link tabs__link_active">Employer</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Claims/Index.aspx") %>" class="tabs__link">Claims</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Reports/Index.aspx") %>" class="tabs__link">Reports</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Setup/Index.aspx") %>" class="tabs__link">Setup</a></li>
        </ul>
    </nav>
    <div class="page__content">
        <div class="active clearfix">
            <div class="active__title-wrapper">
                <div class="active__title">Amato</div>
                <ul class="active__info-list inlinefix">
                    <li class="active__info-item"><span class="active__info-label">Code</span> Amato</li>
                    <li class="active__info-item"><span class="active__info-label">City</span> New York</li>
                    <li class="active__info-item"><span class="active__info-label">Zip code</span> 214</li>
                    <li class="active__info-item"><span class="active__info-label">Phone</span> +375 29 635-21-21</li>
                </ul>
            </div>
            <div class="active__controls">
                <a href="<%= Page.ResolveUrl("~/Admin/Employer/Index.aspx") %>" class="btn btn_priority_normal"><span class="btn__text">Select a different employer</span></a>
            </div>
        </div>
        <div class="table__wrapper">
            <div class="table__title-wrapper">
                <div class="table__title">Plan year</div>
                <div class="table__controls">
                    <a href="<%= Page.ResolveUrl("~/Admin/Employer/Manage/PlanYear/Add.aspx") %>" class="btn btn_priority_normal"><span class="btn__text">Add plan year</span></a>
                </div>
            </div>
            <div class="filter table_filter">
                <ul class="filter__list inlinefix">
                    <li class="filter__item"><span class="filter__link filter__link_active">All</span></li>
                    <li class="filter__item"><a href="#" class="filter__link">Initialized</a></li>
                    <li class="filter__item"><a href="#" class="filter__link">Not initialized</a></li>
                </ul>
            </div>
            <table class="table">
                <thead class="table__thead">
                    <tr class="table__head">
                        <th class="table__column table__column_filter">
                            <div class="column__text">Name</div>
                        </th>
                        <th class="table__column">
                            <div class="column__text">Status</div>
                        </th>
                        <th class="table__column"></th>
                    </tr>
                </thead>
                <tbody class="table__tbody">
                    <tr class="table__row">
                        <td class="table__cell">Plan for 2016-2017</td>
                        <td class="table__cell">Not initizalized</td>
                        <td class="table__cell table__cell_type_action">
                            
                            <a href="<%= Page.ResolveUrl("~/Admin/Employer/Manage/PlanYear/Edit.aspx") %>" class="btn btn_priority_normal" style="margin: 0 0 0 10px;"><span class="btn__text">Update</span></a>

                            <a href="#" class="btn btn_priority_normal" style="margin: 0 0 0 10px;"><span class="btn__text">Initizalize</span></a>
                            <a href="#" class="btn btn_priority_normal" style="margin:0 0 0 40px;" ><span class="btn__text">Remove</span></a>
                        </td>
                    </tr>
                    <tr class="table__row">
                        <td class="table__cell">Plan for 2017-2018</td>
                        <td class="table__cell">Initialized on 7 July 2016</td>
                        <td class="table__cell table__cell_type_action">
                            <a href="#" class="btn btn_priority_normal" style="margin: 0 0 0 10px;"><span class="btn__text">Remove</span></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
