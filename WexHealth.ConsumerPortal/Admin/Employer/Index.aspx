﻿<%@ Page Title="Employer" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WexHealth.ConsumerPortal.Admin.Employer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <header class="header clearfix">
        <div class="header__logout">
            <a href="#" class="link">Log out</a>
        </div>
    </header>
    <nav class="tabs">
        <ul class="tabs__list inlinefix">
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Index.aspx") %>" class="tabs__link">Home</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Consumer/Index.aspx") %>" class="tabs__link">Consumer</a></li>
            <li class="tabs__item"><span class="tabs__link tabs__link_active">Employer</span></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Claims/Index.aspx") %>" class="tabs__link">Claims</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Reports/Index.aspx") %>" class="tabs__link">Reports</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Setup/Index.aspx") %>" class="tabs__link">Setup</a></li>
        </ul>
    </nav>
    <div class="page__content">

        <div class="table__wrapper">
            <div class="table__search">
                <div class="form__wrapper">
                    <form class="form form_orientation_horizontal inlinefix">
                        <div class="form__row form_orientation_horizontal__row">
                            <div class="form__label form_orientation_horizontal__label">Name</div>
                            <div class="form__input-wrapper form_orientation_horizontal__input-wrapper">
                                <input type="text" class="input input_type_text" />
                            </div>
                        </div>
                        <div class="form__row form_orientation_horizontal__row">
                            <div class="form__label form_orientation_horizontal__label">Code</div>
                            <div class="form__input-wrapper form_orientation_horizontal__input-wrapper">
                                <input type="text" class="input input_type_text" />
                            </div>
                        </div>
                        <div class="form__btns form_orientation_horizontal__row__btns">
                            <input type="submit" class="btn btn_priority_high" value="Search" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="table__title-wrapper clearfix">
                <div class="table__title">All employers</div>
                <div class="table__controls">
                    <a href="<%= Page.ResolveUrl("~/Admin/Employer/Add.aspx") %>" class="btn btn_priority_normal"><span class="btn__text">Add employer</span></a>
                </div>
            </div>
            <table class="table">
                <thead class="table__thead">
                    <tr class="table__head">
                        <th class="table__column table__column_filter">
                            <div class="column__text">Name</div>
                        </th>
                        <th class="table__column">
                            <div class="column__text">Code</div>
                        </th>
                    </tr>
                </thead>
                <tbody class="table__tbody">
                    <tr class="table__row js-table__row" data-href="<%= Page.ResolveUrl("~/Admin/Employer/Manage/Index.aspx") %>">
                        <td class="table__cell">Starbucks</td>
                        <td class="table__cell">SB</td>
                    </tr>
                    <tr class="table__row js-table__row" data-href="<%= Page.ResolveUrl("~/Admin/Employer/Manage/Index.aspx") %>">
                        <td class="table__cell">Guess</td>
                        <td class="table__cell">GSS</td>
                    </tr>
                    <tr class="table__row js-table__row" data-href="<%= Page.ResolveUrl("~/Admin/Employer/Manage/Index.aspx") %>">
                        <td class="table__cell">American Apparel</td>
                        <td class="table__cell">AMA</td>
                    </tr>
                    <tr class="table__row js-table__row" data-href="<%= Page.ResolveUrl("~/Admin/Employer/Manage/Index.aspx") %>">
                        <td class="table__cell">Papa Johns</td>
                        <td class="table__cell">PJ</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
