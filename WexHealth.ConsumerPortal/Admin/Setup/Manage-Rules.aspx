﻿<%@ Page Title="Manage rules | Setup" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Manage-Rules.aspx.cs" Inherits="WexHealth.ConsumerPortal.Admin.Setup.Manage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <header class="header clearfix">
        <div class="header__logout">
            <a href="#" class="link">Log out</a>
        </div>
    </header>
    <nav class="tabs">
        <ul class="tabs__list inlinefix">
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Index.aspx") %>" class="tabs__link">Home</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Consumer/Index.aspx") %>" class="tabs__link">Consumer</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Employer/Index.aspx") %>" class="tabs__link">Employer</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Claims/Index.aspx") %>" class="tabs__link">Claims</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Reports/Index.aspx") %>" class="tabs__link">Reports</a></li>
            <li class="tabs__item"><span class="tabs__link tabs__link_active">Setup</span></li>
        </ul>
    </nav>
    <div class="page__content">
        <div class="form__wrapper">
            <div class="form__head">
                <div class="form__title">Consumer management</div>
            </div>
            <form class="form">
                <div class="form__row inlinefix">
                    <div class="form__label">Claim Filing</div>
                    <div class="form__input-wrapper">
                        <input class="input input_type_radiobutton" type="radio" name="setup" checked="checked" />
                        <div class="input_type_radiobutton__label">Allow</div>
                        <input class="input input_type_radiobutton" type="radio" name="setup" />
                        <div class="input_type_radiobutton__label">Prevent</div>
                    </div>
                </div>
                <div class="form__btns">
                    <input type="submit" class="btn btn_priority_high" value="Submit" />
                    <a href="<%= Page.ResolveUrl("~/Admin/Setup/Index.aspx") %>" class="btn btn_priority_normal"><span class="btn__text">Cancel</span></a>
                </div>
            </form>
        </div>
    </div>
</asp:Content>
