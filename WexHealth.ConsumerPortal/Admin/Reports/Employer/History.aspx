﻿<%@ Page Title="History of requests | Employer" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="History.aspx.cs" Inherits="WexHealth.ConsumerPortal.Admin.Reports.Employers.Reports_Employers_History" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <header class="header clearfix">
        <div class="header__logout">
            <a href="#" class="link">Log out</a>
        </div>
    </header>
    <nav class="tabs">
        <ul class="tabs__list inlinefix">
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Index.aspx") %>" class="tabs__link">Home</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Consumer/Index.aspx") %>" class="tabs__link">Consumer</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Employer/Index.aspx") %>" class="tabs__link">Employer</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Claims/Index.aspx") %>" class="tabs__link">Claims</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Reports/Index.aspx") %>" class="tabs__link tabs__link_active">Reports</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Setup/Index.aspx") %>" class="tabs__link">Setup</a></li>
        </ul>
    </nav>
    <div class="page__content">
        <div class="table__wrapper">
            <div class="table__title-wrapper clearfix">
                <div class="table__title">Emoloyer's requested reports</div>
                <div class="table__controls">
                    <a href="<%= Page.ResolveUrl("~/Admin/Reports/Employer/Add-Request.aspx") %>" class="btn btn_priority_normal">Add request</a>
                </div>
            </div>
            <div class="filter table_filter">
                <ul class="filter__list inlinefix">
                    <li class="filter__item"><span class="filter__link filter__link_active">All</span></li>
                    <li class="filter__item"><a href="#" class="filter__link">Requested</a></li>
                    <li class="filter__item"><a href="#" class="filter__link">Available</a></li>
                    <li class="filter__item"><a href="#" class="filter__link">Failed</a></li>
                </ul>
            </div>
            <table class="table">
                <thead class="table__thead">
                    <tr class="table__head">
                        <th class="table__column table__column_filter">
                            <div class="column__text">Request status</div>
                        </th>
                        <th class="table__column">
                            <div class="column__text">Date</div>
                        </th>
                        <th class="table__column">
                            <div class="column__text">Requested by</div>
                        </th>
                        <th class="table__column">
                            <div class="column__text">Format</div>
                        </th>
                        <th class="table__column">
                        </th>
                    </tr>
                </thead>
                <tbody class="table__tbody">
                    <tr class="table__row">
                        <td class="table__cell">Requested</td>
                        <td class="table__cell">7 July 2016</td>
                        <td class="table__cell">SAM\Sergey Babitskiy</td>
                        <td class="table__cell">CSV</td>
                        <td class="table__cell table__cell_type_action">
                            <a href="#" class="btn btn_priority_normal"><span class="btn__text">Reject</span></a>
                        </td>
                    </tr>
                    <tr class="table__row">
                        <td class="table__cell"><a href="#" class="link">Available</a></td>
                        <td class="table__cell">7 July 2016</td>
                        <td class="table__cell">SAM\Sergey Babitskiy</td>
                        <td class="table__cell">CSV</td>
                        <td class="table__cell table__cell_type_action">
                            <a href="#" class="btn btn_priority_normal"><span class="btn__text">Remove</span></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
