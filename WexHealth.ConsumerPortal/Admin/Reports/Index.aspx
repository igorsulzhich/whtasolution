﻿<%@ Page Title="Reports" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WexHealth.ConsumerPortal.Admin.Reports_Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <header class="header clearfix">
        <div class="header__logout">
            <a href="#" class="link">Log out</a>
        </div>
    </header>
    <nav class="tabs">
        <ul class="tabs__list inlinefix">
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Index.aspx") %>" class="tabs__link">Home</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Consumer/Index.aspx") %>" class="tabs__link">Consumer</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Employer/Index.aspx") %>" class="tabs__link">Employer</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Claims/Index.aspx") %>" class="tabs__link">Claims</a></li>
            <li class="tabs__item"><span class="tabs__link tabs__link_active">Reports</span></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Setup/Index.aspx") %>" class="tabs__link">Setup</a></li>
        </ul>
    </nav>
    <div class="page__content">
        <div class="setting-wrapper">
            <div class="setting__groups inlinefix">
                <div class="setting__group-item">
                    <div class="setting__title">Consumer's reports</div>
                    <ul class="setting__list">
                        <li class="setting__item"><a href="<%= Page.ResolveUrl("~/Admin/Reports/Consumer/Add-Request.aspx") %>" class="setting__link link">Add request</a></li>
                        <li class="setting__item"><a href="<%= Page.ResolveUrl("~/Admin/Reports/Consumer/History.aspx") %>" class="setting__link link">History of requests</a></li>
                    </ul>
                </div>
                <div class="setting__group-item">
                    <div class="setting__title">Employer's reports</div>
                    <ul class="setting__list">
                        <li class="setting__item"><a href="<%= Page.ResolveUrl("~/Admin/Reports/Employer/Add-Request.aspx") %>" class="setting__link link">Add request</a></li>
                        <li class="setting__item"><a href="<%= Page.ResolveUrl("~/Admin/Reports/Employer/History.aspx") %>" class="setting__link link">History of requests</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
