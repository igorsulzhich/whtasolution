﻿<%@ Page Title="Add request | Consumer" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Add-Request.aspx.cs" Inherits="WexHealth.ConsumerPortal.Admin.Reports.Consumers.Reports_Consumer_AddRequest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <header class="header clearfix">
        <div class="header__logout">
            <a href="#" class="link">Log out</a>
        </div>
    </header>
    <nav class="tabs">
        <ul class="tabs__list inlinefix">
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Index.aspx") %>" class="tabs__link">Home</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Consumer/Index.aspx") %>" class="tabs__link">Consumer</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Employer/Index.aspx") %>" class="tabs__link">Employer</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Claims/Index.aspx") %>" class="tabs__link">Claims</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Reports/Index.aspx") %>" class="tabs__link tabs__link_active">Reports</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Setup/Index.aspx") %>" class="tabs__link">Setup</a></li>
        </ul>
    </nav>
    <div class="page__content">
        <div class="form__wrapper">
            <div class="form__head">
                <div class="form__title">Request for consumer's report</div>
            </div>
            <form class="form">
                <div class="form__row inlinefix">
                    <div class="form__label">Report format</div>
                    <div class="form__input-wrapper">
                        <input class="input input_type_radiobutton" type="radio" name="setup" checked="checked" />
                        <div class="input_type_radiobutton__label">CSV</div>
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Employer</div>
                    <div class="form__input-wrapper">
                        <select class="input input_type_dropdown">
                            <option value="0">None</option>
                            <option value="1">General medical insurance (Medical)</option>
                            <option value="2">Dental insurance (Dental)</option>
                        </select>
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label" style="vertical-align: top;">Include field</div>
                    <div class="form__input-wrapper input__list">
                        <div class="input__item inlinefix">
                            <input class="input_type_checkbox" type="checkbox" name="setup" checked="checked" />
                            <label class="input_item__label">First name</label>
                        </div>
                        <div class="input__item inlinefix">
                            <input class="input_type_checkbox" type="checkbox" name="setup" checked="checked" />
                            <label class="input_item__label">Last name</label>
                        </div>
                        <div class="input__item inlinefix">
                            <input class="input_type_checkbox" type="checkbox" name="setup" checked="checked" />
                            <label class="input_item__label">SSN</label>
                        </div>
                        <div class="input__item inlinefix">
                            <input class="input_type_checkbox" type="checkbox" name="setup" checked="checked" />
                            <label class="input_item__label">Phone</label>
                        </div>
                        <div class="input__item inlinefix">
                            <input class="input_type_checkbox" type="checkbox" name="setup" checked="checked" />
                            <label class="input_item__label">Street</label>
                        </div>
                        <div class="input__item inlinefix">
                            <input class="input_type_checkbox" type="checkbox" name="setup" checked="checked" />
                            <label class="input_item__label">City</label>
                        </div>
                        <div class="input__item inlinefix">
                            <input class="input_type_checkbox" type="checkbox" name="setup" checked="checked" />
                            <label class="input_item__label">State</label>
                        </div>
                        <div class="input__item inlinefix">
                            <input class="input_type_checkbox" type="checkbox" name="setup" checked="checked" />
                            <label class="input_item__label">Zip code</label>
                        </div>
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Notifications</div>
                    <div class="form__input-wrapper input__list">
                        <div class="input__item inlinefix">
                            <input class="input_type_checkbox" type="checkbox" name="setup" checked="checked" />
                            <label class="input_item__label">Email me when the report is available</label>
                        </div>
                    </div>
                </div>
                <div class="form__btns">
                    <input type="submit" class="btn btn_priority_high" value="Submit" />
                    <a href="<%= Page.ResolveUrl("~/Admin/Reports/Index.aspx") %>" class="btn btn_priority_normal"><span class="btn__text">Cancel</span></a>
                </div>
            </form>
        </div>
    </div>
</asp:Content>
