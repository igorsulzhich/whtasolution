﻿<%@ Page Title="Consumer" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WexHealth.ConsumerPortal.Admin.Consumer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <header class="header clearfix">
        <div class="header__logout">
            <a href="#" class="link">Log out</a>
        </div>
    </header>
    <nav class="tabs">
        <ul class="tabs__list inlinefix">
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Index.aspx") %>" class="tabs__link">Home</a></li>
            <li class="tabs__item"><span class="tabs__link tabs__link_active">Consumer</span></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Employer/Index.aspx") %>" class="tabs__link">Employer</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Claims/Index.aspx") %>" class="tabs__link">Claims</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Reports/Index.aspx") %>" class="tabs__link">Reports</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Setup/Index.aspx") %>" class="tabs__link">Setup</a></li>
        </ul>
    </nav>
    <div class="page__content">
        <div class="table__wrapper">
            <div class="table__search">
                <div class="form__wrapper">
                    <form class="form form_orientation_horizontal inlinefix">
                        <div class="form__row form_orientation_horizontal__row">
                            <div class="form__label form_orientation_horizontal__label">Last name</div>
                            <div class="form__input-wrapper form_orientation_horizontal__input-wrapper">
                                <input type="text" class="input input_type_text" />
                            </div>
                        </div>
                        <div class="form__row form_orientation_horizontal__row">
                            <div class="form__label form_orientation_horizontal__label">First name</div>
                            <div class="form__input-wrapper form_orientation_horizontal__input-wrapper">
                                <input type="text" class="input input_type_text" />
                            </div>
                        </div>
                        <div class="form__row form_orientation_horizontal__row">
                            <div class="form__label form_orientation_horizontal__label">Employer</div>
                            <div class="form__input-wrapper">
                                <select class="input input_type_dropdown">
                                    <option value="1">Starbucks</option>
                                    <option value="2">American Apparel</option>
                                    <option value="2">Guess</option>
                                    <option value="2">Papa Johns</option>
                                </select>
                            </div>
                        </div>
                        <div class="form__btns form_orientation_horizontal__row__btns">
                            <input type="submit" class="btn btn_priority_high" value="Search" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="table__title-wrapper clearfix">
                <div class="table__title">All consumers</div>
            </div>
            <table class="table">
                <thead class="table__thead">
                    <tr class="table__head">
                        <th class="table__column table__column_filter">
                            <div class="column__text">Last name</div>
                        </th>
                        <th class="table__column">
                            <div class="column__text">First name</div>
                        </th>
                        <th class="table__column table__cell_type_numeric">
                            <div class="column__text">SSN</div>
                        </th>
                    </tr>
                </thead>
                <tbody class="table__tbody">
                    <tr class="table__row js-table__row" data-href="<%= Page.ResolveUrl("~/Admin/Consumer/Manage/Index.aspx") %>">
                        <td class="table__cell">Grabovski</td>
                        <td class="table__cell">Artem</td>
                        <td class="table__cell table__cell_type_numeric">126-35-1953</td>
                    </tr>
                    <tr class="table__row js-table__row" data-href="<%= Page.ResolveUrl("~/Admin/Consumer/Manage/Index.aspx") %>">
                        <td class="table__cell">Sulzhich</td>
                        <td class="table__cell">Igor</td>
                        <td class="table__cell table__cell_type_numeric">686-19-1267</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
