﻿<%@ Page Title="Manage | Consumer" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WexHealth.ConsumerPortal.Admin.Consumer_Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <header class="header clearfix">
        <div class="header__logout">
            <a href="#" class="link">Log out</a>
        </div>
    </header>
    <nav class="tabs">
        <ul class="tabs__list inlinefix">
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Index.aspx") %>" class="tabs__link">Home</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Consumer/Index.aspx") %>" class="tabs__link tabs__link_active">Consumer</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Employer/Index.aspx") %>" class="tabs__link">Employer</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Claims/Index.aspx") %>" class="tabs__link">Claims</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Reports/Index.aspx") %>" class="tabs__link">Reports</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Setup/Index.aspx") %>" class="tabs__link">Setup</a></li>
        </ul>
    </nav>
    <div class="page__content">
        <div class="active clearfix">
            <div class="active__title-wrapper">
                <div class="active__title">Igor Sulzhich</div>
                <ul class="active__info-list inlinefix">
                    <li class="active__info-item"><span class="active__info-label">SSN</span> 123-23-421</li>
                    <li class="active__info-item"><span class="active__info-label">Employer</span> <a href="<%= Page.ResolveUrl("~/Admin/Employer/Manage/Index.aspx") %>" class="link">AMATO</a></li>                    
                </ul>
            </div>
            <div class="active__controls">
                <a href="<%= Page.ResolveUrl("~/Admin/Consumer/Index.aspx") %>" class="btn btn_priority_normal"><span class="btn__text">Select a different consumer</span></a>
            </div>
        </div>

        <div class="setting-wrapper">
            <div class="setting__groups inlinefix">
                <div class="setting__group-item">
                    <div class="setting__title">Profile</div>
                    <ul class="setting__list">
                        <li class="setting__item"><a href="<%= Page.ResolveUrl("~/Admin/Consumer/Manage/Edit-Profile.aspx") %>" class="setting__link link">Edit</a></li>
                        <li class="setting__item"><a href="<%= Page.ResolveUrl("~/Admin/Consumer/Manage/Enrollment/Index.aspx") %>" class="setting__link link">Manage enrollment</a></li>
                    </ul>
                </div>
                <div class="setting__group-item">
                    <div class="setting__title">Claims</div>
                    <ul class="setting__list">
                        <li class="setting__item"><a href="<%= Page.ResolveUrl("~/Admin/Claims/Index.aspx") %>" class="setting__link link">Manage</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
