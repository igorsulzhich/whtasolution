﻿<%@ Page Title="Enrollments | Consumer" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WexHealth.ConsumerPortal.Admin.Enrollment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <header class="header clearfix">
        <div class="header__logout">
            <a href="#" class="link">Log out</a>
        </div>
    </header>
    <nav class="tabs">
        <ul class="tabs__list inlinefix">
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Index.aspx") %>" class="tabs__link">Home</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Consumer/Index.aspx") %>" class="tabs__link tabs__link_active">Consumer</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Employer/Index.aspx") %>" class="tabs__link">Employer</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Claims/Index.aspx") %>" class="tabs__link">Claims</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Reports/Index.aspx") %>" class="tabs__link">Reports</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Setup/Index.aspx") %>" class="tabs__link">Setup</a></li>
        </ul>
    </nav>

    <div class="page__content">
        <div class="active clearfix">
            <div class="active__title-wrapper">
                <div class="active__title">Igor Sulzhich</div>
                <ul class="active__info-list inlinefix">
                    <li class="active__info-item"><span class="active__info-label">SSN</span> 123-23-421</li>
                    <li class="active__info-item"><span class="active__info-label">Employer</span> <a href="<%= Page.ResolveUrl("~/Admin/Employer/Manage/Index.aspx") %>" class="link">AMATO</a></li>
                </ul>
            </div>
            <div class="active__controls">
                <a href="<%= Page.ResolveUrl("~/Admin/Consumer/Index.aspx") %>" class="btn btn_priority_normal"><span class="btn__text">Select a different consumer</span></a>
            </div>
        </div>
        <div class="table__wrapper">
            <div class="table__title-wrapper">
                <div class="table__title">
                    Enrollment
                </div>
                <div class="table__controls">
                    <a href="#" class="btn btn_priority_normal"><span class="btn__text">Add enrollment</span></a>
                </div>
            </div>
            <table class="table">
                <thead class="table__thead">
                    <tr class="table__head">
                        <th class="table__column ">
                            <div class="column__text">Plan</div>
                        </th>
                        <th class="table__column table__column_type_numeric">
                            <div class="column__text">Election</div>
                        </th>
                        <th class="table__column table__column_type_numeric">
                            <div class="column__text">Contribution</div>
                        </th>
                        <th class="table__column"></th>
                    </tr>
                </thead>
                <tbody class="table__tbody">
                    <tr class="table__row">
                        <td class="table__cell">General Medical incurance</td>
                        <td class="table__cell table__cell_type_numeric">$100</td>
                        <td class="table__cell table__cell_type_numeric">$2000</td>
                        <td class="table__cell table__cell_type_action">
                            <a href="#" class="btn btn_priority_normal"><span class="btn__text">Update</span></a>
                        </td>
                    </tr>
                </tbody>
            </table>
            <%--<input type="submit" class="btn btn_priority_high" value="Submit" />
            <a href="/Admin/Consumer.aspx" class="btn btn_priority_normal" style="margin: 0 0 0 10px;"><span class="btn__text">Cancel</span></a>--%>
        </div>
    </div>

    <div class="pop-up">
        <div class="pop-up__title__wrapper">
            <div class="pop-up__title">Add Enrollment</div>
            <div class="pop-up__close"><a href="#" class="pop-up__close_link">X</a></div>
        </div>
        <div class="pop-up__form__wrapper">
            <div class="form__wrapper">
                <form class="form">
                    <div class="form__row inlinefix">
                        <div class="form__label">Plan</div>
                        <div class="form__input-wrapper">
                            <select class="input input_type_dropdown">
                                <option value="1">General Medical</option>
                                <option value="2">General Medical Plan</option>
                            </select>
                        </div>
                    </div>
                    <div class="form__row inlinefix">
                        <div class="form__label">Election</div>
                        <div class="form__input-wrapper">
                            <input type="text" class="input input_type_text" />
                        </div>
                    </div>
                    <div class="form__row inlinefix">
                        <div class="form__label">Contributions</div>
                        <div class="form__input-wrapper">
                            $2000
                        </div>
                    </div>
                    <div class="form__btns">
                        <input type="submit" class="btn btn_priority_high" value="Submit" />
                        <a href="<%= Page.ResolveUrl("~/Admin/Consumer/Manage/Index.aspx") %>>" class="btn btn_priority_normal"><span class="btn__text">Cancel</span></a>
                    </div>
                </form>
            </div>
        </div>
    </div>

</asp:Content>

