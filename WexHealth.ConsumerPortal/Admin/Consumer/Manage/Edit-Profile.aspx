﻿<%@ Page Title="Edit profile | Consumer" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Edit-Profile.aspx.cs" Inherits="WexHealth.ConsumerPortal.Admin.Consumer_Edit" %>

<asp:Content ID="ContentEdit" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <header class="header clearfix">
        <div class="header__logout">
            <a href="#" class="link">Log out</a>
        </div>
    </header>
    <nav class="tabs">
        <ul class="tabs__list inlinefix">
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Index.aspx") %>" class="tabs__link">Home</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Consumer/Index.aspx") %>" class="tabs__link tabs__link_active">Consumer</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Employer/Index.aspx") %>" class="tabs__link">Employer</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Claims/Index.aspx") %>" class="tabs__link">Claims</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Reports/Index.aspx") %>" class="tabs__link">Reports</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Setup/Index.aspx") %>" class="tabs__link">Setup</a></li>
        </ul>
    </nav>
    <div class="page__content">
        <div class="active clearfix">
            <div class="active__title-wrapper">
                <div class="active__title">Igor Sulzhich</div>
                <ul class="active__info-list inlinefix">
                    <li class="active__info-item"><span class="active__info-label">SSN</span> 123-23-421</li>
                    <li class="active__info-item"><span class="active__info-label">Employer</span> <a href="<%= Page.ResolveUrl("~/Admin/Employer/Manage/Index.aspx") %>" class="link">AMATO</a></li>
                </ul>
            </div>
            <div class="active__controls">
                <a href="<%= Page.ResolveUrl("~/Admin/Consumer/Index.aspx") %>" class="btn btn_priority_normal"><span class="btn__text">Select a different consumer</span></a>
            </div>
        </div>
        <div class="form__wrapper">
            <div class="form__head">
                <div class="form__title">Edit profile</div>
            </div>
            <form class="form">
                <div class="form__row inlinefix">
                    <div class="form__label">First Name</div>
                    <div class="form__input-wrapper">
                        <input type="text" class="input input_type_text" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Last Name</div>
                    <div class="form__input-wrapper">
                        <input type="text" class="input input_type_text" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">SSN</div>
                    <div class="form__input-wrapper">
                        <input type="text" class="input input_type_text" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Phone</div>
                    <div class="form__input-wrapper">
                        <input type="text" class="input input_type_text" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Street</div>
                    <div class="form__input-wrapper">
                        <input type="text" class="input input_type_text" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">City</div>
                    <div class="form__input-wrapper">
                        <input type="text" class="input input_type_text" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">State</div>
                    <div class="form__input-wrapper">
                        <select class="input input_type_dropdown">
                            <option value="1">Minnesota</option>
                            <option value="2">Next State</option>
                        </select>
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Zip Code</div>
                    <div class="form__input-wrapper">
                        <input type="text" class="input input_type_text" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Username</div>
                    <div class="form__input-wrapper">
                        <input type="text" class="input input_type_text" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Password</div>
                    <div class="form__input-wrapper">
                        <input type="password" class="input input_type_text" />
                    </div>
                </div>
                <div class="form__btns">
                    <input type="submit" class="btn btn_priority_high" value="Submit" />
                    <a href="<%= Page.ResolveUrl("~/Admin/Consumer/Manage/Index.aspx") %>" class="btn btn_priority_normal"><span class="btn__text">Cancel</span></a>
                </div>
            </form>
        </div>
    </div>
</asp:Content>
