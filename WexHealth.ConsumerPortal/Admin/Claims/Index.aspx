﻿<%@ Page Title="Claims" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WexHealth.ConsumerPortal.Admin.Claims" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <header class="header clearfix">
        <div class="header__logout">
            <a href="#" class="link">Log out</a>
        </div>
    </header>
    <nav class="tabs">
        <ul class="tabs__list inlinefix">
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Index.aspx") %>" class="tabs__link">Home</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Consumer/Index.aspx") %>" class="tabs__link">Consumer</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Employer/Index.aspx") %>" class="tabs__link">Employer</a></li>
            <li class="tabs__item"><span class="tabs__link tabs__link_active">Claims</span></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Reports/Index.aspx") %>" class="tabs__link">Reports</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Setup/Index.aspx") %>" class="tabs__link">Setup</a></li>
        </ul>
    </nav>
    <div class="page__content">
        <div class="table__wrapper">
            <div class="table__search">
                <div class="form__wrapper">
                    <form class="form form_orientation_horizontal inlinefix">
                        <div class="form__row form_orientation_horizontal__row">
                            <div class="form__label form_orientation_horizontal__label">Number</div>
                            <div class="form__input-wrapper form_orientation_horizontal__input-wrapper">
                                <input type="text" class="input input_type_text" />
                            </div>
                        </div>
                        <div class="form__row form_orientation_horizontal__row">
                            <div class="form__label form_orientation_horizontal__label">Employer</div>
                            <div class="form__input-wrapper">
                                <select class="input input_type_dropdown">
                                    <option value="1">Starbucks</option>
                                    <option value="2">American Apparel</option>
                                    <option value="2">Guess</option>
                                    <option value="2">Papa Johns</option>
                                </select>
                            </div>
                        </div>
                        <div class="form__btns form_orientation_horizontal__row__btns">
                            <input type="submit" class="btn btn_priority_high" value="Search" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="table__title-wrapper clearfix">
                <div class="table__title">All claims</div>
            </div>
            <div class="filter table_filter">
                <ul class="filter__list inlinefix">
                    <li class="filter__item"><span class="filter__link filter__link_active">All</span></li>
                    <li class="filter__item"><a href="#" class="filter__link">Pending approval</a></li>
                    <li class="filter__item"><a href="#" class="filter__link">Approved</a></li>
                    <li class="filter__item"><a href="#" class="filter__link">Denied</a></li>
                </ul>
            </div>
            <table class="table">
                <thead class="table__thead">
                    <tr class="table__head">
                        <th class="table__column table__column_filter">
                            <div class="column__text">Number</div>
                        </th>
                        <th class="table__column">
                            <div class="column__text">Consumer</div>
                        </th>
                        <th class="table__column">
                            <div class="column__text">Employer</div>
                        </th>
                        <th class="table__column">
                            <div class="column__text">Date of service</div>
                        </th>
                        <th class="table__column">
                            <div class="column__text">Type</div>
                        </th>
                        <th class="table__column table__column_type_numeric">
                            <div class="column__text">Amount</div>
                        </th>
                    </tr>
                </thead>
                <tbody class="table__tbody">
                    <tr class="table__row js-table__row" data-href="<%= Page.ResolveUrl("~/Admin/Claims/Manage/Index.aspx") %>">
                        <td class="table__cell">SAMAMATO120716R0000101</td>
                        <td class="table__cell"><a href="<%= Page.ResolveUrl("~/Admin/Consumer/Manage/Index.aspx") %>" class="link">Artem Grabovski</a></td>
                        <td class="table__cell"><a href="<%= Page.ResolveUrl("~/Admin/Employer/Manage/Index.aspx") %>" class="link">Starbucks</a></td>
                        <td class="table__cell">12 July 2016</td>
                        <td class="table__cell">Medical</td>
                        <td class="table__cell table__cell_type_numeric">$ 4.99</td>
                    </tr>
                    <tr class="table__row js-table__row" data-href="<%= Page.ResolveUrl("~/Admin/Claims/Manage/Index.aspx") %>">
                        <td class="table__cell">SAMAMATO120716R0003501</td>
                        <td class="table__cell"><a href="<%= Page.ResolveUrl("~/Admin/Consumer/Manage/Index.aspx") %>" class="link">Igor Sulzhich</a></td>
                        <td class="table__cell"><a href="<%= Page.ResolveUrl("~/Admin/Employer/Manage/Index.aspx") %>" class="link">Starbucks</a></td>
                        <td class="table__cell">12 July 2016</td>
                        <td class="table__cell">Medical</td>
                        <td class="table__cell table__cell_type_numeric">$ 185.52</td>
                    </tr>
                    <tr class="table__row js-table__row" data-href="<%= Page.ResolveUrl("~/Admin/Claims/Manage/Index.aspx") %>">
                        <td class="table__cell">SAMAMATO120716R0001501</td>
                        <td class="table__cell"><a href="<%= Page.ResolveUrl("~/Admin/Consumer/Manage/Index.aspx") %>" class="link">Sergei Babtskiy</a></td>
                        <td class="table__cell"><a href="<%= Page.ResolveUrl("~/Admin/Employer/Manage/Index.aspx") %>" class="link">American Apparel</a></td>
                        <td class="table__cell">12 July 2016</td>
                        <td class="table__cell">Dental</td>
                        <td class="table__cell table__cell_type_numeric">$ 185.52</td>
                    </tr>
                    <tr class="table__row js-table__row" data-href="<%= Page.ResolveUrl("~/Admin/Claims/Manage/Index.aspx") %>">
                        <td class="table__cell">SAMAMATO120716R0010101</td>
                        <td class="table__cell"><a href="<%= Page.ResolveUrl("~/Admin/Consumer/Manage/Index.aspx") %>" class="link">Slava Leventyuev</a></td>
                        <td class="table__cell"><a href="<%= Page.ResolveUrl("~/Admin/Employer/Manage/Index.aspx") %>" class="link">American Apparel</a></td>
                        <td class="table__cell">12 July 2016</td>
                        <td class="table__cell">Medical</td>
                        <td class="table__cell table__cell_type_numeric">$ 4.33</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
