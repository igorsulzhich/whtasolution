﻿<%@ Page Title="Edit claim" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Edit-Claim.aspx.cs" Inherits="WexHealth.ConsumerPortal.Admin.Edit_Claims" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <header class="header clearfix">
        <div class="header__logout">
            <a href="#" class="link">Log out</a>
        </div>
    </header>
    <nav class="tabs">
        <ul class="tabs__list inlinefix">
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Index.aspx") %>" class="tabs__link">Home</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Consumer/Index.aspx") %>" class="tabs__link">Consumer</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Employer/Index.aspx") %>" class="tabs__link">Employer</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Claims/Index.aspx") %>" class="tabs__link tabs__link_active">Claims</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Reports/Index.aspx") %>" class="tabs__link">Reports</a></li>
            <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/Admin/Setup/Index.aspx") %>" class="tabs__link">Setup</a></li>
        </ul>
    </nav>
    <div class="page__content">
        <div class="active clearfix">
            <div class="active__title-wrapper">
                <div class="active__title">
                    <div class="active__title-label">Сlaim number</div>
                    SAMAMATO20170642
                </div>
            </div>
            <div class="active__controls">
            </div>
        </div>
        <div class="form__wrapper">
            <div class="form__head">
                <div class="form__title">Details</div>
            </div>
            <form class="form">
                <div class="form__row inlinefix">
                    <div class="form__label">First Name</div>
                    <div class="form__input-wrapper">
                        <a href="<%= Page.ResolveUrl("~/Admin/Consumer/Manage/Index.aspx") %>" class="link">Sergei Babitskiy</a>
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Phone</div>
                    <div class="form__input-wrapper">
                        <a href="#" class="link">+375 29 376-07-96</a>
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Date of service</div>
                    <div class="form__input-wrapper">
                        <input type="text" class="input input_type_text" value="7 July 2016" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Plan</div>
                    <div class="form__input-wrapper">
                        <select class="input input_type_dropdown">
                            <option value="1">Medical</option>
                            <option value="2">Dental</option>
                        </select>
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">State</div>
                    <div class="form__input-wrapper">
                        <input type="text" class="input input_type_text" value="$ 4.33" />
                    </div>
                </div>

                <div class="form__btns">
                    <input type="submit" class="btn btn_priority_high" value="Save" />
                    <a href="<%= Page.ResolveUrl("~/Admin/Claims/Manage/Index.aspx") %>" class="btn btn_priority_normal"><span class="btn__text">Cancel</span></a>
                </div>
            </form>
        </div>
    </div>
</asp:Content>
