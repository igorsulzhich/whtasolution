﻿<%@ Page Title="Login" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WexHealth.ConsumerPortal.Admin.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="page__content">
        <div class="form__wrapper">
            <form class="form">
                <div class="form__row inlinefix">
                    <div class="form__label">Login</div>
                    <div class="form__input-wrapper">
                        <input type="text" class="input input_type_text" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Password</div>
                    <div class="form__input-wrapper">
                        <input type="text" class="input input_type_text" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Domain</div>
                    <div class="form__input-wrapper">
                        <select class="input input_type_dropdown">
                            <option value="1">SAM</option>
                            <option value="2">Oktopus</option>
                        </select>
                    </div>
                </div>
                <div class="form__btns">
                    <input type="submit" class="btn btn_priority_high" value="Login" />
                </div>
            </form>
        </div>
    </div>
</asp:Content>
