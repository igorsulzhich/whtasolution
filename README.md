## Database setup ##

Database is stored in **WHTASolution\db** folder. Database is compatible with SQL Server 2014 and upper. To use this database change connection strings path in configuration files in the following projects:

* **WexHealth.AdminPortal**
* **WexHealth.EmployerPortal**
* **WexHealth.ParticipantPortal**
* **WexHealth.TPASetupTool**

```
#!C#

<connectionStrings>

<add 
name="DefaultConnection" 
connectionString="Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='[PATH_TO_DIRECTORY]\whtasolution\db\db.mdf';Integrated Security=True;" 
providerName="System.Data.SqlClient" />

</connectionStrings>
```

## Logs ##
Logs for web projects are stored in **WHTASolution\logs** folder. Logs for TPA Setup are stored in **WHTASolution\WexHealth.TPASetupTool\bin\Debug\logs** folder.