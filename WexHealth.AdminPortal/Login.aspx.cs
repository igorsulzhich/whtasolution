﻿using System;
using System.Web;
using System.Web.Security;
using System.Collections.Generic;
using Microsoft.Practices.ServiceLocation;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Presentation.Presenters.Common.Interfaces;
using WexHealth.Presentation.Views;
using WexHealth.Presentation.Presenters.Results;

namespace WexHealth.AdminPortal
{
    public partial class Login : System.Web.UI.Page, ILoginView
    {
        private readonly ILoginPresenter _presenter;

        public string UserName
        {
            get
            {
                return textboxUserName.Value;
            }
        }

        public string Password
        {
            get
            {
                return textboxPassword.Value;
            }
        }

        public Guid SelectedDomain
        {
            get
            {
                Guid result = Guid.Empty;
                string param = dropdownSecurityDomains.SelectedValue;

                if (!Guid.TryParse(param, out result))
                {
                    throw new InvalidOperationException("Invalid security domain");
                }

                return result;
            }
        }

        public IEnumerable<SecurityDomain> Domains
        {
            set
            {
                dropdownSecurityDomains.DataSource = value;
                dropdownSecurityDomains.DataTextField = "Alias";
                dropdownSecurityDomains.DataValueField = "Id";
                dropdownSecurityDomains.DataBind();
            }
        }

        public Login()
        {
            var loginService = ServiceLocator.Current.GetInstance<ILoginUserService>("Administrator");
            var presenterFactory = ServiceLocator.Current.GetInstance<LoginPresenterFactory>();
            _presenter = presenterFactory(this, loginService);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.InitView(Page.IsPostBack);
        }

        protected void Login_Click(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                Page.Validate("validationGroupLogin");

                if (Page.IsValid)
                {
                    LoginResult result = (LoginResult)_presenter.Login();

                    if (result.Status == LoginStatus.Success)
                    {
                        var ticket = new FormsAuthenticationTicket(
                            1,
                            UserName,
                            DateTime.Now,
                            DateTime.Now.AddMinutes(FormsAuthentication.Timeout.Minutes),
                            true,
                            "",
                            FormsAuthentication.FormsCookiePath
                            );

                        string encryptedTicket = FormsAuthentication.Encrypt(ticket);
                        string redirectUrl = FormsAuthentication.GetRedirectUrl(UserName, true);

                        var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                        cookie.Expires = DateTime.Now.AddDays(30);
                        Response.Cookies.Add(cookie);

                        Response.Redirect(redirectUrl);
                    }
                    else
                    {
                        validateLogin.IsValid = false;
                        validateLogin.ErrorMessage = result.ErrorMessage;
                    }
                }
            }
        }
    }
}