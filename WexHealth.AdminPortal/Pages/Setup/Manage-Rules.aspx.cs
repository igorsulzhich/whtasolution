﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Microsoft.Practices.ServiceLocation;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Models;
using WexHealth.Security.Infrastructure;
using WexHealth.Presentation.Views;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.AdminPortal.Pages.Setup
{
    public partial class Manage_Rules : System.Web.UI.Page, ISettingView
    {
        private readonly ISetupManagePresenter _presenter;

        public IEnumerable<SettingInputModel> SelectedSettings
        {
            get
            {
                foreach (var rowSetting in tableSettings.Controls.OfType<RepeaterItem>())
                {
                    var setttingId = rowSetting.FindControl("settingId") as HtmlInputHidden;

                    var allowRadio = rowSetting.FindControl("allowSetting") as HtmlInputRadioButton;
                    var disallowRadio = rowSetting.FindControl("disallowSetting") as HtmlInputRadioButton;

                    yield return new SettingInputModel
                    {
                        Id = Guid.Parse(setttingId.Value),
                        isAllowed = (allowRadio.Checked) ? true : false
                    };
                }
            }
        }

        public IEnumerable<Setting> SettingList
        {
            set
            {
                tableSettings.DataSource = value;
                tableSettings.DataBind();

                foreach (var rowSetting in tableSettings.Controls.OfType<RepeaterItem>())
                {
                    var settingIdInput = rowSetting.FindControl("settingId") as HtmlInputHidden;
                    Guid settingId = Guid.Parse(settingIdInput.Value);

                    var allowRadio = rowSetting.FindControl("allowSetting") as HtmlInputRadioButton;
                    var disallowRadio = rowSetting.FindControl("disallowSetting") as HtmlInputRadioButton;

                    bool? isAllowed = value.Where(x => x.Id.Equals(settingId)).Select(x => x.isAllowed).FirstOrDefault();

                    if (!isAllowed.HasValue || isAllowed.Value)
                    {
                        allowRadio.Checked = true;
                        disallowRadio.Checked = false;
                    }
                    else
                    {
                        allowRadio.Checked = false;
                        disallowRadio.Checked = true;
                    }
                }
            }
        }

        public Manage_Rules()
        {
            var factory = ServiceLocator.Current.GetInstance<SetupManagePresenterFactory>();
            var principal = (WexHealthPrincipal)HttpContext.Current.User;
            _presenter = factory(this, principal);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.InitView(Page.IsPostBack);
        }

        protected void Setting_Click(object sender, EventArgs e)
        {
            OperationResult saveResult = (OperationResult)_presenter.SaveSettings();
            if (OperationStatus.Success == saveResult.Status)
            {
                Response.Redirect(Page.ResolveUrl("~/setup"));
            }
            else
            {
                ShowErrorMessage(saveResult.Message);
            }
        }

        public void ShowErrorMessage(string errorMessage)
        {
            throw new NotImplementedException();
        }
    }
}