﻿<%@ Page Title="Setup" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true"  EnableEventValidation="false"  CodeBehind="Index.aspx.cs" Inherits="WexHealth.AdminPortal.Pages.Setup.Index" %>
<%@ Register TagPrefix="UC" TagName="NavBar" Src="~/Controls/NavigationControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <UC:NavBar runat="server" CurrentPageName="Setup"/>

    <div class="page__content">
        <div class="setting-wrapper">
            <div class="setting__groups inlinefix">
                <div class="setting__group-item">
                    <div class="setting__title">Setup</div>
                    <ul class="setting__list">
                        <li class="setting__item"><a href="<%= Page.ResolveUrl("~/setup/manage-rules") %>" class="setting__link link">Manage consumer portal rules</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</asp:Content>