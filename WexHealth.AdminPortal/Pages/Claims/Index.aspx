﻿<%@ Page Title="Claims" Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin.Master" EnableEventValidation="false" CodeBehind="Index.aspx.cs" Inherits="WexHealth.AdminPortal.Pages.Claims.Index" %>

<%@ Register TagPrefix="UC" TagName="NavBar" Src="~/Controls/NavigationControl.ascx" %>
<%@ Import Namespace="System.Globalization" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <UC:NavBar runat="server" CurrentPageName="Claims" />

    <div class="page__content">
        <div class="table__wrapper">
            <div class="table__search">
                <div class="form__wrapper">
                    <div class="form form_orientation_horizontal inlinefix">
                        <div class="form__row form_orientation_horizontal__row">
                            <div class="form__label form_orientation_horizontal__label">Number</div>
                            <div class="form__input-wrapper form_orientation_horizontal__input-wrapper">
                                <asp:TextBox ID="textboxClaimNumber" runat="server" CssClass="input input_type_text" autocomplete="off" />
                            </div>
                        </div>
                        <div class="form__row form_orientation_horizontal__row">
                            <div class="form__label form_orientation_horizontal__label">Employer</div>
                            <div class="form__input-wrapper">
                                <asp:DropDownList runat="server" ID="dropdownEmployers" CssClass="input input_type_dropdown" AppendDataBoundItems="true">
                                    <asp:ListItem Text="- Select employer -" Value="" />
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form__btns form_orientation_horizontal__row__btns">
                            <asp:Button ID="submitSearch" OnClick="submitSearch_Click" CssClass="btn btn_priority_high" Text="Search" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="table__title-wrapper clearfix">
                <div class="table__title">All claims</div>
            </div>
            <div class="filter table_filter">
                <ul class="filter__list inlinefix">
                    <asp:Repeater runat="server" ID="listClaimStatuses" ItemType="WexHealth.Domain.Entities.ClaimStatus" OnItemDataBound="listClaimStatuses_ItemDataBound">
                        <ItemTemplate>
                            <li class="filter__item"><a runat="server" id="linkClaimStatus" href="<%# GetClaimLink(Item.Name) %>" class="filter__link"><%#: Item.Name %></a></li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </div>
            <table class="table">
                <thead class="table__thead">
                    <tr class="table__head">
                        <th class="table__column table__column_filter">
                            <div class="column__text">Number</div>
                        </th>
                        <th class="table__column">
                            <div class="column__text">Consumer</div>
                        </th>
                        <th class="table__column">
                            <div class="column__text">Employer</div>
                        </th>
                        <th class="table__column">
                            <div class="column__text">Date of service</div>
                        </th>
                        <th class="table__column">
                            <div class="column__text">Type</div>
                        </th>
                        <th class="table__column table__column_type_numeric">
                            <div class="column__text">Amount</div>
                        </th>
                    </tr>
                </thead>
                <tbody class="table__tbody">
                    <asp:Repeater ID="listClaims" runat="server" ItemType="WexHealth.Domain.Entities.Claim">
                        <ItemTemplate>
                            <tr class="table__row table__row_linked js-table__row" data-href="<%# Page.ResolveUrl("~/claims/" + Item.Id + "/manage") %>">
                                <td class="table__cell"><%#: Item.Id %></td>
                                <td class="table__cell"><a href="<%# Page.ResolveUrl("~/consumer/" + Item.Enrollment.ConsumerId + "/manage") %>" class="link"><%#: Item.Enrollment.Consumer.Individual.FirstName %> <%#: Item.Enrollment.Consumer.Individual.LastName %></a></td>
                                <td class="table__cell"><a href="<%# Page.ResolveUrl("~/employer/" + Item.Enrollment.Consumer.Employer.Code + "/manage") %>" class="link"><%#: Item.Enrollment.Consumer.Employer.Name %> (<%#: Item.Enrollment.Consumer.Employer.Code %>)</a></td>
                                <td class="table__cell"><%#: Item.DateOfService.ToString("MM/d/yyyy", CultureInfo.InvariantCulture) %></td>
                                <td class="table__cell"><%#: Item.Enrollment.BenefitsOffering.PlanType.Name %></td>
                                <td class="table__cell table__cell_type_numeric">$ <%#: Item.Amount %></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
        </div>
    </div>

</asp:Content>
