﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.Routing;
using Microsoft.Practices.ServiceLocation;
using WexHealth.Security.Infrastructure;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;
using WexHealth.AdminPortal.Presentation.Views;

namespace WexHealth.AdminPortal.Pages.Claims.Manage
{
    public partial class Claims_Manage_Index : System.Web.UI.Page, IClaimManageView
    {
        private readonly IClaimManagePresenter _presenter;

        public string ClaimId
        {
            get
            {
                return RouteData.Values["select"].ToString();
            }
        }

        public Guid ConsumerId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PhoneNumber { get; set; }

        public DateTime DateOfService { get; set; }

        public string PlanType { get; set; }

        public decimal Amount { get; set; }

        public string ImageFile { get; set; }

        public string StatusName
        {
            set
            {
                if (value.Equals("Approved"))
                {
                    btnApprove.Visible = false;
                }
                else if (value.Equals("Denied"))
                {
                    btnDeny.Visible = false;
                }
            }
        }

        public Claims_Manage_Index()
        {
            var factory = ServiceLocator.Current.GetInstance<ClaimManagePresenterFactory>();
            var principal = (WexHealthPrincipal)HttpContext.Current.User;
            _presenter = factory(this, principal);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.InitView(Page.IsPostBack);

            Page.Title = "Claim #" + ClaimId;
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            var result = (OperationResult)_presenter.ApproveClaim();
            if (result.Status == OperationStatus.Success)
            {
                Response.Redirect(Page.ResolveUrl("~/claims"));
            }
        }

        protected void btnDeny_Click(object sender, EventArgs e)
        {
            var result = (OperationResult)_presenter.DenyClaim();
            if (result.Status == OperationStatus.Success)
            {
                Response.Redirect(Page.ResolveUrl("~/claims"));
            }
        }
    }
}