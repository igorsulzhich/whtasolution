﻿<%@ Page Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WexHealth.AdminPortal.Pages.Claims.Manage.Claims_Manage_Index" %>

<%@ Register TagPrefix="UC" TagName="NavBar" Src="~/Controls/NavigationControl.ascx" %>
<%@ Import Namespace="System.Globalization" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <UC:NavBar runat="server" CurrentPageName="Claims" />

    <div class="page__content">
        <div class="active clearfix">
            <div class="active__title-wrapper">
                <div class="active__title">
                    <div class="active__title-label">Сlaim number</div>
                    <%= ClaimId %></div>
            </div>
            <div class="active__controls">
                <a href="<%= Page.ResolveUrl("~/claims/" + ClaimId + "/edit-claim") %>" class="btn btn_priority_normal">Edit</a>
            </div>
        </div>

        <div class="form__wrapper">
            <div class="form__head">
                <div class="form__title">Details</div>
            </div>
            <div class="form">
                <div class="form__row inlinefix">
                    <div class="form__label">Name</div>
                    <div class="form__input-wrapper">
                        <a href="<%= Page.ResolveUrl("~/consumer/" + ConsumerId + "/manage") %>" class="link"><%= FirstName + " " + LastName %></a>
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Phone</div>
                    <div class="form__input-wrapper">
                        <%= PhoneNumber %>
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Date of service</div>
                    <div class="form__input-wrapper">
                        <%= DateOfService.ToString("MM/d/yyyy", CultureInfo.InvariantCulture) %>
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Plan</div>
                    <div class="form__input-wrapper">
                        <%= PlanType %>
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">State</div>
                    <div class="form__input-wrapper">
                        $ <%= Amount %>
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label"></div>
                    <div class="form__input-wrapper">
                        <%= ImageFile.Equals(String.Empty) ? "" : String.Format("<img style='height:300px; display:block;' src='{0}'/>", ImageFile) %>
                    </div>
                </div>
                <div class="form__btns">
                    <asp:LinkButton runat="server" ID="btnApprove" CssClass="btn btn_priority_high" OnClick="btnApprove_Click">
                        <span class="btn__text">Approve</span>
                    </asp:LinkButton>
                    <asp:LinkButton runat="server" ID="btnDeny" CssClass="btn btn_priority_normal" OnClick="btnDeny_Click">
                        <span class="btn__text">Deny</span>
                    </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
