﻿<%@ Page Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Edit-Claim.aspx.cs" Inherits="WexHealth.AdminPortal.Pages.Claims.Manage.Edit_Claim" %>

<%@ Register TagPrefix="UC" TagName="NavBar" Src="~/Controls/NavigationControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <UC:NavBar runat="server" CurrentPageName="Claims" />

    <div class="page__content">
        <div class="active clearfix">
            <div class="active__title-wrapper">
                <div class="active__title">
                    <div class="active__title-label">Сlaim number</div>
                    <%= ClaimId %>
                </div>
            </div>
            <div class="active__controls">
            </div>
        </div>

        <div class="form__wrapper">
            <asp:ValidationSummary runat="server" DisplayMode="BulletList" ValidationGroup="validationGroupEditClaim" ShowMessageBox="False" ShowSummary="True" CssClass="message message_type_error" />
            <asp:CustomValidator runat="server" ID="validateEditClaim" ValidationGroup="validationGroupEditClaim" Display="None" EnableClientScript="false" />
            <div class="form__head">
                <div class="form__title">Details</div>
            </div>
            <div class="form">
                <div class="form__row inlinefix">
                    <div class="form__label">Name</div>
                    <div class="form__input-wrapper">
                        <a href="<%= Page.ResolveUrl("~/consumer/" + ConsumerId + "/manage") %>" class="link"><%= FirstName + " " + LastName %></a>
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Phone</div>
                    <div class="form__input-wrapper">
                        <%= PhoneNumber %>
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Date of service</div>
                    <div class="form__input-wrapper">
                        <input type="text" runat="server" id="textboxDateOfService" class="input input_type_text js-input_type_date" autocomplete="off" />
                        <asp:RequiredFieldValidator runat="server" ValidationGroup="validationGroupEditClaim" ControlToValidate="textboxDateOfService" ErrorMessage="Enter date" Display="none" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Plan</div>
                    <div class="form__input-wrapper">
                        <asp:DropDownList runat="server" ID="dropdownPlans" CssClass="input input_type_dropdown">
                            <asp:ListItem Text="- Select plan -" Value="" />
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator runat="server" ValidationGroup="validationGroupEditClaim" InitialValue="" ControlToValidate="dropdownPlans" ErrorMessage="Select plan" Display="none" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Amount</div>
                    <div class="form__input-wrapper">
                        <input type="text" runat="server" id="textboxAmount" class="input input_type_text" autocomplete="off" />
                        <asp:RequiredFieldValidator runat="server" ValidationGroup="validationGroupEditClaim" ControlToValidate="textboxAmount" ErrorMessage="Enter amount" Display="none" />
                        <asp:RegularExpressionValidator runat="server" ValidationGroup="validationGroupEditClaim" ControlToValidate="textboxAmount" ErrorMessage="Enter valid amount" Display="None" ValidationExpression="^[0-9]{1,10}([.][0-9]{1,2})?$" />
                    </div>
                </div>

                <div class="form__btns">
                    <asp:LinkButton runat="server" ID="submitClaim" ValidationGroup="validationGroupEditClaim" CssClass="btn btn_priority_high" OnClick="submitClaim_Click">Submit</asp:LinkButton>
                    <a href="<%= Page.ResolveUrl("~/claims/" + ClaimId + "/manage") %>" class="btn btn_priority_normal"><span class="btn__text">Cancel</span></a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
