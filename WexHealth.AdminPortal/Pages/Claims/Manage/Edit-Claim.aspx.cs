﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Globalization;
using System.Collections.Generic;
using Microsoft.Practices.ServiceLocation;
using WexHealth.Domain.Entities;
using WexHealth.Security.Infrastructure;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;
using WexHealth.AdminPortal.Presentation.Views;

namespace WexHealth.AdminPortal.Pages.Claims.Manage
{
    public partial class Edit_Claim : System.Web.UI.Page, IClaimEditView
    {
        private readonly IClaimEditPresenter _presenter;

        public string ClaimId
        {
            get
            {
                return RouteData.Values["select"].ToString();
            }
        }

        public Guid ConsumerId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PhoneNumber { get; set; }

        public DateTime DateOfService
        {
            get
            {
                DateTime result;

                if (!DateTime.TryParse(textboxDateOfService.Value, CultureInfo.InvariantCulture, DateTimeStyles.None, out result))
                {
                    throw new InvalidOperationException("Invalid date format");
                }

                return result;
            }

            set
            {
                textboxDateOfService.Value = value.ToString("MM/d/yyyy", CultureInfo.InvariantCulture);
            }
        }

        public IEnumerable<Enrollment> Enrollments
        {
            set
            {
                dropdownPlans.DataSource = value.Select(x => new
                {
                    Id = x.Id,
                    Text = $"{x.BenefitsOffering.Name} ({x.BenefitsOffering.PlanType.Name})"
                });
                dropdownPlans.DataTextField = "Text";
                dropdownPlans.DataValueField = "Id";
                dropdownPlans.DataBind();
            }
        }

        public int SelectedEnrollment
        {
            get
            {
                int result = 0;

                if (!Int32.TryParse(dropdownPlans.SelectedValue, out result))
                {
                    throw new InvalidOperationException("Invalid selected enrollment");
                }

                return result;
            }
            set
            {
                if (value > 0)
                {
                    dropdownPlans.SelectedValue = value.ToString();
                }
            }
        }

        public decimal Amount
        {
            get
            {
                Decimal result;

                if (!Decimal.TryParse(textboxAmount.Value, out result))
                {
                    return Decimal.Parse(textboxAmount.Value);
                }

                return result;
            }

            set
            {
                textboxAmount.Value = value.ToString();
            }
        }

        public Edit_Claim()
        {
            var factory = ServiceLocator.Current.GetInstance<ClaimEditPresenterFactory>();
            var principal = (WexHealthPrincipal)HttpContext.Current.User;
            _presenter = factory(this, principal);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.InitView(Page.IsPostBack);

            Page.Title = "Edit Claim #" + ClaimId;
        }

        protected void submitClaim_Click(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                Page.Validate();

                if (Page.IsValid)
                {
                    OperationResult result = (OperationResult)_presenter.EditClaim();
                    if (result.Status == OperationStatus.Success)
                    {
                        Response.Redirect(Page.ResolveUrl("~/claims/" + ClaimId + "/manage"));
                    }
                    else
                    {
                        validateEditClaim.IsValid = false;
                        validateEditClaim.ErrorMessage = result.Message;
                    }
                }
            }
        }
    }
}