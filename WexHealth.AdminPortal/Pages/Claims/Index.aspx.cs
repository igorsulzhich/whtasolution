﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Collections.Specialized;
using Microsoft.Practices.ServiceLocation;
using WexHealth.Domain.Entities;
using WexHealth.Web.Helpers;
using WexHealth.Security.Infrastructure;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;
using WexHealth.AdminPortal.Presentation.Views;

namespace WexHealth.AdminPortal.Pages.Claims
{
    public partial class Index : System.Web.UI.Page, IClaimIndexView
    {
        private readonly IClaimIndexPresenter _presenter;

        public string ClaimNumber
        {
            get
            {
                string result = null;
                string param = Request.QueryString["n"];

                if (!string.IsNullOrEmpty(param))
                {
                    result = param;
                }

                return result;
            }
        }

        public IEnumerable<Domain.Entities.Employer> Employers
        {
            set
            {
                dropdownEmployers.DataSource = value;
                dropdownEmployers.DataTextField = "Name";
                dropdownEmployers.DataValueField = "Code";
                dropdownEmployers.DataBind();
            }
        }

        public string SelectedEmployer
        {
            get
            {
                return Request.QueryString["e"];
            }
        }

        public IEnumerable<ClaimStatus> ClaimStatuses
        {
            set
            {
                listClaimStatuses.DataSource = value;
                listClaimStatuses.DataBind();
            }
        }

        public string SelectedClaimStatus
        {
            get
            {
                string result = Request.QueryString["s"];

                if (!string.IsNullOrEmpty(result))
                {
                    result = result.Replace('+', ' ');
                }
                else
                {
                    result = "Pending approval";
                }

                return result;
            }
        }

        public Guid ConsumerId
        {
            get
            {
                Guid result = Guid.Empty;
                string param = Request.QueryString["c"];

                if (!string.IsNullOrEmpty(param))
                {
                    Guid.TryParse(param, out result);
                }

                return result;
            }
        }

        public IEnumerable<Claim> Claims
        {
            set
            {
                listClaims.DataSource = value;
                listClaims.DataBind();
            }
        }

        public Index()
        {
            var factory = ServiceLocator.Current.GetInstance<ClaimIndexPresenterFactory>();
            var principal = (WexHealthPrincipal)HttpContext.Current.User;
            _presenter = factory(this, principal);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.InitView(Page.IsPostBack);

            if (!Page.IsPostBack)
            {
                InitClaimNumber();
                InitSelectedEmployer();
            }
        }

        private void InitClaimNumber()
        {
            if (!string.IsNullOrEmpty(ClaimNumber))
            {
                textboxClaimNumber.Text = ClaimNumber;
                textboxClaimNumber.DataBind();
            }
        }

        private void InitSelectedEmployer()
        {
            if (null != dropdownEmployers.Items.FindByValue(SelectedEmployer))
            {
                dropdownEmployers.SelectedValue = SelectedEmployer;
            }
        }

        protected void submitSearch_Click(object sender, EventArgs e)
        {
            var paramList = new NameValueCollection()
            {
                { "n", textboxClaimNumber.Text },
                { "e", dropdownEmployers.SelectedValue }
            };

            Response.Redirect(UrlHelper.AddParamsUrl(paramList));
        }

        protected void listClaimStatuses_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                ClaimStatus p = (ClaimStatus)e.Item.DataItem;

                if (!string.IsNullOrEmpty(SelectedClaimStatus) && SelectedClaimStatus.Equals(p.Name))
                {
                    ((HtmlAnchor)e.Item.FindControl("linkClaimStatus")).Attributes.Add("class", "filter__link filter__link_active");
                }
            }
        }

        protected string GetClaimLink(string statusName)
        {
            var paramList = new NameValueCollection()
            {
                { "s", statusName }
            };

            return UrlHelper.AddParamsUrl(paramList);
        }
    }
}