﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Collections.Specialized;
using Microsoft.Practices.ServiceLocation;
using WexHealth.Web.Helpers;
using WexHealth.Security.Infrastructure;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.AdminPortal.Employer
{
    public partial class Index : System.Web.UI.Page, IEmployerIndexView
    {
        private readonly IEmployerIndexPresenter _presenter;

        public string EmployerName
        {
            get
            {
                return Request.QueryString["n"];
            }
        }

        public string EmployerCode
        {
            get
            {
                return Request.QueryString["c"];
            }
        }

        public IEnumerable<Domain.Entities.Employer> Employers
        {
            set
            {
                listEmployers.DataSource = value;
                listEmployers.DataBind();
            }
        }

        public Index()
        {
            var factory = ServiceLocator.Current.GetInstance<EmployerIndexPresenterFactory>();
            var principal = (WexHealthPrincipal)HttpContext.Current.User;
            _presenter = factory(this, principal);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.InitView(Page.IsPostBack);

            if (!Page.IsPostBack)
            {
                InitEmployerName();
                InitEmployerCode();
            }
        }

        private void InitEmployerName()
        {
            if (!string.IsNullOrEmpty(EmployerName))
            {
                textboxEmployerName.Text = EmployerName;
                textboxEmployerName.DataBind();
            }
        }

        private void InitEmployerCode()
        {
            if (!string.IsNullOrEmpty(EmployerCode))
            {
                textboxEmployerCode.Text = EmployerCode;
                textboxEmployerName.DataBind();
            }
        }

        protected void submitSearch_Click(object sender, EventArgs e)
        {
            var paramList = new NameValueCollection()
            {
                { "n", textboxEmployerName.Text },
                { "c", textboxEmployerCode.Text }
            };

            Response.Redirect(UrlHelper.AddParamsUrl(paramList));
        }
    }
}