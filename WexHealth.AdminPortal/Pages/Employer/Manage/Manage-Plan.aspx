﻿<%@ Page Title="Manage plan | Employer" Language="C#" MasterPageFile="~/Admin.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="Manage-Plan.aspx.cs" Inherits="WexHealth.AdminPortal.Pages.Employer.Manage.Manage_Plan" %>
<%@ Register TagPrefix="UC" TagName="NavBar" Src="~/Controls/NavigationControl.ascx" %>
<%@ Register TagPrefix="UC" TagName="EmployerInfoBox" Src="~/Controls/EmployerInfoBoxControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <UC:NavBar runat="server" CurrentPageName="Employer"/>

    <div class="page__content">
        
        <UC:EmployerInfoBox runat="server"/>       
        
        <div class="form__wrapper">
            <asp:ValidationSummary runat="server" ValidationGroup="validationGroupManagePlan" DisplayMode="BulletList" ShowMessageBox="False" ShowSummary="True" CssClass="message message_type_error" />
            <asp:CustomValidator runat="server" ID="validatorManagePlan" ValidationGroup="validationGroupManagePlan" Display="None" EnableClientScript="false" />
            <div class="form__head">
                <div class="form__title">Manage plan year</div>
            </div>
            <div class="form">
                <asp:HiddenField ID="hidden_Id" runat="server"/>
                <div class="form__row inlinefix">
                    <div class="form__label">Name</div>
                    <div class="form__input-wrapper">
                        <asp:TextBox ID="textbox_name" runat="server" CssClass="input input_type_text" autocomplete="off"/>
                        <asp:RequiredFieldValidator runat="server" ValidationGroup="validationGroupManagePlan"  ControlToValidate="textbox_name" ErrorMessage="Enter name" Display="none" />
                    </div>
                </div>               
                <div class="form__row inlinefix">
                    <div class="form__label">Type</div>
                    <div class="form__input-wrapper">
                        <asp:Label ID="label_type" runat="server" autocomplete="off"/>
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Contributions</div>
                    <div class="form__input-wrapper">
                        <asp:TextBox ID="textbox_contribution" runat="server" CssClass="input input_type_text" autocomplete="off"/>
                        <asp:RequiredFieldValidator runat="server" ValidationGroup="validationGroupManagePlan"  ControlToValidate="textbox_contribution" ErrorMessage="Enter contribution" Display="none" />
                        <asp:RegularExpressionValidator runat="server" ValidationGroup="validationGroupManagePlan"  ControlToValidate="textbox_contribution" ErrorMessage="Enter valid contribution" Display="None" ValidationExpression="^[0-9]{1,10}([.][0-9]{1,3})?$" />
                    </div>
                </div>
                <div class="form__btns">
                    <asp:Button ID="Manage_newPlan" ValidationGroup="validationGroupManagePlan"  OnClick="SavePlan" CssClass="btn btn_priority_high" Text="Submit" runat="server" />
                    <a href="<%= Page.ResolveUrl("~/employer/" + Identity + "/manage-planyear/" + BenefitsPackageId) %>" class="btn btn_priority_normal"><span class="btn__text">Cancel</span></a>
                </div>
            </div>       
        </div>
    </div>
</asp:Content>