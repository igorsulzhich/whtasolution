﻿<%@ Page Title="Setup | Employer" Language="C#" MasterPageFile="~/Admin.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="Setup.aspx.cs" Inherits="WexHealth.AdminPortal.Pages.Employer.Manage.Setup" %>

<%@ Register TagPrefix="UC" TagName="NavBar" Src="~/Controls/NavigationControl.ascx" %>
<%@ Register TagPrefix="UC" TagName="EmployerInfoBox" Src="~/Controls/EmployerInfoBoxControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <UC:NavBar runat="server" CurrentPageName="Employer" />

    <div class="page__content">

        <UC:EmployerInfoBox runat="server" />
        <div class="form__wrapper">
            <div class="form__head">
                <div class="form__title">Consumer management</div>
            </div>
            <form class="form">
                <asp:Repeater runat="server" ID="tableSettings" ItemType="WexHealth.Domain.Entities.Setting">
                    <ItemTemplate>
                        <div class="form__row inlinefix">
                            <div class="form__label">Claim Filing</div>
                            <div class="form__input-wrapper">
                                <input type="hidden" runat="server" value='<%#: Item.Id %>' id="settingId" name="settingId" />
                                <input class="input input_type_radiobutton" type="radio" id="defaultSetting" name="radioSetting" runat="server" />
                                <div class="input_type_radiobutton__label">Administrator Default</div>
                                <input class="input input_type_radiobutton" type="radio" id="allowSetting" name="radioSetting" runat="server" />
                                <div class="input_type_radiobutton__label">Allow</div>
                                <input class="input input_type_radiobutton" type="radio" id="disallowSetting" name="radioSetting" runat="server" />
                                <div class="input_type_radiobutton__label">Prevent</div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <div class="form__btns">
                    <asp:Button ID="Button" runat="server" Text="Submit" OnClick="Setting_Click" CssClass="btn btn_priority_high" />
                    <a href="<%= Page.ResolveUrl("~/employer/" + Identity + "/manage") %>" class="btn btn_priority_normal"><span class="btn__text">Cancel</span></a>
                </div>
            </form>
        </div>
    </div>
</asp:Content>
