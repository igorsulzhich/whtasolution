﻿<%@ Page Title="Edit profile | Employer" Language="C#" MasterPageFile="~/Admin.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="Edit-Profile.aspx.cs" Inherits="WexHealth.AdminPortal.Pages.Employer.Manage.Edit_Profile" %>

<%@ Register TagPrefix="UC" TagName="NavBar" Src="~/Controls/NavigationControl.ascx" %>
<%@ Register TagPrefix="UC" TagName="EmployerInfoBox" Src="~/Controls/EmployerInfoBoxControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <UC:NavBar runat="server" CurrentPageName="Employer" />

    <div class="page__content">

        <UC:EmployerInfoBox runat="server" />

        <div class="form__wrapper">
            <asp:ValidationSummary runat="server" ValidationGroup="validationGroupEditProfile" DisplayMode="BulletList" ShowMessageBox="False" ShowSummary="True" CssClass="message message_type_error" />
            <asp:CustomValidator runat="server" ID="validatorEditProfile" ValidationGroup="validationGroupEditProfile" Display="None" EnableClientScript="false" />
            <div class="form__head">
                <div class="form__title">Edit profile</div>
            </div>
            <div class="form">
                <asp:HiddenField ID="hidden_Id" runat="server" />
                <div class="form__row inlinefix">
                    <div class="form__label">Name</div>
                    <div class="form__input-wrapper">
                        <asp:TextBox ID="textbox_name" runat="server" CssClass="input input_type_text" autocomplete="off" />
                        <asp:RequiredFieldValidator runat="server"
                            ValidationGroup="validationGroupEditProfile"
                            ControlToValidate="textbox_name"
                            CausesValidation="true"
                            ErrorMessage="Enter name"
                            Display="none" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Code</div>
                    <div class="form__input-wrapper">
                        <asp:TextBox ID="textbox_code" runat="server" CssClass="input input_type_text" autocomplete="off" />
                        <asp:RequiredFieldValidator runat="server"
                            ValidationGroup="validationGroupEditProfile"
                            ControlToValidate="textbox_code"
                            CausesValidation="true"
                            ErrorMessage="Enter code"
                            Display="none" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Street</div>
                    <div class="form__input-wrapper">
                        <asp:TextBox ID="textbox_street" runat="server" CssClass="input input_type_text" autocomplete="off" />
                        <asp:RequiredFieldValidator runat="server"
                            ValidationGroup="validationGroupEditProfile"
                            ControlToValidate="textbox_street"
                            CausesValidation="true"
                            ErrorMessage="Enter street"
                            Display="none" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">City</div>
                    <div class="form__input-wrapper">
                        <asp:TextBox ID="textbox_city" runat="server" CssClass="input input_type_text" autocomplete="off" />
                        <asp:RequiredFieldValidator runat="server"
                            ValidationGroup="validationGroupEditProfile"
                            ControlToValidate="textbox_city"
                            CausesValidation="true"
                            ErrorMessage="Enter city"
                            Display="none" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">State</div>
                    <div class="form__input-wrapper">
                        <asp:DropDownList runat="server" ID="dropdown_states" CssClass="input input_type_dropdown" AppendDataBoundItems="true">
                            <asp:ListItem Text="- Select state -" Value="" />
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator runat="server"
                            ValidationGroup="validationGroupEditProfile"
                            ControlToValidate="dropdown_states"
                            CausesValidation="true"
                            ErrorMessage="Select state"
                            InitialValue=""
                            Display="none" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Zip code</div>
                    <div class="form__input-wrapper">
                        <asp:TextBox ID="textbox_zipcode" runat="server" CssClass="input input_type_text" autocomplete="off" />
                        <asp:RequiredFieldValidator runat="server"
                            ValidationGroup="validationGroupEditProfile"
                            ControlToValidate="textbox_zipcode"
                            CausesValidation="true"
                            ErrorMessage="Enter zip code"
                            Display="none" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Phone</div>
                    <div class="form__input-wrapper">
                        <asp:TextBox ID="textbox_phone" runat="server" CssClass="input input_type_text js-input_type_phone" autocomplete="off" />
                        <asp:RequiredFieldValidator runat="server"
                            ValidationGroup="validationGroupEditProfile"
                            ControlToValidate="textbox_phone"
                            CausesValidation="true"
                            ErrorMessage="Enter phone"
                            Display="none" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Logo</div>
                    <div class="form__input-wrapper">
                        <asp:FileUpload class="input input_type_file" runat="server" ID="file_image" />
                    </div>
                </div>
                <div class="form__btns">
                    <asp:Button ID="Edit_Employer" ValidationGroup="validationGroupEditProfile" OnClick="EditEmployer" CssClass="btn btn_priority_high" Text="Submit" runat="server" />
                    <a href="<%= Page.ResolveUrl("~/employer/" + Code + "/manage") %>" class="btn btn_priority_normal"><span class="btn__text">Cancel</span></a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
