﻿using System;
using System.Web;
using Microsoft.Practices.ServiceLocation;
using WexHealth.Security.Infrastructure;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.AdminPortal.Employer.Manage
{
    public partial class Index : System.Web.UI.Page, IEmployerManageView
    {
        private readonly IEmployerManagePresenter _presenter;

        public string Code
        {
            get
            {
                if (String.IsNullOrEmpty((string)RouteData.Values["select"]))
                {
                    return null;
                }
                else
                {
                    return (string)RouteData.Values["select"];
                }
            }

            set
            {
                RouteData.Values["select"] = value;
            }
        }

        public Index()
        {
            var factory = ServiceLocator.Current.GetInstance<EmployerManagePresenterFactory>();
            var principal = (WexHealthPrincipal)HttpContext.Current.User;
            _presenter = factory(this, principal);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.InitView(Page.IsPostBack);
            DataBind();
        }

        public bool IsSettingAllow(string settingKey)
        {
            return _presenter.IsSettingAllow(settingKey);
        }
    }
}