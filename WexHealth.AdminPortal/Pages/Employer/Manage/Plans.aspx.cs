﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using Microsoft.Practices.ServiceLocation;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.Domain.Entities;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.Security.Infrastructure;

namespace WexHealth.AdminPortal.Pages.Employer.Manage
{
    public partial class Plans : System.Web.UI.Page, IBenefitsPackageView
    {
        private readonly IBenefitsPackagePresenter _presenter;

        public IEnumerable<BenefitsPackage> BenefitsPackages
        {
            set
            {
                listBenefitsPackages.DataSource = value;
                listBenefitsPackages.DataBind();
            }
        }

        public string Identity
        {
            get
            {
                if (String.IsNullOrEmpty((string)RouteData.Values["select"]))
                {
                    return null;
                }
                else
                {
                    return (string)RouteData.Values["select"];
                }
            }

            set
            {
                RouteData.Values["select"] = value;
            }
        }

        public Plans()
        {
            var factory = ServiceLocator.Current.GetInstance<BenefitsPackagePresenterFactory>();
            var principal = (WexHealthPrincipal)HttpContext.Current.User;
            _presenter = factory(this, principal);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.InitView(Page.IsPostBack);
        }

        protected void Initizalize_Command(object sender, CommandEventArgs e)
        {
            int id = 0;

            if (int.TryParse(e.CommandArgument.ToString(), out id))
            {
                OperationResult updateResult = (OperationResult)_presenter.Initialize(id);
                if (OperationStatus.Success == updateResult.Status)
                {
                    Response.Redirect("~/employer/" + Identity + "/plans");
                }
            }
        }

        protected void Remove_Command(object sender, CommandEventArgs e)
        {
            int id = 0;

            if (int.TryParse(e.CommandArgument.ToString(), out id))
            {
                OperationResult updateResult = (OperationResult)_presenter.Delete(id);
                if (OperationStatus.Success == updateResult.Status)
                {
                    Response.Redirect("~/employer/" + Identity + "/plans");
                }
            }
        }

        protected bool CheckEnrolls(int id)
        {
            return _presenter.HaveEnroll(id);
        }
    }
}