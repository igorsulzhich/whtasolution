﻿<%@ Page Title="Manage plan year | Employer" Language="C#" MasterPageFile="~/Admin.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="Manage-PlanYear.aspx.cs" Inherits="WexHealth.AdminPortal.Pages.Employer.Manage.Manage_PlanYear" %>

<%@ Register TagPrefix="UC" TagName="NavBar" Src="~/Controls/NavigationControl.ascx" %>
<%@ Register TagPrefix="UC" TagName="EmployerInfoBox" Src="~/Controls/EmployerInfoBoxControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <UC:NavBar runat="server" CurrentPageName="Employer" />

    <div class="page__content">

        <UC:EmployerInfoBox runat="server" />

        <div class="form__wrapper">
            <asp:ValidationSummary runat="server" ValidationGroup="validationGroupManagePlanYear" DisplayMode="BulletList" ShowMessageBox="False" ShowSummary="True" CssClass="message message_type_error" />
            <asp:CustomValidator runat="server" ID="validatorManagePlanYear" ValidationGroup="validationGroupManagePlanYear" Display="None" EnableClientScript="false" />
            <div class="form__head">
                <div class="form__title">Manage plan year</div>
            </div>
            <div class="form">
                <asp:HiddenField ID="hidden_Id" runat="server" />
                <div class="form__row inlinefix">
                    <div class="form__label">Name</div>
                    <div class="form__input-wrapper">
                        <asp:TextBox ID="textbox_name" runat="server" CssClass="input input_type_text" autocomplete="off" />
                        <asp:RequiredFieldValidator runat="server" ValidationGroup="validationGroupManagePlanYear" ControlToValidate="textbox_name" ErrorMessage="Enter name" Display="none" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Start date</div>
                    <div class="form__input-wrapper">
                        <asp:TextBox ID="textbox_start_date" runat="server" CssClass="input input_type_text js-input_type_date" autocomplete="off" />
                        <asp:RequiredFieldValidator runat="server" ValidationGroup="validationGroupManagePlanYear" ControlToValidate="textbox_start_date" ErrorMessage="Enter start date" Display="none" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">End date</div>
                    <div class="form__input-wrapper">
                        <asp:TextBox ID="textbox_end_date" runat="server" CssClass="input input_type_text js-input_type_date" autocomplete="off" />
                        <asp:RequiredFieldValidator runat="server" ValidationGroup="validationGroupManagePlanYear" ControlToValidate="textbox_end_date" ErrorMessage="Enter end date" Display="none" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Payroll frequency</div>
                    <div class="form__input-wrapper">
                        <asp:DropDownList runat="server" ID="dropdown_payrollFrequencies" CssClass="input input_type_dropdown">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator runat="server" ValidationGroup="validationGroupManagePlanYear" ControlToValidate="dropdown_payrollFrequencies" ErrorMessage="Select payroll frequency" InitialValue="" Display="none" />
                    </div>
                </div>
                <div class="table__wrapper" style="-moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box; margin: 60px 0 10px 0; padding: 0 30px 0 150px;">
                    <div class="table__title-wrapper">
                        <div class="form__row inlinefix">
                            <div class="form__input-wrapper">
                                <asp:DropDownList runat="server" ID="dropdown_planTypes" CssClass="input input_type_dropdown" Visible="false">
                                    <asp:ListItem Text="- Select plan type -" />
                                </asp:DropDownList>
                            </div>
                            <asp:LinkButton ID="linkButtonAdd"
                                Text="Add"
                                OnClick="ToAddPlan"
                                CssClass="btn btn_priority_normal"
                                Style="margin: 0 0 0 10px;"
                                runat="server"
                                Visible="false" />
                        </div>
                    </div>
                    <table class="table">
                        <thead class="table__thead">
                            <tr class="table__head" id="tableOffering" runat="server" visible="false">
                                <th class="table__column">
                                    <div class="column__text">Name</div>
                                </th>
                                <th class="table__column">
                                    <div class="column__text">Type</div>
                                </th>
                                <th class="table__column table__column_type_numeric">
                                    <div class="column__text">Contributions</div>
                                </th>
                            </tr>
                        </thead>
                        <tbody class="table__tbody">
                            <asp:Repeater ID="listBenefitsOfferings" runat="server" ItemType="WexHealth.Domain.Entities.BenefitsOffering">
                                <ItemTemplate>
                                    <tr class="table__row table__row_linked js-table__row" data-href="<%# Page.ResolveUrl(String.Format("~/employer/{0}/manage-plan?package={1}&plan={2}", Identity, Id, Item.Id)) %>">
                                        <td class="table__cell"><%#: Item.Name %></td>
                                        <td class="table__cell"><%#: Item.PlanType.Name %></td>
                                        <td class="table__cell table__cell_type_numeric">$ <%#: Item.ContributionAmount %></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
                <div class="form__btns">
                    <asp:Button ID="Manage_newPlanYear" ValidationGroup="validationGroupManagePlanYear" OnClick="ManagePlanYear" CssClass="btn btn_priority_high" Text="Submit" runat="server" />
                    <a href="<%= Page.ResolveUrl("~/employer/" + Identity + "/plans") %>" class="btn btn_priority_normal"><span class="btn__text">Cancel</span></a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
