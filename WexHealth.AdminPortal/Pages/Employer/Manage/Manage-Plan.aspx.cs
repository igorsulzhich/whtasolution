﻿using System;
using System.Web;
using System.Web.UI;
using log4net;
using Microsoft.Practices.ServiceLocation;
using WexHealth.Security.Infrastructure;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;
using WexHealth.Presentation.Presenters.Results;

namespace WexHealth.AdminPortal.Pages.Employer.Manage
{
    public partial class Manage_Plan : System.Web.UI.Page, IEmployerManagePlanView
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Manage_Plan));

        private readonly IEmployerManagePlanPresenter _presenter;

        public int Id
        {
            get
            {
                int result;

                if (!Int32.TryParse(hidden_Id.Value, out result))
                {
                    result = 0;
                }

                return result;
            }

            set
            {
                hidden_Id.Value = value.ToString();
            }
        }

        public string Name
        {
            get
            {
                return textbox_name.Text;
            }

            set
            {
                textbox_name.Text = value;
            }
        }

        public string Type
        {
            get
            {
                return label_type.Text;
            }

            set
            {
                label_type.Text = value;
            }
        }

        public decimal Contribution
        {
            get
            {
                return Decimal.Parse(textbox_contribution.Text);
            }

            set
            {
                textbox_contribution.Text = value.ToString();
                textbox_contribution.Enabled = IsStart;
            }
        }

        public string Identity
        {
            get
            {
                string result = null;

                if (!String.IsNullOrEmpty((string)RouteData.Values["select"]))
                {
                    result = (string)RouteData.Values["select"];
                }

                return result;
            }

            set
            {
                RouteData.Values["select"] = value;
            }
        }

        public int BenefitsPackageId
        {
            get
            {
                int result = 0;

                if (!String.IsNullOrEmpty(Request.QueryString["package"]))
                {
                    if (!Int32.TryParse(Request.QueryString["package"], out result))
                    {
                        result = 0;
                    }
                }

                return result;
            }

            set
            {
                Request.QueryString["package"] = value.ToString();
            }
        }

        public int PlanId
        {
            get
            {
                int result = 0;

                if (!String.IsNullOrEmpty(Request.QueryString["plan"]))
                {
                    if (!Int32.TryParse(Request.QueryString["plan"], out result))
                    {
                        result = 0;
                    }
                }

                return result;
            }

            set
            {
                Request.QueryString["plan"] = value.ToString();
            }
        }

        public string PlanType
        {
            get
            {
                return Request.QueryString["type"];
            }

            set
            {
                RouteData.Values["type"] = value;
            }
        }

        public bool IsStart { get; set; }

        public Manage_Plan()
        {
            var factory = ServiceLocator.Current.GetInstance<EmployerManagePlanPresenterFactory>();
            var principal = (WexHealthPrincipal)HttpContext.Current.User;
            _presenter = factory(this, principal);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.InitView(Page.IsPostBack);         
        }

        protected void SavePlan(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                Page.Validate("validationGroupManagePlan");

                if (Page.IsValid)
                {
                    OperationResult result = (OperationResult)_presenter.SaveBenefitsOffering();
                    if (OperationStatus.Success == result.Status)
                    {
                        Response.Redirect("~/employer/" + Identity + "/manage-planyear/" + BenefitsPackageId);
                    }
                    else
                    {
                        validatorManagePlan.IsValid = false;
                        validatorManagePlan.ErrorMessage = result.Message;
                    }
                }
            }
        }
    }
}