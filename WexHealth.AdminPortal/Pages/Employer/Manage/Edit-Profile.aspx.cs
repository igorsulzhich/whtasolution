﻿using System;
using System.Web;
using System.Collections.Generic;
using Microsoft.Practices.ServiceLocation;
using WexHealth.Domain.Entities;
using WexHealth.Security.Infrastructure;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.AdminPortal.Pages.Employer.Manage
{
    public partial class Edit_Profile : System.Web.UI.Page, IEmployerManageProfileView
    {
        private readonly IEmployerManageProfilePresenter _presenter;

        public Guid Id
        {
            get
            {
                return Guid.Parse(hidden_Id.Value);
            }

            set
            {
                hidden_Id.Value = value.ToString();
            }
        }

        public string Code
        {
            get
            {
                return textbox_code.Text;
            }

            set
            {
                textbox_code.Text = value;
            }
        }

        public string Name
        {
            get
            {
                return textbox_name.Text;
            }

            set
            {
                textbox_name.Text = value;
            }
        }

        public string Identity
        {
            get
            {
                if (String.IsNullOrEmpty((string)RouteData.Values["select"]))
                {
                    return null;
                }
                else
                {
                    return (string)RouteData.Values["select"];
                }
            }

            set
            {
                RouteData.Values["select"] = value;
            }
        }

        public byte[] Logo
        {
            get
            {
                return file_image.HasFile ? file_image.FileBytes : null;
            }
        }

        public string Street
        {
            get
            {
                return textbox_street.Text;
            }

            set
            {
                textbox_street.Text = value;
            }
        }

        public string City
        {
            get
            {
                return textbox_city.Text;
            }

            set
            {
                textbox_city.Text = value;
            }
        }

        public string Phone
        {
            get
            {
                return textbox_phone.Text;
            }

            set
            {
                textbox_phone.Text = value;
            }
        }

        public string SelectState
        {
            get
            {
                return dropdown_states.SelectedValue;
            }

            set
            {
                dropdown_states.SelectedValue = value;
            }
        }

        public IEnumerable<State> States
        {
            set
            {
                dropdown_states.DataSource = value;
                dropdown_states.DataValueField = "Name";
                dropdown_states.DataBind();
            }
        }

        public string ZipCode
        {
            get
            {
                return textbox_zipcode.Text;
            }

            set
            {
                textbox_zipcode.Text = value;
            }
        }

        public Edit_Profile()
        {
            var factory = ServiceLocator.Current.GetInstance<EmployerManageProfilePresenterFactory>();
            var principal = (WexHealthPrincipal)HttpContext.Current.User;
            _presenter = factory(this, principal);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _presenter.InitView(Page.IsPostBack);
            }
            catch (ArgumentNullException)
            {
                Response.Redirect("~/employer");
            }
        }

        protected void EditEmployer(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                Page.Validate("validationGroupEditProfile");

                if (Page.IsValid)
                {
                    OperationResult result = (OperationResult)_presenter.SaveEmployer();
                    if (OperationStatus.Success == result.Status)
                    {
                        Response.Redirect("~/employer/" + Code + "/manage");
                    }
                    else
                    {
                        validatorEditProfile.IsValid = false;
                        validatorEditProfile.ErrorMessage = result.Message;
                    }
                }
            }
        }
    }
}