﻿using System;
using System.Web;
using System.Linq;
using System.Globalization;
using System.Collections.Generic;
using Microsoft.Practices.ServiceLocation;
using log4net;
using WexHealth.Domain.Entities;
using WexHealth.Security.Infrastructure;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.Presentation.Presenters.Results;

namespace WexHealth.AdminPortal.Pages.Employer.Manage
{
    public partial class Manage_PlanYear : System.Web.UI.Page, IEmployerManagePlanYearView
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Manage_PlanYear));

        private readonly IEmployerManagePlanYearPresenter _presenter;

        public int Id
        {
            get
            {
                int result;

                if (!Int32.TryParse(hidden_Id.Value, out result))
                {
                    result = 0;
                }

                return result;
            }

            set
            {
                hidden_Id.Value = value.ToString();
            }
        }

        public string Name
        {
            get
            {
                return textbox_name.Text;
            }

            set
            {
                textbox_name.Text = value;
            }
        }

        public DateTime StartDate
        {
            get
            {
                return DateTime.ParseExact(textbox_start_date.Text, "MM/d/yyyy", CultureInfo.InvariantCulture);
            }

            set
            {
                textbox_start_date.Text = value.ToString("MM/d/yyyy", CultureInfo.InvariantCulture);
            }
        }

        public DateTime EndDate
        {
            get
            {
                return DateTime.ParseExact(textbox_end_date.Text, "MM/d/yyyy", CultureInfo.InvariantCulture);
            }

            set
            {
                textbox_end_date.Text = value.ToString("MM/d/yyyy", CultureInfo.InvariantCulture);
            }
        }

        public IEnumerable<BenefitsOffering> BenefitsOfferings
        {
            set
            {
                if (value.Any())
                {
                    tableOffering.Visible = true;
                }

                listBenefitsOfferings.DataSource = value;
                DataBind();
            }
        }

        public IEnumerable<PlanType> PlanTypes
        {
            set
            {
                dropdown_planTypes.DataSource = value;
                dropdown_planTypes.DataTextField = "Name";
                dropdown_planTypes.DataValueField = "Name";
                DataBind();
            }
        }

        public string SelectedPlanType
        {
            get
            {
                return dropdown_planTypes.SelectedValue;
            }

            set
            {
                dropdown_planTypes.SelectedValue = value.ToString();
            }
        }

        public IEnumerable<PayrollFrequency> PayrollFrequencies
        {
            set
            {
                dropdown_payrollFrequencies.DataSource = value;
                dropdown_payrollFrequencies.DataTextField = "Name";
                dropdown_payrollFrequencies.DataValueField = "Days";
                dropdown_payrollFrequencies.DataBind();
            }
        }

        public int SelectedPayrollFrequency
        {
            get
            {
                int result;

                if (!Int32.TryParse(dropdown_payrollFrequencies.SelectedValue, out result))
                {
                    result = 0;
                }

                return result;
            }

            set
            {
                dropdown_payrollFrequencies.SelectedValue = value.ToString();
            }
        }

        public string Identity
        {
            get
            {
                string result = null;

                if (!String.IsNullOrEmpty((string)RouteData.Values["select"]))
                {
                    result = (string)RouteData.Values["select"];
                }

                return result;
            }
            set
            {
                RouteData.Values["select"] = value;
            }
        }

        public int PlanId
        {
            get
            {
                int result = 0;

                if (!String.IsNullOrEmpty((string)RouteData.Values["id"]))
                {
                    if (!Int32.TryParse(RouteData.Values["id"].ToString(), out result))
                    {
                        result = 0;
                    }
                }

                return result;
            }

            set
            {
                RouteData.Values["id"] = value;
            }
        }

        public string Status
        {
            set
            {
                if (value == "Initialized")
                {
                    textbox_start_date.Enabled = false;
                    dropdown_payrollFrequencies.Enabled = false;

                    if (StartDate <= DateTime.Now)
                    {
                        textbox_end_date.Enabled = false;
                    }
                }
                else
                {
                    linkButtonAdd.Visible = true;
                    dropdown_planTypes.Visible = true;
                }
            }
        }

        public Manage_PlanYear()
        {
            var factory = ServiceLocator.Current.GetInstance<EmployerManagePlanYearPresenterFactory>();
            var principal = (WexHealthPrincipal)HttpContext.Current.User;
            _presenter = factory(this, principal);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.InitView(Page.IsPostBack);
        }

        protected void ManagePlanYear(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                Page.Validate("validationManagePlanYear");

                if (Page.IsValid)
                {
                    OperationResult result = (OperationResult)_presenter.SaveBenefitsPackage();
                    if (result.Status == OperationStatus.Success)
                    {
                        Response.Redirect(String.Format($"~/employer/{Identity}/plans"));
                    }
                    else
                    {
                        validatorManagePlanYear.IsValid = false;
                        validatorManagePlanYear.ErrorMessage = result.Message;
                    }
                }
            }
        }

        protected void ToAddPlan(object sender, EventArgs e)
        {
            Response.Redirect(String.Format($"~/employer/{Identity}/manage-plan?package={Id}&type={SelectedPlanType}"));
        }
    }
}