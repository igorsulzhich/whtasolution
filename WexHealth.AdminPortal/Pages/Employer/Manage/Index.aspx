﻿<%@ Page Title="Manage | Employer" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WexHealth.AdminPortal.Employer.Manage.Index" %>

<%@ Register TagPrefix="UC" TagName="NavBar" Src="~/Controls/NavigationControl.ascx" %>
<%@ Register TagPrefix="UC" TagName="EmployerInfoBox" Src="~/Controls/EmployerInfoBoxControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <UC:NavBar runat="server" CurrentPageName="Employer" />

    <div class="page__content">

        <UC:EmployerInfoBox runat="server" />

        <div class="setting-wrapper">
            <div class="setting__groups inlinefix">
                <div class="setting__group-item">
                    <div class="setting__title">Profile</div>
                    <ul class="setting__list">
                        <li class="setting__item"><a href="<%= Page.ResolveUrl("~/employer/" + Code + "/edit-profile") %>" class="setting__link link">Edit</a></li>
                    </ul>
                </div>

                <div class="setting__group-item">
                    <div class="setting__title">Setup</div>
                    <ul class="setting__list">
                        <li class="setting__item"><a href="<%= Page.ResolveUrl("~/employer/" + Code + "/setup") %>" class="setting__link link">Manage consumer portal rules</a></li>
                    </ul>
                </div>
                <div class="setting__group-item">
                    <div class="setting__title">Plans</div>
                    <ul class="setting__list">
                        <li class="setting__item"><a href="<%= Page.ResolveUrl("~/employer/" + Code + "/plans") %>" class="setting__link link">Manage</a></li>
                    </ul>
                </div>
                <div class="setting__group-item">
                    <div class="setting__title">Consumers</div>
                    <ul class="setting__list">
                        <li class="setting__item"><a href="<%= Page.ResolveUrl("~/consumer/?e=" + Code) %>" class="setting__link link">Manage</a></li>
                        <li class="setting__item" runat="server" visible='<%# IsSettingAllow("Create consumers") %>'><a href="<%= Page.ResolveUrl("~/employer/" + Code + "/add-consumer/") %>" class="setting__link link">Add consumer</a></li>
                    </ul>
                </div>
                <div class="setting__group-item">
                    <div class="setting__title">Users</div>
                    <ul class="setting__list">
                        <li class="setting__item"><a href="<%= Page.ResolveUrl("~/employer/" + Code + "/users") %>" class="setting__link link">Manage</a></li>
                    </ul>
                </div>


            </div>
        </div>
    </div>
</asp:Content>
