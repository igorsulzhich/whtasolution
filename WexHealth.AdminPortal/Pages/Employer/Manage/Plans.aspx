﻿<%@ Page Title="Plans | Employer" Language="C#" MasterPageFile="~/Admin.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="Plans.aspx.cs" Inherits="WexHealth.AdminPortal.Pages.Employer.Manage.Plans" %>

<%@ Register TagPrefix="UC" TagName="NavBar" Src="~/Controls/NavigationControl.ascx" %>
<%@ Register TagPrefix="UC" TagName="EmployerInfoBox" Src="~/Controls/EmployerInfoBoxControl.ascx" %>
<%@ Import Namespace="System.Globalization" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <UC:NavBar runat="server" CurrentPageName="Employer"/>

    <div class="page__content">
        
        <UC:EmployerInfoBox runat="server"/>

        <div class="table__wrapper">
            <div class="table__title-wrapper">
                <div class="table__title">Plan year</div>
                <div class="table__controls">
                    <a href="<%= Page.ResolveUrl(String.Format("~/employer/{0}/manage-planyear", Identity)) %>" class="btn btn_priority_normal"><span class="btn__text">Add plan year</span></a>
                </div>
            </div>
            <table class="table">
                <thead class="table__thead">
                    <tr class="table__head">
                        <th class="table__column table__column_filter">
                            <div class="column__text">Name</div>
                        </th>
                        <th class="table__column">
                            <div class="column__text">Status</div>
                        </th>
                        <th class="table__column"></th>
                    </tr>
                </thead>
                <tbody class="table__tbody">
                    <asp:Repeater ID="listBenefitsPackages" runat="server" ItemType="WexHealth.Domain.Entities.BenefitsPackage">
                        <ItemTemplate>
                            <tr class="table__row">
                                <td class="table__cell"><%#: Item.Name %></td>
                                <td class='table__cell'><%#: Item.Status.Name %> <%#: Item.Status.Name == "Initialized" ? "on " + Item.DateOfInitialized.ToString("MM/d/yyyy", CultureInfo.InvariantCulture) : ""%></td>
                                <td class="table__cell table__cell_type_action">                           
                                    <a href="<%#: Page.ResolveUrl(String.Format("~/employer/{0}/manage-planyear/{1}", Identity, Item.Id)) %>" class="btn btn_priority_normal" style="margin: 0 0 0 10px;"><span class="btn__text">Update</span></a>
                                    <asp:LinkButton ID="Initizalize" 
                                        CssClass="btn btn_priority_normal"
                                        Style="margin: 0 0 0 10px;"
                                        Text="Initizalize" 
                                        OnCommand="Initizalize_Command" 
                                        CommandArgument="<%#: Item.Id %>"
                                        runat="server"
                                        Visible='<%# Item.Status.Name == "Not initialized" %>' />
                                    <asp:LinkButton ID="Remove" 
                                        CssClass="btn btn_priority_normal"
                                        Style="margin: 0 0 0 10px;"
                                        Text="Remove" 
                                        OnCommand="Remove_Command" 
                                        CommandArgument="<%#: Item.Id %>"
                                        runat="server"
                                        Visible='<%# !CheckEnrolls(Item.Id) && DateTime.Now <= Item.DateOfStart %>' />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
        </div>
    </div>

</asp:Content>


