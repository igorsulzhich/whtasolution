﻿<%@ Page Title="Edit profile | Employer" Language="C#" MasterPageFile="~/Admin.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="WexHealth.AdminPortal.Pages.Employer.Manage.Employer_Manage_Users" %>

<%@ Register TagPrefix="UC" TagName="NavBar" Src="~/Controls/NavigationControl.ascx" %>
<%@ Register TagPrefix="UC" TagName="EmployerInfoBox" Src="~/Controls/EmployerInfoBoxControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <UC:NavBar runat="server" CurrentPageName="Employer" />

    <div class="page__content">

        <UC:EmployerInfoBox runat="server" />

        <div class="form__wrapper">
            <asp:ValidationSummary runat="server" ValidationGroup="validationGroupManageUsers" DisplayMode="BulletList" ShowMessageBox="False" ShowSummary="True" CssClass="message message_type_error" />
            <asp:CustomValidator runat="server" ID="validatorManageUsers" ValidationGroup="validationGroupManageUsers" Display="None" EnableClientScript="false" />
            <div class="form__head">
                <div class="form__title">User info</div>
            </div>
            <div class="form">
                <asp:HiddenField ID="hiddenUserId" runat="server" />

                <div class="form__row inlinefix">
                    <div class="form__label">First Name</div>
                    <div class="form__input-wrapper">
                        <asp:TextBox ID="textbox_firstname" runat="server" CssClass="input input_type_text" autocomplete="off"/>
                        <asp:RequiredFieldValidator runat="server"
                            ValidationGroup="validationGroupManageUsers"
                            ControlToValidate="textbox_firstname"
                            CausesValidation="true"
                            ErrorMessage="Enter first name"
                            Display="none" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Last Name</div>
                    <div class="form__input-wrapper">
                        <asp:TextBox ID="textbox_lastname" runat="server" CssClass="input input_type_text" autocomplete="off"/>
                        <asp:RequiredFieldValidator runat="server"
                            ValidationGroup="validationGroupManageUsers"
                            ControlToValidate="textbox_lastname"
                            CausesValidation="true"
                            ErrorMessage="Enter last name"
                            Display="none" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Email</div>
                    <div class="form__input-wrapper">
                        <asp:TextBox ID="textbox_email" runat="server" CssClass="input input_type_text" autocomplete="off"/>
                        <asp:RequiredFieldValidator runat="server"
                            ValidationGroup="validationGroupManageUsers"
                            ControlToValidate="textbox_email"
                            CausesValidation="true"
                            ErrorMessage="Enter email"
                            Display="none" />
                        <asp:RegularExpressionValidator runat="server"
                            ValidationGroup="validationGroupManageUsers"
                            ControlToValidate="textbox_email"
                            CausesValidation="true"
                            ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            ErrorMessage="Enter valid email"
                            Display="none" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Username</div>
                    <div class="form__input-wrapper">
                        <asp:TextBox ID="textbox_username" runat="server" CssClass="input input_type_text" autocomplete="off"/>
                        <asp:RequiredFieldValidator runat="server"
                            ValidationGroup="validationGroupManageUsers"
                            ControlToValidate="textbox_username"
                            CausesValidation="true"
                            ErrorMessage="Enter username"
                            Display="none" />
                        <asp:RegularExpressionValidator runat="server"
                            ValidationGroup="validationGroupManageUsers"
                            ControlToValidate="textbox_username"
                            CausesValidation="true"
                            ValidationExpression="^[\s\S]{8,256}$"
                            ErrorMessage="Enter username with length between 8 and 256 characters"
                            Display="none" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Password</div>
                    <div class="form__input-wrapper">
                        <asp:TextBox ID="textbox_password" TextMode="Password" runat="server" CssClass="input input_type_text js-textbox_password" autocomplete="off"/>
                        <asp:RequiredFieldValidator runat="server"
                            ValidationGroup="validationGroupManageUsers"
                            ControlToValidate="textbox_password"
                            CausesValidation="true"
                            ErrorMessage="Enter password"
                            Display="none" ID="validatePasswordRequired" />
                        <asp:RegularExpressionValidator runat="server"
                            ValidationGroup="validationGroupManageUsers"
                            ControlToValidate="textbox_password"
                            CausesValidation="true"
                            ValidationExpression="^[\s\S]{8,}$"
                            ErrorMessage="Enter password with lenght above 8 characters"
                            Display="none" />
                    </div>
                    <div class="form__comment">
                        <a href="#" class="link js-generate-password" data-action="<%= ResolveUrl("~/Pages/Employer/Manage/Users.aspx/GeneratePassword") %>" data-binding="js-textbox_password">Generate password</a>
                    </div>
                </div>
                <div class="form__btns">
                    <asp:Button ID="Add_User" ValidationGroup="validationGroupManageUsers" OnClick="SaveUser" CssClass="btn btn_priority_high" Text="Save" runat="server" />
                    <a href="<%= Page.ResolveUrl("~/employer/" + Code + "/manage") %>" class="btn btn_priority_normal"><span class="btn__text">Cancel</span></a>
                </div>
            </div>
        </div>

        <div class="table__wrapper" style="margin: 80px 0 40px 0;">
            <div class="table__title-wrapper">
                <div class="table__title">Employer's users</div>
            </div>
        </div>
        <table class="table">
            <thead class="table__thead">
                <tr class="table__head">
                    <th class="table__column table__column_filter">
                        <div class="column__text">Name</div>
                    </th>
                    <th class="table__column">
                        <div class="column__text">Username</div>
                    </th>
                    <th class="table__column">
                        <div class="column__text">Email</div>
                    </th>
                </tr>
            </thead>
            <tbody class="table__tbody">
                <asp:Repeater ID="tableUsers" runat="server" ItemType="WexHealth.Domain.Entities.User">
                    <ItemTemplate>
                        <tr class="table__row table__row_linked js-user-row js-table__row" data-href="<%#: Page.ResolveUrl("~/employer/" + Code + "/users/?selected=" + Item.Id) %>" data-action="<%= ResolveUrl("~/Pages/Employer/Manage/Users.aspx/GetUser") %>">
                            <td class="table__cell"><%#: Item.Individual.FirstName %> <%#: Item.Individual.LastName %></td>
                            <td class="table__cell"><%#: Item.UserName %></td>
                            <td class="table__cell"><%#: Item.Individual.Email %></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
    </div>
</asp:Content>

<asp:Content ID="ScriptContent" ContentPlaceHolderID="PageScripts" runat="server">
    <script>
        $(document).on('click', '.js-generate-password', function (event) {
            event.preventDefault ? event.preventDefault() : (event.returnValue = false);

            $this = $(this);

            $.ajax({
                type: 'POST',
                url: $this.data('action'),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $binding = $('.' + $this.data('binding'));
                    $binding.attr('type', 'text').val(data.d);
                }
            });
        });
    </script>
</asp:Content>
