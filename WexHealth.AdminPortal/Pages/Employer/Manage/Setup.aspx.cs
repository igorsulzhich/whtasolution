﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Microsoft.Practices.ServiceLocation;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.BLL.Models;
using WexHealth.Domain.Entities;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.Security.Infrastructure;

namespace WexHealth.AdminPortal.Pages.Employer.Manage
{
    public partial class Setup : System.Web.UI.Page, IEmployerSetupView
    {
        IEmployerSetupPresenter _presenter;

        public string Identity
        {
            get
            {
                return (string)RouteData.Values["select"];
            }
        }

        public IEnumerable<SettingInputModel> SelectedSettings
        {
            get
            {
                foreach (var rowSetting in tableSettings.Controls.OfType<RepeaterItem>())
                {
                    var setttingId = rowSetting.FindControl("settingId") as HtmlInputHidden;

                    var allowRadio = rowSetting.FindControl("allowSetting") as HtmlInputRadioButton;
                    var disallowRadio = rowSetting.FindControl("disallowSetting") as HtmlInputRadioButton;
                    var defaultRadio = rowSetting.FindControl("defaultSetting") as HtmlInputRadioButton;

                    if (defaultRadio.Checked)
                    {
                        yield return new SettingInputModel
                        {
                            Id = Guid.Parse(setttingId.Value),
                            isAllowed = null
                        };
                    }
                    else if (allowRadio.Checked)
                    {
                        yield return new SettingInputModel
                        {
                            Id = Guid.Parse(setttingId.Value),
                            isAllowed = true
                        };
                    }
                    else
                    {
                        yield return new SettingInputModel
                        {
                            Id = Guid.Parse(setttingId.Value),
                            isAllowed = false
                        };
                    }
                }
            }
        }

        public IEnumerable<Setting> SettingList
        {
            set
            {
                tableSettings.DataSource = value;
                tableSettings.DataBind();

                foreach (var rowSetting in tableSettings.Controls.OfType<RepeaterItem>())
                {
                    var settingIdInput = rowSetting.FindControl("settingId") as HtmlInputHidden;
                    Guid settingId = Guid.Parse(settingIdInput.Value);

                    var allowRadio = rowSetting.FindControl("allowSetting") as HtmlInputRadioButton;
                    var disallowRadio = rowSetting.FindControl("disallowSetting") as HtmlInputRadioButton;
                    var defaultRadio = rowSetting.FindControl("defaultSetting") as HtmlInputRadioButton;

                    bool? isAllowed = value.Where(x => x.Id.Equals(settingId)).Select(x => x.isAllowed).FirstOrDefault();

                    if (!isAllowed.HasValue)
                    {
                        defaultRadio.Checked = true;
                        allowRadio.Checked = false;
                        disallowRadio.Checked = false;
                    }
                    else if (isAllowed.Value)
                    {
                        defaultRadio.Checked = false;
                        allowRadio.Checked = true;
                        disallowRadio.Checked = false;
                    }
                    else
                    {
                        defaultRadio.Checked = false;
                        allowRadio.Checked = false;
                        disallowRadio.Checked = true;
                    }
                }
            }
        }

        public Setup()
        {
            var factory = ServiceLocator.Current.GetInstance<EmployerSetupPresenterFactory>();
            var principal = (WexHealthPrincipal)HttpContext.Current.User;
            _presenter = factory(this, principal);
        }

        protected void Setting_Click(object sender, EventArgs e)
        {
            OperationResult saveResult = (OperationResult)_presenter.SaveSettings();
            if (OperationStatus.Success == saveResult.Status)
            {
                Response.Redirect(Page.ResolveUrl("~/employer/" + Identity + "/manage"));
            }
            else
            {
                ShowErrorMessage(saveResult.Message);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.InitView(Page.IsPostBack);
        }

        public void ShowErrorMessage(string errorMessage)
        {
            throw new NotImplementedException();
        }
    }
}