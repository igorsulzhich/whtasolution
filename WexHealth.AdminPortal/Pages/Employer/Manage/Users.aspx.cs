﻿using System;
using System.Web;
using System.Collections.Generic;
using Microsoft.Practices.ServiceLocation;
using WexHealth.Security.Infrastructure;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Presentation.Views;
using WexHealth.Presentation.Presenters.Common.Interfaces;
using WexHealth.Domain.Entities;
using WexHealth.Presentation.Presenters.Results;

namespace WexHealth.AdminPortal.Pages.Employer.Manage
{
    public partial class Employer_Manage_Users : System.Web.UI.Page, IManageUsersView
    {
        private readonly IManageUsersPresenter _presenter;

        protected string Code
        {
            get
            {
                return (string)RouteData.Values["select"];
            }
        }

        public string FirstName
        {
            get
            {
                return textbox_firstname.Text;
            }

            set
            {
                textbox_firstname.Text = value;
            }
        }

        public string LastName
        {
            get
            {
                return textbox_lastname.Text;
            }

            set
            {
                textbox_lastname.Text = value;
            }
        }

        public string Email
        {
            get
            {
                return textbox_email.Text;
            }

            set
            {
                textbox_email.Text = value;
            }
        }

        public string UserName
        {
            get
            {
                return textbox_username.Text;
            }

            set
            {
                textbox_username.Text = value;
            }
        }

        public string Password
        {
            get
            {
                return textbox_password.Text;
            }

            set
            {
                textbox_password.Text = value;
            }
        }

        public Guid SelectedUserId
        {
            get
            {
                Guid result = Guid.Empty;

                try
                {
                    result = Guid.Parse(hiddenUserId.Value);
                }
                catch (FormatException)
                { }

                return result;
            }
            set
            {
                hiddenUserId.Value = value.ToString();

                _presenter.LoadUser();
            }
        }

        public IEnumerable<User> Users
        {
            set
            {
                tableUsers.DataSource = value;
                tableUsers.DataBind();
            }
        }

        public Employer_Manage_Users()
        {
            var principal = (WexHealthPrincipal)HttpContext.Current.User;
            var identity = (WexHealthIdentity)principal.Identity;

            if (!string.IsNullOrEmpty(Code))
            {
                var serviceFactory = ServiceLocator.Current.GetInstance<EmployerUserServiceFactory>();
                IDomainUserService userService = serviceFactory(Code, identity.User.Id);

                var presenterFactory = ServiceLocator.Current.GetInstance<ManageUsersPresenterFactoryWithPrincipal>();
                _presenter = presenterFactory(this, principal, userService);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.InitView(Page.IsPostBack);

            if (!Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["selected"]))
                {
                    SelectedUserId = Guid.Parse(Request.QueryString["selected"]);

                    validatePasswordRequired.Enabled = false;
                }
            }
        }

        protected void SaveUser(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                Page.Validate("validationGroupManageUsers");

                if (Page.IsValid)
                {
                    OperationResult result = null;

                    if (Guid.Empty.Equals(SelectedUserId))
                    {
                        result = (OperationResult)_presenter.CreateUser();
                    }
                    else
                    {
                        result = (OperationResult)_presenter.UpdateUser();

                        if (result.Status == OperationStatus.Success)
                        {
                            Response.Redirect(Request.Url.GetLeftPart(UriPartial.Path));
                        }
                    }

                    if (result.Status == OperationStatus.Failed)
                    {
                        validatorManageUsers.IsValid = false;
                        validatorManageUsers.ErrorMessage = result.Message;
                    }
                }
            }
        }

        [System.Web.Services.WebMethod]
        public static string GeneratePassword()
        {
            int length = 8;

            string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
            var chars = new char[length];

            Random random = new Random();

            for (int i = 0; i < length; i++)
            {
                chars[i] = allowedChars[random.Next(0, allowedChars.Length)];
            }

            return new string(chars);
        }
    }
}