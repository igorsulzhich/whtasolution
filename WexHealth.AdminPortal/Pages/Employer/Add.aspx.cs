﻿using System;
using System.Web;
using System.Collections.Generic;
using Microsoft.Practices.ServiceLocation;
using WexHealth.Domain.Entities;
using WexHealth.Security.Infrastructure;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.AdminPortal.Employer
{
    public partial class Add : System.Web.UI.Page, IEmployerManageProfileView
    {
        private readonly IEmployerManageProfilePresenter _presenter;

        public Guid Id { get; set; }

        public string Code
        {
            get
            {
                return textbox_employercode.Text;
            }

            set
            {
                textbox_employercode.Text = value;
            }
        }

        public string Name
        {
            get
            {
                return textbox_employername.Text;
            }

            set
            {
                textbox_employername.Text = value;
            }
        }

        public byte[] Logo
        {
            get
            {
                if (file_image.HasFile)
                {
                    return file_image.FileBytes;
                }
                else
                {
                    return null;
                }
            }
        }

        public string Street
        {
            get
            {
                return textbox_street.Text;
            }

            set
            {
                textbox_street.Text = value;
            }
        }

        public string City
        {
            get
            {
                return textbox_street.Text;
            }

            set
            {
                textbox_street.Text = value;
            }
        }

        public string Phone
        {
            get
            {
                return textbox_phone.Text;
            }

            set
            {
                textbox_phone.Text = value;
            }
        }

        public string ZipCode
        {
            get
            {
                return textbox_zipcode.Text;
            }

            set
            {
                textbox_zipcode.Text = value;
            }
        }

        public string SelectState
        {
            get
            {
                return dropdown_states.SelectedValue;
            }

            set
            {
                dropdown_states.SelectedValue = value;
            }
        }

        public IEnumerable<State> States
        {
            set
            {
                dropdown_states.DataSource = value;
                dropdown_states.DataValueField = "Name";
                dropdown_states.DataBind();
            }
        }

        public string Identity { get; set; }

        public Add()
        {
            var factory = ServiceLocator.Current.GetInstance<EmployerManageProfilePresenterFactory>();
            var principal = (WexHealthPrincipal)HttpContext.Current.User;
            _presenter = factory(this, principal);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.InitView(Page.IsPostBack);
        }

        protected void AddEmployer(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                Page.Validate("validationGroupAddEmployer");

                if (Page.IsValid)
                {
                    OperationResult result = (OperationResult)_presenter.SaveEmployer();
                    if (result.Status == OperationStatus.Success)
                    {
                        Response.Redirect("~/employer");
                    }
                    else
                    {
                        validatorAddEmployer.IsValid = false;
                        validatorAddEmployer.ErrorMessage = result.Message;
                    }
                }
            }
        }
    }
}