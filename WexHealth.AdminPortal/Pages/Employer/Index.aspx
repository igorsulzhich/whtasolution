﻿<%@ Page Title="Employer" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true"  EnableEventValidation="false" CodeBehind="Index.aspx.cs" Inherits="WexHealth.AdminPortal.Employer.Index" %>
<%@ Register TagPrefix="UC" TagName="NavBar" Src="~/Controls/NavigationControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <UC:NavBar runat="server" CurrentPageName="Employer"/>

    <div class="page__content">

        <div class="table__wrapper">
            <div class="table__search">
                <div class="form__wrapper">
                    <form class="form form_orientation_horizontal inlinefix">
                        <div class="form__row form_orientation_horizontal__row">
                            <div class="form__label form_orientation_horizontal__label">Name</div>
                            <div class="form__input-wrapper form_orientation_horizontal__input-wrapper">
                                <asp:TextBox ID="textboxEmployerName" runat="server" CssClass="input input_type_text" autocomplete="off"/>
                            </div>
                        </div>
                        <div class="form__row form_orientation_horizontal__row">
                            <div class="form__label form_orientation_horizontal__label">Code</div>
                            <div class="form__input-wrapper form_orientation_horizontal__input-wrapper">
                                <asp:TextBox ID="textboxEmployerCode" runat="server" CssClass="input input_type_text" autocomplete="off"/>
                            </div>
                        </div>
                        <div class="form__btns form_orientation_horizontal__row__btns">
                            <asp:Button ID="submitSearch" OnClick="submitSearch_Click" CssClass="btn btn_priority_high" Text="Search" runat="server" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="table__title-wrapper clearfix">
                <div class="table__title">All employers</div>
                <div class="table__controls">
                    <a href="<%= Page.ResolveUrl("~/employer/add") %>" class="btn btn_priority_normal"><span class="btn__text">Add employer</span></a>
                </div>
            </div>
            <table class="table">
                <thead class="table__thead">
                    <tr class="table__head">
                        <th class="table__column table__column_filter">
                            <div class="column__text">Name</div>
                        </th>
                        <th class="table__column">
                            <div class="column__text">Code</div>
                        </th>
                    </tr>
                </thead>
                <tbody class="table__tbody">
                    <asp:Repeater ID="listEmployers" runat="server" ItemType="WexHealth.Domain.Entities.Employer">
                        <ItemTemplate>
                            <tr class="table__row table__row_linked js-table__row" data-href="<%# Page.ResolveUrl("~/employer/" + Item.Code + "/manage") %>">
                                <td class="table__cell"><%#: Item.Name %></td>
                                <td class="table__cell"><%# Item.Code %></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
