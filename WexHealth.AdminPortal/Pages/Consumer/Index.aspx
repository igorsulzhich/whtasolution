﻿<%@ Page Title="Consumer" Language="C#" MasterPageFile="~/Admin.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WexHealth.AdminPortal.Consumer.Index" %>

<%@ Register TagPrefix="UC" TagName="NavBar" Src="~/Controls/NavigationControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <UC:NavBar runat="server" CurrentPageName="Consumer" />

    <div class="page__content">
        <div class="table__wrapper">
            <div class="table__search">
                <div class="form__wrapper">
                    <div class="form form_orientation_horizontal inlinefix">
                        <div class="form__row form_orientation_horizontal__row">
                            <div class="form__label form_orientation_horizontal__label">Last name</div>
                            <div class="form__input-wrapper form_orientation_horizontal__input-wrapper">
                                <asp:TextBox ID="textboxLastName" runat="server" CssClass="input input_type_text" autocomplete="off" />
                            </div>
                        </div>
                        <div class="form__row form_orientation_horizontal__row">
                            <div class="form__label form_orientation_horizontal__label">First name</div>
                            <div class="form__input-wrapper form_orientation_horizontal__input-wrapper">
                                <asp:TextBox ID="textboxFirstName" runat="server" CssClass="input input_type_text" autocomplete="off" />
                            </div>
                        </div>
                        <div class="form__row form_orientation_horizontal__row">
                            <div class="form__label form_orientation_horizontal__label">Employer</div>
                            <div class="form__input-wrapper">
                                <asp:DropDownList runat="server" ID="dropdownEmployers" CssClass="input input_type_dropdown" AppendDataBoundItems="true">
                                    <asp:ListItem Text="- Select employer -" Value="" />
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form__btns form_orientation_horizontal__row__btns">
                            <asp:Button ID="submitSearch" OnClick="submitSearch_Click" CssClass="btn btn_priority_high" Text="Search" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="table__title-wrapper clearfix">
                <div class="table__title">All consumers</div>
            </div>
            <table class="table">
                <thead class="table__thead">
                    <tr class="table__head">
                        <th class="table__column table__column_filter">
                            <div class="column__text">Last name</div>
                        </th>
                        <th class="table__column">
                            <div class="column__text">First name</div>
                        </th>
                        <th class="table__column table__cell_type_numeric">
                            <div class="column__text">SSN</div>
                        </th>
                    </tr>
                </thead>
                <tbody class="table__tbody">
                    <asp:Repeater ID="rptConsumers" runat="server">
                        <ItemTemplate>
                            <tr class="table__row table__row_linked js-table__row" data-href="<%# Page.ResolveUrl("~/consumer/" + Eval("Id") + "/manage") %>">
                                <td class="table__cell"><%# Eval("Individual.LastName") %></td>
                                <td class="table__cell"><%# Eval("Individual.FirstName") %></td>
                                <td class="table__cell table__cell_type_numeric"><%# Eval("SSN") %></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
