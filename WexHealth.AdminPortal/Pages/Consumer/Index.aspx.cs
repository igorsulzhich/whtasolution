﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Collections.Specialized;
using Microsoft.Practices.ServiceLocation;
using WexHealth.Web.Helpers;
using WexHealth.Security.Infrastructure;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.AdminPortal.Consumer
{
    public partial class Index : System.Web.UI.Page, IConsumerIndexView
    {
        private readonly IConsumerIndexPresenter _presenter;

        public string ConsumerFirstName
        {
            get
            {
                return Request.QueryString["fn"];
            }
        }

        public string ConsumerLastName
        {
            get
            {
                return Request.QueryString["ln"];
            }
        }

        public IEnumerable<Domain.Entities.Employer> Employers
        {
            set
            {
                dropdownEmployers.DataSource = value;
                dropdownEmployers.DataTextField = "Name";
                dropdownEmployers.DataValueField = "Code";
                dropdownEmployers.DataBind();
            }
        }

        public string SelectedEmployer
        {
            get
            {
                return Request.QueryString["e"];
            }
        }

        public IEnumerable<Domain.Entities.Consumer> Consumers
        {
            set
            {
                rptConsumers.DataSource = value;
                rptConsumers.DataBind();
            }
        }

        public Index()
        {
            var factory = ServiceLocator.Current.GetInstance<ConsumerIndexPresenterFactory>();
            var principal = (WexHealthPrincipal)HttpContext.Current.User;
            _presenter = factory(this, principal);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.InitView(Page.IsPostBack);

            if (!Page.IsPostBack)
            {
                InitConsumerFirstName();
                InitConsumerLastName();
                InitSelectedEmployer();
            }
        }

        private void InitConsumerFirstName()
        {
            if (!string.IsNullOrEmpty(ConsumerFirstName))
            {
                textboxFirstName.Text = ConsumerFirstName;
            }
        }

        private void InitConsumerLastName()
        {
            if (!string.IsNullOrEmpty(ConsumerLastName))
            {
                textboxLastName.Text = ConsumerLastName;
            }
        }

        private void InitSelectedEmployer()
        {
            if (!string.IsNullOrEmpty(SelectedEmployer) && null != dropdownEmployers.Items.FindByValue(SelectedEmployer))
            {
                dropdownEmployers.SelectedValue = SelectedEmployer;
            }
        }

        protected void submitSearch_Click(object sender, EventArgs e)
        {
            var paramList = new NameValueCollection()
            {
                { "fn", textboxFirstName.Text },
                { "ln", textboxLastName.Text },
                { "e", dropdownEmployers.SelectedValue }
            };

            Response.Redirect(UrlHelper.AddParamsUrl(paramList));
        }
    }
}