﻿<%@ Page Title="Edit profile | Consumer" Language="C#" MasterPageFile="~/Admin.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="Edit-Profile.aspx.cs" Inherits="WexHealth.AdminPortal.Consumer.Manage.Edit_Profile" %>

<%@ Register TagPrefix="UC" TagName="NavBar" Src="~/Controls/NavigationControl.ascx" %>
<%@ Register TagPrefix="UC" TagName="ConsumerInfoBox" Src="~/Controls/ConsumerInfoBoxControl.ascx" %>

<asp:Content ID="ContentEdit" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <UC:NavBar runat="server" CurrentPageName="Consumer" />

    <div class="page__content">

        <UC:ConsumerInfoBox runat="server" />

        <div class="form__wrapper">
            <asp:ValidationSummary runat="server" ValidationGroup="validationGroupEditProfile" DisplayMode="BulletList" ShowMessageBox="False" ShowSummary="True" CssClass="message message_type_error" />
            <asp:CustomValidator runat="server" ID="validateEditProfile" ValidationGroup="validationGroupEditProfile" Display="None" EnableClientScript="false" />
            <div class="form__head">
                <div class="form__title">Edit profile</div>
            </div>
            <div class="form">
                <asp:HiddenField ID="hidden_Id" runat="server" />
                <asp:HiddenField ID="hidden_employerId" runat="server" />
                <div class="form__row inlinefix">
                    <div class="form__label">First Name</div>
                    <div class="form__input-wrapper">
                        <asp:TextBox ID="textboxFirstName" runat="server" CssClass="input input_type_text" autocomplete="off" />
                        <asp:RequiredFieldValidator runat="server"
                            ValidationGroup="validationGroupEditProfile"
                            ControlToValidate="textboxFirstName"
                            CausesValidation="true"
                            ErrorMessage="Enter first name"
                            Display="none" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Last Name</div>
                    <div class="form__input-wrapper">
                        <asp:TextBox ID="textboxLastName" runat="server" CssClass="input input_type_text" autocomplete="off" />
                        <asp:RequiredFieldValidator runat="server"
                            ValidationGroup="validationGroupEditProfile"
                            ControlToValidate="textboxLastName"
                            CausesValidation="true"
                            ErrorMessage="Enter last name"
                            Display="none" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">SSN</div>
                    <div class="form__input-wrapper">
                        <asp:TextBox ID="textboxSsn" runat="server" CssClass="input input_type_text js-input_type_ssn" autocomplete="off" />
                        <asp:RequiredFieldValidator runat="server"
                            ValidationGroup="validationGroupEditProfile"
                            ControlToValidate="textboxSsn"
                            CausesValidation="true"
                            ErrorMessage="Enter SSN"
                            Display="none" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Email</div>
                    <div class="form__input-wrapper">
                        <asp:TextBox ID="textboxEmail" runat="server" CssClass="input input_type_text" autocomplete="off" />
                        <asp:RequiredFieldValidator runat="server"
                            ValidationGroup="validationGroupEditProfile"
                            ControlToValidate="textboxEmail"
                            CausesValidation="true"
                            ErrorMessage="Enter email"
                            Display="none" />
                        <asp:RegularExpressionValidator runat="server"
                            ValidationGroup="validationGroupEditProfile"
                            ControlToValidate="textboxEmail"
                            CausesValidation="true"
                            ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            ErrorMessage="Enter valid email"
                            Display="none" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Phone</div>
                    <div class="form__input-wrapper">
                        <asp:TextBox ID="textboxPhone" runat="server" CssClass="input input_type_text js-input_type_phone" autocomplete="off" />
                        <asp:RequiredFieldValidator runat="server"
                            ValidationGroup="validationGroupEditProfile"
                            ControlToValidate="textboxPhone"
                            CausesValidation="true"
                            ErrorMessage="Enter phone"
                            Display="none" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Street</div>
                    <div class="form__input-wrapper">
                        <asp:TextBox ID="textboxStreet" runat="server" CssClass="input input_type_text" autocomplete="off" />
                        <asp:RequiredFieldValidator runat="server"
                            ValidationGroup="validationGroupEditProfile"
                            ControlToValidate="textboxStreet"
                            CausesValidation="true"
                            ErrorMessage="Enter street"
                            Display="none" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">City</div>
                    <div class="form__input-wrapper">
                        <asp:TextBox ID="textboxCity" runat="server" CssClass="input input_type_text" autocomplete="off" />
                        <asp:RequiredFieldValidator runat="server"
                            ValidationGroup="validationGroupEditProfile"
                            ControlToValidate="textboxCity"
                            CausesValidation="true"
                            ErrorMessage="Enter city"
                            Display="none" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">State</div>
                    <div class="form__input-wrapper">
                        <asp:DropDownList runat="server" ID="dropdownStates" CssClass="input input_type_dropdown" AppendDataBoundItems="true">
                            <asp:ListItem Text="- Select state -" Value="" />
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator runat="server"
                            ValidationGroup="validationGroupEditProfile"
                            ControlToValidate="dropdownStates"
                            CausesValidation="true"
                            ErrorMessage="Enter state"
                            InitialValue=""
                            Display="none" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Zip Code</div>
                    <div class="form__input-wrapper">
                        <asp:TextBox ID="textboxZipCode" runat="server" CssClass="input input_type_text" autocomplete="off" />
                        <asp:RequiredFieldValidator runat="server"
                            ValidationGroup="validationGroupEditProfile"
                            ControlToValidate="textboxZipCode"
                            CausesValidation="true"
                            ErrorMessage="Enter zip code"
                            Display="none" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Date of birth</div>
                    <div class="form__input-wrapper">
                        <asp:TextBox ID="textboxDateOfBirth" runat="server" CssClass="input input_type_text js-input_type_date" autocomplete="off" />
                        <asp:RequiredFieldValidator runat="server"
                            ValidationGroup="validationGroupEditProfile"
                            ControlToValidate="textboxDateOfBirth"
                            CausesValidation="true"
                            ErrorMessage="Enter date of birth"
                            Display="none" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Username</div>
                    <div class="form__input-wrapper">
                        <asp:TextBox ID="textboxUserName" runat="server" CssClass="input input_type_text" autocomplete="off" />
                        <asp:RegularExpressionValidator runat="server"
                            ValidationGroup="validationGroupEditProfile"
                            ControlToValidate="textboxUserName"
                            ValidationExpression="^[\s\S]{5,256}$"
                            ErrorMessage="Enter username with length between 5 and 256 characters"
                            Display="none" />
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label">Password</div>
                    <div class="form__input-wrapper">
                        <asp:TextBox ID="textboxPassword" TextMode="Password" runat="server" CssClass="input input_type_text" autocomplete="off" />
                        <asp:RegularExpressionValidator runat="server"
                            ValidationGroup="validationGroupEditProfile"
                            ControlToValidate="textboxPassword"
                            ValidationExpression="^[\s\S]{8,}$"
                            ErrorMessage="Enter password with length above 8 characters"
                            Display="none" />
                    </div>
                </div>
                <div class="form__btns">
                    <asp:Button ID="Update_ConsumerItem" ValidationGroup="validationGroupEditProfile" OnClick="UpdateConsumer" CssClass="btn btn_priority_high" Text="Submit" runat="server" />
                    <a href="<%= Page.ResolveUrl("~/consumer/" + Identity + "/manage") %>" class="btn btn_priority_normal"><span class="btn__text">Cancel</span></a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
