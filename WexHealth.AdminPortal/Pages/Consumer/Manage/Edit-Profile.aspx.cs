﻿using System;
using System.Web;
using System.Globalization;
using System.Collections.Generic;
using Microsoft.Practices.ServiceLocation;
using WexHealth.Domain.Entities;
using WexHealth.Security.Infrastructure;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;
using WexHealth.Presentation.Presenters.Results;

namespace WexHealth.AdminPortal.Consumer.Manage
{
    public partial class Edit_Profile : System.Web.UI.Page, IConsumerManageProfileView
    {
        IConsumerManageProfilePresenter _presenter;

        public Guid ConsumerId
        {
            get
            {
                return Guid.Parse(hidden_Id.Value);
            }

            set
            {
                hidden_Id.Value = value.ToString();
            }
        }

        public string EmployerCode { get; set; }

        public string City
        {
            get
            {
                return textboxCity.Text;
            }

            set
            {
                textboxCity.Text = value;
            }
        }

        public string Email
        {
            get
            {
                return textboxEmail.Text;
            }

            set
            {
                textboxEmail.Text = value;
            }
        }

        public string FirstName
        {
            get
            {
                return textboxFirstName.Text;
            }

            set
            {
                textboxFirstName.Text = value;
            }
        }

        public string Identity
        {
            get
            {
                if (string.IsNullOrEmpty((string)RouteData.Values["select"]))
                {
                    return null;
                }
                else
                {
                    return (string)RouteData.Values["select"];
                }
            }
            set
            {
                RouteData.Values["select"] = value;
            }
        }

        public string LastName
        {
            get
            {
                return textboxLastName.Text;
            }

            set
            {
                textboxLastName.Text = value;
            }
        }

        public string Password
        {
            get
            {
                return textboxPassword.Text;
            }

            set
            {
                textboxPassword.Text = value;
            }
        }

        public string Phone
        {
            get
            {
                return textboxPhone.Text;
            }

            set
            {
                textboxPhone.Text = value;
            }
        }

        public string SSN
        {
            get
            {
                return textboxSsn.Text;
            }

            set
            {
                textboxSsn.Text = value;
            }
        }

        public string SelectState
        {
            get
            {
                return dropdownStates.SelectedValue;
            }

            set
            {
                dropdownStates.SelectedValue = value;
            }
        }

        public IEnumerable<State> States
        {
            set
            {
                dropdownStates.DataSource = value;
                dropdownStates.DataValueField = "Name";
                dropdownStates.DataBind();
            }
        }

        public string UserName
        {
            get
            {
                return textboxUserName.Text;
            }

            set
            {
                textboxUserName.Text = value;
            }
        }

        public string ZipCode
        {
            get
            {
                return textboxZipCode.Text;
            }

            set
            {
                textboxZipCode.Text = value;
            }
        }

        public string Street
        {
            get
            {
                return textboxStreet.Text;
            }

            set
            {
                textboxStreet.Text = value;
            }
        }

        public DateTime DateOfBirth
        {
            get
            {
                DateTime result = DateTime.MinValue;

                if (DateTime.TryParse(textboxDateOfBirth.Text, CultureInfo.InvariantCulture, DateTimeStyles.None, out result))
                {
                    return result;
                }
                else
                {
                    throw new InvalidOperationException("Date of birth incorrect");
                }
            }

            set
            {
                textboxDateOfBirth.Text = value.ToString("MM/d/yyyy", CultureInfo.InvariantCulture);
            }
        }

        public Edit_Profile()
        {
            var factory = ServiceLocator.Current.GetInstance<ConsumerManageProfilePresenterFactory>();
            var principal = (WexHealthPrincipal)HttpContext.Current.User;
            _presenter = factory(this, principal);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.InitView(Page.IsPostBack);

            if (!IsPostBack)
            {
                _presenter.LoadConsumer();
            }
        }

        protected void UpdateConsumer(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                Page.Validate("validationGroupEditProfile");

                if (Page.IsValid)
                {
                    OperationResult result = (OperationResult)_presenter.SaveConsumer();
                    if (result.Status == OperationStatus.Success)
                    {
                        Response.Redirect("~/consumer/" + Identity + "/manage");
                    }
                    else
                    {
                        validateEditProfile.IsValid = false;
                        validateEditProfile.ErrorMessage = result.Message;
                    }
                }
            }
        }
    }
}