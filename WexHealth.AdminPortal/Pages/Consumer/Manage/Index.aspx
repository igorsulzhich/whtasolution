﻿<%@ Page Title="Manage | Consumer" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WexHealth.AdminPortal.Consumer.Manage.Index" %>

<%@ Register TagPrefix="UC" TagName="NavBar" Src="~/Controls/NavigationControl.ascx" %>
<%@ Register TagPrefix="UC" TagName="ConsumerInfoBox" Src="~/Controls/ConsumerInfoBoxControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <UC:NavBar runat="server" CurrentPageName="Consumer" />

    <div class="page__content">

        <UC:ConsumerInfoBox runat="server" />

        <div class="setting-wrapper">
            <div class="setting__groups inlinefix">
                <div class="setting__group-item">
                    <div class="setting__title">Profile</div>
                    <ul class="setting__list">
                        <li class="setting__item" runat="server" visible='<%# IsSettingAllow("Create consumers") %>'><a href="<%= Page.ResolveUrl("~/consumer/" + Identity + "/edit-profile") %>" class="setting__link link">Edit</a></li>
                        <li class="setting__item"><a href="<%= Page.ResolveUrl("~/consumer/" + Identity + "/manage/enrollment") %>" class="setting__link link">Manage enrollments</a></li>
                    </ul>
                </div>
                <div class="setting__group-item">
                    <div class="setting__title">Claims</div>
                    <ul class="setting__list">
                        <li class="setting__item"><a href="<%= Page.ResolveUrl("~/claims?c=" + Identity + "&e=" + EmployerCode) %>" class="setting__link link">Manage</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
