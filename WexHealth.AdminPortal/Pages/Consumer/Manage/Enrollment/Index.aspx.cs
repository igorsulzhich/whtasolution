﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Microsoft.Practices.ServiceLocation;
using WexHealth.Domain.Entities;
using WexHealth.Security.Infrastructure;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.AdminPortal.Consumer.Manage.Enrollment
{
    public partial class Consumer_Manage_Enrollment : System.Web.UI.Page, IConsumerManageEnrollmentView
    {
        private IConsumerManageEnrollmentPresenter _presenter;

        public IEnumerable<Domain.Entities.Enrollment> Enrollments
        {
            set
            {
                listEnrollments.DataSource = value;
                listEnrollments.DataBind();
            }
        }

        public string ConsumerId
        {
            get
            {
                return (string)RouteData.Values["select"];
            }
        }

        public int EnrollmentId
        {
            get
            {
                int result = 0;

                if (!Int32.TryParse(hiddenEnrollmentId.Value, out result))
                {
                    throw new InvalidOperationException("Invalid enrollment id");
                }

                return result;
            }
            set
            {
                hiddenEnrollmentId.Value = value.ToString();
            }
        }

        public IEnumerable<BenefitsOffering> Plans
        {
            set
            {
                dropdownPlans.DataSource = value;
                dropdownPlans.DataTextField = "Name";
                dropdownPlans.DataValueField = "Id";
                dropdownPlans.DataBind();
            }
        }

        public int SelectedPlan
        {
            get
            {
                int result = 0;

                if (!Int32.TryParse(dropdownPlans.SelectedValue, out result))
                {
                    throw new InvalidOperationException("Invalid selected plan");
                }

                return result;
            }
            set
            {
                dropdownPlans.SelectedValue = value.ToString();
            }
        }

        public decimal Election
        {
            get
            {
                Decimal result = 0;

                if (!Decimal.TryParse(textboxElection.Value, out result))
                {
                    throw new InvalidOperationException("Invalid election");
                }

                return result;
            }
            set
            {
                textboxElection.Value = value.ToString();
            }
        }

        public decimal Contribution
        {
            set
            {
                labelContribution.InnerText = value.ToString();
            }
        }

        public Consumer_Manage_Enrollment()
        {
            var factory = ServiceLocator.Current.GetInstance<ConsumerManageEnrollmentPresenterFactory>();
            var principal = (WexHealthPrincipal)HttpContext.Current.User;
            _presenter = factory(this, principal);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.InitView(Page.IsPostBack);
        }

        protected void submitEnrollment_Click(object sender, EventArgs e)
        {
            OperationResult result;

            if (EnrollmentId == 0)
            {
                result = (OperationResult)_presenter.CreateEnrollment();
            }
            else
            {
                result = (OperationResult)_presenter.UpdateEnrollment();
            }

            if (result.Status == OperationStatus.Success)
            {
                ClearModal();
            }
            else
            {
                validateEnrollments.IsValid = false;
                validateEnrollments.ErrorMessage = result.Message;

                modalManageEnrollment.Show();
            }
        }

        protected void dropdownPlans_SelectedIndexChanged(object sender, EventArgs e)
        {
            _presenter.LoadPlanContribution();
            modalManageEnrollment.Show();
        }

        protected void buttonEdit_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;

            int enrollmentId = Int32.Parse(btn.CommandArgument);

            EnrollmentId = enrollmentId;
            _presenter.LoadEnrollment(enrollmentId);

            modalManageEnrollment.Show();
        }

        private void ClearModal()
        {
            EnrollmentId = 0;
            SelectedPlan = 0;
            Election = 0;
            Contribution = 0;
        }
    }
}