﻿<%@ Page Title="Enrollments | Consumer" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WexHealth.AdminPortal.Consumer.Manage.Enrollment.Consumer_Manage_Enrollment" %>

<%@ Register TagPrefix="UC" TagName="NavBar" Src="~/Controls/NavigationControl.ascx" %>
<%@ Register TagPrefix="UC" TagName="ConsumerInfoBox" Src="~/Controls/ConsumerInfoBoxControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <UC:NavBar runat="server" CurrentPageName="Consumer" />

    <div class="page__content">

        <UC:ConsumerInfoBox runat="server" />

        <div class="table__wrapper">
            <div class="table__title-wrapper">
                <div class="table__title">
                    Enrollments
                </div>
                <div class="table__controls">
                    <asp:LinkButton runat="server" ID="buttonAddEnrollment" CssClass="btn btn_priority_normal" CausesValidation="false"><span class="btn__text">Add enrollment</span></asp:LinkButton>
                </div>
            </div>
            <table class="table">
                <thead class="table__thead">
                    <tr class="table__head">
                        <th class="table__column ">
                            <div class="column__text">Plan</div>
                        </th>
                        <th class="table__column table__column_type_numeric">
                            <div class="column__text">Election</div>
                        </th>
                        <th class="table__column table__column_type_numeric">
                            <div class="column__text">Plan contribution</div>
                        </th>
                        <th class="table__column table__column_type_numeric">
                            <div class="column__text">Balance</div>
                        </th>
                        <th class="table__column"></th>
                    </tr>
                </thead>
                <tbody class="table__tbody">
                    <asp:Repeater runat="server" ID="listEnrollments" ItemType="WexHealth.Domain.Entities.Enrollment">
                        <ItemTemplate>
                            <tr class="table__row">
                                <td class="table__cell"><%#: Item.BenefitsOffering.Name %></td>
                                <td class="table__cell table__cell_type_numeric">$ <%#: Item.ElectionAmount %></td>
                                <td class="table__cell table__cell_type_numeric">$ <%#: Item.BenefitsOffering.ContributionAmount %></td>
                                <td class="table__cell table__cell_type_numeric">$ <%#: Item.Contributions.Sum(x => x.Amount) %></td>
                                <td class="table__cell table__cell_type_action">
                                    <asp:LinkButton runat="server" ID="buttonEdit" CssClass="btn btn_priority_normal" OnClick="buttonEdit_Click" CausesValidation="false" CommandArgument='<%#: Item.Id %>'><span class="btn__text">Update</span></asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
        </div>

        <div style="margin: 50px 0 0 0;">
            <a href="<%= Page.ResolveUrl("~/consumer/" + ConsumerId + "/manage") %>" class="btn btn_priority_normal"><span class="btn__text">Cancel</span></a>
        </div>
    </div>

    <div id="popupEnrollment" runat="server" class="popup" style="display: none;">
        <asp:ValidationSummary runat="server" DisplayMode="BulletList" ShowMessageBox="False" ShowSummary="True" CssClass="message message_type_error" />
        <asp:CustomValidator runat="server" ID="validateEnrollments" Display="None" EnableClientScript="false" />
        <div class="form__head">
            <div class="form__title">Manage enrollment</div>
        </div>
        <div class="form">
            <input type="hidden" runat="server" id="hiddenEnrollmentId" value="0" />
            <div class="form__row inlinefix">
                <div class="form__label">Plan</div>
                <div class="form__input-wrapper">
                    <asp:DropDownList runat="server" ID="dropdownPlans" CssClass="input input_type_dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="dropdownPlans_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem Text="- Select plan -" Value="0"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="dropdownPlans" Display="None" InitialValue="0" ErrorMessage="Select plan" />
                </div>
            </div>
            <div class="form__row inlinefix">
                <div class="form__label">Election</div>
                <div class="form__input-wrapper">
                    <input type="text" runat="server" id="textboxElection" class="input input_type_text" autocomplete="off" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="textboxElection" Display="None" ErrorMessage="Enter election" />
                    <asp:RegularExpressionValidator runat="server" ControlToValidate="textboxElection" ErrorMessage="Enter valid election" Display="None" ValidationExpression="^[0-9]{1,10}([.][0-9]{1,2})?$" />
                </div>
            </div>
            <div class="form__row inlinefix">
                <div class="form__label">Contributions</div>
                <div class="form__input-wrapper">
                    $ <span runat="server" id="labelContribution">0</span>
                </div>
            </div>
            <div class="form__btns">
                <asp:Button runat="server" ID="submitEnrollment" CssClass="btn btn_priority_high" Text="Submit" OnClick="submitEnrollment_Click" />
                <asp:LinkButton ID="buttonCancel" runat="server" CssClass="btn btn_priority_normal"><span class="btn__text">Cancel</span></asp:LinkButton>
            </div>
        </div>
    </div>

    <ajaxToolkit:ModalPopupExtender ID="modalManageEnrollment" runat="server" TargetControlID="buttonAddEnrollment"
        PopupControlID="popupEnrollment" OkControlID="buttonCancel" />

</asp:Content>
