﻿<%@ Page Title="Reports" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WexHealth.AdminPortal.Pages.Reports.Index" %>
<%@ Register TagPrefix="UC" TagName="NavBar" Src="~/Controls/NavigationControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <UC:NavBar runat="server" CurrentPageName="Reports"/>

    <div class="page__content">
        <div class="setting-wrapper">
            <div class="setting__groups inlinefix">
                <div class="setting__group-item">
                    <div class="setting__title">Consumer's reports</div>
                    <ul class="setting__list">
                        <li class="setting__item"><a href="<%= Page.ResolveUrl("~/reports/consumer/add-request") %>" class="setting__link link">Add request</a></li>
                        <li class="setting__item"><a href="<%= Page.ResolveUrl("~/reports/consumer") %>" class="setting__link link">History of requests</a></li>
                    </ul>
                </div>
                <div class="setting__group-item">
                    <div class="setting__title">Employer's reports</div>
                    <ul class="setting__list">
                        <li class="setting__item"><a href="<%= Page.ResolveUrl("~/reports/employer/add-request") %>" class="setting__link link">Add request</a></li>
                        <li class="setting__item"><a href="<%= Page.ResolveUrl("~/reports/employer") %>" class="setting__link link">History of requests</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
