﻿using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.Domain.Entities;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.Security.Infrastructure;

namespace WexHealth.AdminPortal.Pages.Reports.Employer
{
    public partial class Index : System.Web.UI.Page, IReportHistoryView
    {
        private readonly IReportHistoryEmployerPresenter _presenter;

        public IEnumerable<ReportRequest> ReportRequests
        {
            set
            {
                listRequests.DataSource = value;
                listRequests.DataBind();
            }
        }

        public Index()
        {
            var factory = ServiceLocator.Current.GetInstance<ReportHistoryEmployerPresenterFactory>();
            var principal = (WexHealthPrincipal)HttpContext.Current.User;
            _presenter = factory(this, principal);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.InitView(Page.IsPostBack);
        }

        protected void Reject_Command(object sender, CommandEventArgs e)
        {
            int id = 0;

            if (int.TryParse(e.CommandArgument.ToString(), out id))
            {
                OperationResult updateResult = (OperationResult)_presenter.RejectRequest(id);
                if (OperationStatus.Success == updateResult.Status)
                {
                    Response.Redirect("/reports/employer");
                }
            }
        }

        protected void Delete_Command(object sender, CommandEventArgs e)
        {
            int id = 0;

            if (int.TryParse(e.CommandArgument.ToString(), out id))
            {
                OperationResult updateResult = (OperationResult)_presenter.DeleteRequest(id);
                if (OperationStatus.Success == updateResult.Status)
                {
                    Response.Redirect("/reports/employer");
                }
            }
        }

        protected string GetRelativePath(string filespec)
        {
            Uri pathUri = new Uri(filespec);

            string folder = Server.MapPath("");

            if (!folder.EndsWith(Path.DirectorySeparatorChar.ToString()))
            {
                folder += Path.DirectorySeparatorChar;
            }

            Uri folderUri = new Uri(folder);
            return Uri.UnescapeDataString(folderUri.MakeRelativeUri(pathUri).ToString().Replace('/', Path.DirectorySeparatorChar));
        }
    }
}