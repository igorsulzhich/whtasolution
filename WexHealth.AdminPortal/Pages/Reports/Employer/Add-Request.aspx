﻿<%@ Page Title="Add request | Admin Portal" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="Add-Request.aspx.cs" Inherits="WexHealth.AdminPortal.Pages.Reports.Employer.Add_Request" %>
<%@ Register TagPrefix="UC" TagName="NavBar" Src="~/Controls/NavigationControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <UC:NavBar runat="server" CurrentPageName="Reports"/>

    <div class="page__content">
        <div class="form__wrapper">
            <asp:ValidationSummary runat="server" ValidationGroup="validationGroupRequestEmployer" DisplayMode="BulletList" ShowMessageBox="False" ShowSummary="True" CssClass="message message_type_error" />
            <asp:CustomValidator runat="server" ID="validatorRequestEmployer" ValidationGroup="validationGroupRequestEmployer" Display="None" EnableClientScript="false" />
            <div class="form__head">
                <div class="form__title">Request for employer's report</div>
            </div>
            <div class="form">
                <div class="form__row inlinefix">
                    <div class="form__label">Report format</div>
                    <div class="form__input-wrapper">
                        <input class="input input_type_radiobutton" type="radio" name="setup" checked="checked" />
                        <div class="input_type_radiobutton__label">CSV</div>
                    </div>
                </div>
                <div class="form__row inlinefix">
                    <div class="form__label" style="vertical-align: top;">Include field</div>
                    <div class="form__input-wrapper input__list">
                        <asp:Repeater runat="server" ID="fieldsList" ItemType="WexHealth.Domain.Entities.ObjectField">
                            <ItemTemplate>
                                <div class="input__item inlinefix">
                                    <input type="hidden" runat="server" value='<%#: Item.Id %>' id="objectFieldId" name="fieldId" />
                                    <input class="input_type_checkbox" type="checkbox" id="checkedField" name="checkedField" checked="checked" runat="server"/>
                                    <label class="input_item__label"><%#: Item.Name %></label>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <div class="form__btns">
                    <asp:Button ID="Report" OnClick="SendRequest" CssClass="btn btn_priority_high" ValidationGroup="validationGroupRequestEmployer" Text="Submit" runat="server" />
                    <a href="<%= Page.ResolveUrl("~/reports/employer") %>" class="btn btn_priority_normal"><span class="btn__text">Cancel</span></a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>