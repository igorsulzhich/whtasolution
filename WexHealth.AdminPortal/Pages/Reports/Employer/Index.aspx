﻿<%@ Page Title="Employer's requested reports | Admin Portal" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WexHealth.AdminPortal.Pages.Reports.Employer.Index" %>
<%@ Register TagPrefix="UC" TagName="NavBar" Src="~/Controls/NavigationControl.ascx" %>
<%@ Import Namespace="System.Globalization" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <UC:NavBar runat="server" CurrentPageName="Reports"/>

    <div class="page__content">
        <div class="table__wrapper">
            <div class="table__title-wrapper clearfix">
                <div class="table__title">Emoloyer's requested reports</div>
                <div class="table__controls">
                    <a href="<%= Page.ResolveUrl("~/reports/employer/add-request") %>" class="btn btn_priority_normal">Add request</a>
                </div>
            </div>
            <table class="table">
                <thead class="table__thead">
                    <tr class="table__head">
                        <th class="table__column table__column_filter">
                            <div class="column__text">Request status</div>
                        </th>
                        <th class="table__column">
                            <div class="column__text">Date</div>
                        </th>
                        <th class="table__column">
                            <div class="column__text">Requested by</div>
                        </th>
                        <th class="table__column">
                            <div class="column__text">Format</div>
                        </th>
                        <th class="table__column">
                        </th>
                    </tr>
                </thead>
                <tbody class="table__tbody">
                    <asp:Repeater ID="listRequests" runat="server" ItemType="WexHealth.Domain.Entities.ReportRequest">
                        <ItemTemplate>
                            <tr class="table__row">

                                <td class="table__cell">                                    
                                    <%# !string.IsNullOrEmpty(Item.FilePath) && Item.Status.Name == "Completed" ? 
                                            "<a href='" + GetRelativePath(Item.FilePath) + "' class='link'>" + Item.Status.Name + "</a>" :
                                            Item.Status.Name %>
                                </td>
                                <td class="table__cell"><%#: Item.DateOfRequest.ToString("MM/d/yyyy H:mm", CultureInfo.InvariantCulture) %></td>
                                <td class="table__cell"><%#: Item.RequestedBy %></td>
                                <td class="table__cell"><%#: Item.FormatReport %></td>
                                <td class="table__cell table__cell_type_action">
                                    <asp:LinkButton ID="Reject" 
                                        CssClass="btn btn_priority_normal"
                                        Text="Reject"
                                        OnCommand="Reject_Command" 
                                        CommandArgument="<%#: Item.Id %>"
                                        runat="server"
                                        Visible='<%# Item.Status.Name == "Requested" %>' />
                                    <asp:LinkButton ID="Delete" 
                                        CssClass="btn btn_priority_normal"
                                        Text="Remove" 
                                        OnCommand="Delete_Command" 
                                        CommandArgument="<%#: Item.Id %>"
                                        runat="server"
                                        Visible='<%# Item.Status.Name == "Completed" || Item.Status.Name == "Rejected" %>' />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
