﻿using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.Security.Infrastructure;
using WexHealth.Domain.Entities;
using System.Web.UI.HtmlControls;
using WexHealth.Presentation.Presenters.Results;
using log4net;

namespace WexHealth.AdminPortal.Pages.Reports.Employer
{
    public partial class Add_Request : System.Web.UI.Page, IReportEmployerView
    {
        private readonly IReportEmployerPresenter _presenter;

        public IEnumerable<ObjectField> Fields
        {
            set
            {
                fieldsList.DataSource = value;
                fieldsList.DataBind();
            }
        }

        public IEnumerable<ObjectField> SelectedFields
        {
            get
            {
                foreach (var rowSetting in fieldsList.Controls.OfType<RepeaterItem>())
                {
                    var fieldId = rowSetting.FindControl("objectFieldId") as HtmlInputHidden;
                    var checkedField = rowSetting.FindControl("checkedField") as HtmlInputCheckBox;

                    if (checkedField.Checked == true)
                    {
                        yield return new ObjectField
                        {
                            Id = Int32.Parse(fieldId.Value)                           
                        };                        
                    }
                }
            }
        }

        public Add_Request()
        {
            var factory = ServiceLocator.Current.GetInstance<ReportEmployerPresenterFactory>();
            var principal = (WexHealthPrincipal)HttpContext.Current.User;
            _presenter = factory(this, principal);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.InitView(Page.IsPostBack);
        }

        protected void SendRequest(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                Page.Validate("validationGroupRequestEmployer");

                if (Page.IsValid)
                {
                    OperationResult result = (OperationResult)_presenter.SendRequest();
                    if (OperationStatus.Success == result.Status)
                    {
                        Response.Redirect(Page.ResolveUrl("/reports/employer"));
                    }
                    else
                    {
                        validatorRequestEmployer.IsValid = false;
                        validatorRequestEmployer.ErrorMessage = result.Message;
                    }
                }
            }
        }
    }
}