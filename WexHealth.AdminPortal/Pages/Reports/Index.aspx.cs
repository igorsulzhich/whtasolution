﻿using System;
using System.Web;
using Microsoft.Practices.ServiceLocation;
using WexHealth.Security.Infrastructure;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;
using WexHealth.Presentation.Views;

namespace WexHealth.AdminPortal.Pages.Reports
{
    public partial class Index : System.Web.UI.Page, IIndexView
    {
        private readonly IReportIndexPresenter _presenter;

        public Index()
        {
            var factory = ServiceLocator.Current.GetInstance<ReportIndexPresenterFactory>();
            var principal = (WexHealthPrincipal)HttpContext.Current.User;
            _presenter = factory(this, principal);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.InitView(Page.IsPostBack);
        }
    }
}