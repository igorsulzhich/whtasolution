﻿using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.Domain.Entities;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.Security.Infrastructure;

namespace WexHealth.AdminPortal.Pages.Reports.Consumer
{
    public partial class Add_Request : System.Web.UI.Page, IReportConsumerView
    {
        private readonly IReportConsumerPresenter _presenter;

        public IEnumerable<ObjectField> Fields
        {
            set
            {
                fieldsList.DataSource = value;
                fieldsList.DataBind();
            }
        }

        public IEnumerable<ObjectField> SelectedFields
        {
            get
            {
                foreach (var rowSetting in fieldsList.Controls.OfType<RepeaterItem>())
                {
                    var fieldId = rowSetting.FindControl("objectFieldId") as HtmlInputHidden;
                    var checkedField = rowSetting.FindControl("checkedField") as HtmlInputCheckBox;

                    if (checkedField.Checked == true)
                    {
                        yield return new ObjectField
                        {
                            Id = Int32.Parse(fieldId.Value)
                        };
                    }
                }
            }
        }

        public IEnumerable<Domain.Entities.Employer> Employers
        {
            set
            {
                dropdownEmployers.DataSource = value;
                dropdownEmployers.DataTextField = "Name";
                dropdownEmployers.DataValueField = "Id";
                dropdownEmployers.DataBind();
            }
        }

        public Guid SelectedEmployer
        {
            get
            {
                Guid result = Guid.Empty;

                if (Guid.TryParse(dropdownEmployers.SelectedValue, out result))
                {
                    return result;                                   
                }
                else
                {
                    throw new InvalidOperationException("Select employer");
                }
            }
        }

        public Add_Request()
        {
            var factory = ServiceLocator.Current.GetInstance<ReportConsumerPresenterFactory>();
            var principal = (WexHealthPrincipal)HttpContext.Current.User;
            _presenter = factory(this, principal);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.InitView(Page.IsPostBack);
        }

        protected void SendRequest(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                Page.Validate("validationGroupRequestConsumer");

                if (Page.IsValid)
                {
                    OperationResult result = (OperationResult)_presenter.SendRequest();
                    if (result.Status == OperationStatus.Success)
                    {
                        Response.Redirect(Page.ResolveUrl("/reports/consumer"));
                    }
                    else
                    {
                        validatorRequestConsumer.IsValid = false;
                        validatorRequestConsumer.ErrorMessage = result.Message;
                    }
                }
            }
        }
    }
}