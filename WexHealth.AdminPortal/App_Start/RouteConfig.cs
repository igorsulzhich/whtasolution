﻿using System.Web.Routing;

namespace WexHealth.AdminPortal
{
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.Ignore("{resource}.axd/{*pathInfo}");
            routes.Ignore("bundles/{*catch}");
            routes.Ignore("Content/{*catch}");

            routes.MapPageRoute("Default", "", "~/Index.aspx");

            routes.MapPageRoute(null, "{page}/", "~/Pages/{page}/Index.aspx");
            routes.MapPageRoute(null, "reports/{object}", "~/Pages/reports/{object}/Index.aspx");
            routes.MapPageRoute(null, "reports/{object}/{action}", "~/Pages/reports/{object}/{action}.aspx");
            routes.MapPageRoute(null, "{page}/{action}/", "~/Pages/{page}/{action}.aspx");
            routes.MapPageRoute(null, "{page}/{select}/manage/", "~/Pages/{page}/Manage/Index.aspx");
            routes.MapPageRoute(null, "{page}/{select}/manage/{manage-action}", "~/Pages/{page}/Manage/{manage-action}/Index.aspx");
            routes.MapPageRoute(null, "{page}/{select}/{action}/", "~/Pages/{page}/Manage/{action}.aspx");
            routes.MapPageRoute(null, "{page}/{select}/{action}/{id}", "~/Pages/{page}/Manage/{action}.aspx");
        }
    }
}