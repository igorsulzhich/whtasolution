﻿using System;
using System.Web;
using System.Web.Optimization;
using System.Web.Security;
using System.Web.Routing;
using Microsoft.Practices.ServiceLocation;
using Autofac;
using Autofac.Integration.Web;
using Autofac.Extras.CommonServiceLocator;
using log4net;
using log4net.Config;
using WexHealth.Security.Infrastructure;
using WexHealth.Common.Modules;

namespace WexHealth.AdminPortal
{
    public class Global : HttpApplication, IContainerProviderAccessor
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Global));

        static IContainerProvider _containerProvider;

        public IContainerProvider ContainerProvider
        {
            get
            {
                return _containerProvider;
            }
        }

        protected void Application_Start(object sender, EventArgs e)
        {
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            InitializeLog4Net();
            InitializeAutofac();
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            string cookieName = FormsAuthentication.FormsCookieName;
            HttpCookie authCookie = Context.Request.Cookies[cookieName];
            FormsAuthenticationTicket authTicket = null;

            if (null != authCookie)
            {
                try
                {
                    authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                }
                catch (Exception)
                {
                    throw;
                }
            }

            if (null != authTicket)
            {
                FormsIdentity user = new FormsIdentity(authTicket);
                WexHealthIdentity identity = new WexHealthIdentity(user);

                Context.User = new WexHealthPrincipal(identity);
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var exception = Server.GetLastError();
            log.Error("Application error", exception);
        }

        private void InitializeLog4Net()
        {
            XmlConfigurator.Configure();
            log.Info("Application starts");
        }

        private void InitializeAutofac()
        {
            var builder = new ContainerBuilder();

            ModuleLoader.LoadContainer(builder, ".\\bin", "WexHealth.*.dll");

            var container = builder.Build();
            _containerProvider = new ContainerProvider(container);

            var commonServiceLocator = new AutofacServiceLocator(container);
            ServiceLocator.SetLocatorProvider(() => commonServiceLocator);
        }
    }
}