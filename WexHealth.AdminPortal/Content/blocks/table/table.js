﻿; (function ($) {
    $(document).on('click', '.js-table__row', function () {
        document.location = $(this).data('href');
    });
})(jQuery);