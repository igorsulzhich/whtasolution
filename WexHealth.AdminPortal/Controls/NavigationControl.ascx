﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NavigationControl.ascx.cs" Inherits="WexHealth.AdminPortal.Controls.NavigationControl" %>

<nav class="tabs">
    <ul class="tabs__list inlinefix">
        <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/") %>" class="tabs__link <%= IsCurrentPage("Home")  %>">Home</a></li>
        <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/consumer") %>" class="tabs__link <%= IsCurrentPage("Consumer") %>">Consumer</a></li>
        <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/employer") %>" class="tabs__link <%= IsCurrentPage("Employer") %>">Employer</a></li>
        <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/claims") %>" class="tabs__link <%= IsCurrentPage("Claims") %>">Claims</a></li>
        <li class="tabs__item" runat="server" visible='<%# IsSettingAllow("Reports") %>'><a href="<%= Page.ResolveUrl("~/reports") %>" class="tabs__link <%= IsCurrentPage("Reports") %>">Reports</a></li>
        <li class="tabs__item"><a href="<%= Page.ResolveUrl("~/setup") %>" class="tabs__link <%= IsCurrentPage("Setup") %>">Setup</a></li>
    </ul>
</nav>
