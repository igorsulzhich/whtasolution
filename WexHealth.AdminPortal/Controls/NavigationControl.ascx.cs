﻿using System;
using System.Web;
using Microsoft.Practices.ServiceLocation;
using WexHealth.Security.Infrastructure;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;
using WexHealth.Presentation.Views;

namespace WexHealth.AdminPortal.Controls
{
    public partial class NavigationControl : System.Web.UI.UserControl, IIndexView
    {
        private readonly INavigationControlPresenter _presenter;

        public string CurrentPageName { get; set; }

        public NavigationControl()
        {
            var factory = ServiceLocator.Current.GetInstance<NavigationControlPresenterFactory>();
            var principal = (WexHealthPrincipal)HttpContext.Current.User;
            _presenter = factory(this, principal);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.InitView(Page.IsPostBack);

            DataBindChildren();
        }

        public string IsCurrentPage(string itemName)
        {
            return CurrentPageName == itemName ? "tabs__link_active" : string.Empty;
        }

        protected bool IsSettingAllow(string settingKey)
        {
            return _presenter.IsSettingAllow(settingKey);
        }
    }
}