﻿using System;
using System.Web;
using System.Web.UI;
using Microsoft.Practices.ServiceLocation;
using WexHealth.Security.Infrastructure;
using WexHealth.AdminPortal.Presentation.Views;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.AdminPortal.Controls
{
    public partial class EmployerInfoBoxControl : System.Web.UI.UserControl, IEmployerInfoBoxView
    {
        private IEmployerInfoBoxPresenter _presenter;

        public string Name { get; set; }

        public string Code
        {
            get
            {
                if (String.IsNullOrEmpty((string)Page.RouteData.Values["select"]))
                {
                    return null;
                }
                else
                {
                    return (string)Page.RouteData.Values["select"];
                }
            }

            set
            {
                Page.RouteData.Values["select"] = value;
            }
        }

        public string City { get; set; }

        public string Zip { get; set; }

        public string Phone { get; set; }

        public string Logo { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            var factory = ServiceLocator.Current.GetInstance<EmployerInfoBoxPresenterFactory>();
            var principal = (WexHealthPrincipal)HttpContext.Current.User;
            _presenter = factory(this, principal);

            try
            {
                _presenter.InitView(Page.IsPostBack);
            }
            catch (ArgumentNullException)
            {
                Response.Redirect("~/employer");
            }
        }
    }
}