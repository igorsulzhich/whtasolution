﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConsumerInfoBoxControl.ascx.cs" Inherits="WexHealth.AdminPortal.Controls.ConsumerInfoBoxControl" %>

<div class="active clearfix">
    <div class="active__title-wrapper">
        <div class="active__title"><%= FirstName %> <%= LastName %></div>
        <ul class="active__info-list inlinefix">
            <li class="active__info-item"><span class="active__info-label">SSN</span><%= SSN %></li>
            <li class="active__info-item"><span class="active__info-label">Employer</span> <a href="<%= Page.ResolveUrl("~/employer/" + EmployerCode + "/manage") %>" class="link"><%= EmployerName %></a></li>                    
        </ul>
    </div>
    <div class="active__controls">
        <a href="<%= Page.ResolveUrl("~/consumer") %>" class="btn btn_priority_normal"><span class="btn__text">Select a different consumer</span></a>
    </div>
</div>