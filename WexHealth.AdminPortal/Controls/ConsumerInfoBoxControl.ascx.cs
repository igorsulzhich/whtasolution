﻿using System;
using System.Web;
using System.Web.UI;
using Microsoft.Practices.ServiceLocation;
using WexHealth.Security.Infrastructure;
using WexHealth.AdminPortal.Presentation.Presenters.Common.Interfaces;
using WexHealth.AdminPortal.Presentation.Views;


namespace WexHealth.AdminPortal.Controls
{
    public partial class ConsumerInfoBoxControl : System.Web.UI.UserControl, IConsumerInfoBoxView
    {
        private IConsumerInfoBoxPresenter _presenter;

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string EmployerName { get; set; }

        public string EmployerCode { get; set; }

        public string Identity
        {
            get
            {
                if (String.IsNullOrEmpty((string)Page.RouteData.Values["select"]))
                {
                    return null;
                }
                else
                {
                    return (string)Page.RouteData.Values["select"];
                }
            }

            set
            {
                Page.RouteData.Values["select"] = value;
            }
        }

        public string SSN { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            var factory = ServiceLocator.Current.GetInstance<ConsumerInfoBoxPresenterFactory>();
            var principal = (WexHealthPrincipal)HttpContext.Current.User;
            _presenter = factory(this, principal);

            try
            {
                _presenter.InitView(Page.IsPostBack);
            }
            catch (ArgumentNullException)
            {
                Response.Redirect("~/consumer/");
            }
        }
    }
}