﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployerInfoBoxControl.ascx.cs" Inherits="WexHealth.AdminPortal.Controls.EmployerInfoBoxControl" %>

    <div class="active clearfix">
        <%= Logo.Equals(String.Empty) ? "" : String.Format("<img class='active__logo' src='{0}'/>", Logo) %>
        <div class="active__title-wrapper">            
            
            <div class="active__title"><%= Name %></div>
            <ul class="active__info-list inlinefix">            
                <li class="active__info-item"><span class="active__info-label">Code</span> <%= Code %> </li>                                 
                <li class="active__info-item"><span class="active__info-label">City</span> <%= City %></li>
                <li class="active__info-item"><span class="active__info-label">Zip code</span> <%= Zip %></li>
                <li class="active__info-item"><span class="active__info-label">Phone</span> <%= Phone %></li>         
            </ul>
        </div>
        <div class="active__controls">
            <a href="<%= Page.ResolveUrl("~/employer") %>" class="btn btn_priority_normal"><span class="btn__text">Select a different employer</span></a>
        </div>
    </div>