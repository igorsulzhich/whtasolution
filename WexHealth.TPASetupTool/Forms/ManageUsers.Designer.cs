﻿namespace TPASetupTool
{
    partial class ManageUsers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupUser = new System.Windows.Forms.GroupBox();
            this.buttonUpdateUser = new System.Windows.Forms.Button();
            this.buttonGeneratePassword = new System.Windows.Forms.Button();
            this.buttonAddUser = new System.Windows.Forms.Button();
            this.textboxPassword = new System.Windows.Forms.TextBox();
            this.textboxEmail = new System.Windows.Forms.TextBox();
            this.textboxUsername = new System.Windows.Forms.TextBox();
            this.textboxLastname = new System.Windows.Forms.TextBox();
            this.textboxFirstname = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.listUsers = new System.Windows.Forms.ListView();
            this.columnName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnUsername = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnEmail = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.errorProviderManageUsers = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupUser.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderManageUsers)).BeginInit();
            this.SuspendLayout();
            // 
            // groupUser
            // 
            this.groupUser.Controls.Add(this.buttonUpdateUser);
            this.groupUser.Controls.Add(this.buttonGeneratePassword);
            this.groupUser.Controls.Add(this.buttonAddUser);
            this.groupUser.Controls.Add(this.textboxPassword);
            this.groupUser.Controls.Add(this.textboxEmail);
            this.groupUser.Controls.Add(this.textboxUsername);
            this.groupUser.Controls.Add(this.textboxLastname);
            this.groupUser.Controls.Add(this.textboxFirstname);
            this.groupUser.Controls.Add(this.label5);
            this.groupUser.Controls.Add(this.label4);
            this.groupUser.Controls.Add(this.label3);
            this.groupUser.Controls.Add(this.label2);
            this.groupUser.Controls.Add(this.label1);
            this.groupUser.Location = new System.Drawing.Point(27, 22);
            this.groupUser.Margin = new System.Windows.Forms.Padding(3, 3, 3, 30);
            this.groupUser.Name = "groupUser";
            this.groupUser.Size = new System.Drawing.Size(470, 280);
            this.groupUser.TabIndex = 0;
            this.groupUser.TabStop = false;
            this.groupUser.Text = "User information";
            // 
            // buttonUpdateUser
            // 
            this.buttonUpdateUser.Enabled = false;
            this.buttonUpdateUser.Location = new System.Drawing.Point(115, 213);
            this.buttonUpdateUser.Margin = new System.Windows.Forms.Padding(3, 20, 3, 3);
            this.buttonUpdateUser.Name = "buttonUpdateUser";
            this.buttonUpdateUser.Size = new System.Drawing.Size(180, 34);
            this.buttonUpdateUser.TabIndex = 13;
            this.buttonUpdateUser.Text = "Update";
            this.buttonUpdateUser.UseVisualStyleBackColor = true;
            this.buttonUpdateUser.Visible = false;
            this.buttonUpdateUser.Click += new System.EventHandler(this.buttonUpdateUser_Click);
            // 
            // buttonGeneratePassword
            // 
            this.buttonGeneratePassword.Location = new System.Drawing.Point(304, 169);
            this.buttonGeneratePassword.Name = "buttonGeneratePassword";
            this.buttonGeneratePassword.Size = new System.Drawing.Size(75, 23);
            this.buttonGeneratePassword.TabIndex = 12;
            this.buttonGeneratePassword.Text = "Generate";
            this.buttonGeneratePassword.UseVisualStyleBackColor = true;
            this.buttonGeneratePassword.Click += new System.EventHandler(this.buttonGeneratePassword_Click);
            // 
            // buttonAddUser
            // 
            this.buttonAddUser.Location = new System.Drawing.Point(115, 213);
            this.buttonAddUser.Margin = new System.Windows.Forms.Padding(3, 20, 3, 3);
            this.buttonAddUser.Name = "buttonAddUser";
            this.buttonAddUser.Size = new System.Drawing.Size(180, 34);
            this.buttonAddUser.TabIndex = 11;
            this.buttonAddUser.Text = "Add";
            this.buttonAddUser.UseVisualStyleBackColor = true;
            this.buttonAddUser.Click += new System.EventHandler(this.buttonAddUser_Click);
            // 
            // textboxPassword
            // 
            this.textboxPassword.Location = new System.Drawing.Point(115, 170);
            this.textboxPassword.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.textboxPassword.Name = "textboxPassword";
            this.textboxPassword.PasswordChar = '*';
            this.textboxPassword.Size = new System.Drawing.Size(180, 20);
            this.textboxPassword.TabIndex = 9;
            this.textboxPassword.Validating += new System.ComponentModel.CancelEventHandler(this.textboxPassword_Validating);
            // 
            // textboxEmail
            // 
            this.textboxEmail.Location = new System.Drawing.Point(115, 103);
            this.textboxEmail.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.textboxEmail.Name = "textboxEmail";
            this.textboxEmail.Size = new System.Drawing.Size(180, 20);
            this.textboxEmail.TabIndex = 7;
            this.textboxEmail.Validating += new System.ComponentModel.CancelEventHandler(this.textboxEmail_Validating);
            // 
            // textboxUsername
            // 
            this.textboxUsername.Location = new System.Drawing.Point(115, 136);
            this.textboxUsername.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.textboxUsername.Name = "textboxUsername";
            this.textboxUsername.Size = new System.Drawing.Size(180, 20);
            this.textboxUsername.TabIndex = 8;
            this.textboxUsername.Validating += new System.ComponentModel.CancelEventHandler(this.textboxUsername_Validating);
            // 
            // textboxLastname
            // 
            this.textboxLastname.Location = new System.Drawing.Point(115, 70);
            this.textboxLastname.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.textboxLastname.Name = "textboxLastname";
            this.textboxLastname.Size = new System.Drawing.Size(180, 20);
            this.textboxLastname.TabIndex = 6;
            this.textboxLastname.Validating += new System.ComponentModel.CancelEventHandler(this.textboxLastname_Validating);
            // 
            // textboxFirstname
            // 
            this.textboxFirstname.Location = new System.Drawing.Point(115, 37);
            this.textboxFirstname.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.textboxFirstname.Name = "textboxFirstname";
            this.textboxFirstname.Size = new System.Drawing.Size(180, 20);
            this.textboxFirstname.TabIndex = 5;
            this.textboxFirstname.Validating += new System.ComponentModel.CancelEventHandler(this.textboxFirstname_Validating);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 174);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Password";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Username";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Email:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Last name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "First name";
            // 
            // listUsers
            // 
            this.listUsers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnName,
            this.columnUsername,
            this.columnEmail});
            this.listUsers.FullRowSelect = true;
            this.listUsers.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listUsers.HideSelection = false;
            this.listUsers.Location = new System.Drawing.Point(27, 335);
            this.listUsers.MultiSelect = false;
            this.listUsers.Name = "listUsers";
            this.listUsers.Size = new System.Drawing.Size(470, 255);
            this.listUsers.TabIndex = 1;
            this.listUsers.UseCompatibleStateImageBehavior = false;
            this.listUsers.View = System.Windows.Forms.View.Details;
            this.listUsers.ColumnWidthChanging += new System.Windows.Forms.ColumnWidthChangingEventHandler(this.listUsers_ColumnWidthChanging);
            this.listUsers.SelectedIndexChanged += new System.EventHandler(this.listUsers_SelectedIndexChanged);
            // 
            // columnName
            // 
            this.columnName.Text = "Name";
            this.columnName.Width = 150;
            // 
            // columnUsername
            // 
            this.columnUsername.Text = "Username";
            this.columnUsername.Width = 150;
            // 
            // columnEmail
            // 
            this.columnEmail.Text = "Email";
            this.columnEmail.Width = 150;
            // 
            // errorProviderManageUsers
            // 
            this.errorProviderManageUsers.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.errorProviderManageUsers.ContainerControl = this;
            // 
            // ManageUsers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.ClientSize = new System.Drawing.Size(527, 674);
            this.Controls.Add(this.listUsers);
            this.Controls.Add(this.groupUser);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ManageUsers";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Users";
            this.Load += new System.EventHandler(this.ManageUsers_Load);
            this.groupUser.ResumeLayout(false);
            this.groupUser.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderManageUsers)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupUser;
        private System.Windows.Forms.Button buttonAddUser;
        private System.Windows.Forms.TextBox textboxPassword;
        private System.Windows.Forms.TextBox textboxUsername;
        private System.Windows.Forms.TextBox textboxEmail;
        private System.Windows.Forms.TextBox textboxLastname;
        private System.Windows.Forms.TextBox textboxFirstname;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView listUsers;
        private System.Windows.Forms.ColumnHeader columnName;
        private System.Windows.Forms.ColumnHeader columnUsername;
        private System.Windows.Forms.ColumnHeader columnEmail;
        private System.Windows.Forms.Button buttonGeneratePassword;
        private System.Windows.Forms.ErrorProvider errorProviderManageUsers;
        private System.Windows.Forms.Button buttonUpdateUser;
    }
}