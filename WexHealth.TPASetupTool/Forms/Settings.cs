﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.Collections.Generic;
using Microsoft.Practices.ServiceLocation;
using log4net;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Models;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.Presentation.Views;
using WexHealth.TPASetupTool.Presentation.Presenters.Common.Interfaces;

namespace TPASetupTool
{
    public partial class Settings : Form, ISettingView
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Settings));

        private readonly ISettingsPresenter _presenter;

        public IEnumerable<Setting> SettingList
        {
            set
            {
                int x = 0;
                int y = 0;

                foreach (var item in value)
                {
                    var settingRow = new Panel();

                    settingRow.Tag = item.Id;
                    settingRow.Size = new System.Drawing.Size(300, 20);
                    settingRow.Location = new System.Drawing.Point(x, y);

                    var settinglabel = new Label();
                    settinglabel.Text = item.Name;
                    settinglabel.Size = new System.Drawing.Size(100, 17);

                    var settingAllowRadio = new RadioButton();
                    settingAllowRadio.Margin = new Padding(0);
                    settingAllowRadio.Padding = new Padding(0);
                    settingAllowRadio.Text = "Allow";
                    settingAllowRadio.Tag = true;
                    settingAllowRadio.Size = new System.Drawing.Size(50, 17);
                    settingAllowRadio.Location = new System.Drawing.Point(x + 120, 0);

                    var settingPreventRadio = new RadioButton();
                    settingPreventRadio.Text = "Prevent";
                    settingPreventRadio.Tag = false;
                    settingPreventRadio.Size = new System.Drawing.Size(70, 17);
                    settingPreventRadio.Location = new System.Drawing.Point(x + 190, 0);

                    if (item.isAllowed.HasValue)
                    {
                        if (item.isAllowed.Value)
                        {
                            settingAllowRadio.Checked = true;
                        }
                        else if (!item.isAllowed.Value)
                        {
                            settingPreventRadio.Checked = true;
                        }
                    }

                    settingRow.Controls.Add(settinglabel);
                    settingRow.Controls.Add(settingAllowRadio);
                    settingRow.Controls.Add(settingPreventRadio);

                    y += 30;

                    settingsPanel.Controls.Add(settingRow);

                    buttonSave.Location = new System.Drawing.Point(buttonSave.Location.X, y + 15);
                }
            }
        }

        public IEnumerable<SettingInputModel> SelectedSettings
        {
            get
            {
                foreach (var settingRow in settingsPanel.Controls.OfType<Panel>())
                {
                    var checkedButton = settingRow.Controls.OfType<RadioButton>().FirstOrDefault(x => x.Checked);
                    yield return new SettingInputModel
                    {
                        Id = (Guid)settingRow.Tag,
                        isAllowed = (bool)checkedButton.Tag
                    };
                }
            }
        }

        public Settings(Guid currentAdministrator)
        {
            var factory = ServiceLocator.Current.GetInstance<SettingsPresenterFactory>();
            _presenter = factory(this);
            _presenter.AdministratorId = currentAdministrator;

            InitializeComponent();
        }

        private void Settings_Load(object sender, EventArgs e)
        {
            _presenter.InitView(false);
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            OperationResult addResult = (OperationResult)_presenter.SaveSettings();
            if (OperationStatus.Success == addResult.Status)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                ShowErrorMessage(addResult.Message);
            }
        }

        public void ShowErrorMessage(string errorMessage)
        {
            MessageBox.Show(
                    errorMessage,
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                    );
        }
    }
}
