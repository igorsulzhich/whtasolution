﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.Practices.ServiceLocation;
using WexHealth.Domain.Entities;
using WexHealth.TPASetupTool.Presentation.Views;
using WexHealth.TPASetupTool.Presentation.Presenters.Common.Interfaces;

namespace TPASetupTool
{
    public partial class Login : Form, ILoginView
    {
        private readonly ILoginPresenter _presenter;

        public Guid SelectedAdministrator
        {
            get
            {
                return (Guid)administratorsDropdown.SelectedValue;
            }
            set
            {
                administratorsDropdown.SelectedValue = value;
            }
        }

        public IEnumerable<Administrator> Administrators
        {
            set
            {
                var bindingSource = new BindingSource();
                bindingSource.DataSource = value.ToList();

                administratorsDropdown.DataSource = bindingSource;
                administratorsDropdown.DisplayMember = "Name";
                administratorsDropdown.ValueMember = "Id";
                administratorsDropdown.DropDownStyle = ComboBoxStyle.DropDown;
            }
        }

        public Login()
        {
            var factory = ServiceLocator.Current.GetInstance<LoginPresenterFactory>();
            _presenter = factory(this);

            InitializeComponent();
        }

        private void Login_Load(object sender, EventArgs e)
        {
            _presenter.InitView(false);
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void buttonCreate_Click(object sender, EventArgs e)
        {
            Register registerForm = new Register();
            registerForm.ShowDialog(this);

            if (DialogResult.OK == registerForm.DialogResult)
            {
                _presenter.LoadAdministrators();
            }
        }

        public void ShowErrorMessage(string errorMessage)
        {
            MessageBox.Show(
                    errorMessage,
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                    );
        }
    }
}
