﻿using System;
using System.Windows.Forms;
using Microsoft.Practices.ServiceLocation;
using WexHealth.Domain.Entities;
using WexHealth.TPASetupTool.Presentation.Views;
using WexHealth.TPASetupTool.Presentation.Presenters.Common.Interfaces;

namespace TPASetupTool
{
    public partial class Main : Form, IMainView
    {
        private readonly IMainPresenter _presenter;

        private Administrator _currentAdministrator;

        public Administrator CurrentAdministrator
        {
            set
            {
                this.Text = string.Format("{0} \\ {1}", value.Name, value.Alias);
                _currentAdministrator = value;
            }
        }

        public Main(Guid currentAdministrator)
        {
            var factory = ServiceLocator.Current.GetInstance<MainPresenterFactory>();
            _presenter = factory(this);
            _presenter.AdministratorId = currentAdministrator;

            InitializeComponent();

            _presenter.LoadAdministrator();
        }

        private void TPAMain_Load(object sender, EventArgs e)
        {
            _presenter.InitView(false);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void changeAdministratorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void generalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var editForm = new Edit(_currentAdministrator.Id);
            editForm.ShowDialog(this);

            if (editForm.DialogResult == DialogResult.OK)
            {
                _presenter.LoadAdministrator();
            }
        }

        private void manageUsersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var manageUsersForm = new ManageUsers(_currentAdministrator.Id);
            manageUsersForm.ShowDialog();
        }

        private void menuItemSettings_Click(object sender, EventArgs e)
        {
            var settingsForm = new Settings(_currentAdministrator.Id);
            settingsForm.ShowDialog();
        }

        public void ShowErrorMessage(string errorMessage)
        {
            MessageBox.Show(
                    errorMessage,
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                    );
        }
    }
}
