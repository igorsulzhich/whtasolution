﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.Practices.ServiceLocation;
using WexHealth.Domain.Entities;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.TPASetupTool.Presentation.Views;
using WexHealth.TPASetupTool.Presentation.Presenters.Common.Interfaces;
using WexHealth.TPASetupTool.Helpers;

namespace TPASetupTool
{
    public partial class Register : Form, IManageProfileView
    {
        private readonly IManageProfilePresenter _presenter;

        [Required(AllowEmptyStrings = false, ErrorMessage = "Enter name")]
        [StringLength(256, ErrorMessage = "Enter name with length under 256 characters")]
        public string AdministratorName
        {
            get
            {
                return textboxName.Text;
            }
            set
            {
                textboxName.Text = value;
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Enter alias")]
        [StringLength(256, ErrorMessage = "Enter alias with length under 256 characters")]
        public string Alias
        {
            get
            {
                return textboxAlias.Text;
            }
            set
            {
                textboxAlias.Text = value;
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Enter street")]
        [StringLength(256, ErrorMessage = "Enter street with length under 256 characters")]
        public string Street
        {
            get
            {
                return textboxStreet.Text;
            }
            set
            {
                textboxStreet.Text = value;
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Enter city")]
        [StringLength(256, ErrorMessage = "Enter city with length under 256 characters")]
        public string City
        {
            get
            {
                return textboxCity.Text;
            }
            set
            {
                textboxCity.Text = value;
            }
        }

        public IEnumerable<State> States
        {
            set
            {
                dropdownState.DataSource = value.ToList();
                dropdownState.DisplayMember = "Name";
                dropdownState.ValueMember = "Name";
                dropdownState.DropDownStyle = ComboBoxStyle.DropDown;
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Enter state")]
        [StringLength(256, ErrorMessage = "Enter state with length under 256 characters")]
        public string SelectedState
        {
            get
            {
                return (string)dropdownState.SelectedValue;
            }

            set
            {

            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Enter zip code")]
        [StringLength(256, ErrorMessage = "Enter zip code with length under 256 characters")]
        public string ZipCode
        {
            get
            {
                return textboxZipCode.Text;
            }
            set
            {
                textboxZipCode.Text = value;
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Enter phone")]
        [StringLength(256, ErrorMessage = "Enter phone with length under 256 characters")]
        public string Phone
        {
            get
            {
                return textboxPhone.Text;
            }
            set
            {
                textboxPhone.Text = value;
            }
        }

        public Register()
        {
            var factory = ServiceLocator.Current.GetInstance<ManageProfilePresenterFactory>();
            _presenter = factory(this);

            InitializeComponent();
        }

        private void Register_Load(object sender, EventArgs e)
        {
            _presenter.InitView(false);
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (this.ValidateChildren())
            {
                OperationResult saveResult = (OperationResult)_presenter.CreateAdministrator();
                if (OperationStatus.Success == saveResult.Status)
                {
                    DialogResult = DialogResult.OK;
                    this.Close();
                }
                else
                {
                    ShowErrorMessage(saveResult.Message);
                }
            }
        }

        public void ShowErrorMessage(string errorMessage)
        {
            MessageBox.Show(
                    errorMessage,
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                    );
        }

        private void textboxName_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = !ValidateHelper.ValidateField(AdministratorName, "AdministratorName", this, errorProviderRegister, textboxName);
        }

        private void textboxAlias_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = !ValidateHelper.ValidateField(Alias, "Alias", this, errorProviderRegister, textboxAlias);
        }

        private void textboxStreet_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = !ValidateHelper.ValidateField(Street, "Street", this, errorProviderRegister, textboxStreet);
        }

        private void textboxCity_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = !ValidateHelper.ValidateField(City, "City", this, errorProviderRegister, textboxCity);
        }

        private void dropdownState_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = !ValidateHelper.ValidateField(SelectedState, "SelectedState", this, errorProviderRegister, dropdownState);
        }

        private void textboxZipCode_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = !ValidateHelper.ValidateField(ZipCode, "ZipCode", this, errorProviderRegister, textboxZipCode);
        }

        private void textboxPhone_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = !ValidateHelper.ValidateField(Phone, "Phone", this, errorProviderRegister, textboxPhone);
        }
    }
}
