﻿namespace TPASetupTool
{
    partial class Register
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.registerPanel = new System.Windows.Forms.Panel();
            this.buttonSave = new System.Windows.Forms.Button();
            this.labelPhone = new System.Windows.Forms.Label();
            this.dropdownState = new System.Windows.Forms.ComboBox();
            this.labelZipCode = new System.Windows.Forms.Label();
            this.labelState = new System.Windows.Forms.Label();
            this.labelCity = new System.Windows.Forms.Label();
            this.labelStreet = new System.Windows.Forms.Label();
            this.textboxPhone = new System.Windows.Forms.TextBox();
            this.textboxZipCode = new System.Windows.Forms.TextBox();
            this.textboxCity = new System.Windows.Forms.TextBox();
            this.textboxStreet = new System.Windows.Forms.TextBox();
            this.labelAlias = new System.Windows.Forms.Label();
            this.textboxAlias = new System.Windows.Forms.TextBox();
            this.textboxName = new System.Windows.Forms.TextBox();
            this.labelName = new System.Windows.Forms.Label();
            this.errorProviderRegister = new System.Windows.Forms.ErrorProvider(this.components);
            this.registerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderRegister)).BeginInit();
            this.SuspendLayout();
            // 
            // registerPanel
            // 
            this.registerPanel.Controls.Add(this.buttonSave);
            this.registerPanel.Controls.Add(this.labelPhone);
            this.registerPanel.Controls.Add(this.dropdownState);
            this.registerPanel.Controls.Add(this.labelZipCode);
            this.registerPanel.Controls.Add(this.labelState);
            this.registerPanel.Controls.Add(this.labelCity);
            this.registerPanel.Controls.Add(this.labelStreet);
            this.registerPanel.Controls.Add(this.textboxPhone);
            this.registerPanel.Controls.Add(this.textboxZipCode);
            this.registerPanel.Controls.Add(this.textboxCity);
            this.registerPanel.Controls.Add(this.textboxStreet);
            this.registerPanel.Controls.Add(this.labelAlias);
            this.registerPanel.Controls.Add(this.textboxAlias);
            this.registerPanel.Controls.Add(this.textboxName);
            this.registerPanel.Controls.Add(this.labelName);
            this.registerPanel.Location = new System.Drawing.Point(24, 24);
            this.registerPanel.Name = "registerPanel";
            this.registerPanel.Size = new System.Drawing.Size(334, 312);
            this.registerPanel.TabIndex = 4;
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(102, 254);
            this.buttonSave.Margin = new System.Windows.Forms.Padding(3, 20, 3, 3);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(180, 34);
            this.buttonSave.TabIndex = 15;
            this.buttonSave.Text = "Add";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // labelPhone
            // 
            this.labelPhone.AutoSize = true;
            this.labelPhone.Location = new System.Drawing.Point(16, 215);
            this.labelPhone.Name = "labelPhone";
            this.labelPhone.Size = new System.Drawing.Size(38, 13);
            this.labelPhone.TabIndex = 14;
            this.labelPhone.Text = "Phone";
            // 
            // dropdownState
            // 
            this.dropdownState.FormattingEnabled = true;
            this.dropdownState.Location = new System.Drawing.Point(102, 144);
            this.dropdownState.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.dropdownState.Name = "dropdownState";
            this.dropdownState.Size = new System.Drawing.Size(180, 21);
            this.dropdownState.TabIndex = 13;
            this.dropdownState.Validating += new System.ComponentModel.CancelEventHandler(this.dropdownState_Validating);
            // 
            // labelZipCode
            // 
            this.labelZipCode.AutoSize = true;
            this.labelZipCode.Location = new System.Drawing.Point(16, 182);
            this.labelZipCode.Name = "labelZipCode";
            this.labelZipCode.Size = new System.Drawing.Size(50, 13);
            this.labelZipCode.TabIndex = 12;
            this.labelZipCode.Text = "Zip Code";
            // 
            // labelState
            // 
            this.labelState.AutoSize = true;
            this.labelState.Location = new System.Drawing.Point(16, 148);
            this.labelState.Name = "labelState";
            this.labelState.Size = new System.Drawing.Size(32, 13);
            this.labelState.TabIndex = 11;
            this.labelState.Text = "State";
            // 
            // labelCity
            // 
            this.labelCity.AutoSize = true;
            this.labelCity.Location = new System.Drawing.Point(16, 115);
            this.labelCity.Name = "labelCity";
            this.labelCity.Size = new System.Drawing.Size(24, 13);
            this.labelCity.TabIndex = 10;
            this.labelCity.Text = "City";
            // 
            // labelStreet
            // 
            this.labelStreet.AutoSize = true;
            this.labelStreet.Location = new System.Drawing.Point(16, 82);
            this.labelStreet.Name = "labelStreet";
            this.labelStreet.Size = new System.Drawing.Size(35, 13);
            this.labelStreet.TabIndex = 9;
            this.labelStreet.Text = "Street";
            // 
            // textboxPhone
            // 
            this.textboxPhone.Location = new System.Drawing.Point(102, 211);
            this.textboxPhone.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.textboxPhone.Name = "textboxPhone";
            this.textboxPhone.Size = new System.Drawing.Size(180, 20);
            this.textboxPhone.TabIndex = 8;
            this.textboxPhone.Validating += new System.ComponentModel.CancelEventHandler(this.textboxPhone_Validating);
            // 
            // textboxZipCode
            // 
            this.textboxZipCode.Location = new System.Drawing.Point(102, 178);
            this.textboxZipCode.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.textboxZipCode.Name = "textboxZipCode";
            this.textboxZipCode.Size = new System.Drawing.Size(180, 20);
            this.textboxZipCode.TabIndex = 7;
            this.textboxZipCode.Validating += new System.ComponentModel.CancelEventHandler(this.textboxZipCode_Validating);
            // 
            // textboxCity
            // 
            this.textboxCity.Location = new System.Drawing.Point(102, 111);
            this.textboxCity.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.textboxCity.Name = "textboxCity";
            this.textboxCity.Size = new System.Drawing.Size(180, 20);
            this.textboxCity.TabIndex = 5;
            this.textboxCity.Validating += new System.ComponentModel.CancelEventHandler(this.textboxCity_Validating);
            // 
            // textboxStreet
            // 
            this.textboxStreet.Location = new System.Drawing.Point(102, 78);
            this.textboxStreet.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.textboxStreet.Name = "textboxStreet";
            this.textboxStreet.Size = new System.Drawing.Size(180, 20);
            this.textboxStreet.TabIndex = 4;
            this.textboxStreet.Validating += new System.ComponentModel.CancelEventHandler(this.textboxStreet_Validating);
            // 
            // labelAlias
            // 
            this.labelAlias.AutoSize = true;
            this.labelAlias.Location = new System.Drawing.Point(16, 49);
            this.labelAlias.Name = "labelAlias";
            this.labelAlias.Size = new System.Drawing.Size(29, 13);
            this.labelAlias.TabIndex = 3;
            this.labelAlias.Text = "Alias";
            // 
            // textboxAlias
            // 
            this.textboxAlias.Location = new System.Drawing.Point(102, 45);
            this.textboxAlias.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.textboxAlias.Name = "textboxAlias";
            this.textboxAlias.Size = new System.Drawing.Size(180, 20);
            this.textboxAlias.TabIndex = 2;
            this.textboxAlias.Validating += new System.ComponentModel.CancelEventHandler(this.textboxAlias_Validating);
            // 
            // textboxName
            // 
            this.textboxName.Location = new System.Drawing.Point(102, 12);
            this.textboxName.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.textboxName.Name = "textboxName";
            this.textboxName.Size = new System.Drawing.Size(180, 20);
            this.textboxName.TabIndex = 1;
            this.textboxName.Validating += new System.ComponentModel.CancelEventHandler(this.textboxName_Validating);
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(16, 16);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(35, 13);
            this.labelName.TabIndex = 0;
            this.labelName.Text = "Name";
            // 
            // errorProviderRegister
            // 
            this.errorProviderRegister.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.errorProviderRegister.ContainerControl = this;
            // 
            // Register
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.ClientSize = new System.Drawing.Size(396, 371);
            this.Controls.Add(this.registerPanel);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Register";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "New administrator";
            this.Load += new System.EventHandler(this.Register_Load);
            this.registerPanel.ResumeLayout(false);
            this.registerPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderRegister)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel registerPanel;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Label labelPhone;
        private System.Windows.Forms.ComboBox dropdownState;
        private System.Windows.Forms.Label labelZipCode;
        private System.Windows.Forms.Label labelState;
        private System.Windows.Forms.Label labelCity;
        private System.Windows.Forms.Label labelStreet;
        private System.Windows.Forms.TextBox textboxPhone;
        private System.Windows.Forms.TextBox textboxZipCode;
        private System.Windows.Forms.TextBox textboxCity;
        private System.Windows.Forms.TextBox textboxStreet;
        private System.Windows.Forms.Label labelAlias;
        private System.Windows.Forms.TextBox textboxAlias;
        private System.Windows.Forms.TextBox textboxName;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.ErrorProvider errorProviderRegister;
    }
}