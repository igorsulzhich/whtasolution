﻿namespace TPASetupTool
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonLogin = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.administratorsDropdown = new System.Windows.Forms.ComboBox();
            this.buttonCreate = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonLogin
            // 
            this.buttonLogin.Location = new System.Drawing.Point(36, 100);
            this.buttonLogin.Margin = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.buttonLogin.Name = "buttonLogin";
            this.buttonLogin.Size = new System.Drawing.Size(125, 34);
            this.buttonLogin.TabIndex = 0;
            this.buttonLogin.Text = "Update";
            this.buttonLogin.UseVisualStyleBackColor = true;
            this.buttonLogin.Click += new System.EventHandler(this.buttonLogin_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 46);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 0, 3, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(147, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Select administrator to update";
            // 
            // administratorsDropdown
            // 
            this.administratorsDropdown.DropDownHeight = 400;
            this.administratorsDropdown.FormattingEnabled = true;
            this.administratorsDropdown.IntegralHeight = false;
            this.administratorsDropdown.ItemHeight = 13;
            this.administratorsDropdown.Location = new System.Drawing.Point(36, 67);
            this.administratorsDropdown.Margin = new System.Windows.Forms.Padding(0);
            this.administratorsDropdown.Name = "administratorsDropdown";
            this.administratorsDropdown.Size = new System.Drawing.Size(333, 21);
            this.administratorsDropdown.TabIndex = 6;
            // 
            // buttonCreate
            // 
            this.buttonCreate.Location = new System.Drawing.Point(172, 100);
            this.buttonCreate.Margin = new System.Windows.Forms.Padding(0, 20, 0, 0);
            this.buttonCreate.Name = "buttonCreate";
            this.buttonCreate.Size = new System.Drawing.Size(197, 34);
            this.buttonCreate.TabIndex = 7;
            this.buttonCreate.Text = "Create new administrator";
            this.buttonCreate.UseVisualStyleBackColor = true;
            this.buttonCreate.Click += new System.EventHandler(this.buttonCreate_Click);
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(415, 190);
            this.Controls.Add(this.buttonCreate);
            this.Controls.Add(this.administratorsDropdown);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.buttonLogin);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(400, 225);
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.Load += new System.EventHandler(this.Login_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonLogin;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox administratorsDropdown;
        private System.Windows.Forms.Button buttonCreate;
    }
}