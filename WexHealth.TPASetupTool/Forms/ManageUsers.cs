﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.ComponentModel.DataAnnotations;
using Microsoft.Practices.ServiceLocation;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.Presentation.Views;
using WexHealth.Presentation.Presenters.Common.Interfaces;
using WexHealth.TPASetupTool.Helpers;

namespace TPASetupTool
{
    public partial class ManageUsers : Form, IManageUsersView
    {
        private readonly IManageUsersPresenter _presenter;

        [Required(ErrorMessage = "Enter the first name")]
        [StringLength(256, ErrorMessage = "Enter the first name with length under 256 characters")]
        public string FirstName
        {
            get
            {
                return this.textboxFirstname.Text;
            }
            set
            {
                this.textboxFirstname.Text = value;
            }
        }

        [Required(ErrorMessage = "Enter the last name")]
        [StringLength(256, ErrorMessage = "Enter the last name with length under 256 characters")]
        public string LastName
        {
            get
            {
                return this.textboxLastname.Text;
            }
            set
            {
                this.textboxLastname.Text = value;
            }
        }

        [StringLength(256, ErrorMessage = "Enter the email address with length under 256 characters")]
        [EmailAddress(ErrorMessage = "Enter valid email address")]
        public string Email
        {
            get
            {
                return this.textboxEmail.Text;
            }
            set
            {
                this.textboxEmail.Text = value;
            }
        }

        [Required(ErrorMessage = "Enter the user name")]
        [StringLength(256, ErrorMessage = "Enter the user name with length under 256 characters")]
        public string UserName
        {
            get
            {
                return this.textboxUsername.Text;
            }
            set
            {
                this.textboxUsername.Text = value;
            }
        }

        [Required(ErrorMessage = "Enter the password")]
        [MinLength(8, ErrorMessage = "Enter the password with length above 8 characters")]
        public string Password
        {
            get
            {
                return this.textboxPassword.Text;
            }
            set
            {
                textboxPassword.PasswordChar = new char();
                this.textboxPassword.Text = value;
            }
        }

        public Guid SelectedUserId
        {
            get
            {
                Guid result = Guid.Empty;

                if (listUsers.SelectedItems.Count != 0)
                {
                    var selectedItem = listUsers.SelectedItems[0];
                    result = (Guid)selectedItem.Tag;
                }

                return result;
            }
        }

        public IEnumerable<User> Users
        {
            set
            {
                listUsers.Items.Clear();

                foreach (var user in value)
                {
                    string name = null;
                    string email = null;
                    if (null != user.Individual)
                    {
                        name = string.Format("{0} {1}", user.Individual.FirstName, user.Individual.LastName);
                        email = user.Individual.Email;
                    }

                    var item = new ListViewItem(new[] { name, user.UserName, email });
                    item.Tag = user.Id;

                    listUsers.Items.Add(item);
                }
            }
        }

        public ManageUsers(Guid currentAdministrator)
        {
            var serviceFactory = ServiceLocator.Current.GetInstance<AdministratorUserServiceFactory>();
            IDomainUserService userService = serviceFactory(currentAdministrator);

            var presenterFactory = ServiceLocator.Current.GetInstance<ManageUsersPresenterFactory>();
            _presenter = presenterFactory(this, userService);

            InitializeComponent();
        }

        private void ManageUsers_Load(object sender, EventArgs e)
        {
            _presenter.InitView(false);
        }

        private void buttonAddUser_Click(object sender, EventArgs e)
        {
            if (this.ValidateChildren())
            {
                OperationResult result = (OperationResult)_presenter.CreateUser();
                if (OperationStatus.Success == result.Status)
                {
                    ClearTextboxes();

                    errorProviderManageUsers.Clear();
                }
                else
                {
                    ShowErrorMessage(result.Message);
                }
            }
        }

        private void buttonUpdateUser_Click(object sender, EventArgs e)
        {
            if (this.ValidateChildren())
            {
                OperationResult result = (OperationResult)_presenter.UpdateUser();
                if (OperationStatus.Success == result.Status)
                {
                    ClearTextboxes();

                    errorProviderManageUsers.Clear();

                    buttonAddUser.Enabled = true;
                    buttonAddUser.Visible = true;

                    buttonUpdateUser.Enabled = false;
                    buttonUpdateUser.Visible = false;
                }
                else
                {
                    ShowErrorMessage(result.Message);
                }
            }
        }

        private void listUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Guid.Empty != SelectedUserId)
            {
                _presenter.LoadUser();

                buttonAddUser.Enabled = false;
                buttonAddUser.Visible = false;

                buttonUpdateUser.Enabled = true;
                buttonUpdateUser.Visible = true;
            }
            else
            {
                ClearTextboxes();

                buttonAddUser.Enabled = true;
                buttonAddUser.Visible = true;

                buttonUpdateUser.Enabled = false;
                buttonUpdateUser.Visible = false;
            }
        }

        private void buttonGeneratePassword_Click(object sender, EventArgs e)
        {
            _presenter.GeneratePassword();
        }

        private void ClearTextboxes()
        {
            foreach (Control control in groupUser.Controls)
            {
                var textbox = control as TextBox;
                if (textbox != null)
                {
                    textbox.Clear();
                }
            }
        }

        public void ShowErrorMessage(string errorMessage)
        {
            MessageBox.Show(
                    errorMessage,
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                    );
        }

        private void textboxFirstname_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = !ValidateHelper.ValidateField(FirstName, "FirstName", this, errorProviderManageUsers, textboxFirstname);
        }

        private void textboxLastname_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = !ValidateHelper.ValidateField(LastName, "LastName", this, errorProviderManageUsers, textboxLastname);
        }

        private void textboxEmail_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = !ValidateHelper.ValidateField(Email, "Email", this, errorProviderManageUsers, textboxEmail);
        }

        private void textboxUsername_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = !ValidateHelper.ValidateField(UserName, "UserName", this, errorProviderManageUsers, textboxUsername);
        }

        private void textboxPassword_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(Password))
            {
                e.Cancel = !ValidateHelper.ValidateField(Password, "Password", this, errorProviderManageUsers, textboxPassword);
            }
        }

        private void listUsers_ColumnWidthChanging(object sender, ColumnWidthChangingEventArgs e)
        {
            e.Cancel = true;
            e.NewWidth = listUsers.Columns[e.ColumnIndex].Width;
        }
    }
}
