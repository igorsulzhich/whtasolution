﻿using System;
using System.Windows.Forms;
using Autofac;
using Autofac.Extras.CommonServiceLocator;
using Microsoft.Practices.ServiceLocation;
using log4net;
using log4net.Config;
using WexHealth.Common.Modules;

namespace TPASetupTool
{
    static class Program
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));

        private static IContainer _container;

        [STAThread]
        static void Main()
        {
            InitializeLog4Net();
            InitializeAutofac();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            using (_container)
            {
                var loginForm = new Login();
                loginForm.ShowDialog();

                if (DialogResult.OK == loginForm.DialogResult)
                {
                    Application.Run(new Main(loginForm.SelectedAdministrator));
                }
            }
        }

        private static void InitializeLog4Net()
        {
            XmlConfigurator.Configure();
            log.Info("Application starts");
        }

        private static void InitializeAutofac()
        {
            var builder = new ContainerBuilder();

            ModuleLoader.LoadContainer(builder, ".", "WexHealth.*.dll");

            _container = builder.Build();

            var commonServiceLocator = new AutofacServiceLocator(_container);
            ServiceLocator.SetLocatorProvider(() => commonServiceLocator);
        }
    }
}
