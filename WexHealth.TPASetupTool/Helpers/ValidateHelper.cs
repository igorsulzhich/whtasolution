﻿using System.Windows.Forms;
using System.ComponentModel.DataAnnotations;


namespace WexHealth.TPASetupTool.Helpers
{
    public static class ValidateHelper
    {
        /// <summary>
        /// Validate textbox field
        /// </summary>
        /// <param name="value">Value to validate</param>
        /// <param name="memberName">Model property name</param>
        /// <param name="context">Instance of current form</param>
        /// <param name="provider">Intsance of error provider in current form</param>
        /// <param name="binding">Control binding to value</param>
        public static bool ValidateField(object value, string memberName, Form context, ErrorProvider provider, Control binding)
        {
            bool result = false;

            try
            {
                Validator.ValidateProperty(value, new ValidationContext(context, null, null) { MemberName = memberName });
                provider.SetError(binding, "");

                result = true;
            }
            catch (ValidationException exception)
            {
                provider.SetError(binding, exception.Message);
            }

            return result;
        }
    }
}
