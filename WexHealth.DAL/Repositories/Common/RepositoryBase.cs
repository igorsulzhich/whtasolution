﻿using System.Collections.Generic;
using System.Data;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.DAL.Repositories.Common
{
    public abstract class RepositoryBase<TEntity> : IRepositoryBase<TEntity>
        where TEntity : new()
    {
        /// <summary>
        /// Read data records and map them to object entity
        /// </summary>
        /// <param name="command">Database command</param>
        /// <returns>List of entities</returns>
        public IEnumerable<TEntity> ToList(IDbCommand command)
        {
            using (var reader = command.ExecuteReader())
            {
                List<TEntity> items = new List<TEntity>();
                while (reader.Read())
                {
                    var item = new TEntity();
                    Map(reader, item);
                    items.Add(item);
                }
                return items;
            }
        }

        /// <summary>
        /// Map database stored record to object entity
        /// </summary>
        /// <param name="record">Database stored record</param>
        /// <param name="entity">Object entity</param>
        protected abstract void Map(IDataRecord record, TEntity entity);

    }
}
