﻿using System;
using System.Collections.Generic;
using WexHealth.DAL.Models;

namespace WexHealth.DAL.Repositories.Common.Interfaces
{
    public interface IAdministratorRepository
    {
        AdministratorDAL GetAdministratorById(Guid id);

        AdministratorDAL GetAdministratorByName(string name);

        AdministratorDAL GetAdministratorBySecurityDomain(Guid id);

        IEnumerable<AdministratorDAL> GetAdministrators();

        Guid SaveAdministrator(AdministratorDAL administrator);

        bool DeleteAdministrator(Guid id);
    }
}
