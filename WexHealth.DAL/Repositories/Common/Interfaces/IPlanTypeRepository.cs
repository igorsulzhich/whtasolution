﻿using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.DAL.Repositories.Common.Interfaces
{
    public interface IPlanTypeRepository
    {
        IEnumerable<PlanType> GetPlanTypes();

        PlanType GetPlanTypeByName(string name);
    }
}
