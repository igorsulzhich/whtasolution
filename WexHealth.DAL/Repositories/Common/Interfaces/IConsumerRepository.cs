﻿using System;
using System.Collections.Generic;
using WexHealth.DAL.Models;

namespace WexHealth.DAL.Repositories.Common.Interfaces
{
    public interface IConsumerRepository
    {
        ConsumerDAL GetConsumerById(Guid id);

        ConsumerDAL GetConsumerBySSN(string ssn);

        ConsumerDAL GetConsumerByIndividualId(Guid id);

        IEnumerable<ConsumerDAL> GetConsumerByEmployer(Guid employerId);

        IEnumerable<ConsumerDAL> GetConsumersByIndividualData(string firstName, string lastName, string ssn, Guid employerId, Guid administratorId);

        Guid SaveConsumer(ConsumerDAL consumer);

        bool DeleteConsumer(Guid id);
    }
}
