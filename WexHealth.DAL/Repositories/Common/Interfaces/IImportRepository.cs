﻿using WexHealth.Domain.Entities;

namespace WexHealth.DAL.Repositories.Common.Interfaces
{
    public interface IImportRepository
    {
        Import GetById(int id);

        Import GetFirstWithStatus(ImportStatus status);

        int Save(Import entity);
    }
}
