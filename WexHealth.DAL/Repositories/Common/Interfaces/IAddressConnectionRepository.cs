﻿using System;
using System.Collections.Generic;
using WexHealth.DAL.Models;

namespace WexHealth.DAL.Repositories.Common.Interfaces
{
    public interface IAddressConnectionRepository
    {
        AddressConnectionDAL GetAddressConnectionById(Guid id);

        IEnumerable<AddressConnectionDAL> GetAddressConnectionsByObject(Guid objectId, string objectType);

        Guid SaveAddressConnection(AddressConnectionDAL addressConnection);

        bool DeleteAddressConnection(Guid id);
    }
}
