﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.DAL.Repositories.Common.Interfaces
{
    public interface IUserRepository
    {
        User GetUserById(Guid id);

        User GetUserByUserName(string userName);

        User GetUserByIndividualId(Guid id);

        IEnumerable<User> GetUsersByLoginType(string loginType);

        IEnumerable<User> GetUsersBySecurityDomain(Guid id);

        IEnumerable<User> GetUsersBySecurityDomain(Guid id, string loginType);

        void GetPasswordHash(string username, ref string passwordHash, ref string passwordSalt);

        Guid SaveUser(User user);

        bool DeleteUser(Guid id);
    }
}
