﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.DAL.Repositories.Common.Interfaces
{
    public interface IPayrollFrequencyRepository
    {
        IEnumerable<PayrollFrequency> GetPayrollFrequencies();
    }
}
