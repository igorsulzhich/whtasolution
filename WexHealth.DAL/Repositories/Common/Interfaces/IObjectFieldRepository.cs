﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.DAL.Repositories.Common.Interfaces
{
    public interface IObjectFieldRepository
    {
        IEnumerable<ObjectField> GetObjectFields(string objectType);

        ObjectField GetObjectFieldByName(string name);

        ObjectField GetObjectFieldById(int id);
    }
}
