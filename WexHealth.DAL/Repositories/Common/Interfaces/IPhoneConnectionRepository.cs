﻿using System;
using System.Collections.Generic;
using WexHealth.DAL.Models;

namespace WexHealth.DAL.Repositories.Common.Interfaces
{
    public interface IPhoneConnectionRepository
    {
        PhoneConnectionDAL GetPhoneConnectionById(Guid id);

        IEnumerable<PhoneConnectionDAL> GetPhoneConnectionsByObject(Guid objectId, string objectType);

        Guid SavePhoneConnection(PhoneConnectionDAL phoneConnection);

        bool DeletePhoneConnection(Guid id);
    }
}
