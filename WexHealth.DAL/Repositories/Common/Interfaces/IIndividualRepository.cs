﻿using System;
using WexHealth.Domain.Entities;

namespace WexHealth.DAL.Repositories.Common.Interfaces
{
    public interface IIndividualRepository
    {
        Individual GetIndividualById(Guid id);

        Guid SaveIndividual(Individual individual);

        bool DeleteIndividual(Guid id);
    }
}
