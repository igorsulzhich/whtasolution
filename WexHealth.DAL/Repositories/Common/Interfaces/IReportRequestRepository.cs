﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.DAL.Repositories.Common.Interfaces
{
    public interface IReportRequestRepository
    {
        int SaveRequest(ReportRequest request);

        ReportRequest GetRequestFirstStatus(int statusId);

        ReportRequest GetRequestById(int id);

        IEnumerable<ReportRequest> GetRequestByType(string type);

        IEnumerable<ReportRequest> GetRequestByAdministrator(Guid administratorId, string requestType);

        bool DeleteRequest(int id);
    }
}
