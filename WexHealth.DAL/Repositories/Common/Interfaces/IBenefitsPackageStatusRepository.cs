﻿using WexHealth.Domain.Entities;

namespace WexHealth.DAL.Repositories.Common.Interfaces
{
    public interface IBenefitsPackageStatusRepository
    {
        BenefitsPackageStatus GetStatusByName(string name);
    }
}
