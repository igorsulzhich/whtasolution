﻿using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.DAL.Repositories.Common.Interfaces
{
    public interface IContributionRepository
    {
        IEnumerable<Contribution> GetListByEnrollment(int enrollmentId);

        int Save(Contribution entity);
    }
}
