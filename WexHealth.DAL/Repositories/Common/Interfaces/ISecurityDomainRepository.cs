﻿using System;
using System.Collections.Generic;
using WexHealth.DAL.Models;

namespace WexHealth.DAL.Repositories.Common.Interfaces
{
    public interface ISecurityDomainRepository
    {
        SecurityDomainDAL GetSecurityDomainById(Guid id);

        SecurityDomainDAL GetSecurityDomainByAlias(string alias);

        IEnumerable<SecurityDomainDAL> GetSecurityDomains();

        Guid SaveSecurityDomain(SecurityDomainDAL securityDomain);
    }
}
