﻿using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.DAL.Repositories.Common.Interfaces
{
    public interface IClaimStatusRepository
    {
        ClaimStatus GetClaimStatus(string name);

        IEnumerable<ClaimStatus> GetClaimStatuses();
    }
}
