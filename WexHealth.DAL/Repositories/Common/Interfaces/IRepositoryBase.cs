﻿using System.Collections.Generic;
using System.Data;

namespace WexHealth.DAL.Repositories.Common.Interfaces
{
    public interface IRepositoryBase<TEntity>
        where TEntity : new()
    {
        IEnumerable<TEntity> ToList(IDbCommand command);
    }
}
