﻿using System;
using System.Collections.Generic;
using WexHealth.DAL.Models;

namespace WexHealth.DAL.Repositories.Common.Interfaces
{
    public interface ISettingConnectionRepository
    {
        SettingConnectionDAL GetSettingConnectionById(Guid id);

        SettingConnectionDAL GetSettingConnectionByObjectSetting(Guid objectId, string objectType, Guid settingId);

        IEnumerable<SettingConnectionDAL> GetSettingConnectionsByObject(Guid objectId, string objectType);

        Guid SaveSettingConnection(SettingConnectionDAL settingConnection);

        bool DeleteSettingConnection(Guid id);
    }
}
