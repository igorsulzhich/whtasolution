﻿using System;
using WexHealth.DAL.Models;

namespace WexHealth.DAL.Repositories.Common.Interfaces
{
    public interface IAddressRepository
    {
        AddressDAL GetAddressById(Guid id);

        Guid SaveAddress(AddressDAL address);

        bool DeleteAddress(Guid id);
    }
}
