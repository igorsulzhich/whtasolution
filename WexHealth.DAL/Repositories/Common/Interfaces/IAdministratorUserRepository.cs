﻿using System;
using System.Collections.Generic;
using WexHealth.DAL.Models;

namespace WexHealth.DAL.Repositories.Common.Interfaces
{
    public interface IAdministratorUserRepository
    {
        AdministratorUserDAL GetUser(Guid userId);

        AdministratorUserDAL GetUser(Guid userId, Guid? administratorId);

        IEnumerable<AdministratorUserDAL> GetUsers(Guid administratorId);

        Guid SaveUser(AdministratorUserDAL user);
    }
}
