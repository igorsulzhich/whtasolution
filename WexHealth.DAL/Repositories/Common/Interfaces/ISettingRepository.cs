﻿using System;
using System.Collections.Generic;
using WexHealth.DAL.Models;

namespace WexHealth.DAL.Repositories.Common.Interfaces
{
    public interface ISettingRepository
    {
        SettingDAL GetSettingById(Guid id);

        SettingDAL GetSettingByKey(string key);

        Guid SaveSetting(SettingDAL setting);

        IEnumerable<SettingDAL> GetSettingsByParent(Guid parentId);

        bool DeleteSetting(Guid id);
    }
}
