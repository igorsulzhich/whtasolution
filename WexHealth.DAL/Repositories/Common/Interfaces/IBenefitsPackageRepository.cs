﻿
using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.DAL.Repositories.Common.Interfaces
{
    public interface IBenefitsPackageRepository
    {
        IEnumerable<BenefitsPackage> GetPackages(Guid employerId);

        BenefitsPackage GetPackageById(int id);

        int Initialize(int id, int statusId, DateTime dateOfInitialized);

        int SavePackage(BenefitsPackage package);

        bool DeletePackage(int packageId);
    }
}
