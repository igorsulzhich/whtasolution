﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.DAL.Repositories.Common.Interfaces
{
    public interface IBenefitsOfferingRepository
    {
        BenefitsOffering GetBenefitsOfferingById(int id);

        IEnumerable<BenefitsOffering> GetBenefitsOfferingByEmployer(Guid employerId);

        IEnumerable<BenefitsOffering> GetBenefitsOfferingByBenefitsPackageId(int benefitsPackageId);

        int SaveBenefitsOffering(BenefitsOffering benefitsOffering);
    }
}
