﻿using System;
using WexHealth.DAL.Models;

namespace WexHealth.DAL.Repositories.Common.Interfaces
{
    public interface IPhoneRepository
    {
        PhoneDAL GetPhoneById(Guid id);

        PhoneDAL GetPhoneByNumber(string phoneNumber);

        Guid SavePhone(PhoneDAL phone);

        bool DeletePhone(Guid id);
    }
}
