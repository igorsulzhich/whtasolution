﻿using System;
using System.Collections.Generic;
using WexHealth.DAL.Models;

namespace WexHealth.DAL.Repositories.Common.Interfaces
{
    public interface IEmployerUserRepository
    {
        EmployerUserDAL GetUser(Guid userId);

        EmployerUserDAL GetUser(Guid userId, Guid? employerId);

        IEnumerable<EmployerUserDAL> GetUsers(Guid employerId);

        Guid SaveUser(EmployerUserDAL user);
    }
}
