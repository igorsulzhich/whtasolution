﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.DAL.Repositories.Common.Interfaces
{
    public interface IRoleRepository
    {
        IEnumerable<Role> GetAllRoles();

        IEnumerable<Role> GetRolesForUser(string username);

        bool IsInRole(string username, string roleName);

        Guid SaveRole(string roleName);

        void AddUsersToRoles(string[] usernames, string[] rolenames);

        bool RoleExists(string roleName);
    }
}
