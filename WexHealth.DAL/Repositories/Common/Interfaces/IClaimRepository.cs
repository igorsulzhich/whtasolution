﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.DAL.Repositories.Common.Interfaces
{
    public interface IClaimRepository
    {
        Claim GetClaim(string id);

        IEnumerable<Claim> GetClaimsByAdministrator(Guid administratorId);

        IEnumerable<Claim> GetClaimsByConsumer(Guid consumerId);

        IEnumerable<Claim> SearchClaims(string number, string status, Guid consumerId, Guid employerId, Guid administratorId);

        string SaveClaim(Claim claim);
    }
}
