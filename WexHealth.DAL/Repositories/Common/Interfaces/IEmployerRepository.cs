﻿using System;
using System.Collections.Generic;
using WexHealth.DAL.Models;

namespace WexHealth.DAL.Repositories.Common.Interfaces
{
    public interface IEmployerRepository
    {
        EmployerDAL GetEmployerById(Guid id);

        EmployerDAL GetEmployerByName(string name);

        EmployerDAL GetEmployerByCode(string code);

        IEnumerable<EmployerDAL> GetEmployersByNameAndCode(string name, string code, Guid adminId);

        IEnumerable<EmployerDAL> GetEmployersByAdministrator(Guid id);

        Guid SaveEmployer(EmployerDAL employer);

        bool DeleteEmployer(Guid id);
    }
}
