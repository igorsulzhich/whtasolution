﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.DAL.Repositories.Common.Interfaces
{
    public interface IEnrollmentRepository
    {
        Enrollment GetEnrollment(int id);

        Enrollment GetEnrollment(int benefitOfferingId, Guid consumerId);

        IEnumerable<Enrollment> GetEnrollments(Guid consumerId);

        IEnumerable<Enrollment> GetEnrollmentsByBenefitsPackageId(int packageId);

        IEnumerable<Enrollment> GetEnrollmentsByPackage(string status, DateTime dateBetween);

        IEnumerable<Enrollment> GetActiveEnrollments(Guid consumerId, string benefitStatusName);

        int SaveEnrollment(Enrollment enrollment);
    }
}
