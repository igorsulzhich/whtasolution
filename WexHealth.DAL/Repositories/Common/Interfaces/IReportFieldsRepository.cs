﻿using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.DAL.Repositories.Common.Interfaces
{
    public interface IReportFieldsRepository
    {
        int SaveReportFields(ReportField objectField);

        IEnumerable<ReportField> GetReportFieldByRequestId(int requestId);

        bool DeleteReportFieldByRequest(int id);
    }
}
