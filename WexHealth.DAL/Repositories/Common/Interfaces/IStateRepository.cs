﻿using System;
using System.Collections.Generic;
using WexHealth.DAL.Models;

namespace WexHealth.DAL.Repositories.Common.Interfaces
{
    public interface IStateRepository
    {
        StateDAL GetStateById(Guid id);

        StateDAL GetStateByName(string name);

        IEnumerable<StateDAL> GetStates();

        Guid SaveState(StateDAL state);

        bool DeleteState(Guid id);
    }
}
