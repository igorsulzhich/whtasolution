﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WexHealth.Domain.Entities;

namespace WexHealth.DAL.Repositories.Common.Interfaces
{
    public interface IReportRequestStatusRepository
    {
        ReportRequestStatus GetStatusByName(string name);
    }
}
