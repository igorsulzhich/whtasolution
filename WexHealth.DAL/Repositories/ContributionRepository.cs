﻿using System;
using System.Collections.Generic;
using System.Data;
using log4net;
using WexHealth.DAL.Common;
using WexHealth.DAL.Repositories.Common;
using WexHealth.DAL.Repositories.Common.Interfaces;
using WexHealth.Domain.Entities;

namespace WexHealth.DAL.Repositories
{
    public class ContributionRepository : RepositoryBase<Contribution>, IContributionRepository
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ContributionRepository));

        private DbContextBase _context = new DefaultDbContext();

        public IEnumerable<Contribution> GetListByEnrollment(int enrollmentId)
        {
            IEnumerable<Contribution> result = null;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Contribution_GetListByEnrollment";
                    command.CommandType = CommandType.StoredProcedure;

                    var administratorIdParam = command.CreateParameter();
                    administratorIdParam.ParameterName = "@enrollmentId";
                    administratorIdParam.Value = enrollmentId;
                    administratorIdParam.DbType = DbType.Int32;
                    administratorIdParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(administratorIdParam);
                    connection.Open();

                    result = ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public int Save(Contribution entity)
        {
            int result = 0;

            if (null == entity) throw new ArgumentNullException(nameof(entity));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    if (entity.Id == 0)
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "Contribution_Create";

                        var idParam = command.CreateParameter();
                        idParam.ParameterName = "@id";
                        idParam.DbType = DbType.Int32;
                        idParam.Direction = ParameterDirection.Output;

                        var amountParam = command.CreateParameter();
                        amountParam.ParameterName = "@amount";
                        amountParam.DbType = DbType.Decimal;
                        amountParam.Value = entity.Amount;
                        amountParam.Direction = ParameterDirection.Input;

                        var dateOfContributeParam = command.CreateParameter();
                        dateOfContributeParam.ParameterName = "@dateOfContribute";
                        dateOfContributeParam.DbType = DbType.DateTime2;
                        dateOfContributeParam.Size = 7;
                        dateOfContributeParam.Value = entity.DateOfContribute;
                        dateOfContributeParam.Direction = ParameterDirection.Input;

                        var enrollmentId = command.CreateParameter();
                        enrollmentId.ParameterName = "@enrollmentId";
                        enrollmentId.DbType = DbType.Int32;
                        enrollmentId.Value = entity.EnrollmentId;
                        enrollmentId.Direction = ParameterDirection.Input;

                        command.Parameters.Add(idParam);
                        command.Parameters.Add(amountParam);
                        command.Parameters.Add(dateOfContributeParam);
                        command.Parameters.Add(enrollmentId);

                        connection.Open();
                        command.ExecuteNonQuery();

                        result = (int)idParam.Value;
                    }
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        protected override void Map(IDataRecord record, Contribution entity)
        {
            if (null == record) throw new ArgumentNullException(nameof(record));

            if (null == entity) throw new ArgumentNullException(nameof(entity));

            entity.Id = (int)record["Id"];
            entity.Amount = Convert.ToDecimal(record["Amount"]);
            entity.DateOfContribute = (DateTime)record["DateOfContribute"];
            entity.EnrollmentId = (int)record["EnrollmentId"];
        }
    }
}
