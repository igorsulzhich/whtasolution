﻿using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using log4net;
using WexHealth.Domain.Entities;
using WexHealth.DAL.Common;
using WexHealth.DAL.Repositories.Common;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.DAL.Repositories
{
    public class ObjectFieldRepository : RepositoryBase<ObjectField>, IObjectFieldRepository
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ObjectFieldRepository));

        private DbContextBase _context = new DefaultDbContext();

        public IEnumerable<ObjectField> GetObjectFields(string objectType)
        {
            if (string.IsNullOrEmpty(objectType)) throw new ArgumentNullException(nameof(objectType));

            IEnumerable<ObjectField> result = null;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "ObjectField_GetObjectFields";
                    command.CommandType = CommandType.StoredProcedure;

                    var objectTypeParam = command.CreateParameter();
                    objectTypeParam.ParameterName = "@objectType";
                    objectTypeParam.DbType = DbType.String;
                    objectTypeParam.Value = objectType;
                    objectTypeParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(objectTypeParam);

                    connection.Open();

                    result = ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public ObjectField GetObjectFieldByName(string name)
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));

            ObjectField result = null;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "ObjectField_GetObjectFieldByName";
                    command.CommandType = CommandType.StoredProcedure;

                    var nameParam = command.CreateParameter();
                    nameParam.ParameterName = "@name";
                    nameParam.DbType = DbType.String;
                    nameParam.Value = name;
                    nameParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(nameParam);

                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public ObjectField GetObjectFieldById(int id)
        {
            if (id == 0) throw new ArgumentNullException(nameof(id));

            ObjectField result = null;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "ObjectField_GetObjectFieldByName";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.DbType = DbType.Int32;
                    idParam.Value = id;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);

                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        protected override void Map(IDataRecord record, ObjectField entity)
        {
            entity.Id = (int)record["Id"];
            entity.Name = (string)record["Name"];
            entity.ObjectType = (string)record["ObjectType"];
        }
    }
}
