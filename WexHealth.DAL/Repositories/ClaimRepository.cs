﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Collections.Generic;
using log4net;
using WexHealth.DAL.Common;
using WexHealth.DAL.Repositories.Common;
using WexHealth.DAL.Repositories.Common.Interfaces;
using WexHealth.Domain.Entities;

namespace WexHealth.DAL.Repositories
{
    public class ClaimRepository : RepositoryBase<Claim>, IClaimRepository
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ClaimRepository));

        private DbContextBase _context = new DefaultDbContext();

        public Claim GetClaim(string id)
        {
            Claim result = null;

            if (string.Empty.Equals(id)) throw new ArgumentOutOfRangeException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Claim_GetClaim";
                    command.CommandType = CommandType.StoredProcedure;

                    var administratorIdParam = command.CreateParameter();
                    administratorIdParam.ParameterName = "@id";
                    administratorIdParam.Value = id;
                    administratorIdParam.DbType = DbType.String;
                    administratorIdParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(administratorIdParam);
                    connection.Open();

                    result = ToList(command).SingleOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public IEnumerable<Claim> GetClaimsByAdministrator(Guid administratorId)
        {
            IEnumerable<Claim> result = null;

            if (Guid.Empty.Equals(administratorId)) throw new ArgumentOutOfRangeException(nameof(administratorId));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Claim_GetClaimsByAdministrator";
                    command.CommandType = CommandType.StoredProcedure;

                    var administratorIdParam = command.CreateParameter();
                    administratorIdParam.ParameterName = "@administratorId";
                    administratorIdParam.Value = administratorId;
                    administratorIdParam.DbType = DbType.Guid;
                    administratorIdParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(administratorIdParam);
                    connection.Open();

                    result = ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public IEnumerable<Claim> GetClaimsByConsumer(Guid consumerId)
        {
            IEnumerable<Claim> result = null;

            if (Guid.Empty.Equals(consumerId)) throw new ArgumentOutOfRangeException(nameof(consumerId));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Claim_GetClaimsByConsumer";
                    command.CommandType = CommandType.StoredProcedure;

                    var consumerIdParam = command.CreateParameter();
                    consumerIdParam.ParameterName = "@consumerId";
                    consumerIdParam.Value = consumerId;
                    consumerIdParam.DbType = DbType.Guid;
                    consumerIdParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(consumerIdParam);
                    connection.Open();

                    result = ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public IEnumerable<Claim> SearchClaims(string number, string status, Guid consumerId, Guid employerId, Guid administratorId)
        {
            IEnumerable<Claim> result = null;

            if (Guid.Empty.Equals(administratorId)) throw new ArgumentOutOfRangeException(nameof(administratorId));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Claim_SearchClaims";
                    command.CommandType = CommandType.StoredProcedure;

                    var administratorIdParam = command.CreateParameter();
                    administratorIdParam.ParameterName = "@administratorId";
                    administratorIdParam.Value = administratorId;
                    administratorIdParam.DbType = DbType.Guid;
                    administratorIdParam.Direction = ParameterDirection.Input;

                    if (!string.IsNullOrEmpty(number))
                    {
                        var numberParam = command.CreateParameter();
                        numberParam.ParameterName = "@number";
                        numberParam.Value = number;
                        numberParam.DbType = DbType.String;
                        numberParam.Size = 256;
                        numberParam.Direction = ParameterDirection.Input;

                        command.Parameters.Add(numberParam);
                    }

                    if (!string.IsNullOrEmpty(status))
                    {
                        var statusParam = command.CreateParameter();
                        statusParam.ParameterName = "@status";
                        statusParam.Value = status;
                        statusParam.DbType = DbType.String;
                        statusParam.Size = 256;
                        statusParam.Direction = ParameterDirection.Input;

                        command.Parameters.Add(statusParam);
                    }

                    if (!Guid.Empty.Equals(consumerId))
                    {
                        var consumerIdParam = command.CreateParameter();
                        consumerIdParam.ParameterName = "@consumerId";
                        consumerIdParam.Value = consumerId;
                        consumerIdParam.DbType = DbType.Guid;
                        consumerIdParam.Direction = ParameterDirection.Input;

                        command.Parameters.Add(consumerIdParam);
                    }

                    if (!Guid.Empty.Equals(employerId))
                    {
                        var employerIdParam = command.CreateParameter();
                        employerIdParam.ParameterName = "@employerId";
                        employerIdParam.Value = employerId;
                        employerIdParam.DbType = DbType.Guid;
                        employerIdParam.Direction = ParameterDirection.Input;

                        command.Parameters.Add(employerIdParam);
                    }

                    command.Parameters.Add(administratorIdParam);
                    connection.Open();

                    result = ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public string SaveClaim(Claim claim)
        {
            string result;

            if (null == claim) throw new ArgumentNullException(nameof(claim));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.DbType = DbType.String;
                    idParam.Size = 256;

                    if (string.IsNullOrEmpty(claim.Id))
                    {
                        command.CommandText = "Claim_CreateClaim";

                        idParam.Direction = ParameterDirection.Output;

                        var receiptParam = (SqlParameter)command.CreateParameter();
                        receiptParam.ParameterName = "@receipt";
                        receiptParam.Value = claim.File;
                        receiptParam.SqlDbType = SqlDbType.Image;
                        receiptParam.Direction = ParameterDirection.Input;

                        command.Parameters.Add(receiptParam);
                    }
                    else
                    {
                        command.CommandText = "Claim_UpdateClaim";

                        idParam.Value = claim.Id;
                        idParam.Direction = ParameterDirection.Input;
                    }

                    var dateOfServiceParam = command.CreateParameter();
                    dateOfServiceParam.ParameterName = "@dateOfService";
                    dateOfServiceParam.DbType = DbType.DateTime2;
                    dateOfServiceParam.Size = 7;
                    dateOfServiceParam.Value = claim.DateOfService;
                    dateOfServiceParam.Direction = ParameterDirection.Input;

                    var amountParam = command.CreateParameter();
                    amountParam.ParameterName = "@amount";
                    amountParam.DbType = DbType.Decimal;
                    amountParam.Value = claim.Amount;
                    amountParam.Direction = ParameterDirection.Input;

                    var enrollmentIdParam = command.CreateParameter();
                    enrollmentIdParam.ParameterName = "@enrollmentId";
                    enrollmentIdParam.DbType = DbType.Int32;
                    enrollmentIdParam.Value = claim.EnrollmentId;
                    enrollmentIdParam.Direction = ParameterDirection.Input;

                    var statusIdParam = command.CreateParameter();
                    statusIdParam.ParameterName = "@statusId";
                    statusIdParam.DbType = DbType.Int32;
                    statusIdParam.Value = claim.ClaimStatusId;
                    statusIdParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    command.Parameters.Add(dateOfServiceParam);
                    command.Parameters.Add(amountParam);
                    command.Parameters.Add(enrollmentIdParam);
                    command.Parameters.Add(statusIdParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    result = idParam.Value.ToString();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        protected override void Map(IDataRecord record, Claim entity)
        {
            if (null == record) throw new ArgumentNullException(nameof(record));

            if (null == entity) throw new ArgumentNullException(nameof(entity));

            entity.Id = (string)record["Id"];
            entity.DateOfService = (DateTime)record["DateOfService"];
            entity.Amount = Convert.ToDecimal(record["Amount"]);

            if (!DBNull.Value.Equals(record["Receipt"]))
            {
                entity.File = (byte[])record["Receipt"];
            }

            if (!DBNull.Value.Equals(record["StatusId"]))
            {
                entity.ClaimStatusId = (int)record["StatusId"];
                entity.Status = new ClaimStatus
                {
                    Id = (int)record["StatusId"],
                    Name = (string)record["StatusName"]
                };
            }

            if (!DBNull.Value.Equals(record["EnrollmentId"]))
            {
                entity.EnrollmentId = (int)record["EnrollmentId"];
                entity.Enrollment = new Enrollment
                {
                    Id = (int)record["EnrollmentId"],
                    ElectionAmount = Convert.ToDecimal(record["ElectionAmount"])
                };

                if (!DBNull.Value.Equals(record["ConsumerId"]))
                {
                    entity.Enrollment.ConsumerId = (Guid)record["ConsumerId"];
                    entity.Enrollment.Consumer = new Consumer
                    {
                        Id = (Guid)record["ConsumerId"],
                        Individual = new Individual
                        {
                            FirstName = (string)record["Firstname"],
                            LastName = (string)record["Lastname"]
                        },
                        Employer = new Employer
                        {
                            Id = (Guid)record["EmployerId"],
                            Name = (string)record["EmployerName"],
                            Code = (string)record["EmployerCode"]
                        }
                    };
                }

                if (!DBNull.Value.Equals(record["PlanId"]))
                {
                    entity.Enrollment.BenefitsOfferingId = (int)record["PlanId"];
                    entity.Enrollment.BenefitsOffering = new BenefitsOffering
                    {
                        Id = (int)record["PlanId"],
                        Name = (string)record["PlanName"],
                        ContributionAmount = (decimal)record["Contribution"],
                        BenefitsPackageId = (int)record["PackageId"]
                    };

                    if (!DBNull.Value.Equals((int)record["PlanTypeId"]))
                    {
                        entity.Enrollment.BenefitsOffering.PlanTypeId = (int)record["PlanTypeId"];
                        entity.Enrollment.BenefitsOffering.PlanType = new PlanType
                        {
                            Id = (int)record["PlanTypeId"],
                            Name = (string)record["PlanTypeName"]
                        };
                    }
                }
            }
        }
    }
}
