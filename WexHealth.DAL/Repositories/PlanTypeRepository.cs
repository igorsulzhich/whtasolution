﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using log4net;
using WexHealth.Domain.Entities;
using WexHealth.DAL.Common;
using WexHealth.DAL.Repositories.Common;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.DAL.Repositories
{
    public class PlanTypeRepository : RepositoryBase<PlanType>, IPlanTypeRepository
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(PlanTypeRepository));

        private DbContextBase _context = new DefaultDbContext();

        public IEnumerable<PlanType> GetPlanTypes()
        {
            IEnumerable<PlanType> result = null;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "PlanType_GetPlanTypes";
                    command.CommandType = CommandType.StoredProcedure;

                    connection.Open();

                    result = ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public PlanType GetPlanTypeByName(string name)
        {
            PlanType result = null;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "PlanType_GetPlanTypeByName";
                    command.CommandType = CommandType.StoredProcedure;

                    var nameParam = command.CreateParameter();
                    nameParam.ParameterName = "@name";
                    nameParam.Value = name;
                    nameParam.DbType = DbType.String;
                    nameParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(nameParam);
                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        protected override void Map(IDataRecord record, PlanType entity)
        {
            entity.Id = (int)record["Id"];
            entity.Name = (string)record["Name"];
        }
    }
}
