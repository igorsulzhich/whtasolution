﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using PostSharp.Patterns.Contracts;
using log4net;
using WexHealth.DAL.Common;
using WexHealth.DAL.Models;
using WexHealth.DAL.Repositories.Common;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.DAL.Repositories
{
    public class PhoneConnectionRepository : RepositoryBase<PhoneConnectionDAL>, IPhoneConnectionRepository
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(PhoneConnectionRepository));

        private DbContextBase _context = new DefaultDbContext();

        public PhoneConnectionDAL GetPhoneConnectionById(Guid id)
        {
            PhoneConnectionDAL result = null;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "PhoneConnection_GetPhoneConnectionById";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = id;
                    idParam.DbType = DbType.Guid;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public IEnumerable<PhoneConnectionDAL> GetPhoneConnectionsByObject(Guid objectId, [Required] string objectType)
        {
            IEnumerable<PhoneConnectionDAL> result = null;

            if (Guid.Empty.Equals(objectId)) throw new ArgumentNullException(nameof(objectId));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "PhoneConnection_GetPhoneByObject";
                    command.CommandType = CommandType.StoredProcedure;

                    var objectIdParam = command.CreateParameter();
                    objectIdParam.ParameterName = "@objectId";
                    objectIdParam.Value = objectId;
                    objectIdParam.DbType = DbType.Guid;
                    objectIdParam.Direction = ParameterDirection.Input;

                    var objectTypeParam = command.CreateParameter();
                    objectTypeParam.ParameterName = "@objectType";
                    objectTypeParam.Value = objectType;
                    objectTypeParam.DbType = DbType.String;
                    objectTypeParam.Size = 256;
                    objectTypeParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(objectIdParam);
                    command.Parameters.Add(objectTypeParam);

                    connection.Open();

                    result = ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public Guid SavePhoneConnection([NotNull] PhoneConnectionDAL phoneConnection)
        {
            Guid result = Guid.Empty;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = phoneConnection.Id;
                    idParam.DbType = DbType.Guid;
                    idParam.Direction = ParameterDirection.Input;

                    if (Guid.Empty.Equals(phoneConnection.Id))
                    {
                        command.CommandText = "PhoneConnection_CreatePhoneConnection";

                        idParam.Value = Guid.Empty;
                        idParam.Direction = ParameterDirection.Output;
                    }
                    else
                    {
                        command.CommandText = "PhoneConnection_UpdatePhoneConnection";

                        idParam.Value = phoneConnection.Id;
                        idParam.Direction = ParameterDirection.Input;
                    }

                    var objectIdParam = command.CreateParameter();
                    objectIdParam.ParameterName = "@objectId";
                    objectIdParam.Value = phoneConnection.ObjectId;
                    objectIdParam.DbType = DbType.Guid;
                    objectIdParam.Direction = ParameterDirection.Input;

                    var objectTypeParam = command.CreateParameter();
                    objectTypeParam.ParameterName = "@objectType";
                    objectTypeParam.Value = phoneConnection.ObjectType;
                    objectTypeParam.DbType = DbType.String;
                    objectTypeParam.Size = 256;
                    objectTypeParam.Direction = ParameterDirection.Input;

                    var typeParam = command.CreateParameter();
                    typeParam.ParameterName = "@type";
                    typeParam.Value = phoneConnection.Type;
                    typeParam.DbType = DbType.String;
                    typeParam.Size = 256;
                    typeParam.Direction = ParameterDirection.Input;

                    var phoneIdParam = command.CreateParameter();
                    phoneIdParam.ParameterName = "@phoneId";
                    phoneIdParam.Value = phoneConnection.PhoneId;
                    phoneIdParam.DbType = DbType.Guid;
                    phoneIdParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    command.Parameters.Add(objectIdParam);
                    command.Parameters.Add(objectTypeParam);
                    command.Parameters.Add(typeParam);
                    command.Parameters.Add(phoneIdParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    result = (Guid)idParam.Value;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public bool DeletePhoneConnection(Guid id)
        {
            bool result = false;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "PhoneConnection_DeletePhoneConnection";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = id;
                    idParam.DbType = DbType.Guid;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    result = true;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        protected override void Map([NotNull] IDataRecord record, [NotNull] PhoneConnectionDAL entity)
        {
            entity.ObjectId = (Guid)record["ObjectId"];
            entity.ObjectType = (string)record["ObjectType"];

            if (!DBNull.Value.Equals(record["PhoneId"]))
            {
                entity.PhoneId = (Guid)record["PhoneId"];
                entity.Phone = new PhoneDAL
                {
                    Id = (Guid)record["PhoneId"],
                    PhoneNumber = (string)record["PhoneNumber"]
                };
            }
        }
    }
}
