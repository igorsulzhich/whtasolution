﻿using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using log4net;
using WexHealth.DAL.Common;
using WexHealth.DAL.Repositories.Common;
using WexHealth.DAL.Repositories.Common.Interfaces;
using WexHealth.Domain.Entities;


namespace WexHealth.DAL.Repositories
{
    public class BenefitsOfferingRepository : RepositoryBase<BenefitsOffering>, IBenefitsOfferingRepository
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(BenefitsOfferingRepository));

        private DbContextBase _context = new DefaultDbContext();

        public IEnumerable<BenefitsOffering> GetBenefitsOfferingByEmployer(Guid employerId)
        {
            IEnumerable<BenefitsOffering> result = null;

            if (Guid.Empty.Equals(employerId)) throw new ArgumentNullException(nameof(employerId));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "BenefitsOffering_GetBenefitsOfferingsByEmployer";
                    command.CommandType = CommandType.StoredProcedure;

                    var employerIdParam = command.CreateParameter();
                    employerIdParam.ParameterName = "@employerId";
                    employerIdParam.Value = employerId;
                    employerIdParam.DbType = DbType.Guid;
                    employerIdParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(employerIdParam);
                    connection.Open();

                    result = ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public IEnumerable<BenefitsOffering> GetBenefitsOfferingByBenefitsPackageId(int benefitsPackageId)
        {
            IEnumerable<BenefitsOffering> result = null;

            if (benefitsPackageId == 0) throw new ArgumentNullException(nameof(benefitsPackageId));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "BenefitsOffering_GetBenefitsOfferingsByBenefitsPackageId";
                    command.CommandType = CommandType.StoredProcedure;

                    var benefitsPackageIdParam = command.CreateParameter();
                    benefitsPackageIdParam.ParameterName = "@benefitsPackageId";
                    benefitsPackageIdParam.Value = benefitsPackageId;
                    benefitsPackageIdParam.DbType = DbType.Int32;
                    benefitsPackageIdParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(benefitsPackageIdParam);

                    connection.Open();

                    result = ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public BenefitsOffering GetBenefitsOfferingById(int id)
        {
            BenefitsOffering result = null;

            if (id == 0) throw new ArgumentNullException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "BenefitsOffering_GetBenefitsOfferingsById";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = id;
                    idParam.DbType = DbType.Int32;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);

                    connection.Open();

                    result = ToList(command).SingleOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public int SaveBenefitsOffering(BenefitsOffering benefitsOffering)
        {
            int result;

            if (null == benefitsOffering) throw new ArgumentNullException(nameof(benefitsOffering));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.DbType = DbType.Int32;

                    if (benefitsOffering.Id == 0)
                    {
                        command.CommandText = "BenefitsOffering_CreateBenefitsOffering";

                        idParam.Value = 0;
                        idParam.Direction = ParameterDirection.Output;
                    }
                    else
                    {
                        command.CommandText = "BenefitsOffering_UpdateBenefitsOffering";

                        idParam.Value = benefitsOffering.Id;
                        idParam.Direction = ParameterDirection.Input;
                    }

                    if (benefitsOffering.BenefitsPackageId != 0)
                    {
                        var benefitsPackageIdParam = command.CreateParameter();
                        benefitsPackageIdParam.ParameterName = "@benefitsPackageId";
                        benefitsPackageIdParam.DbType = DbType.Int32;
                        benefitsPackageIdParam.Value = benefitsOffering.BenefitsPackageId;
                        benefitsPackageIdParam.Direction = ParameterDirection.Input;

                        command.Parameters.Add(benefitsPackageIdParam);
                    }

                    var nameParam = command.CreateParameter();
                    nameParam.ParameterName = "@name";
                    nameParam.DbType = DbType.String;
                    nameParam.Value = benefitsOffering.Name;
                    nameParam.Direction = ParameterDirection.Input;

                    var contributionAmountParam = command.CreateParameter();
                    contributionAmountParam.ParameterName = "@contributionAmount";
                    contributionAmountParam.DbType = DbType.Decimal;
                    contributionAmountParam.Value = benefitsOffering.ContributionAmount;
                    contributionAmountParam.Direction = ParameterDirection.Input;

                    if (benefitsOffering.PlanTypeId != 0)
                    {
                        var planTypeIdParam = command.CreateParameter();
                        planTypeIdParam.ParameterName = "@planTypeId";
                        planTypeIdParam.DbType = DbType.Int32;
                        planTypeIdParam.Value = benefitsOffering.PlanTypeId;
                        planTypeIdParam.Direction = ParameterDirection.Input;

                        command.Parameters.Add(planTypeIdParam);
                    }

                    command.Parameters.Add(idParam);
                    command.Parameters.Add(nameParam);
                    command.Parameters.Add(contributionAmountParam);


                    connection.Open();
                    command.ExecuteNonQuery();

                    result = (int)idParam.Value;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        protected override void Map(IDataRecord record, BenefitsOffering entity)
        {
            if (null == record) throw new ArgumentNullException(nameof(record));

            if (null == entity) throw new ArgumentNullException(nameof(entity));

            entity.Id = (int)record["Id"];
            entity.Name = (string)record["Name"];
            entity.ContributionAmount = Convert.ToDecimal(record["ContributionAmount"]);
            entity.BenefitsPackageId = (int)record["BenefitsPackageId"];

            if (!DBNull.Value.Equals(record["PlanTypeId"]))
            {
                entity.PlanType = new PlanType
                {
                    Id = (int)record["PlanTypeId"],
                    Name = (string)record["PlanTypeName"]
                };
            }
        }
    }
}
