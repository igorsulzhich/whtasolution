﻿using System;
using System.Data;
using System.Linq;
using log4net;
using WexHealth.DAL.Common;
using WexHealth.DAL.Repositories.Common;
using WexHealth.DAL.Repositories.Common.Interfaces;
using WexHealth.Domain.Entities;

namespace WexHealth.DAL.Repositories
{
    public class ImportRepository : RepositoryBase<Import>, IImportRepository
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ImportRepository));

        private DbContextBase _context = new DefaultDbContext();

        public Import GetById(int id)
        {
            Import result = null;

            if (id <= 0) throw new ArgumentNullException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Import_GetById";
                    command.CommandType = CommandType.StoredProcedure;

                    var administratorIdParam = command.CreateParameter();
                    administratorIdParam.ParameterName = "@id";
                    administratorIdParam.Value = id;
                    administratorIdParam.DbType = DbType.Int32;
                    administratorIdParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(administratorIdParam);
                    connection.Open();

                    result = ToList(command).SingleOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public Import GetFirstWithStatus(ImportStatus status)
        {
            Import result = null;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Import_GetFirstWithStatus";
                    command.CommandType = CommandType.StoredProcedure;

                    var statusParam = command.CreateParameter();
                    statusParam.ParameterName = "@statusName";
                    statusParam.Value = status.ToString();
                    statusParam.DbType = DbType.String;
                    statusParam.Size = 256;
                    statusParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(statusParam);
                    connection.Open();

                    result = ToList(command).SingleOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public int Save(Import entity)
        {
            int result = 0;

            if (null == entity) throw new ArgumentNullException(nameof(entity));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.DbType = DbType.Int32;

                    if (entity.Id == 0)
                    {
                        command.CommandText = "Import_Create";

                        idParam.Direction = ParameterDirection.Output;

                        var filePathParam = command.CreateParameter();
                        filePathParam.ParameterName = "@filePath";
                        filePathParam.DbType = DbType.String;
                        filePathParam.Size = 1024;
                        filePathParam.Value = entity.FilePath;
                        filePathParam.Direction = ParameterDirection.Input;

                        var employerIdParam = command.CreateParameter();
                        employerIdParam.ParameterName = "@employerId";
                        employerIdParam.DbType = DbType.Guid;
                        employerIdParam.Value = entity.EmployerId;
                        employerIdParam.Direction = ParameterDirection.Input;

                        command.Parameters.Add(filePathParam);
                        command.Parameters.Add(employerIdParam);
                    }
                    else
                    {
                        command.CommandText = "Import_Update";

                        idParam.Value = entity.Id;
                        idParam.Direction = ParameterDirection.Input;
                    }

                    var statusParam = command.CreateParameter();
                    statusParam.ParameterName = "@statusName";
                    statusParam.Value = entity.Status.ToString();
                    statusParam.DbType = DbType.String;
                    statusParam.Size = 256;
                    statusParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    command.Parameters.Add(statusParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    result = (int)idParam.Value;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        protected override void Map(IDataRecord record, Import entity)
        {
            if (null == record) throw new ArgumentNullException(nameof(record));

            if (null == entity) throw new ArgumentNullException(nameof(entity));

            entity.Id = (int)record["Id"];
            entity.FilePath = (string)record["FilePath"];
            entity.EmployerId = (Guid)record["EmployerId"];

            if (!DBNull.Value.Equals(record["StatusId"]))
            {
                entity.Status = (ImportStatus)Enum.Parse(typeof(ImportStatus), (string)record["StatusName"]);
            }
        }
    }
}
