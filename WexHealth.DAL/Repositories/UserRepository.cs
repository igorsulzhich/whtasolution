﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using log4net;
using WexHealth.Domain.Entities;
using WexHealth.DAL.Common;
using WexHealth.DAL.Repositories.Common;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.DAL.Repositories
{
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UserRepository));

        private DbContextBase _context = new DefaultDbContext();

        /// <summary>
        /// Get user entity
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns>User entity</returns>
        public User GetUserById(Guid id)
        {
            User result = null;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "User_GetUserById";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = id;
                    idParam.DbType = DbType.Guid;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Get user entity by individual Id
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns>User entity</returns>
        public User GetUserByIndividualId(Guid id)
        {
            User result = null;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "User_GetUserByIndividualId";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = id;
                    idParam.DbType = DbType.Guid;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Get user
        /// </summary>
        /// <param name="userName">User name</param>
        /// <returns></returns>
        public User GetUserByUserName(string userName)
        {
            User result = null;

            if (string.IsNullOrEmpty(userName)) throw new ArgumentNullException(nameof(userName));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "User_GetUserByUsername";
                    command.CommandType = CommandType.StoredProcedure;

                    var userNameParam = command.CreateParameter();
                    userNameParam.ParameterName = "@userName";
                    userNameParam.Value = userName;
                    userNameParam.DbType = DbType.String;
                    userNameParam.Size = 256;
                    userNameParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(userNameParam);
                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Get user
        /// </summary>
        /// <param name="loginType">User login type: administrator, employer or consumer</param>
        /// <returns></returns>
        public IEnumerable<User> GetUsersByLoginType(string loginType)
        {
            IEnumerable<User> result = null;

            if (string.IsNullOrEmpty(loginType)) throw new ArgumentNullException(nameof(loginType));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "User_GetUsersByLoginType";
                    command.CommandType = CommandType.StoredProcedure;

                    var loginTypeParam = command.CreateParameter();
                    loginTypeParam.ParameterName = "@loginType";
                    loginTypeParam.Value = loginType;
                    loginTypeParam.DbType = DbType.String;
                    loginTypeParam.Size = 256;
                    loginTypeParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(loginTypeParam);
                    connection.Open();

                    result = ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Get user
        /// </summary>
        /// <param name="id">Security domain id</param>
        /// <returns></returns>
        public IEnumerable<User> GetUsersBySecurityDomain(Guid id)
        {
            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            return GetUsersBySecurityDomain(id, null);
        }

        /// <summary>
        /// Get user
        /// </summary>
        /// <param name="id">Security domain id</param>
        /// <param name="loginType">User login type</param>
        /// <returns></returns>
        public IEnumerable<User> GetUsersBySecurityDomain(Guid id, string loginType)
        {
            IEnumerable<User> result = null;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "User_GetUsersBySecurityDomain";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.DbType = DbType.Guid;
                    idParam.Value = id;
                    idParam.Direction = ParameterDirection.Input;

                    if (!string.IsNullOrEmpty(loginType))
                    {
                        var loginTypeParam = command.CreateParameter();
                        loginTypeParam.ParameterName = "@loginType";
                        loginTypeParam.DbType = DbType.String;
                        loginTypeParam.Size = 256;
                        loginTypeParam.Value = loginType;
                        loginTypeParam.Direction = ParameterDirection.Input;

                        command.Parameters.Add(loginTypeParam);
                    }

                    command.Parameters.Add(idParam);
                    connection.Open();

                    result = ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Get user password hash
        /// </summary>
        /// <param name="userName">User name</param>
        /// <param name="passwordHash">User db password hash</param>
        /// <param name="passwordSalt">User db password salt</param>
        public void GetPasswordHash(string userName, ref string passwordHash, ref string passwordSalt)
        {
            if (string.IsNullOrEmpty(userName)) throw new ArgumentNullException(nameof(userName));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "User_GetPasswordWithFormat";
                    command.CommandType = CommandType.StoredProcedure;

                    var userNameParam = command.CreateParameter();
                    userNameParam.ParameterName = "@userName";
                    userNameParam.Value = userName;
                    userNameParam.DbType = DbType.String;
                    userNameParam.Size = 256;
                    userNameParam.Direction = ParameterDirection.Input;

                    var passwordHashParam = command.CreateParameter();
                    passwordHashParam.ParameterName = "@passwordHash";
                    passwordHashParam.DbType = DbType.String;
                    passwordHashParam.Size = 256;
                    passwordHashParam.Direction = ParameterDirection.Output;

                    var passwordSaltParam = command.CreateParameter();
                    passwordSaltParam.ParameterName = "@passwordSalt";
                    passwordSaltParam.DbType = DbType.String;
                    passwordSaltParam.Size = 256;
                    passwordSaltParam.Direction = ParameterDirection.Output;

                    command.Parameters.Add(userNameParam);
                    command.Parameters.Add(passwordHashParam);
                    command.Parameters.Add(passwordSaltParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    passwordHash = passwordHashParam.Value.ToString();
                    passwordSalt = passwordSaltParam.Value.ToString();
                }

            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }
        }

        /// <summary>
        /// Create or update user. Leave id property equals to Guid.Empty to create new User
        /// </summary>
        /// <param name="user">User entity with filled properties</param>
        /// <returns></returns>
        public Guid SaveUser(User user)
        {
            Guid result = Guid.Empty;

            if (null == user) throw new ArgumentNullException(nameof(user));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {

                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.DbType = DbType.Guid;

                    if (Guid.Empty.Equals(user.Id))
                    {
                        command.CommandText = "User_CreateUser";

                        idParam.Value = Guid.Empty;
                        idParam.Direction = ParameterDirection.Output;

                        var passwordSaltParam = command.CreateParameter();
                        passwordSaltParam.ParameterName = "@passwordSalt";
                        passwordSaltParam.Value = user.PasswordSalt;
                        passwordSaltParam.DbType = DbType.String;
                        passwordSaltParam.Size = 256;
                        passwordSaltParam.Direction = ParameterDirection.Input;

                        var loginTypeParam = command.CreateParameter();
                        loginTypeParam.ParameterName = "@loginType";
                        loginTypeParam.Value = user.LoginType;
                        loginTypeParam.DbType = DbType.String;
                        loginTypeParam.Size = 256;
                        loginTypeParam.Direction = ParameterDirection.Input;

                        var securityDomainIdParam = command.CreateParameter();
                        securityDomainIdParam.ParameterName = "@securityDomainId";
                        securityDomainIdParam.Value = user.SecurityDomainId;
                        securityDomainIdParam.DbType = DbType.Guid;
                        securityDomainIdParam.Direction = ParameterDirection.Input;

                        command.Parameters.Add(passwordSaltParam);
                        command.Parameters.Add(loginTypeParam);
                        command.Parameters.Add(securityDomainIdParam);
                    }
                    else
                    {
                        command.CommandText = "User_UpdateUser";

                        idParam.Value = user.Id;
                        idParam.Direction = ParameterDirection.Input;
                    }

                    var userNameParam = command.CreateParameter();
                    userNameParam.ParameterName = "@userName";
                    userNameParam.Value = user.UserName;
                    userNameParam.DbType = DbType.String;
                    userNameParam.Size = 256;
                    userNameParam.Direction = ParameterDirection.Input;

                    var passwordHashParam = command.CreateParameter();
                    passwordHashParam.ParameterName = "@passwordHash";
                    passwordHashParam.Value = user.PasswordHash;
                    passwordHashParam.DbType = DbType.String;
                    passwordHashParam.Size = 256;
                    passwordHashParam.Direction = ParameterDirection.Input;

                    var individualIdParam = command.CreateParameter();
                    individualIdParam.ParameterName = "@individualId";
                    individualIdParam.Value = user.IndividualId;
                    individualIdParam.DbType = DbType.Guid;
                    individualIdParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    command.Parameters.Add(userNameParam);
                    command.Parameters.Add(passwordHashParam);
                    command.Parameters.Add(individualIdParam);

                    connection.Open();

                    command.ExecuteNonQuery();

                    result = (Guid)idParam.Value;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Delete user
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns></returns>
        public bool DeleteUser(Guid id)
        {
            bool result = false;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "User_DeleteUser";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = id;
                    idParam.DbType = DbType.Guid;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    result = true;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Map database stored record to user entity
        /// </summary>
        /// <param name="record">Database user record</param>
        /// <param name="entity">User entity</param>
        protected override void Map(IDataRecord record, User entity)
        {
            entity.Id = (Guid)record["Id"];
            entity.UserName = (string)record["Username"];
            entity.LoginType = (string)record["LoginType"];
            entity.SecurityDomainId = (Guid)record["SecurityDomainId"];

            if (!DBNull.Value.Equals(record["IndividualId"]))
            {
                entity.IndividualId = (Guid)record["IndividualId"];
                entity.Individual = new Individual
                {
                    Id = (Guid)record["IndividualId"],
                    FirstName = (string)record["Firstname"],
                    LastName = (string)record["Lastname"],
                    Email = (string)record["Email"],
                    DateBirth = (DateTime)record["DateBirth"]
                };
            }
        }
    }
}
