﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using PostSharp.Patterns.Contracts;
using log4net;
using WexHealth.DAL.Common;
using WexHealth.DAL.Models;
using WexHealth.DAL.Repositories.Common;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.DAL.Repositories
{
    public class AdministratorRepository : RepositoryBase<AdministratorDAL>, IAdministratorRepository
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(AdministratorRepository));

        private DbContextBase _context = new DefaultDbContext();

        /// <summary>
        /// Get administrator entity by id
        /// </summary>
        /// <param name="id">Administrator id</param>
        /// <returns>Administrator entity</returns>
        public AdministratorDAL GetAdministratorById(Guid id)
        {
            AdministratorDAL result = null;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Administrator_GetAdministratorById";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = id;
                    idParam.DbType = DbType.Guid;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Get administrator entity by name
        /// </summary>
        /// <param name="name">Administrator name</param>
        /// <returns>Administrator entity</returns>
        public AdministratorDAL GetAdministratorByName([Required] string name)
        {
            AdministratorDAL result = null;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Administrator_GetAdministratorByName";
                    command.CommandType = CommandType.StoredProcedure;

                    var nameParam = command.CreateParameter();
                    nameParam.ParameterName = "@name";
                    nameParam.Value = name;
                    nameParam.DbType = DbType.String;
                    nameParam.Size = 256;
                    nameParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(nameParam);
                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Get administrator entity by security domain
        /// </summary>
        /// <param name="id">Security domain id</param>
        /// <returns>Administrator entity</returns>
        public AdministratorDAL GetAdministratorBySecurityDomain(Guid id)
        {
            AdministratorDAL result = null;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Administrator_GetAdministratorBySecurityDomain";
                    command.CommandType = CommandType.StoredProcedure;

                    var securityDomainParam = command.CreateParameter();
                    securityDomainParam.ParameterName = "@securityDomain";
                    securityDomainParam.Value = id;
                    securityDomainParam.DbType = DbType.Guid;
                    securityDomainParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(securityDomainParam);
                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Get all administrator entities
        /// </summary>
        /// <returns>List of administrator entities</returns>
        public IEnumerable<AdministratorDAL> GetAdministrators()
        {
            IEnumerable<AdministratorDAL> result = null;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Administrator_GetAdministrators";
                    command.CommandType = CommandType.StoredProcedure;

                    connection.Open();

                    result = ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Create or update administator. Leave administrator id equals to Guid.Empty to create new administrator
        /// </summary>
        /// <param name="administrator">Administrator entity with filled properties</param>
        /// <returns>Id of insereted administrator</returns>
        public Guid SaveAdministrator([NotNull] AdministratorDAL administrator)
        {
            Guid result = Guid.Empty;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.DbType = DbType.Guid;

                    if (Guid.Empty.Equals(administrator.Id))
                    {
                        command.CommandText = "Administrator_CreateAdministrator";

                        idParam.Value = Guid.Empty;
                        idParam.Direction = ParameterDirection.Output;
                    }
                    else
                    {
                        command.CommandText = "Administrator_UpdateAdministrator";

                        idParam.Value = administrator.Id;
                        idParam.Direction = ParameterDirection.Input;
                    }

                    var nameParam = command.CreateParameter();
                    nameParam.ParameterName = "@name";
                    nameParam.Value = administrator.Name;
                    nameParam.DbType = DbType.String;
                    nameParam.Size = 256;
                    nameParam.Direction = ParameterDirection.Input;

                    var securityDomainIdParam = command.CreateParameter();
                    securityDomainIdParam.ParameterName = "@securityDomainId";
                    securityDomainIdParam.Value = administrator.SecurityDomainId;
                    securityDomainIdParam.DbType = DbType.Guid;
                    securityDomainIdParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    command.Parameters.Add(nameParam);
                    command.Parameters.Add(securityDomainIdParam);

                    connection.Open();

                    command.ExecuteNonQuery();

                    result = (Guid)idParam.Value;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Delete administrator
        /// </summary>
        /// <param name="id">Administrator id</param>
        /// <returns></returns>
        public bool DeleteAdministrator(Guid id)
        {
            bool result = false;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Administrator_DeleteAdministrator";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = id;
                    idParam.DbType = DbType.Guid;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    result = true;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Map database store record to administrator entity
        /// </summary>
        /// <param name="record">Database administrator record</param>
        /// <param name="entity">Administrator entity</param>
        protected override void Map([NotNull] IDataRecord record, [NotNull] AdministratorDAL entity)
        {
            entity.Id = (Guid)record["Id"];
            entity.Name = (string)record["Name"];
            entity.SecurityDomainId = (Guid)record["SecurityDomainId"];

            entity.SecurityDomain = new SecurityDomainDAL
            {
                Id = (Guid)record["SecurityDomainId"],
                Alias = (string)record["Alias"]
            };
        }
    }
}
