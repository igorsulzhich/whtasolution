﻿using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using log4net;
using WexHealth.DAL.Common;
using WexHealth.DAL.Models;
using WexHealth.DAL.Repositories.Common;
using WexHealth.DAL.Repositories.Common.Interfaces;


namespace WexHealth.DAL.Repositories
{
    public class SecurityDomainRepository : RepositoryBase<SecurityDomainDAL>, ISecurityDomainRepository
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SecurityDomainRepository));

        private DbContextBase _context = new DefaultDbContext();

        /// <summary>
        /// Get security domain entity by id
        /// </summary>
        /// <param name="id">Security domain id</param>
        /// <returns>Security domain entity</returns>
        public SecurityDomainDAL GetSecurityDomainById(Guid id)
        {
            SecurityDomainDAL result = null;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "SecurityDomain_GetSecurityDomainById";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = id;
                    idParam.DbType = DbType.Guid;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Get security domain entity by alias
        /// </summary>
        /// <param name="alias">Alias name</param>
        /// <returns>Security domain entity</returns>
        public SecurityDomainDAL GetSecurityDomainByAlias(string alias)
        {
            SecurityDomainDAL result = null;

            if (string.IsNullOrEmpty(alias)) throw new ArgumentNullException(nameof(alias));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "SecurityDomain_GetSecurityDomainByAlias";
                    command.CommandType = CommandType.StoredProcedure;

                    var aliasParam = command.CreateParameter();
                    aliasParam.ParameterName = "@alias";
                    aliasParam.Value = alias;
                    aliasParam.DbType = DbType.String;
                    aliasParam.Size = 256;
                    aliasParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(aliasParam);
                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Get all security domain entities
        /// </summary>
        /// <returns>List of security domain entities</returns>
        public IEnumerable<SecurityDomainDAL> GetSecurityDomains()
        {
            IEnumerable<SecurityDomainDAL> result = null;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "SecurityDomain_GetSecurityDomains";
                    command.CommandType = CommandType.StoredProcedure;

                    connection.Open();

                    result = ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Create or update security domain. Leave security domain id equals to Guid.Empty to create new security domain
        /// </summary>
        /// <param name="securityDomain">Security domain entity with filled properties</param>
        /// <returns>Id of inserted security domain</returns>
        public Guid SaveSecurityDomain(SecurityDomainDAL securityDomain)
        {
            Guid result = Guid.Empty;

            if (null == securityDomain) throw new ArgumentNullException(nameof(securityDomain));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.DbType = DbType.Guid;

                    if (securityDomain.Id == Guid.Empty)
                    {
                        command.CommandText = "SecurityDomain_CreateSecurityDomain";

                        idParam.Value = Guid.Empty;
                        idParam.Direction = ParameterDirection.Output;
                    }
                    else
                    {
                        command.CommandText = "SecurityDomain_UpdateSecurityDomain";

                        idParam.Value = securityDomain.Id;
                        idParam.Direction = ParameterDirection.Input;
                    }

                    var aliasParam = command.CreateParameter();
                    aliasParam.ParameterName = "@alias";
                    aliasParam.Value = securityDomain.Alias;
                    aliasParam.DbType = DbType.String;
                    aliasParam.Size = 256;
                    aliasParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    command.Parameters.Add(aliasParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    result = (Guid)idParam.Value;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Map database stored record to security domain entity
        /// </summary>
        /// <param name="record">Database stored security domain record</param>
        /// <param name="entity">Security domain record</param>
        protected override void Map(IDataRecord record, SecurityDomainDAL entity)
        {
            entity.Id = (Guid)record["Id"];
            entity.Alias = (string)record["Alias"];
        }
    }
}
