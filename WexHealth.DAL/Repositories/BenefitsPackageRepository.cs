﻿using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using log4net;
using WexHealth.DAL.Common;
using WexHealth.DAL.Repositories.Common;
using WexHealth.DAL.Repositories.Common.Interfaces;
using WexHealth.Domain.Entities;

namespace WexHealth.DAL.Repositories
{
    public class BenefitsPackageRepository : RepositoryBase<BenefitsPackage>, IBenefitsPackageRepository
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(BenefitsPackageRepository));

        private DbContextBase _context = new DefaultDbContext();

        /// <summary>
        /// Get benefits packages by employer Id
        /// </summary>
        /// <param name="employerId">Current employer Id</param>
        /// <returns></returns>
        public IEnumerable<BenefitsPackage> GetPackages(Guid employerId)
        {
            IEnumerable<BenefitsPackage> result = null;

            if (Guid.Empty.Equals(employerId)) throw new ArgumentOutOfRangeException(nameof(employerId));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "BenefitsPackage_GetPackages";
                    command.CommandType = CommandType.StoredProcedure;

                    var employerIdParam = command.CreateParameter();
                    employerIdParam.ParameterName = "@employerId";
                    employerIdParam.Value = employerId;
                    employerIdParam.DbType = DbType.Guid;
                    employerIdParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(employerIdParam);
                    connection.Open();

                    result = ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Get benefits package by Id
        /// </summary>
        /// <param name="id">Current benefits package Id</param>
        /// <returns></returns>
        public BenefitsPackage GetPackageById(int id)
        {
            BenefitsPackage result = null;

            if (Guid.Empty.Equals(id)) throw new ArgumentOutOfRangeException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "BenefitsPackage_GetPackageById";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = id;
                    idParam.DbType = DbType.Int32;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Create or update benefits package
        /// </summary>
        /// <param name="package">BenefitsPackage input entity</param>
        /// <returns></returns>
        public int SavePackage(BenefitsPackage package)
        {
            int result;

            if (null == package) throw new ArgumentNullException(nameof(package));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.DbType = DbType.Int32;

                    if (package.Id == 0)
                    {
                        command.CommandText = "BenefitsPackage_CreatePackage";

                        idParam.Value = 0;
                        idParam.Direction = ParameterDirection.Output;
                    }
                    else
                    {
                        command.CommandText = "BenefitsPackage_UpdatePackage";

                        idParam.Value = package.Id;
                        idParam.Direction = ParameterDirection.Input;
                    }

                    var nameParam = command.CreateParameter();
                    nameParam.ParameterName = "@name";
                    nameParam.DbType = DbType.String;
                    nameParam.Value = package.Name;
                    nameParam.Direction = ParameterDirection.Input;

                    var dateOfStartParam = command.CreateParameter();
                    dateOfStartParam.ParameterName = "@dateOfStart";
                    dateOfStartParam.DbType = DbType.DateTime2;
                    dateOfStartParam.Size = 7;
                    dateOfStartParam.Value = package.DateOfStart;
                    dateOfStartParam.Direction = ParameterDirection.Input;

                    var dateOfEndParam = command.CreateParameter();
                    dateOfEndParam.ParameterName = "@dateOfEnd";
                    dateOfEndParam.DbType = DbType.DateTime2;
                    dateOfEndParam.Size = 7;
                    dateOfEndParam.Value = package.DateOfEnd;
                    dateOfEndParam.Direction = ParameterDirection.Input;

                    var frequencyParam = command.CreateParameter();
                    frequencyParam.ParameterName = "@frequency";
                    frequencyParam.DbType = DbType.Int32;
                    frequencyParam.Value = package.Frequency;
                    frequencyParam.Direction = ParameterDirection.Input;

                    var statusIdParam = command.CreateParameter();
                    statusIdParam.ParameterName = "@statusId";
                    statusIdParam.DbType = DbType.Int32;
                    statusIdParam.Value = package.StatusId;
                    statusIdParam.Direction = ParameterDirection.Input;

                    var employerIdParam = command.CreateParameter();
                    employerIdParam.ParameterName = "@employerId";
                    employerIdParam.DbType = DbType.Guid;
                    employerIdParam.Value = package.EmployerId;
                    employerIdParam.Direction = ParameterDirection.Input;

                    if (package.DateOfInitialized != DateTime.MinValue)
                    {
                        var dateOfInitialized = command.CreateParameter();
                        dateOfInitialized.ParameterName = "@dateOfInitialized";
                        dateOfInitialized.DbType = DbType.DateTime2;
                        dateOfInitialized.Size = 7;
                        dateOfInitialized.Value = package.DateOfInitialized;
                        dateOfInitialized.Direction = ParameterDirection.Input;

                        command.Parameters.Add(dateOfInitialized);
                    }

                    command.Parameters.Add(idParam);
                    command.Parameters.Add(nameParam);
                    command.Parameters.Add(dateOfStartParam);
                    command.Parameters.Add(dateOfEndParam);                   
                    command.Parameters.Add(frequencyParam);                  
                    command.Parameters.Add(employerIdParam);
                    command.Parameters.Add(statusIdParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    result = (int)idParam.Value;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Initialize current benefits package
        /// </summary>
        /// <param name="id">Current benefits package Id</param>
        /// <param name="statusId">Initialized status Ids</param>
        /// <param name="dateOfInitialized">Date of initialized</param>
        /// <returns></returns>
        public int Initialize(int id, int statusId, DateTime dateOfInitialized)
        {
            int result;

            if (id == 0) throw new ArgumentNullException(nameof(id));

            if (statusId == 0) throw new ArgumentNullException(nameof(statusId));

            if (dateOfInitialized.Equals(DateTime.MinValue)) throw new ArgumentNullException(nameof(dateOfInitialized));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "BenefitsPackage_InitializePackage";

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.DbType = DbType.Int32;
                    idParam.Value = id;
                    idParam.Direction = ParameterDirection.Input;

                    var dateOfInitializedParam = command.CreateParameter();
                    dateOfInitializedParam.ParameterName = "@dateOfInitialized";
                    dateOfInitializedParam.DbType = DbType.DateTime2;
                    dateOfInitializedParam.Size = 7;
                    dateOfInitializedParam.Value = dateOfInitialized;
                    dateOfInitializedParam.Direction = ParameterDirection.Input;

                    var statusIdParam = command.CreateParameter();
                    statusIdParam.ParameterName = "@statusId";
                    statusIdParam.DbType = DbType.Int32;
                    statusIdParam.Value = statusId;
                    statusIdParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    command.Parameters.Add(dateOfInitializedParam);
                    command.Parameters.Add(statusIdParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    result = (int)idParam.Value;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Delete current benefits package
        /// </summary>
        /// <param name="packageId">Current benefits package Id</param>
        /// <returns></returns>
        public bool DeletePackage(int packageId)
        {
            bool result = false;

            if (packageId == 0) throw new ArgumentNullException(nameof(packageId));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "BenefitsPackage_DeletePackage";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = packageId;
                    idParam.DbType = DbType.Int32;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    result = true;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        protected override void Map(IDataRecord record, BenefitsPackage entity)
        {
            if (null == record) throw new ArgumentNullException(nameof(record));

            if (null == entity) throw new ArgumentNullException(nameof(entity));

            entity.Id = (int)record["Id"];
            entity.Name = (string)record["Name"];
            entity.Frequency = (int)record["Frequency"];
            entity.DateOfStart = (DateTime)record["DateOfStart"];
            entity.DateOfEnd = (DateTime)record["DateOfEnd"];
            entity.EmployerId = (Guid)record["EmployerId"];

            if (!DBNull.Value.Equals(record["DateOfInitialize"]))
            {
                entity.DateOfInitialized = (DateTime)record["DateOfInitialize"];
            }
            
            if (!DBNull.Value.Equals(record["StatusId"]))
            {
                entity.StatusId = (int)record["StatusId"];
                entity.Status = new BenefitsPackageStatus
                {
                    Id = (int)record["StatusId"],
                    Name = (string)record["BenefitsPackageStatusName"]
                };
            }
        }       
    }
}
