﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using PostSharp.Patterns.Contracts;
using log4net;
using WexHealth.Domain.Entities;
using WexHealth.DAL.Common;
using WexHealth.DAL.Models;
using WexHealth.DAL.Repositories.Common;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.DAL.Repositories
{
    public class AdministratorUserRepository : RepositoryBase<AdministratorUserDAL>, IAdministratorUserRepository
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(AdministratorUserRepository));

        private DbContextBase _context = new DefaultDbContext();

        public AdministratorUserDAL GetUser(Guid userId)
        {
            return GetUser(userId, null);
        }

        public AdministratorUserDAL GetUser(Guid userId, Guid? administratorId)
        {
            AdministratorUserDAL result = null;

            if (Guid.Empty.Equals(userId)) throw new ArgumentNullException(nameof(userId));

            if (administratorId.HasValue && Guid.Empty.Equals(administratorId)) throw new ArgumentNullException(nameof(administratorId));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "AdministratorUser_GetUser";
                    command.CommandType = CommandType.StoredProcedure;

                    var userIdParam = command.CreateParameter();
                    userIdParam.ParameterName = "@userId";
                    userIdParam.Value = userId;
                    userIdParam.DbType = DbType.Guid;
                    userIdParam.Direction = ParameterDirection.Input;

                    if (administratorId.HasValue)
                    {
                        var administratorIdParam = command.CreateParameter();
                        administratorIdParam.ParameterName = "@administratorId";
                        administratorIdParam.Value = administratorId.Value;
                        administratorIdParam.DbType = DbType.Guid;
                        administratorIdParam.Direction = ParameterDirection.Input;

                        command.Parameters.Add(administratorIdParam);
                    }

                    command.Parameters.Add(userIdParam);

                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public IEnumerable<AdministratorUserDAL> GetUsers(Guid administratorId)
        {
            IEnumerable<AdministratorUserDAL> result = null;

            if (Guid.Empty.Equals(administratorId)) throw new ArgumentNullException(nameof(administratorId));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "AdministratorUser_GetUsers";
                    command.CommandType = CommandType.StoredProcedure;

                    var administratorIdParam = command.CreateParameter();
                    administratorIdParam.ParameterName = "@administratorId";
                    administratorIdParam.Value = administratorId;
                    administratorIdParam.DbType = DbType.Guid;
                    administratorIdParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(administratorIdParam);

                    connection.Open();

                    result = ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public Guid SaveUser([NotNull] AdministratorUserDAL user)
        {
            Guid result = Guid.Empty;

            if (null == user) throw new ArgumentNullException(nameof(user));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "AdministratorUser_CreateUser";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = Guid.Empty;
                    idParam.DbType = DbType.Guid;
                    idParam.Direction = ParameterDirection.Output;

                    var userIdParam = command.CreateParameter();
                    userIdParam.ParameterName = "@userId";
                    userIdParam.Value = user.UserId;
                    userIdParam.DbType = DbType.Guid;
                    userIdParam.Direction = ParameterDirection.Input;

                    var administratorIdParam = command.CreateParameter();
                    administratorIdParam.ParameterName = "@administratorId";
                    administratorIdParam.Value = user.AdministratorId;
                    administratorIdParam.DbType = DbType.Guid;
                    administratorIdParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    command.Parameters.Add(userIdParam);
                    command.Parameters.Add(administratorIdParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    result = (Guid)idParam.Value;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        protected override void Map([NotNull] IDataRecord record, [NotNull] AdministratorUserDAL entity)
        {
            entity.Id = (Guid)record["Id"];
            entity.AdministratorId = (Guid)record["AdministratorId"];

            if (!DBNull.Value.Equals(record["UserId"]))
            {
                entity.UserId = (Guid)record["UserId"];
                entity.User = new User
                {
                    Id = (Guid)record["UserId"],
                    UserName = (string)record["UserName"],
                    SecurityDomainId = (Guid)record["SecurityDomainId"]
                };

                if (!DBNull.Value.Equals(record["IndividualId"]))
                {
                    entity.User.IndividualId = (Guid)record["IndividualId"];
                    entity.User.Individual = new Individual
                    {
                        Id = (Guid)record["IndividualId"],
                        FirstName = (string)record["FirstName"],
                        LastName = (string)record["LastName"],
                        Email = (string)record["Email"],
                        DateBirth = (DateTime)record["DateBirth"]
                    };
                }
            }
        }
    }
}
