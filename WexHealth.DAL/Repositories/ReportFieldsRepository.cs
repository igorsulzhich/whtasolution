﻿using System;
using System.Data;
using System.Collections.Generic;
using log4net;
using WexHealth.Domain.Entities;
using WexHealth.DAL.Common;
using WexHealth.DAL.Repositories.Common;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.DAL.Repositories
{
    public class ReportFieldsRepository : RepositoryBase<ReportField>, IReportFieldsRepository
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ReportFieldsRepository));

        private DbContextBase _context = new DefaultDbContext();

        public int SaveReportFields(ReportField objectField)
        {
            int result;

            if (null == objectField) throw new ArgumentNullException(nameof(objectField));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "ReportFields_CreateReportFields";

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.DbType = DbType.Int32;
                    idParam.Value = 0;
                    idParam.Direction = ParameterDirection.Output;

                    var objectFieldsIdParam = command.CreateParameter();
                    objectFieldsIdParam.ParameterName = "@objectFieldsId";
                    objectFieldsIdParam.DbType = DbType.Int32;
                    objectFieldsIdParam.Value = objectField.ObjectFieldsId;
                    objectFieldsIdParam.Direction = ParameterDirection.Input;

                    var reportRequestParam = command.CreateParameter();
                    reportRequestParam.ParameterName = "@reportRequestId";
                    reportRequestParam.DbType = DbType.Int32;
                    reportRequestParam.Value = objectField.ReportRequestId;
                    reportRequestParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    command.Parameters.Add(objectFieldsIdParam);
                    command.Parameters.Add(reportRequestParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    result = (int)idParam.Value;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public IEnumerable<ReportField> GetReportFieldByRequestId(int requestId)
        {
            if (requestId == 0) throw new ArgumentNullException(nameof(requestId));

            IEnumerable<ReportField> result = null;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "ReportFields_GetReportFieldsByRequestId";
                    command.CommandType = CommandType.StoredProcedure;

                    var requestIdParam = command.CreateParameter();
                    requestIdParam.ParameterName = "@requestId";
                    requestIdParam.DbType = DbType.Int32;
                    requestIdParam.Value = requestId;
                    requestIdParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(requestIdParam);

                    connection.Open();

                    result = ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public bool DeleteReportFieldByRequest(int id)
        {
            bool result = false;

            if (id <= 0) throw new ArgumentOutOfRangeException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "ReportFields_DeleteByReportRequest";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@requestId";
                    idParam.Value = id;
                    idParam.DbType = DbType.Int32;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    result = true;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        protected override void Map(IDataRecord record, ReportField entity)
        {
            entity.Id = (int)record["Id"];
            entity.ObjectFieldsId = (int)record["ObjectFieldsId"];
            entity.ObjectFieldName = (string)record["ObjectFieldName"];
            entity.ReportRequestId = (int)record["ReportRequestId"];
        }
    }
}
