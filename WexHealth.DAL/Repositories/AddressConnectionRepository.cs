﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using PostSharp.Patterns.Contracts;
using log4net;
using WexHealth.DAL.Common;
using WexHealth.DAL.Models;
using WexHealth.DAL.Repositories.Common;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.DAL.Repositories
{
    public class AddressConnectionRepository : RepositoryBase<AddressConnectionDAL>, IAddressConnectionRepository
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(AddressConnectionRepository));

        private DbContextBase _context = new DefaultDbContext();

        public AddressConnectionDAL GetAddressConnectionById(Guid id)
        {
            AddressConnectionDAL result = null;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "AddressConnection_GetAddressConnectionById";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = id;
                    idParam.DbType = DbType.Guid;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public IEnumerable<AddressConnectionDAL> GetAddressConnectionsByObject(Guid objectId, [Required] string objectType)
        {
            IEnumerable<AddressConnectionDAL> result = null;

            if (Guid.Empty.Equals(objectId)) throw new ArgumentNullException(nameof(objectId));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "AddressConnection_GetAddressConnectionByObject";
                    command.CommandType = CommandType.StoredProcedure;

                    var objectIdParam = command.CreateParameter();
                    objectIdParam.ParameterName = "@objectId";
                    objectIdParam.Value = objectId;
                    objectIdParam.DbType = DbType.Guid;
                    objectIdParam.Direction = ParameterDirection.Input;

                    var objectTypeParam = command.CreateParameter();
                    objectTypeParam.ParameterName = "@objectType";
                    objectTypeParam.Value = objectType;
                    objectTypeParam.DbType = DbType.String;
                    objectTypeParam.Size = 256;
                    objectTypeParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(objectIdParam);
                    command.Parameters.Add(objectTypeParam);

                    connection.Open();

                    result = ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public Guid SaveAddressConnection([NotNull] AddressConnectionDAL addressConnection)
        {
            Guid result = Guid.Empty;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.DbType = DbType.Guid;

                    if (Guid.Empty.Equals(addressConnection.Id))
                    {
                        command.CommandText = "AddressConnection_CreateAddressConnection";

                        idParam.Value = Guid.Empty;
                        idParam.Direction = ParameterDirection.Output;
                    }
                    else
                    {
                        command.CommandText = "AddressConnection_UpdateAddressConnection";

                        idParam.Value = addressConnection.Id;
                        idParam.Direction = ParameterDirection.Input;
                    }

                    var objectIdParam = command.CreateParameter();
                    objectIdParam.ParameterName = "@objectId";
                    objectIdParam.Value = addressConnection.ObjectId;
                    objectIdParam.DbType = DbType.Guid;
                    objectIdParam.Direction = ParameterDirection.Input;

                    var objectTypeParam = command.CreateParameter();
                    objectTypeParam.ParameterName = "@objectType";
                    objectTypeParam.Value = addressConnection.ObjectType;
                    objectTypeParam.DbType = DbType.String;
                    objectTypeParam.Size = 256;
                    objectTypeParam.Direction = ParameterDirection.Input;

                    var typeParam = command.CreateParameter();
                    typeParam.ParameterName = "@type";
                    typeParam.Value = addressConnection.Type;
                    typeParam.DbType = DbType.String;
                    typeParam.Size = 256;
                    typeParam.Direction = ParameterDirection.Input;

                    var addressIdParam = command.CreateParameter();
                    addressIdParam.ParameterName = "@addressId";
                    addressIdParam.Value = addressConnection.AddressId;
                    addressIdParam.DbType = DbType.Guid;
                    addressIdParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    command.Parameters.Add(objectIdParam);
                    command.Parameters.Add(objectTypeParam);
                    command.Parameters.Add(typeParam);
                    command.Parameters.Add(addressIdParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    result = (Guid)idParam.Value;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public bool DeleteAddressConnection(Guid id)
        {
            bool result = false;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "AddressConnection_DeleteAddressConnection";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = id;
                    idParam.DbType = DbType.Guid;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    result = true;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        protected override void Map([NotNull] IDataRecord record, [NotNull] AddressConnectionDAL entity)
        {
            entity.ObjectId = (Guid)record["ObjectId"];
            entity.ObjectType = (string)record["ObjectType"];

            if (!DBNull.Value.Equals(record["AddressId"]))
            {
                entity.AddressId = (Guid)record["AddressId"];
                entity.Address = new AddressDAL
                {
                    Id = (Guid)record["AddressId"],
                    Street = (string)record["Street"],
                    City = (string)record["City"],
                    State = (string)record["State"],
                    ZipCode = (string)record["ZipCode"]
                };
            }
        }
    }
}
