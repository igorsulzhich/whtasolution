﻿using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using log4net;
using WexHealth.DAL.Common;
using WexHealth.DAL.Repositories.Common;
using WexHealth.DAL.Repositories.Common.Interfaces;
using WexHealth.Domain.Entities;

namespace WexHealth.DAL.Repositories
{
    public class EnrollmentRepository : RepositoryBase<Enrollment>, IEnrollmentRepository
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(EnrollmentRepository));

        private DbContextBase _context = new DefaultDbContext();

        public Enrollment GetEnrollment(int id)
        {
            Enrollment result = null;

            if (id < 0) throw new ArgumentOutOfRangeException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Enrollment_GetEnrollmentById";
                    command.CommandType = CommandType.StoredProcedure;

                    var administratorIdParam = command.CreateParameter();
                    administratorIdParam.ParameterName = "@id";
                    administratorIdParam.Value = id;
                    administratorIdParam.DbType = DbType.Int32;
                    administratorIdParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(administratorIdParam);
                    connection.Open();

                    result = ToList(command).SingleOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public Enrollment GetEnrollment(int benefitOfferingId, Guid consumerId)
        {
            Enrollment result = null;

            if (benefitOfferingId < 0) throw new ArgumentOutOfRangeException(nameof(benefitOfferingId));

            if (Guid.Empty.Equals(consumerId)) throw new ArgumentNullException(nameof(consumerId));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Enrollment_GetEnrollmentByBenefitOffering";
                    command.CommandType = CommandType.StoredProcedure;

                    var benefitIdParam = command.CreateParameter();
                    benefitIdParam.ParameterName = "@benefitOfferingId";
                    benefitIdParam.Value = benefitOfferingId;
                    benefitIdParam.DbType = DbType.Int32;
                    benefitIdParam.Direction = ParameterDirection.Input;

                    var consumerIdParam = command.CreateParameter();
                    consumerIdParam.ParameterName = "@consumerId";
                    consumerIdParam.Value = consumerId;
                    consumerIdParam.DbType = DbType.Guid;
                    consumerIdParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(benefitIdParam);
                    command.Parameters.Add(consumerIdParam);
                    connection.Open();

                    result = ToList(command).SingleOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public IEnumerable<Enrollment> GetEnrollments(Guid consumerId)
        {
            IEnumerable<Enrollment> result = null;

            if (Guid.Empty.Equals(consumerId)) throw new ArgumentOutOfRangeException(nameof(consumerId));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Enrollment_GetEnrollments";
                    command.CommandType = CommandType.StoredProcedure;

                    var consumerIdParam = command.CreateParameter();
                    consumerIdParam.ParameterName = "@consumerId";
                    consumerIdParam.Value = consumerId;
                    consumerIdParam.DbType = DbType.Guid;
                    consumerIdParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(consumerIdParam);
                    connection.Open();

                    result = ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public IEnumerable<Enrollment> GetEnrollmentsByBenefitsPackageId(int packageId)
        {
            IEnumerable<Enrollment> result = null;

            if (packageId == 0) throw new ArgumentOutOfRangeException(nameof(packageId));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Enrollment_GetEnrollmentsByBenefitsPackageId";
                    command.CommandType = CommandType.StoredProcedure;

                    var packageIdParam = command.CreateParameter();
                    packageIdParam.ParameterName = "@benefitsPackageId";
                    packageIdParam.Value = packageId;
                    packageIdParam.DbType = DbType.Int32;
                    packageIdParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(packageIdParam);
                    connection.Open();

                    result = ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public IEnumerable<Enrollment> GetEnrollmentsByPackage(string status, DateTime dateBetween)
        {
            IEnumerable<Enrollment> result = null;

            if (string.IsNullOrEmpty(status)) throw new ArgumentNullException(nameof(status));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Enrollment_GetByPackage";
                    command.CommandType = CommandType.StoredProcedure;

                    var statusParam = command.CreateParameter();
                    statusParam.ParameterName = "@status";
                    statusParam.Value = status;
                    statusParam.DbType = DbType.String;
                    statusParam.Size = 256;
                    statusParam.Direction = ParameterDirection.Input;

                    var dateBetweenParam = command.CreateParameter();
                    dateBetweenParam.ParameterName = "@dateBetween";
                    dateBetweenParam.Value = dateBetween;
                    dateBetweenParam.DbType = DbType.DateTime2;
                    dateBetweenParam.Size = 7;
                    dateBetweenParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(statusParam);
                    command.Parameters.Add(dateBetweenParam);
                    connection.Open();

                    result = ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public IEnumerable<Enrollment> GetActiveEnrollments(Guid consumerId, string benefitStatusName)
        {
            IEnumerable<Enrollment> result = null;

            if (Guid.Empty.Equals(consumerId)) throw new ArgumentOutOfRangeException(nameof(consumerId));

            if (string.IsNullOrEmpty(benefitStatusName)) throw new ArgumentNullException(nameof(benefitStatusName));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Enrollment_GetActiveEnrollments";
                    command.CommandType = CommandType.StoredProcedure;

                    var consumerIdParam = command.CreateParameter();
                    consumerIdParam.ParameterName = "@consumerId";
                    consumerIdParam.Value = consumerId;
                    consumerIdParam.DbType = DbType.Guid;
                    consumerIdParam.Direction = ParameterDirection.Input;

                    var statusNameParam = command.CreateParameter();
                    statusNameParam.ParameterName = "@statusName";
                    statusNameParam.Value = benefitStatusName;
                    statusNameParam.DbType = DbType.String;
                    statusNameParam.Size = 256;
                    statusNameParam.Direction = ParameterDirection.Input;

                    var dateParam = command.CreateParameter();
                    dateParam.ParameterName = "@date";
                    dateParam.Value = DateTime.Now;
                    dateParam.DbType = DbType.DateTime2;
                    dateParam.Size = 7;
                    dateParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(consumerIdParam);
                    command.Parameters.Add(statusNameParam);
                    command.Parameters.Add(dateParam);
                    connection.Open();

                    result = ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public int SaveEnrollment(Enrollment enrollment)
        {
            int result = 0;

            if (null == enrollment) throw new ArgumentNullException(nameof(enrollment));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.DbType = DbType.Int32;

                    if (enrollment.Id == 0)
                    {
                        command.CommandText = "Enrollment_CreateEnrollment";

                        idParam.Value = 0;
                        idParam.Direction = ParameterDirection.Output;

                        var consumerIdParam = command.CreateParameter();
                        consumerIdParam.ParameterName = "@consumerId";
                        consumerIdParam.DbType = DbType.Guid;
                        consumerIdParam.Value = enrollment.ConsumerId;
                        consumerIdParam.Direction = ParameterDirection.Input;

                        command.Parameters.Add(consumerIdParam);
                    }
                    else
                    {
                        command.CommandText = "Enrollment_UpdateEnrollment";

                        idParam.Value = enrollment.Id;
                        idParam.Direction = ParameterDirection.Input;
                    }

                    var benefitIdParam = command.CreateParameter();
                    benefitIdParam.ParameterName = "@benefitsOfferingId";
                    benefitIdParam.DbType = DbType.Int32;
                    benefitIdParam.Value = enrollment.BenefitsOfferingId;
                    benefitIdParam.Direction = ParameterDirection.Input;

                    var electionParam = command.CreateParameter();
                    electionParam.ParameterName = "@election";
                    electionParam.DbType = DbType.Decimal;
                    electionParam.Value = enrollment.ElectionAmount;
                    electionParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    command.Parameters.Add(benefitIdParam);
                    command.Parameters.Add(electionParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    result = (int)idParam.Value;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        protected override void Map(IDataRecord record, Enrollment entity)
        {
            if (null == record) throw new ArgumentNullException(nameof(record));

            if (null == entity) throw new ArgumentNullException(nameof(entity));

            entity.Id = (int)record["Id"];
            entity.ElectionAmount = (decimal)record["ElectionAmount"];
            entity.ConsumerId = (Guid)record["ConsumerId"];

            if (!DBNull.Value.Equals(record["BenefitsOfferingId"]))
            {
                entity.BenefitsOfferingId = (int)record["BenefitsOfferingId"];
                entity.BenefitsOffering = new BenefitsOffering
                {
                    Id = (int)record["BenefitsOfferingId"],
                    Name = (string)record["BenefitsOfferingName"],
                    ContributionAmount = (decimal)record["BenefitsOfferingContribution"],
                    BenefitsPackageId = (int)record["BenefitsOfferingPackageId"]
                };

                if (!DBNull.Value.Equals((int)record["BenefitsOfferingPlanTypeId"]))
                {
                    entity.BenefitsOffering.PlanTypeId = (int)record["BenefitsOfferingPlanTypeId"];
                    entity.BenefitsOffering.PlanType = new PlanType
                    {
                        Id = (int)record["BenefitsOfferingPlanTypeId"],
                        Name = (string)record["BenefitsOfferingPlanTypeName"]
                    };
                }
            }
        }
    }
}
