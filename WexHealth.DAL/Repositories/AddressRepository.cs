﻿using System;
using System.Data;
using System.Linq;
using PostSharp.Patterns.Contracts;
using log4net;
using WexHealth.DAL.Common;
using WexHealth.DAL.Models;
using WexHealth.DAL.Repositories.Common;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.DAL.Repositories
{
    public class AddressRepository : RepositoryBase<AddressDAL>, IAddressRepository
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(AddressRepository));

        private DbContextBase _context = new DefaultDbContext();

        public AddressDAL GetAddressById(Guid id)
        {
            AddressDAL result = null;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Address_GetAddressById";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = id;
                    idParam.DbType = DbType.Guid;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public Guid SaveAddress([NotNull] AddressDAL address)
        {
            Guid result = Guid.Empty;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.DbType = DbType.Guid;

                    if (Guid.Empty.Equals(address.Id))
                    {
                        command.CommandText = "Address_CreateAddress";

                        idParam.Value = Guid.Empty;
                        idParam.Direction = ParameterDirection.Output;
                    }
                    else
                    {
                        command.CommandText = "Address_UpdateAddress";

                        idParam.Value = address.Id;
                        idParam.Direction = ParameterDirection.Input;
                    }

                    var streetParam = command.CreateParameter();
                    streetParam.ParameterName = "@street";
                    streetParam.DbType = DbType.String;
                    streetParam.Size = 256;
                    streetParam.Value = address.Street;
                    streetParam.Direction = ParameterDirection.Input;

                    var cityParam = command.CreateParameter();
                    cityParam.ParameterName = "@city";
                    cityParam.DbType = DbType.String;
                    cityParam.Size = 256;
                    cityParam.Value = address.City;
                    cityParam.Direction = ParameterDirection.Input;

                    var stateParam = command.CreateParameter();
                    stateParam.ParameterName = "@state";
                    stateParam.DbType = DbType.String;
                    stateParam.Size = 256;
                    stateParam.Value = address.State;
                    stateParam.Direction = ParameterDirection.Input;

                    var zipCodeParam = command.CreateParameter();
                    zipCodeParam.ParameterName = "@zipCode";
                    zipCodeParam.DbType = DbType.String;
                    zipCodeParam.Size = 256;
                    zipCodeParam.Value = address.ZipCode;
                    zipCodeParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    command.Parameters.Add(streetParam);
                    command.Parameters.Add(cityParam);
                    command.Parameters.Add(stateParam);
                    command.Parameters.Add(zipCodeParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    result = (Guid)idParam.Value;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public bool DeleteAddress(Guid id)
        {
            bool result = false;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Address_DeleteAddress";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = id;
                    idParam.DbType = DbType.Guid;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    result = true;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        protected override void Map([NotNull] IDataRecord record, [NotNull] AddressDAL entity)
        {
            entity.Id = (Guid)record["Id"];
            entity.Street = (string)record["Street"];
            entity.City = (string)record["City"];
            entity.State = (string)record["State"];
            entity.ZipCode = (string)record["ZipCode"];
        }
    }
}
