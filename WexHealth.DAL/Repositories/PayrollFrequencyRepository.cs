﻿using System;
using System.Data;
using System.Collections.Generic;
using log4net;
using WexHealth.Domain.Entities;
using WexHealth.DAL.Common;
using WexHealth.DAL.Repositories.Common;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.DAL.Repositories
{
    public class PayrollFrequencyRepository : RepositoryBase<PayrollFrequency>, IPayrollFrequencyRepository
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(EnrollmentRepository));

        private DbContextBase _context = new DefaultDbContext();

        public IEnumerable<PayrollFrequency> GetPayrollFrequencies()
        {
            IEnumerable<PayrollFrequency> result = null;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "PayrollFrequency_GetPayrollFrequencies";
                    command.CommandType = CommandType.StoredProcedure;

                    connection.Open();

                    result = ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        protected override void Map(IDataRecord record, PayrollFrequency entity)
        {
            entity.Id = (int)record["Id"];
            entity.Name = (string)record["Name"];
            entity.Days = (int)record["Days"];
        }
    }
}
