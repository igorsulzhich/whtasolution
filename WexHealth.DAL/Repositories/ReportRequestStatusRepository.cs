﻿using System;
using System.Data;
using System.Linq;
using log4net;
using WexHealth.DAL.Common;
using WexHealth.DAL.Repositories.Common;
using WexHealth.DAL.Repositories.Common.Interfaces;
using WexHealth.Domain.Entities;

namespace WexHealth.DAL.Repositories
{
    public class ReportRequestStatusRepository : RepositoryBase<ReportRequestStatus>, IReportRequestStatusRepository
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ReportRequestStatusRepository));

        private DbContextBase _context = new DefaultDbContext();

        public ReportRequestStatus GetStatusByName(string name)
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));

            ReportRequestStatus result = null;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "ReportRequestStatus_GetStatusByName";
                    command.CommandType = CommandType.StoredProcedure;

                    var statusNameParam = command.CreateParameter();
                    statusNameParam.ParameterName = "@name";
                    statusNameParam.DbType = DbType.String;
                    statusNameParam.Value = name;
                    statusNameParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(statusNameParam);

                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        protected override void Map(IDataRecord record, ReportRequestStatus entity)
        {
            entity.Id = (int)record["Id"];
            entity.Name = (string)record["Name"];
        }
    }
}
