﻿using System;
using System.Data;
using System.Linq;
using log4net;
using WexHealth.DAL.Common;
using WexHealth.DAL.Models;
using WexHealth.DAL.Repositories.Common;
using WexHealth.DAL.Repositories.Common.Interfaces;
using System.Collections.Generic;

namespace WexHealth.DAL.Repositories
{
    public class SettingConnectionRepository : RepositoryBase<SettingConnectionDAL>, ISettingConnectionRepository
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SettingConnectionRepository));

        private DbContextBase _context = new DefaultDbContext();

        public SettingConnectionDAL GetSettingConnectionById(Guid id)
        {
            SettingConnectionDAL result = null;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "SettingConnection_GetSettingConnectionById";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = id;
                    idParam.DbType = DbType.Guid;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);

                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Get object setting connections info
        /// </summary>
        /// <param name="objectId">Object id</param>
        /// <param name="objectType">Type of object</param>
        /// <param name="settingId">Setting id</param>
        /// <returns>Setting connection to current object</returns>
        public SettingConnectionDAL GetSettingConnectionByObjectSetting(Guid objectId, string objectType, Guid settingId)
        {
            SettingConnectionDAL result = null;

            if (string.IsNullOrEmpty(objectType)) throw new ArgumentNullException(nameof(objectType));

            if (Guid.Empty.Equals(settingId)) throw new ArgumentNullException(nameof(settingId));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "SettingConnection_GetSettingConnectionsByObjectSetting";
                    command.CommandType = CommandType.StoredProcedure;

                    if (!Guid.Empty.Equals(objectId))
                    {
                        var objectIdParam = command.CreateParameter();
                        objectIdParam.ParameterName = "@objectId";
                        objectIdParam.Value = objectId;
                        objectIdParam.DbType = DbType.Guid;
                        objectIdParam.Direction = ParameterDirection.Input;

                        command.Parameters.Add(objectIdParam);
                    }

                    var objectTypeParam = command.CreateParameter();
                    objectTypeParam.ParameterName = "@objectType";
                    objectTypeParam.Value = objectType;
                    objectTypeParam.DbType = DbType.String;
                    objectTypeParam.Size = 256;
                    objectTypeParam.Direction = ParameterDirection.Input;

                    var settingIdParam = command.CreateParameter();
                    settingIdParam.ParameterName = "@settingId";
                    settingIdParam.Value = settingId;
                    settingIdParam.DbType = DbType.Guid;
                    settingIdParam.Direction = ParameterDirection.Input;
                    
                    command.Parameters.Add(objectTypeParam);
                    command.Parameters.Add(settingIdParam);

                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Get object setting connections info
        /// </summary>
        /// <param name="objectId">Object id</param>
        /// <param name="objectType">Type of object</param>
        /// <returns>Setting connections to current object</returns>
        public IEnumerable<SettingConnectionDAL> GetSettingConnectionsByObject(Guid objectId, string objectType)
        {
            IEnumerable<SettingConnectionDAL> result = null;

            if (Guid.Empty.Equals(objectId)) throw new ArgumentNullException(nameof(objectId));

            if (Guid.Empty.Equals(objectType)) throw new ArgumentNullException(nameof(objectType));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "SettingConnection_GetSettingConnectionsByObject";
                    command.CommandType = CommandType.StoredProcedure;

                    var objectIdParam = command.CreateParameter();
                    objectIdParam.ParameterName = "@objectId";
                    objectIdParam.Value = objectId;
                    objectIdParam.DbType = DbType.Guid;
                    objectIdParam.Direction = ParameterDirection.Input;

                    var objectTypeParam = command.CreateParameter();
                    objectTypeParam.ParameterName = "@objectType";
                    objectTypeParam.Value = objectType;
                    objectTypeParam.DbType = DbType.String;
                    objectTypeParam.Size = 256;
                    objectTypeParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(objectIdParam);
                    command.Parameters.Add(objectTypeParam);

                    connection.Open();

                    result = ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public Guid SaveSettingConnection(SettingConnectionDAL settingConnection)
        {
            Guid result = Guid.Empty;

            if (null == settingConnection) throw new ArgumentNullException(nameof(settingConnection));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.DbType = DbType.Guid;

                    if (Guid.Empty.Equals(settingConnection.Id))
                    {
                        command.CommandText = "SettingConnection_CreateSettingConnection";

                        idParam.Value = Guid.Empty;
                        idParam.Direction = ParameterDirection.Output;
                    }
                    else
                    {
                        command.CommandText = "SettingConnection_UpdateSettingConnection";

                        idParam.Value = settingConnection.Id;
                        idParam.Direction = ParameterDirection.Input;
                    }

                    var objectIdParam = command.CreateParameter();
                    objectIdParam.ParameterName = "@objectId";
                    objectIdParam.Value = settingConnection.ObjectId;
                    objectIdParam.DbType = DbType.Guid;
                    objectIdParam.Direction = ParameterDirection.Input;

                    var objectTypeParam = command.CreateParameter();
                    objectTypeParam.ParameterName = "@objectType";
                    objectTypeParam.Value = settingConnection.ObjectType;
                    objectTypeParam.DbType = DbType.String;
                    objectTypeParam.Size = 256;
                    objectTypeParam.Direction = ParameterDirection.Input;

                    var valueParam = command.CreateParameter();
                    valueParam.ParameterName = "@value";
                    valueParam.Value = settingConnection.Value;
                    valueParam.DbType = DbType.String;
                    valueParam.Direction = ParameterDirection.Input;

                    var settingIdParam = command.CreateParameter();
                    settingIdParam.ParameterName = "@settingId";
                    settingIdParam.Value = settingConnection.SettingId;
                    settingIdParam.DbType = DbType.Guid;
                    settingIdParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    command.Parameters.Add(objectIdParam);
                    command.Parameters.Add(objectTypeParam);
                    command.Parameters.Add(valueParam);
                    command.Parameters.Add(settingIdParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    result = (Guid)idParam.Value;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public bool DeleteSettingConnection(Guid id)
        {
            bool result = false;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "SettingConnection_DeleteSettingConnection";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = id;
                    idParam.DbType = DbType.Guid;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    result = true;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        protected override void Map(IDataRecord record, SettingConnectionDAL entity)
        {
            entity.Id = (Guid)record["Id"];

            if (!DBNull.Value.Equals(record["ObjectId"]))
            {
                entity.ObjectId = (Guid)record["ObjectId"];
            }

            entity.ObjectType = (string)record["ObjectType"];
            entity.Value = (string)record["Value"];

            if (!DBNull.Value.Equals(record["SettingId"]))
            {
                entity.SettingId = (Guid)record["SettingId"];
                entity.Setting = new SettingDAL
                {
                    Id = (Guid)record["SettingId"],
                    Key = (string)record["Key"]
                };
            }

        }
    }
}
