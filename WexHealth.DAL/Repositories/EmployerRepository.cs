﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using PostSharp.Patterns.Contracts;
using log4net;
using WexHealth.DAL.Common;
using WexHealth.DAL.Models;
using WexHealth.DAL.Repositories.Common;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.DAL.Repositories
{
    public class EmployerRepository : RepositoryBase<EmployerDAL>, IEmployerRepository
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(EmployerRepository));

        protected DbContextBase _context = new DefaultDbContext();

        /// <summary>
        /// Get employer entity by id
        /// </summary>
        /// <param name="id">Employer id</param>
        /// <returns>Employer entity with specified id</returns>
        public EmployerDAL GetEmployerById(Guid id)
        {
            EmployerDAL result = null;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Employer_GetEmployerById";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = id;
                    idParam.DbType = DbType.Guid;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Get employer entity by name
        /// </summary>
        /// <param name="name">Employer name</param>
        /// <returns>Employer entity with specified name</returns>
        public EmployerDAL GetEmployerByName([Required] string name)
        {
            EmployerDAL result = null;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Employer_GetEmployerByName";
                    command.CommandType = CommandType.StoredProcedure;

                    var nameParam = command.CreateParameter();
                    nameParam.ParameterName = "@name";
                    nameParam.Value = name;
                    nameParam.DbType = DbType.String;
                    nameParam.Size = 256;
                    nameParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(nameParam);
                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Get employer entity by code
        /// </summary>
        /// <param name="code">Employer code</param>
        /// <returns>Employer entity with specified code</returns>
        public EmployerDAL GetEmployerByCode([Required] string code)
        {
            EmployerDAL result = null;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Employer_GetEmployerByCode";
                    command.CommandType = CommandType.StoredProcedure;

                    var codeParam = command.CreateParameter();
                    codeParam.ParameterName = "@code";
                    codeParam.Value = code;
                    codeParam.DbType = DbType.String;
                    codeParam.Size = 256;
                    codeParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(codeParam);
                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Search employers by name and code
        /// </summary>
        /// <param name="name">Employer name</param>
        /// <param name="code">Employer code</param>
        /// <param name="adminId">Employer's administrator id</param>
        /// <returns></returns>
        public IEnumerable<EmployerDAL> GetEmployersByNameAndCode(string name, string code, Guid adminId)
        {
            IEnumerable<EmployerDAL> result = null;

            if (Guid.Empty.Equals(adminId)) throw new ArgumentNullException(nameof(adminId));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Employer_SearchEmployers";

                    command.CommandType = CommandType.StoredProcedure;

                    if (!String.IsNullOrEmpty(name))
                    {
                        var nameParam = command.CreateParameter();
                        nameParam.ParameterName = "@name";
                        nameParam.Value = name;
                        nameParam.DbType = DbType.String;
                        nameParam.Size = 256;
                        nameParam.Direction = ParameterDirection.Input;

                        command.Parameters.Add(nameParam);
                    }

                    if (!String.IsNullOrEmpty(code))
                    {
                        var codeParam = command.CreateParameter();
                        codeParam.ParameterName = "@code";
                        codeParam.Value = code;
                        codeParam.DbType = DbType.String;
                        codeParam.Size = 256;
                        codeParam.Direction = ParameterDirection.Input;

                        command.Parameters.Add(codeParam);
                    }

                    var adminIdParam = command.CreateParameter();
                    adminIdParam.ParameterName = "@admin";
                    adminIdParam.Value = adminId;
                    adminIdParam.DbType = DbType.Guid;
                    adminIdParam.Direction = ParameterDirection.Input;
                   
                    command.Parameters.Add(adminIdParam);

                    connection.Open();

                    result = ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Get all employers for current administrator
        /// </summary>
        /// <param name="id">Id of current administrator</param>
        /// <returns>List of employer entities related ti cuurent administrator</returns>
        public IEnumerable<EmployerDAL> GetEmployersByAdministrator(Guid id)
        {
            IEnumerable<EmployerDAL> result = null;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Employer_GetEmployerByAdmin";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = id;
                    idParam.DbType = DbType.Guid;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    connection.Open();

                    result = ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }
               
        public Guid SaveEmployer([NotNull] EmployerDAL employer)
        {
            Guid result = Guid.Empty;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.DbType = DbType.Guid;

                    if (employer.Id == Guid.Empty)
                    {
                        command.CommandText = "Employer_CreateEmployer";

                        idParam.Value = Guid.Empty;
                        idParam.Direction = ParameterDirection.Output;
                    }
                    else
                    {
                        command.CommandText = "Employer_UpdateEmployer";

                        idParam.Value = employer.Id;
                        idParam.Direction = ParameterDirection.Input;
                    }

                    var nameParam = command.CreateParameter();
                    nameParam.ParameterName = "@employername";
                    nameParam.Value = employer.Name;
                    nameParam.DbType = DbType.String;
                    nameParam.Size = 256;
                    nameParam.Direction = ParameterDirection.Input;

                    var codeParam = command.CreateParameter();
                    codeParam.ParameterName = "@employercode";
                    codeParam.Value = employer.Code;
                    codeParam.DbType = DbType.String;
                    codeParam.Size = 256;
                    codeParam.Direction = ParameterDirection.Input;

                    var logoParam = (SqlParameter)command.CreateParameter();
                    logoParam.ParameterName = "@logo";
                    logoParam.Value = employer.Logo;
                    logoParam.SqlDbType = SqlDbType.Image;
                    logoParam.Direction = ParameterDirection.Input;

                    var adminIdParam = command.CreateParameter();
                    adminIdParam.ParameterName = "@adminId";
                    adminIdParam.Value = employer.AdministratorId;
                    adminIdParam.DbType = DbType.Guid;
                    adminIdParam.Size = 1024;
                    adminIdParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    command.Parameters.Add(nameParam);
                    command.Parameters.Add(codeParam);
                    command.Parameters.Add(logoParam);
                    command.Parameters.Add(adminIdParam);

                    connection.Open();

                    command.ExecuteNonQuery();

                    result = (Guid)idParam.Value;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Delete employer
        /// </summary>
        /// <param name="id">Employer id</param>
        /// <returns></returns>
        public bool DeleteEmployer(Guid id)
        {
            bool result = false;

            if (Guid.Empty.Equals(id))
            {
                throw new ArgumentNullException("id");
            }

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Employer_DeleteEmployer";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = id;
                    idParam.DbType = DbType.Guid;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    result = true;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Map database stored record to Employer entity
        /// </summary>
        /// <param name="record">Database stored Employer record</param>
        /// <param name="entity">Employer entity</param>
        protected override void Map([NotNull] IDataRecord record, [NotNull] EmployerDAL entity)
        {
            entity.Id = (Guid)record["Id"];
            entity.Name = (string)record["Name"];
            entity.Code = (string)record["Code"];
            entity.AdministratorId = (Guid)record["AdministratorId"];

            if (!DBNull.Value.Equals(record["Logo"]))
            {
                entity.Logo = (byte[])record["Logo"];
            }
        }
    }
}
