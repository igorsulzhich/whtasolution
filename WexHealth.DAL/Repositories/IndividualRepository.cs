﻿using System;
using System.Data;
using System.Linq;
using PostSharp.Patterns.Contracts;
using log4net;
using WexHealth.Domain.Entities;
using WexHealth.DAL.Common;
using WexHealth.DAL.Repositories.Common;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.DAL.Repositories
{
    public class IndividualRepository : RepositoryBase<Individual>, IIndividualRepository
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(IndividualRepository));

        private DbContextBase _context = new DefaultDbContext();

        /// <summary>
        /// Get individual information of user
        /// </summary>
        /// <param name="id">Individual id</param>
        /// <returns></returns>
        public Individual GetIndividualById(Guid id)
        {
            Individual result = null;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Individual_GetIndividualById";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = id;
                    idParam.DbType = DbType.Guid;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);

                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Create or update individual info. Leave individual id equals Guid.Empty to create new individual
        /// </summary>
        /// <param name="individual">Individual entity with filled properties</param>
        /// <returns></returns>
        public Guid SaveIndividual([NotNull] Individual individual)
        {
            Guid result = Guid.Empty;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.DbType = DbType.Guid;

                    if (Guid.Empty.Equals(individual.Id))
                    {
                        command.CommandText = "Individual_CreateIndividual";

                        idParam.Value = Guid.Empty;
                        idParam.Direction = ParameterDirection.Output;
                    }
                    else
                    {
                        command.CommandText = "Individual_UpdateIndividual";

                        idParam.Value = individual.Id;
                        idParam.Direction = ParameterDirection.Input;
                    }

                    var emailParam = command.CreateParameter();
                    emailParam.ParameterName = "@email";
                    emailParam.Value = individual.Email;
                    emailParam.DbType = DbType.String;
                    emailParam.Size = 256;
                    emailParam.Direction = ParameterDirection.Input;

                    var dateBirthParam = command.CreateParameter();
                    dateBirthParam.ParameterName = "@dateBirth";
                    dateBirthParam.Value = individual.DateBirth;
                    dateBirthParam.DbType = DbType.DateTime2;
                    dateBirthParam.Size = 7;
                    dateBirthParam.Direction = ParameterDirection.Input;

                    var firstNameParam = command.CreateParameter();
                    firstNameParam.ParameterName = "@firstName";
                    firstNameParam.Value = individual.FirstName;
                    firstNameParam.DbType = DbType.String;
                    firstNameParam.Size = 256;
                    firstNameParam.Direction = ParameterDirection.Input;

                    var lastNameParam = command.CreateParameter();
                    lastNameParam.ParameterName = "@lastName";
                    lastNameParam.Value = individual.LastName;
                    lastNameParam.DbType = DbType.String;
                    lastNameParam.Size = 256;
                    lastNameParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    command.Parameters.Add(emailParam);
                    command.Parameters.Add(dateBirthParam);
                    command.Parameters.Add(firstNameParam);
                    command.Parameters.Add(lastNameParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    result = (Guid)idParam.Value;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Delete individual info
        /// </summary>
        /// <param name="id">Individual id</param>
        public bool DeleteIndividual(Guid id)
        {
            bool result = false;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Individual_DeleteIndividual";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = id;
                    idParam.DbType = DbType.Guid;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    result = true;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        protected override void Map([NotNull] IDataRecord record, [NotNull] Individual entity)
        {
            entity.Id = (Guid)record["Id"];
            entity.DateBirth = (DateTime)record["DateBirth"];
            entity.FirstName = (string)record["Firstname"];
            entity.LastName = (string)record["Lastname"];

            if (!DBNull.Value.Equals(record["Email"]))
            {
                entity.Email = (string)record["Email"];
            }
        }
    }
}
