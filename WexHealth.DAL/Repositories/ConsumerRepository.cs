﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using PostSharp.Patterns.Contracts;
using log4net;
using WexHealth.DAL.Common;
using WexHealth.DAL.Models;
using WexHealth.DAL.Repositories.Common;
using WexHealth.DAL.Repositories.Common.Interfaces;
using WexHealth.Domain.Entities;

namespace WexHealth.DAL.Repositories
{
    public class ConsumerRepository : RepositoryBase<ConsumerDAL>, IConsumerRepository
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ConsumerRepository));

        protected DbContextBase _context = new DefaultDbContext();

        /// <summary>
        /// Get consumer entity by id
        /// </summary>
        /// <param name="id">Consumer id</param>
        /// <returns>Consumer entity with specified id</returns>
        public ConsumerDAL GetConsumerById(Guid id)
        {
            ConsumerDAL result = null;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Consumer_GetConsumerById";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = id;
                    idParam.DbType = DbType.Guid;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Get consumer entity by social security number
        /// </summary>
        /// <param name="ssn">Social security number</param>
        /// <returns>Consumer entity with specified SSN</returns>
        public ConsumerDAL GetConsumerBySSN([Required] string ssn)
        {
            ConsumerDAL result = null;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Consumer_GetConsumerBySSN";
                    command.CommandType = CommandType.StoredProcedure;

                    var ssnParam = command.CreateParameter();
                    ssnParam.ParameterName = "@ssn";
                    ssnParam.Value = ssn;
                    ssnParam.DbType = DbType.String;
                    ssnParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(ssnParam);
                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Get consumer entity by employer Id
        /// </summary>
        /// <param name="employerId">Current employer Id</param>
        /// <returns></returns>
        public IEnumerable<ConsumerDAL> GetConsumerByEmployer([Required] Guid employerId)
        {
            IEnumerable<ConsumerDAL> result = null;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Consumer_GetConsumerByEmployer";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = employerId;
                    idParam.DbType = DbType.Guid;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    connection.Open();

                    result = ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }


        /// <summary>
        /// Get consumer entity by social individual Id
        /// </summary>
        /// <param name="id">Current individual Id</param>
        /// <returns></returns>
        public ConsumerDAL GetConsumerByIndividualId(Guid id)
        {
            ConsumerDAL result = null;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Consumer_GetConsumerByIndividualId";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = id;
                    idParam.DbType = DbType.Guid;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }


        public IEnumerable<ConsumerDAL> GetConsumersByIndividualData(string firstName, string lastName, string ssn, Guid employerId, Guid administratorId)
        {
            IEnumerable<ConsumerDAL> result = null;

            if (Guid.Empty.Equals(administratorId)) throw new ArgumentNullException(nameof(administratorId));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Consumer_SearchConsumers";
                    command.CommandType = CommandType.StoredProcedure;

                    if (Guid.Empty != employerId)
                    {
                        var employerIdParam = command.CreateParameter();
                        employerIdParam.ParameterName = "@id";
                        employerIdParam.Value = employerId;
                        employerIdParam.DbType = DbType.Guid;
                        employerIdParam.Direction = ParameterDirection.Input;

                        command.Parameters.Add(employerIdParam);
                    }

                    if (!String.IsNullOrEmpty(firstName))
                    {
                        var firstNameParam = command.CreateParameter();
                        firstNameParam.ParameterName = "@firstname";
                        firstNameParam.Value = firstName;
                        firstNameParam.Size = 256;
                        firstNameParam.DbType = DbType.String;
                        firstNameParam.Direction = ParameterDirection.Input;

                        command.Parameters.Add(firstNameParam);
                    }

                    if (!String.IsNullOrEmpty(lastName))
                    {
                        var lastNameParam = command.CreateParameter();
                        lastNameParam.ParameterName = "@lastname";
                        lastNameParam.Value = lastName;
                        lastNameParam.Size = 256;
                        lastNameParam.DbType = DbType.String;
                        lastNameParam.Direction = ParameterDirection.Input;

                        command.Parameters.Add(lastNameParam);
                    }

                    if (!String.IsNullOrEmpty(ssn))
                    {
                        var ssnParam = command.CreateParameter();
                        ssnParam.ParameterName = "@ssn";
                        ssnParam.Value = ssn;
                        ssnParam.Size = 256;
                        ssnParam.DbType = DbType.String;
                        ssnParam.Direction = ParameterDirection.Input;

                        command.Parameters.Add(ssnParam);
                    }

                    var administratorIdParam = command.CreateParameter();
                    administratorIdParam.ParameterName = "@administratorId";
                    administratorIdParam.Value = administratorId;
                    administratorIdParam.DbType = DbType.Guid;
                    administratorIdParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(administratorIdParam);

                    connection.Open();

                    result = ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Create or update consumer. Leave consumer id equals to Guid.Empty to create new consumer
        /// </summary>
        /// <param name="consumer">Consumer entity with filled properties</param>
        /// <returns>Id of inserted consumer</returns>
        public Guid SaveConsumer([NotNull] ConsumerDAL consumer)
        {
            Guid result = Guid.Empty;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.DbType = DbType.Guid;                    

                    if (consumer.Id == Guid.Empty)
                    {
                        command.CommandText = "Consumer_CreateConsumer";

                        idParam.Value = Guid.Empty;
                        idParam.Direction = ParameterDirection.Output;
                    }
                    else
                    {
                        command.CommandText = "Consumer_UpdateConsumer";

                        idParam.Value = consumer.Id;
                        idParam.Direction = ParameterDirection.Input;
                    }                    

                    var ssnParam = command.CreateParameter();
                    ssnParam.ParameterName = "@ssn";
                    ssnParam.Value = consumer.SSN;
                    ssnParam.DbType = DbType.String;
                    ssnParam.Size = 256;
                    ssnParam.Direction = ParameterDirection.Input;

                    var individualIdParam = command.CreateParameter();
                    individualIdParam.ParameterName = "@individualId";
                    individualIdParam.Value = consumer.IndividualId;
                    individualIdParam.DbType = DbType.Guid;
                    individualIdParam.Size = 256;
                    individualIdParam.Direction = ParameterDirection.Input;

                    var emloyerIdParam = command.CreateParameter();
                    emloyerIdParam.ParameterName = "@employerId";
                    emloyerIdParam.Value = consumer.EmployerId;
                    emloyerIdParam.DbType = DbType.Guid;
                    emloyerIdParam.Size = 256;
                    emloyerIdParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    command.Parameters.Add(ssnParam);
                    command.Parameters.Add(individualIdParam);
                    command.Parameters.Add(emloyerIdParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    result = (Guid)idParam.Value;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Delete consumer
        /// </summary>
        /// <param name="id">Consumer id</param>
        /// <returns></returns>
        public bool DeleteConsumer(Guid id)
        {
            bool result = false;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Consumer_DeleteConsumer";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = id;
                    idParam.DbType = DbType.Guid;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    result = true;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Map database stored record to Consumer entity
        /// </summary>
        /// <param name="record">Database stored Consumer record</param>
        /// <param name="entity">Database stored Consumer entity</param>
        protected override void Map([NotNull]  IDataRecord record, [NotNull] ConsumerDAL entity)
        {
            entity.Id = (Guid)record["Id"];
            entity.SSN = (string)record["SSN"];
            entity.EmployerId = (Guid)record["EmployerId"];
            entity.IndividualId = (Guid)record["IndividualId"];

            entity.Employer = new Employer
            {
                Id = (Guid)record["EmployerId"],
                Name = (string)record["Name"],
                Code = (string)record["Code"]
            };

            entity.Individual = new Individual
            {
                Id = (Guid)record["IndividualId"],
                FirstName = (string)record["Firstname"],
                Email = (string)record["Email"],
                LastName = (string)record["Lastname"],
                DateBirth = (DateTime)record["DateBirth"]
            };
        }
    }
}
