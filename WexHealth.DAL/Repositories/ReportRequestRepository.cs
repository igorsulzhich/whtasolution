﻿using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using log4net;
using WexHealth.DAL.Common;
using WexHealth.DAL.Repositories.Common;
using WexHealth.DAL.Repositories.Common.Interfaces;
using WexHealth.Domain.Entities;

namespace WexHealth.DAL.Repositories
{
    public class ReportRequestRepository : RepositoryBase<ReportRequest>, IReportRequestRepository
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ReportRequestRepository));

        private DbContextBase _context = new DefaultDbContext();

        public ReportRequest GetRequestFirstStatus(int statusId)
        {
            if (statusId <= 0) throw new ArgumentOutOfRangeException(nameof(statusId));

            ReportRequest result = null;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "ReportRequest_GetRequestFirstStatus";
                    command.CommandType = CommandType.StoredProcedure;

                    var statusIdParam = command.CreateParameter();
                    statusIdParam.ParameterName = "@statusId";
                    statusIdParam.DbType = DbType.Int32;
                    statusIdParam.Value = statusId;
                    statusIdParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(statusIdParam);

                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public IEnumerable<ReportRequest> GetRequestByType(string type)
        {
            if (string.IsNullOrEmpty(type)) throw new ArgumentNullException(nameof(type));

            IEnumerable<ReportRequest> result = null;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "ReportRequest_GetRequestByType";
                    command.CommandType = CommandType.StoredProcedure;

                    var typeParam = command.CreateParameter();
                    typeParam.ParameterName = "@type";
                    typeParam.DbType = DbType.String;
                    typeParam.Value = type;
                    typeParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(typeParam);

                    connection.Open();

                    result = ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public IEnumerable<ReportRequest> GetRequestByAdministrator(Guid administratorId, string requestType)
        {
            IEnumerable<ReportRequest> result = null;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "ReportRequest_GetRequestsByAdministratorId";
                    command.CommandType = CommandType.StoredProcedure;

                    var administratorIdParam = command.CreateParameter();
                    administratorIdParam.ParameterName = "@administratorId";
                    administratorIdParam.DbType = DbType.Guid;
                    administratorIdParam.Value = administratorId;
                    administratorIdParam.Direction = ParameterDirection.Input;

                    var requestTypeParam = command.CreateParameter();
                    requestTypeParam.ParameterName = "@requestType";
                    requestTypeParam.DbType = DbType.String;
                    requestTypeParam.Value = requestType;
                    requestTypeParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(administratorIdParam);
                    command.Parameters.Add(requestTypeParam);

                    connection.Open();

                    result = ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public ReportRequest GetRequestById(int id)
        {
            ReportRequest result = null;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "ReportRequest_GetRequestById";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.DbType = DbType.Int32;
                    idParam.Value = id;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);

                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        /// <summary>
        /// Create or update report request
        /// </summary>
        /// <param name="request">Input ReportRequest entity</param>
        /// <returns></returns>
        public int SaveRequest(ReportRequest request)
        {
            int result;

            if (null == request) throw new ArgumentNullException(nameof(request));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.DbType = DbType.Int32;

                    if (request.Id == 0)
                    {
                        command.CommandText = "ReportRequest_CreateRequest";

                        idParam.Value = 0;
                        idParam.Direction = ParameterDirection.Output;
                    }
                    else
                    {
                        command.CommandText = "ReportRequest_UpdateRequest";

                        idParam.Value = request.Id;
                        idParam.Direction = ParameterDirection.Input;
                    }

                    var formatParam = command.CreateParameter();
                    formatParam.ParameterName = "@format";
                    formatParam.DbType = DbType.String;
                    formatParam.Value = request.FormatReport;
                    formatParam.Direction = ParameterDirection.Input;

                    var dateOfRequestParam = command.CreateParameter();
                    dateOfRequestParam.ParameterName = "@dateOfRequest";
                    dateOfRequestParam.DbType = DbType.DateTime2;
                    dateOfRequestParam.Size = 7;
                    dateOfRequestParam.Value = request.DateOfRequest;
                    dateOfRequestParam.Direction = ParameterDirection.Input;

                    var requestedByParam = command.CreateParameter();
                    requestedByParam.ParameterName = "@requestedBy";
                    requestedByParam.DbType = DbType.String;
                    requestedByParam.Value = request.RequestedBy;
                    requestedByParam.Direction = ParameterDirection.Input;

                    var statusIdParam = command.CreateParameter();
                    statusIdParam.ParameterName = "@statusId";
                    statusIdParam.DbType = DbType.Int32;
                    statusIdParam.Value = request.StatusId;
                    statusIdParam.Direction = ParameterDirection.Input;

                    var requestTypeParam = command.CreateParameter();
                    requestTypeParam.ParameterName = "@requestType";
                    requestTypeParam.DbType = DbType.String;
                    requestTypeParam.Value = request.RequestType;
                    requestTypeParam.Direction = ParameterDirection.Input;

                    var administratorIdParam = command.CreateParameter();
                    administratorIdParam.ParameterName = "@administratorId";
                    administratorIdParam.DbType = DbType.Guid;
                    administratorIdParam.Value = request.AdministratorId;
                    administratorIdParam.Direction = ParameterDirection.Input;

                    if (!Guid.Empty.Equals(request.EmployerId))
                    {
                        var employerIdParam = command.CreateParameter();
                        employerIdParam.ParameterName = "@employerId";
                        employerIdParam.DbType = DbType.Guid;
                        employerIdParam.Value = request.EmployerId;
                        employerIdParam.Direction = ParameterDirection.Input;

                        command.Parameters.Add(employerIdParam);
                    }

                    if (!string.IsNullOrEmpty(request.FilePath))
                    {
                        var fileParam = command.CreateParameter();
                        fileParam.ParameterName = "@filePath";
                        fileParam.DbType = DbType.String;
                        fileParam.Value = request.FilePath;
                        fileParam.Direction = ParameterDirection.Input;

                        command.Parameters.Add(fileParam);
                    }

                    command.Parameters.Add(idParam);
                    command.Parameters.Add(formatParam);
                    command.Parameters.Add(dateOfRequestParam);
                    command.Parameters.Add(requestedByParam);
                    command.Parameters.Add(requestTypeParam);
                    command.Parameters.Add(statusIdParam);                    
                    command.Parameters.Add(administratorIdParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    result = (int)idParam.Value;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public bool DeleteRequest(int id)
        {
            bool result = false;

            if (id <= 0) throw new ArgumentOutOfRangeException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "ReportRequest_DeleteRequest";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = id;
                    idParam.DbType = DbType.Int32;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    result = true;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        protected override void Map(IDataRecord record, ReportRequest entity)
        {
            entity.Id = (int)record["Id"];
            entity.FormatReport = (string)record["FormatReport"];
            entity.DateOfRequest = (DateTime)record["DateOfRequest"];
            entity.RequestedBy = (string)record["RequestedBy"];
            entity.RequestType = (string)record["RequestType"];
            entity.StatusId = (int)record["StatusId"];            
            entity.Status = new ReportRequestStatus
            {
                Id = (int)record["StatusId"],
                Name = (string)record["StatusName"]
            };

            if (!DBNull.Value.Equals(record["FilePath"]))
            {
                entity.FilePath = (string)record["FilePath"];
            }

            if (!DBNull.Value.Equals(record["EmployerId"]))
            {
                entity.EmployerId = (Guid)record["EmployerId"];
            }

            entity.AdministratorId = (Guid)record["AdministratorId"];
        }
    }
}
