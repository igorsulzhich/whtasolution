﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using PostSharp.Patterns.Contracts;
using log4net;
using WexHealth.Domain.Entities;
using WexHealth.DAL.Common;
using WexHealth.DAL.Models;
using WexHealth.DAL.Repositories.Common;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.DAL.Repositories
{
    public class EmployerUserRepository : RepositoryBase<EmployerUserDAL>, IEmployerUserRepository
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(EmployerUserRepository));

        private DbContextBase _context = new DefaultDbContext();

        public EmployerUserDAL GetUser(Guid userId)
        {
            return GetUser(userId, null);
        }

        public EmployerUserDAL GetUser(Guid userId, Guid? employerId)
        {
            EmployerUserDAL result = null;

            if (Guid.Empty.Equals(userId)) throw new ArgumentNullException(nameof(userId));

            if (employerId.HasValue && Guid.Empty.Equals(employerId)) throw new ArgumentNullException(nameof(employerId));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "EmployerUser_GetUser";
                    command.CommandType = CommandType.StoredProcedure;

                    var userIdParam = command.CreateParameter();
                    userIdParam.ParameterName = "@userId";
                    userIdParam.Value = userId;
                    userIdParam.DbType = DbType.Guid;
                    userIdParam.Direction = ParameterDirection.Input;

                    if (employerId.HasValue)
                    {
                        var employerIdParam = command.CreateParameter();
                        employerIdParam.ParameterName = "@employerId";
                        employerIdParam.Value = employerId;
                        employerIdParam.DbType = DbType.Guid;
                        employerIdParam.Direction = ParameterDirection.Input;

                        command.Parameters.Add(employerIdParam);
                    }

                    command.Parameters.Add(userIdParam);

                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public IEnumerable<EmployerUserDAL> GetUsers(Guid employerId)
        {
            IEnumerable<EmployerUserDAL> result = null;

            if (Guid.Empty.Equals(employerId)) throw new ArgumentNullException(nameof(employerId));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "EmployerUser_GetUsers";
                    command.CommandType = CommandType.StoredProcedure;

                    var employerIdParam = command.CreateParameter();
                    employerIdParam.ParameterName = "@employerId";
                    employerIdParam.Value = employerId;
                    employerIdParam.DbType = DbType.Guid;
                    employerIdParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(employerIdParam);

                    connection.Open();

                    result = ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public Guid SaveUser([NotNull] EmployerUserDAL user)
        {
            Guid result = Guid.Empty;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "EmployerUser_CreateUser";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = Guid.Empty;
                    idParam.DbType = DbType.Guid;
                    idParam.Direction = ParameterDirection.Output;

                    var userIdParam = command.CreateParameter();
                    userIdParam.ParameterName = "@userId";
                    userIdParam.Value = user.UserId;
                    userIdParam.DbType = DbType.Guid;
                    userIdParam.Direction = ParameterDirection.Input;

                    var employerIdParam = command.CreateParameter();
                    employerIdParam.ParameterName = "@employerId";
                    employerIdParam.Value = user.EmployerId;
                    employerIdParam.DbType = DbType.Guid;
                    employerIdParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    command.Parameters.Add(userIdParam);
                    command.Parameters.Add(employerIdParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    result = (Guid)idParam.Value;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        protected override void Map([NotNull] IDataRecord record, [NotNull] EmployerUserDAL entity)
        {
            entity.Id = (Guid)record["Id"];
            entity.EmployerId = (Guid)record["EmployerId"];

            if (!DBNull.Value.Equals(record["UserId"]))
            {
                entity.UserId = (Guid)record["UserId"];
                entity.User = new User
                {
                    Id = (Guid)record["UserId"],
                    UserName = (string)record["Username"],
                    SecurityDomainId = (Guid)record["SecurityDomainId"]
                };

                if (!DBNull.Value.Equals(record["IndividualId"]))
                {
                    entity.User.IndividualId = (Guid)record["IndividualId"];
                    entity.User.Individual = new Individual
                    {
                        Id = (Guid)record["IndividualId"],
                        FirstName = (string)record["Firstname"],
                        LastName = (string)record["Lastname"],
                        Email = (string)record["Email"],
                        DateBirth = (DateTime)record["DateBirth"]
                    };
                }
            }
        }
    }
}
