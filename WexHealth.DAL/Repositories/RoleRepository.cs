﻿using System;
using System.Collections.Generic;
using System.Data;
using log4net;
using WexHealth.Domain.Entities;
using WexHealth.DAL.Common;
using WexHealth.DAL.Repositories.Common;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.DAL.Repositories
{
    public class RoleRepository : RepositoryBase<Role>, IRoleRepository
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(RoleRepository));

        private DbContextBase _context = new DefaultDbContext();

        public IEnumerable<Role> GetAllRoles()
        {
            var connection = _context.GetConnection();

            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Role_GetAllRoles";
                    command.CommandType = CommandType.StoredProcedure;

                    connection.Open();
                    command.ExecuteNonQuery();

                    return ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }
        }

        public bool IsInRole(string username, string rolename)
        {
            var connection = _context.GetConnection();
            bool userIsInRole = true;

            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Role_IsInRole";
                    command.CommandType = CommandType.StoredProcedure;

                    var userName = command.CreateParameter();
                    userName.ParameterName = "@username";
                    userName.Value = username;
                    userName.DbType = DbType.String;
                    userName.Size = 256;
                    userName.Direction = ParameterDirection.Input;

                    var roleName = command.CreateParameter();
                    roleName.ParameterName = "@rolename";
                    roleName.Value = rolename;
                    roleName.DbType = DbType.String;
                    roleName.Size = 256;
                    roleName.Direction = ParameterDirection.Input;

                    var usernameOUT = command.CreateParameter();
                    usernameOUT.ParameterName = "@usernameOut";
                    usernameOUT.DbType = DbType.String;
                    usernameOUT.Size = 256;
                    usernameOUT.Direction = ParameterDirection.Output;

                    command.Parameters.Add(userName);
                    command.Parameters.Add(roleName);
                    command.Parameters.Add(usernameOUT);

                    connection.Open();
                    command.ExecuteNonQuery();

                    string usernameResult = usernameOUT.Value != DBNull.Value ? (string)usernameOUT.Value : string.Empty;

                    if (string.IsNullOrEmpty(usernameResult))
                    {
                        userIsInRole = false;
                    }
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return userIsInRole;
        }

        public IEnumerable<Role> GetRolesForUser(string username)
        {
            var connection = _context.GetConnection();

            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Role_GetRolesForUser";
                    command.CommandType = CommandType.StoredProcedure;

                    var userName = command.CreateParameter();
                    userName.ParameterName = "@username";
                    userName.Value = username;
                    userName.DbType = DbType.String;
                    userName.Size = 256;
                    userName.Direction = ParameterDirection.Input;

                    command.Parameters.Add(userName);

                    connection.Open();
                    command.ExecuteNonQuery();

                    return ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }
        }

        public Guid SaveRole(string roleName)
        {
            Guid result = Guid.Empty;

            if (null == roleName) throw new ArgumentNullException(nameof(roleName));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Role_CreateRole";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.DbType = DbType.Guid;
                    idParam.Value = Guid.Empty;
                    idParam.Direction = ParameterDirection.Output;

                    var userrole = command.CreateParameter();
                    userrole.ParameterName = "@userrole";
                    userrole.Value = roleName;
                    userrole.DbType = DbType.String;
                    userrole.Size = 256;
                    userrole.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    command.Parameters.Add(userrole);

                    connection.Open();

                    command.ExecuteNonQuery();

                    result = (Guid)idParam.Value;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public void AddUsersToRoles(string[] usernames, string[] rolenames)
        {
            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Role_AddUsersToRoles";
                    command.CommandType = CommandType.StoredProcedure;

                    var username = command.CreateParameter();
                    username.ParameterName = "@username";
                    username.DbType = DbType.String;
                    username.Size = 256;
                    username.Direction = ParameterDirection.Input;

                    var rolename = command.CreateParameter();
                    rolename.ParameterName = "@rolename";
                    rolename.DbType = DbType.String;
                    rolename.Size = 256;
                    rolename.Direction = ParameterDirection.Input;

                    command.Parameters.Add(username);
                    command.Parameters.Add(rolename);

                    connection.Open();

                    foreach (string user in usernames)
                    {
                        foreach (string role in rolenames)
                        {
                            username.Value = username;
                            rolename.Value = rolename;
                            command.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }
        }

        public bool RoleExists(string roleName)
        {
            var connection = _context.GetConnection();
            bool existingRole = false;
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Role_RoleExists";
                    command.CommandType = CommandType.StoredProcedure;

                    var userrole = command.CreateParameter();
                    userrole.ParameterName = "@roleName";
                    userrole.Value = roleName;
                    userrole.DbType = DbType.String;
                    userrole.Size = 256;
                    userrole.Direction = ParameterDirection.Input;

                    command.Parameters.Add(userrole);

                    connection.Open();

                    int resultRole = (int)command.ExecuteScalar();

                    if (resultRole > 0)
                    {
                        existingRole = true;
                    }
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return existingRole;
        }

        protected override void Map(IDataRecord record, Role entity)
        {
            entity.Id = (Guid)record["Id"];
            entity.Name = (string)record["Name"];
            entity.Notes = (string)record["Notes"];
        }
    }
}
