﻿using System;
using System.Data;
using System.Linq;
using PostSharp.Patterns.Contracts;
using log4net;
using WexHealth.DAL.Common;
using WexHealth.DAL.Models;
using WexHealth.DAL.Repositories.Common;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.DAL.Repositories
{
    public class PhoneRepository : RepositoryBase<PhoneDAL>, IPhoneRepository
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(PhoneRepository));

        private DbContextBase _context = new DefaultDbContext();

        public PhoneDAL GetPhoneById(Guid id)
        {
            PhoneDAL result = null;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Phone_GetPhoneById";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = id;
                    idParam.DbType = DbType.Guid;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);

                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public PhoneDAL GetPhoneByNumber([Required] string phoneNumber)
        {
            PhoneDAL result = null;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "GetPhoneByNumber";
                    command.CommandType = CommandType.StoredProcedure;

                    var phoneParam = command.CreateParameter();
                    phoneParam.ParameterName = "@phoneNumber";
                    phoneParam.Value = phoneNumber;
                    phoneParam.DbType = DbType.String;
                    phoneParam.Size = 256;
                    phoneParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(phoneParam);

                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public Guid SavePhone([NotNull] PhoneDAL phone)
        {
            Guid result = Guid.Empty;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.DbType = DbType.Guid;

                    if (Guid.Empty.Equals(phone.Id))
                    {
                        command.CommandText = "Phone_CreatePhone";

                        idParam.Value = Guid.Empty;
                        idParam.Direction = ParameterDirection.Output;
                    }
                    else
                    {
                        command.CommandText = "Phone_UpdatePhone";

                        idParam.Value = phone.Id;
                        idParam.Direction = ParameterDirection.Input;
                    }

                    var phoneParam = command.CreateParameter();
                    phoneParam.ParameterName = "@phoneNumber";
                    phoneParam.Value = phone.PhoneNumber;
                    phoneParam.DbType = DbType.String;
                    phoneParam.Size = 256;
                    phoneParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    command.Parameters.Add(phoneParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    result = (Guid)idParam.Value;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public bool DeletePhone(Guid id)
        {
            bool result = false;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Phone_DeletePhone";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = id;
                    idParam.DbType = DbType.Guid;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    result = true;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        protected override void Map([NotNull] IDataRecord record, [NotNull] PhoneDAL entity)
        {
            entity.Id = (Guid)record["Id"];
            entity.PhoneNumber = (string)record["PhoneNumber"];
        }
    }
}
