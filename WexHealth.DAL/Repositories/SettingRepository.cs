﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using log4net;
using WexHealth.DAL.Common;
using WexHealth.DAL.Models;
using WexHealth.DAL.Repositories.Common;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.DAL.Repositories
{
    public class SettingRepository : RepositoryBase<SettingDAL>, ISettingRepository
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SettingRepository));

        private DbContextBase _context = new DefaultDbContext();

        public SettingDAL GetSettingById(Guid id)
        {
            SettingDAL result = null;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Setting_GetSettingById";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = id;
                    idParam.DbType = DbType.Guid;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);
                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public SettingDAL GetSettingByKey(string key)
        {
            SettingDAL result = null;

            if (Guid.Empty.Equals(key)) throw new ArgumentNullException(nameof(key));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Setting_GetSettingByKey";
                    command.CommandType = CommandType.StoredProcedure;

                    var keyParam = command.CreateParameter();
                    keyParam.ParameterName = "@key";
                    keyParam.Value = key;
                    keyParam.DbType = DbType.String;
                    keyParam.Size = 256;
                    keyParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(keyParam);
                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public IEnumerable<SettingDAL> GetSettingsByParent(Guid parentId)
        {
            IEnumerable<SettingDAL> result = null;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Setting_GetSettingsByParentId";
                    command.CommandType = CommandType.StoredProcedure;

                    if (!Guid.Empty.Equals(parentId))
                    {
                        var parentIdParam = command.CreateParameter();
                        parentIdParam.ParameterName = "@parentId";
                        parentIdParam.Value = parentId;
                        parentIdParam.DbType = DbType.Guid;
                        parentIdParam.Direction = ParameterDirection.Input;

                        command.Parameters.Add(parentIdParam);
                    }

                    connection.Open();

                    result = ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public Guid SaveSetting(SettingDAL setting)
        {
            Guid result = Guid.Empty;

            if (null == setting) throw new ArgumentNullException(nameof(setting));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.DbType = DbType.Guid;

                    if (Guid.Empty.Equals(setting.Id))
                    {
                        command.CommandText = "Setting_CreateSetting";

                        idParam.Value = Guid.Empty;
                        idParam.Direction = ParameterDirection.Output;
                    }
                    else
                    {
                        command.CommandText = "Setting_UpdateSetting";

                        idParam.Value = setting.Id;
                        idParam.Direction = ParameterDirection.Input;
                    }

                    var keyParam = command.CreateParameter();
                    keyParam.ParameterName = "@key";
                    keyParam.Value = setting.Key;
                    keyParam.DbType = DbType.String;
                    keyParam.Size = 256;
                    keyParam.Direction = ParameterDirection.Input;

                    if (!Guid.Empty.Equals(setting.ParentId))
                    {
                        var parentIdParam = command.CreateParameter();
                        keyParam.ParameterName = "@parentId";
                        keyParam.Value = setting.ParentId;
                        keyParam.DbType = DbType.Guid;
                        keyParam.Direction = ParameterDirection.Input;

                        command.Parameters.Add(parentIdParam);
                    }

                    command.Parameters.Add(idParam);
                    command.Parameters.Add(keyParam);                    

                    connection.Open();
                    command.ExecuteNonQuery();

                    result = (Guid)idParam.Value;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public bool DeleteSetting(Guid id)
        {
            bool result = false;

            if (Guid.Empty.Equals(id)) throw new ArgumentNullException(nameof(id));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Setting_DeleteSetting";
                    command.CommandType = CommandType.StoredProcedure;

                    var idParam = command.CreateParameter();
                    idParam.ParameterName = "@id";
                    idParam.Value = id;
                    idParam.DbType = DbType.Guid;
                    idParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(idParam);

                    connection.Open();
                    command.ExecuteNonQuery();

                    result = true;
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        protected override void Map(IDataRecord record, SettingDAL entity)
        {
            entity.Id = (Guid)record["Id"];
            entity.Key = (string)record["Key"];

            if (!DBNull.Value.Equals(record["SettingId"]))
            {
                entity.ParentId = (Guid)record["SettingId"];
            }
        }
    }
}
