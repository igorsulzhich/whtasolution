﻿using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using log4net;
using WexHealth.DAL.Common;
using WexHealth.DAL.Repositories.Common;
using WexHealth.DAL.Repositories.Common.Interfaces;
using WexHealth.Domain.Entities;

namespace WexHealth.DAL.Repositories
{
    public class ClaimStatusRepository : RepositoryBase<ClaimStatus>, IClaimStatusRepository
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ClaimStatusRepository));

        private DbContextBase _context = new DefaultDbContext();

        public ClaimStatus GetClaimStatus(string name)
        {
            ClaimStatus result = null;

            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "ClaimStatus_GetClaimStatusByName";
                    command.CommandType = CommandType.StoredProcedure;

                    var nameParam = command.CreateParameter();
                    nameParam.ParameterName = "@name";
                    nameParam.Value = name;
                    nameParam.DbType = DbType.String;
                    nameParam.Size = 256;
                    nameParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(nameParam);
                    connection.Open();

                    result = ToList(command).SingleOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        public IEnumerable<ClaimStatus> GetClaimStatuses()
        {
            IEnumerable<ClaimStatus> result = null;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "ClaimStatus_GetClaimStatuses";
                    command.CommandType = CommandType.StoredProcedure;

                    connection.Open();

                    result = ToList(command);
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        protected override void Map(IDataRecord record, ClaimStatus entity)
        {
            if (null == record) throw new ArgumentNullException(nameof(record));

            if (null == entity) throw new ArgumentNullException(nameof(entity));

            entity.Id = (int)record["Id"];
            entity.Name = (string)record["Name"];
        }
    }
}
