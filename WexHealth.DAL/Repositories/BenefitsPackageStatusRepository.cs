﻿using System;
using System.Linq;
using System.Data;
using log4net;
using WexHealth.Domain.Entities;
using WexHealth.DAL.Common;
using WexHealth.DAL.Repositories.Common;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.DAL.Repositories
{
    public class BenefitsPackageStatusRepository : RepositoryBase<BenefitsPackageStatus>, IBenefitsPackageStatusRepository
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(BenefitsPackageStatusRepository));

        private DbContextBase _context = new DefaultDbContext();

        public BenefitsPackageStatus GetStatusByName(string name)
        {
            BenefitsPackageStatus result = null;

            var connection = _context.GetConnection();
            try
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "BenefitsPackageStatus_GetStatusByName";
                    command.CommandType = CommandType.StoredProcedure;

                    var nameParam = command.CreateParameter();
                    nameParam.ParameterName = "@name";
                    nameParam.Value = name;
                    nameParam.DbType = DbType.String;
                    nameParam.Size = 256;
                    nameParam.Direction = ParameterDirection.Input;

                    command.Parameters.Add(nameParam);

                    connection.Open();

                    result = ToList(command).FirstOrDefault();
                }
            }
            catch (Exception exception)
            {
                log.Error("Database error", exception);
                throw;
            }
            finally
            {
                connection.Dispose();
            }

            return result;
        }

        protected override void Map(IDataRecord record, BenefitsPackageStatus entity)
        {
            entity.Id = (int)record["Id"];
            entity.Name = (string)record["Name"];
        }
    }
}
