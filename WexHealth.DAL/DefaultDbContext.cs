﻿using WexHealth.DAL.Common;

namespace WexHealth.DAL
{
    public class DefaultDbContext : DbContextBase
    {
        public DefaultDbContext() : base("DefaultConnection") { }
    }
}
