﻿using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using WexHealth.DAL.Common.Interfaces;
using WexHealth.DAL.Common.Exceptions;

namespace WexHealth.DAL.Common
{
    public abstract class DbContextBase : IDbContextBase
    {
        private readonly DbProviderFactory _providerFactory;
        private readonly string _providerName;
        private readonly string _connectionString;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionStringName">Connection string to data source</param>
        public DbContextBase(string connectionStringName)
        {
            if (string.IsNullOrEmpty(connectionStringName)) throw new ArgumentNullException("connectionStringName", "ConnectionStringName can not be null.");

            var connectionString = ConfigurationManager.ConnectionStrings[connectionStringName];

            if (connectionString == null) throw new ConfigurationErrorsException(string.Format("Connection string {0} doesn't exists in web.config", connectionStringName));

            _providerFactory = DbProviderFactories.GetFactory(connectionString.ProviderName);
            _providerName = connectionString.ProviderName;
            _connectionString = connectionString.ConnectionString;
        }

        /// <summary>
        /// Open connection to data source and return an instance of <seealso cref="DbConnection"/>
        /// </summary>
        public IDbConnection GetConnection()
        {
            var connection = _providerFactory.CreateConnection();
            if (connection == null) throw new DbConnectionFailedException(_connectionString);
            connection.ConnectionString = _connectionString;
            return connection;
        }
    }
}
