﻿using System.Data;

namespace WexHealth.DAL.Common.Interfaces
{
    public interface IDbContextBase
    {
        IDbConnection GetConnection();
    }
}
