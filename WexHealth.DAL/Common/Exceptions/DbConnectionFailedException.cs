﻿using System;

namespace WexHealth.DAL.Common.Exceptions
{
    /// <summary>
    /// An exception that occurs when the connection to database can't be created
    /// </summary>
    public class DbConnectionFailedException : Exception
    {
        /// <summary>
        /// Initialize an instance of <seealso cref="DbConnectionFailedException"/>
        /// </summary>
        public DbConnectionFailedException() : base("Connection can't be created using specified connection string")
        {

        }

        /// <summary>
        /// Initialize an instance of <seealso cref="DbConnectionFailedException"/>
        /// </summary>
        /// <param name="connectionString">Connection string that affects the exception</param>
        public DbConnectionFailedException(string connectionString)
            : base(string.Format("Connection can't be created using connection string: {0}", connectionString))
        {

        }

        /// <summary>
        /// Initialize an instance of <seealso cref="DbConnectionFailedException"/>
        /// </summary>
        /// <param name="connectionString">Connection string that affects the exception</param>
        /// <param name="inner">An <seealso cref="Exception"/> instance that caused the current <seealso cref="DbConnectionFailedException"/></param>
        public DbConnectionFailedException(string connectionString, Exception inner)
            : base(string.Format("Connection can't be created using connection string: {0}", connectionString), inner)
        {

        }
    }
}
