﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WexHealth.DAL.Models
{
    public class PhoneDAL
    {
        public Guid Id { get; set; }

        [Required]
        [StringLength(256)]
        public string PhoneNumber { get; set; }
    }
}
