﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WexHealth.DAL.Models
{
    public class SettingConnectionDAL
    {
        public Guid Id { get; set; }

        public Guid ObjectId { get; set; }
        
        [Required]
        [StringLength(256)]
        public string ObjectType { get; set; }

        [Required]
        [StringLength(256)]
        public string Value { get; set; }

        public Guid SettingId { get; set; }

        public virtual SettingDAL Setting { get; set; }
    }
}
