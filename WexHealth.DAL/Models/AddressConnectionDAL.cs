﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WexHealth.DAL.Models
{
    public class AddressConnectionDAL
    {
        public Guid Id { get; set; }

        public Guid ObjectId { get; set; }

        [Required]
        [StringLength(256)]
        public string ObjectType { get; set; }

        [StringLength(256)]
        public string Type { get; set; }

        public Guid AddressId { get; set; }

        public virtual AddressDAL Address { get; set; }
    }
}
