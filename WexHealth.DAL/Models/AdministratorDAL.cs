﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WexHealth.DAL.Models
{
    public class AdministratorDAL
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Enter name")]
        [StringLength(256, ErrorMessage = "Enter name with length under 256 characters")]
        public string Name { get; set; }

        public Guid SecurityDomainId { get; set; }

        public virtual SecurityDomainDAL SecurityDomain { get; set; }
    }
}
