﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WexHealth.DAL.Models
{
    public class StateDAL
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Enter state name")]
        [StringLength(256, ErrorMessage = "Enter state with length under 256 characters")]
        public string Name { get; set; }
    }
}
