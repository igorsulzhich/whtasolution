﻿using System;
using WexHealth.Domain.Entities;

namespace WexHealth.DAL.Models
{
    public class EmployerUserDAL
    {
        public Guid Id { get; set; }

        public Guid EmployerId { get; set; }

        public Guid UserId { get; set; }

        public User User { get; set; }
    }
}
