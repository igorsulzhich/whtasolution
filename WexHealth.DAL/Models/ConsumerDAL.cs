﻿using System;
using System.ComponentModel.DataAnnotations;
using WexHealth.Domain.Entities;

namespace WexHealth.DAL.Models
{
    public class ConsumerDAL
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Enter SSN")]
        [StringLength(256, ErrorMessage = "Enter SSN with length under 256 characters")]
        public string SSN { get; set; }

        public Guid EmployerId { get; set; }

        public Guid IndividualId { get; set; }

        public Employer Employer { get; set; }

        public Individual Individual { get; set; }
    }
}
