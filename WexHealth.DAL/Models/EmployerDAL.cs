﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WexHealth.DAL.Models
{
    public class EmployerDAL
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public byte[] Logo { get; set; }

        public Guid AdministratorId { get; set; }
    }
}
