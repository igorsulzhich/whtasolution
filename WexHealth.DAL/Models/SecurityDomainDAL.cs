﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WexHealth.DAL.Models
{
    public class SecurityDomainDAL
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Enter alias")]
        [StringLength(256, ErrorMessage = "Enter alias with length under 256 characters")]
        public string Alias { get; set; }
    }
}
