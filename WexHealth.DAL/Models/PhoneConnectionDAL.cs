﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WexHealth.DAL.Models
{
    public class PhoneConnectionDAL
    {
        public Guid Id { get; set; }

        public Guid ObjectId { get; set; }

        [Required]
        [StringLength(256)]
        public string ObjectType { get; set; }

        [StringLength(256)]
        public string Type { get; set; }

        public Guid PhoneId { get; set; }

        public virtual PhoneDAL Phone { get; set; }
    }
}
