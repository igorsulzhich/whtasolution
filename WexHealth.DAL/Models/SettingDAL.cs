﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WexHealth.DAL.Models
{
    public class SettingDAL
    {
        public Guid Id { get; set; }

        [Required]
        [StringLength(256)]
        public string Key { get; set; }

        public Guid ParentId { get; set; }
    }
}
