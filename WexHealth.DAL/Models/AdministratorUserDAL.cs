﻿using System;
using WexHealth.Domain.Entities;

namespace WexHealth.DAL.Models
{
    public class AdministratorUserDAL
    {
        public Guid Id { get; set; }

        public Guid AdministratorId { get; set; }

        public Guid UserId { get; set; }

        public User User { get; set; }
    }
}
