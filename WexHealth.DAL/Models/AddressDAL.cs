﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WexHealth.DAL.Models
{
    public class AddressDAL
    {
        public Guid Id { get; set; }

        [Required]
        [StringLength(256)]
        public string Street { get; set; }

        [Required]
        [StringLength(256)]
        public string City { get; set; }

        [Required]
        [StringLength(256)]
        public string State { get; set; }

        [Required]
        [StringLength(256)]
        public string ZipCode { get; set; }
    }
}
