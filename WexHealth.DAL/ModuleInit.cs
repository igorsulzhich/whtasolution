﻿using System.ComponentModel.Composition;
using Autofac;
using System.Reflection;
using WexHealth.Common.Modules.Common.Interfaces;

namespace WexHealth.DAL
{
    [Export(typeof(IModule))]
    public class ModuleInit : IModule
    {
        public void Initialize(ContainerBuilder builder)
        {
            var assembly = Assembly.GetExecutingAssembly();

            builder.RegisterAssemblyTypes(assembly)
                   .Where(x => x.Name.EndsWith("Repository"))
                   .AsImplementedInterfaces();
        }
    }
}
