﻿using System;
using System.Security.Principal;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Models;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Presentation.Views;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.Presentation.Presenters.Common.Interfaces;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;

namespace WexHealth.Presentation.Presenters
{
    public class ManageUsersPresenter : ViewPresenterBase<IManageUsersView>, IManageUsersPresenter
    {
        private readonly IDomainUserService _userService;
        private readonly IDomainService _domainService;

        public ManageUsersPresenter(IManageUsersView view, IPrincipal principal, IDomainUserService userService, IDomainService domainService) :
            base(view, principal)
        {
            _userService = userService;
            _domainService = domainService;
        }

        public ManageUsersPresenter(IManageUsersView view, IDomainUserService userService, IDomainService domainService) :
            base(view, false)
        {
            _userService = userService;
            _domainService = domainService;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            if (!isPostBack)
            {
                LoadUsers();
            }
        }

        private void LoadUsers()
        {
            _view.Users = _userService.GetUsers();
        }

        public void LoadUser()
        {
            Guid id = _view.SelectedUserId;

            if (!Guid.Empty.Equals(id))
            {
                User user = _userService.GetUser(id);
                if (null != user)
                { 
                    Map(user);
                }
            }
        }

        public IResult CreateUser()
        {
            IResult result = null;

            try
            {
                var userModel = new UserInputModel();
                Map(userModel);

                var individualModel = new IndividualInputModel();
                Map(individualModel);

                Guid individualId = _domainService.CreateIndividual(individualModel);
                _userService.CreateUser(userModel, individualId);

                result = new OperationResult(OperationStatus.Success);

                LoadUsers();
            }
            catch (Exception exception)
            {
                result = new OperationResult(OperationStatus.Failed, exception.Message);
            }

            return result;
        }

        public IResult UpdateUser()
        {
            IResult result = null;

            try
            {
                var userModel = new UserInputModel();
                Map(userModel);

                var individualModel = new IndividualInputModel();
                Map(individualModel);

                if (Guid.Empty.Equals(userModel.Id))
                {
                    result = new OperationResult(OperationStatus.Failed, "User was not selected");
                }
                else
                {
                    User user = _userService.GetUser(userModel.Id);
                    if (null == user || Guid.Empty.Equals(user.IndividualId))
                    {
                        result = new OperationResult(OperationStatus.Failed, "Individual id doesn't exist");
                    }
                    else
                    {
                        individualModel.Id = user.IndividualId;
                        _domainService.UpdateIndividual(individualModel);

                        _userService.UpdateUser(userModel);

                        result = new OperationResult(OperationStatus.Success);

                        LoadUsers();
                    }
                }
            }
            catch (Exception exception)
            {
                result = new OperationResult(OperationStatus.Failed, exception.Message);
            }

            return result;
        }

        public void GeneratePassword()
        {
            _view.Password = GetGeneratedPassword();
        }

        public string GetGeneratedPassword()
        {
            return _userService.GeneratePassword();
        }

        //View -> BLL Model
        private void Map(UserInputModel model)
        {
            if (!Guid.Empty.Equals(_view.SelectedUserId))
            {
                model.Id = _view.SelectedUserId;
            }

            model.UserName = _view.UserName;
            model.Password = _view.Password;
        }

        private void Map(IndividualInputModel model)
        {
            model.FirstName = _view.FirstName;
            model.LastName = _view.LastName;
            model.Email = _view.Email;
        }

        //Domain Entity -> View
        private void Map(User user)
        {
            _view.UserName = user.UserName;
            if (null != user.Individual)
            {
                _view.FirstName = user.Individual.FirstName;
                _view.LastName = user.Individual.LastName;
                _view.Email = user.Individual.Email;
            }
        }
    }
}
