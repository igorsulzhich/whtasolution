﻿using System;
using System.Linq;
using System.Collections.Generic;
using log4net;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Web.Cache.Common.Interfaces;
using WexHealth.Presentation.Views;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.Presentation.Presenters.Common.Interfaces;
using WexHealth.Presentation.Presenters.Results;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;

namespace WexHealth.Presentation.Presenters
{
    public class LoginPresenter : ViewPresenterBase<ILoginView>, ILoginPresenter
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(LoginPresenter));

        private readonly ILoginUserService _loginService;

        private readonly ICache _cache;

        public LoginPresenter(ILoginView view, ILoginUserService loginService, ICache cache) :
            base(view, false)
        {
            _loginService = loginService;
            _cache = cache;
        }

        protected override void InternalInitView(bool isPostBack)
        {
            if (!isPostBack)
            {
                LoadDomains();
            }
        }

        private void LoadDomains()
        {
            _cache.Key = "Domains";
            var cacheData = _cache.GetCache();

            if (null == cacheData)
            {
                IEnumerable<SecurityDomain> dbSecurityDomains = _loginService.GetSecurityDomains().OrderBy(x => x.Alias);
                _cache.SetCache(dbSecurityDomains);
            }

            _view.Domains = _cache.GetCache() as IEnumerable<SecurityDomain>;
        }

        public IResult Login()
        {
            LoginResult result = null;

            try
            {
                bool validateResult = _loginService.ValidateUser(_view.UserName, _view.Password, _view.SelectedDomain);
                if (validateResult)
                {
                    result = new LoginResult(LoginStatus.Success);
                }
                else
                {
                    result = new LoginResult(LoginStatus.Failed, "Invalid user credentials");
                }
            }
            catch (InvalidOperationException e)
            {
                result = new LoginResult(LoginStatus.Failed, e.Message);
            }
            catch (Exception e)
            {
                log.Error(e);
                result = new LoginResult(LoginStatus.Failed, "Server error");
            }

            return result;
        }
    }
}
