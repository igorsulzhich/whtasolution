﻿using System.Security.Principal;
using WexHealth.Presentation.Views;
using WexHealth.Presentation.Presenters.Common;
using WexHealth.Presentation.Presenters.Common.Interfaces;

namespace WexHealth.Presentation.Presenters
{
    public class IndexPresenter : ViewPresenterBase<IIndexView>, IIndexPresenter
    {
        public IndexPresenter(IIndexView view, IPrincipal principal) :
            base(view, principal)
        { }

        protected override void InternalInitView(bool isPostBack)
        {
            
        }
    }
}
