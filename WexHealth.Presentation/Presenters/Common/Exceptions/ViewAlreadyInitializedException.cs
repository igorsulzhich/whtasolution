﻿using System;

namespace WexHealth.Presentation.Presenters.Common.Exceptions
{
    /// <summary>
    /// An exception that occurs when the presenter tries to initialize the view more than once.
    /// </summary>
    public sealed class ViewAlreadyInitializedException : Exception
    {
        public ViewAlreadyInitializedException(string message)
            : base(message)
        { }
    }
}
