﻿using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Presentation.Views;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;

namespace WexHealth.Presentation.Presenters.Common.Interfaces
{
    public delegate ILoginPresenter LoginPresenterFactory(ILoginView view, ILoginUserService loginService);

    public interface ILoginPresenter
    {
        void InitView(bool isPostBack);

        IResult Login();
    }
}
