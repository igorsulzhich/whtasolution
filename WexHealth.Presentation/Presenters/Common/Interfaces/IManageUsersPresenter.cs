﻿using System;
using System.Security.Principal;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.Presentation.Views;
using WexHealth.Presentation.Presenters.Results.Common.Interfaces;

namespace WexHealth.Presentation.Presenters.Common.Interfaces
{
    public delegate IManageUsersPresenter ManageUsersPresenterFactory(IManageUsersView view, IDomainUserService userService);

    public delegate IManageUsersPresenter ManageUsersPresenterFactoryWithPrincipal(IManageUsersView view, IPrincipal principal, IDomainUserService userService);

    public interface IManageUsersPresenter
    {
        void InitView(bool isPostBack);

        void LoadUser();

        IResult CreateUser();

        IResult UpdateUser();

        void GeneratePassword();

        string GetGeneratedPassword();
    }
}
