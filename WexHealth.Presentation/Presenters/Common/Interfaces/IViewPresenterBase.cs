﻿namespace WexHealth.Presentation.Presenters.Common.Interfaces
{
    public interface IViewPresenterBase
    {
        void InitView(bool isPostBack);
    }
}
