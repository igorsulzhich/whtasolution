﻿using System.Security.Principal;
using WexHealth.Presentation.Views;

namespace WexHealth.Presentation.Presenters.Common.Interfaces
{
    public delegate IIndexPresenter IndexPresenterFactory(IIndexView view, IPrincipal principal);

    public interface IIndexPresenter
    {
        void InitView(bool isPostBack);
    }
}
