﻿using WexHealth.Presentation.Presenters.Results.Common.Interfaces;

namespace WexHealth.Presentation.Presenters.Results
{
    public class OperationResult : IResult
    {
        private readonly OperationStatus _status;
        private readonly string _message;

        public OperationResult(OperationStatus status)
        {
            _status = status;
        }

        public OperationResult(OperationStatus status, string message) : 
            this(status)
        {
            _message = message;
        }

        public OperationStatus Status
        {
            get
            {
                return _status;
            }
        }

        public string Message
        {
            get
            {
                return _message;
            }
        }
    }

    public enum OperationStatus
    {
        Success, Failed
    }
}
