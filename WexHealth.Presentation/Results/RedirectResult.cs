﻿using WexHealth.Presentation.Presenters.Results.Common.Interfaces;

namespace WexHealth.Presentation.Presenters.Results
{
    public class RedirectResult : IResult
    {
        private readonly string _url;

        public RedirectResult(string url)
        {
            _url = url;
        }

        public string Url
        {
            get
            {
                return _url;
            }
        }
    }
}
