﻿using WexHealth.Presentation.Presenters.Results.Common.Interfaces;

namespace WexHealth.Presentation.Presenters.Results
{
    public class LoginResult : IResult
    {
        public LoginResult(LoginStatus status)
        {
            Status = status;
        }

        public LoginResult(LoginStatus status, string errorMessage) : this(status)
        {
            ErrorMessage = errorMessage;
        }

        public LoginStatus Status { get; set; }

        public string ErrorMessage { get; set; }
    }

    public enum LoginStatus
    {
        Success, Failed
    }
}
