﻿using System.Collections.Generic;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Models;

namespace WexHealth.Presentation.Views
{
    public interface ISettingView
    {
        IEnumerable<Setting> SettingList { set; }

        IEnumerable<SettingInputModel> SelectedSettings { get; }

        void ShowErrorMessage(string errorMessage);
    }
}
