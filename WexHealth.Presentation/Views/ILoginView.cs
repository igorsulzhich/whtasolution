﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.Presentation.Views
{
    public interface ILoginView
    {
        string UserName { get; }

        string Password { get; }

        Guid SelectedDomain { get; }

        IEnumerable<SecurityDomain> Domains { set; }
    }
}
