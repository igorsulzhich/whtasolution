﻿using System;
using System.Collections.Generic;
using WexHealth.Domain.Entities;

namespace WexHealth.Presentation.Views
{
    public interface IManageUsersView
    {
        string FirstName { get; set; }

        string LastName { get; set; }

        string Email { get; set; }

        string UserName { get; set; }

        string Password { get; set; }

        Guid SelectedUserId { get; }

        IEnumerable<User> Users { set; }
    }
}
