﻿using System;
using System.IO;
using System.Linq;
using Microsoft.VisualBasic.FileIO;
using WexHealth.Domain.Entities;
using WexHealth.ImportService.Handlers.Common.Interfaces;

namespace WexHealth.ImportService.Handlers
{
    public class ConsumerFileFormatHandler : IRequestHandler
    {
        public IRequestHandler NextHandler { get; set; }

        public void Process(Import request)
        {
            if (!File.Exists(request.FilePath))
            {
                throw new InvalidOperationException($"File: {request.FilePath}, doesn't exist");
            }

            string extension = Path.GetExtension(request.FilePath);
            if (!extension.Equals(".csv"))
            {
                throw new InvalidOperationException("Invalid file extension. Support extensions: .csv");
            }

            using (var parser = new TextFieldParser(request.FilePath))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                parser.ReadLine();

                while (!parser.EndOfData)
                {
                    string[] values = parser.ReadFields();
                    if (values.Where(x => !string.IsNullOrEmpty(x)).Count() != 10)
                    {
                        throw new InvalidOperationException($"Invalid file content format");
                    }
                }
            }

            if (null != NextHandler)
            {
                NextHandler.Process(request);
            }
        }
    }
}