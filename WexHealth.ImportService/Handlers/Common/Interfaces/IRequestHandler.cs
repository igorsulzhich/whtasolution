﻿using WexHealth.Domain.Entities;

namespace WexHealth.ImportService.Handlers.Common.Interfaces
{
    public interface IRequestHandler
    {
        IRequestHandler NextHandler { get; set; }

        void Process(Import request);
    }
}
