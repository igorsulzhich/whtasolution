﻿using System;
using System.Globalization;
using Microsoft.VisualBasic.FileIO;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Models;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.ImportService.Handlers.Common.Interfaces;

namespace WexHealth.ImportService.Handlers
{
    public class ConsumerImportHandler : IRequestHandler
    {
        public IRequestHandler NextHandler { get; set; }

        private readonly IConsumerService _consumerService;
        private readonly IDomainService _domainService;

        public ConsumerImportHandler(IConsumerService consumerService, IDomainService domainService)
        {
            _consumerService = consumerService;
            _domainService = domainService;
        }

        public void Process(Import request)
        {
            using (var parser = new TextFieldParser(request.FilePath))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                parser.ReadLine();

                while (!parser.EndOfData)
                {
                    string[] values = parser.ReadFields();

                    IndividualInputModel model = new IndividualInputModel
                    {
                        FirstName = values[1],
                        LastName = values[2],
                        Email = values[3],
                        DateBirth = DateTime.Parse(values[9], CultureInfo.InvariantCulture, DateTimeStyles.None)
                    };

                    Guid individualId = _domainService.CreateIndividual(model);
                    ConsumerInputModel consumer = new ConsumerInputModel
                    {
                        SSN = values[0],
                        Address = new AddressInputModel
                        {
                            Street = values[4],
                            City = values[5],
                            State = values[6],
                            ZipCode = values[7]
                        },
                        Phone = new PhoneInputModel
                        {
                            Phone = values[8]
                        }
                    };

                    _consumerService.CreateConsumer(consumer, individualId, request.EmployerId);
                }
            }

            if (null != NextHandler)
            {
                NextHandler.Process(request);
            }
        }
    }
}
