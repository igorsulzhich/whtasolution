﻿using System;
using System.ServiceProcess;
using System.Timers;
using Microsoft.Practices.ServiceLocation;
using WexHealth.Domain.Entities;
using WexHealth.BLL.Services.Common.Interfaces;
using WexHealth.ImportService.Handlers.Common.Interfaces;

namespace WexHealth.ImportService
{
    public partial class Service1 : ServiceBase
    {
        private Timer _timer;
        private DateTime _prevRun;

        private readonly IImportManageService _importService;

        public Service1(IImportManageService importService)
        {
            InitializeComponent();

            _importService = importService;
        }

        protected override void OnStart(string[] args)
        {
            _timer = new Timer(30 * 1000);
            _timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
            _timer.Start();
        }

        protected override void OnStop()
        {
        }

        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _timer.Stop();

            try
            {
                Import dbRequest = _importService.GetAvailableRequest();
                if (null != dbRequest)
                {
                    _importService.ChangeStatus(dbRequest.Id, ImportStatus.Processed);
                    eventsLog.WriteEntry($"Change request (id: {dbRequest.Id}) status to: {ImportStatus.Processed}");

                    try
                    {
                        IRequestHandler fileFormatHandler = ServiceLocator.Current.GetInstance<IRequestHandler>("ConsumerFileFormat");
                        IRequestHandler dataImportHandler = ServiceLocator.Current.GetInstance<IRequestHandler>("ConsumerImport");

                        fileFormatHandler.NextHandler = dataImportHandler;

                        fileFormatHandler.Process(dbRequest);
                    }
                    catch (Exception)
                    {
                        _importService.ChangeStatus(dbRequest.Id, ImportStatus.Failed);
                        eventsLog.WriteEntry($"Change request (id: {dbRequest.Id}) status to: {ImportStatus.Failed}");
                        throw;
                    }

                    _importService.ChangeStatus(dbRequest.Id, ImportStatus.Completed);
                    eventsLog.WriteEntry($"Change request (id: {dbRequest.Id}) status to: {ImportStatus.Completed}");
                }
            }
            catch (Exception ex)
            {
                eventsLog.WriteEntry(ex.Message);
            }

            _prevRun = DateTime.Now;
            _timer.Start();
        }
    }
}
