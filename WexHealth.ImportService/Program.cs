﻿using System.ServiceProcess;
using Microsoft.Practices.ServiceLocation;
using Autofac;
using Autofac.Extras.CommonServiceLocator;
using WexHealth.Common.Modules;
using WexHealth.ImportService.Handlers;
using WexHealth.ImportService.Handlers.Common.Interfaces;

namespace WexHealth.ImportService
{
    static class Program
    {
        private static IContainer _container;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            InitAutofac();

            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                ServiceLocator.Current.GetInstance<Service1>()
            };
            ServiceBase.Run(ServicesToRun);
        }

        private static void InitAutofac()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<Service1>();
            builder.RegisterType<ConsumerFileFormatHandler>().Keyed<IRequestHandler>("ConsumerFileFormat");
            builder.RegisterType<ConsumerImportHandler>().Keyed<IRequestHandler>("ConsumerImport");

            ModuleLoader.LoadContainer(builder, ".", "WexHealth.*.dll");

            _container = builder.Build();

            var commonServiceLocator = new AutofacServiceLocator(_container);
            ServiceLocator.SetLocatorProvider(() => commonServiceLocator);
        }
    }
}
