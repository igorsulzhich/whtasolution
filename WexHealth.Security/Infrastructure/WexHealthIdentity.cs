﻿using System.Security.Principal;
using System.Web.Security;
using Microsoft.Practices.ServiceLocation;
using WexHealth.Domain.Entities;
using WexHealth.DAL.Repositories.Common.Interfaces;

namespace WexHealth.Security.Infrastructure
{
    public class WexHealthIdentity : IIdentity
    {
        private readonly IUserRepository _userRepository = ServiceLocator.Current.GetInstance<IUserRepository>();

        public WexHealthIdentity(FormsIdentity user)
        {
            this.Name = user.Name;
            this.IsAuthenticated = user.IsAuthenticated;
            this.AuthenticationType = user.AuthenticationType;
        }

        public string AuthenticationType { get; }

        public bool IsAuthenticated { get; }

        public string Name { get; }

        public User User
        {
            get
            {
                return _userRepository.GetUserByUserName(Name);
            }
        }
    }
}