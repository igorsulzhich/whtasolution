﻿using System.Security.Principal;
using Microsoft.Practices.ServiceLocation;
using WexHealth.BLL.Services.Common.Interfaces;

namespace WexHealth.Security.Infrastructure
{
    public class WexHealthPrincipal : IPrincipal
    {
        private IIdentity _identity;
        private IRoleService _roleService = ServiceLocator.Current.GetInstance<IRoleService>();

        public WexHealthPrincipal(WexHealthIdentity identity)
        {
            _identity = identity;
        }

        public IIdentity Identity
        {
            get
            {
                return (WexHealthIdentity)_identity;
            }
        }

        public bool IsInRole(string role)
        {
            return _roleService.IsUserInRole(_identity.Name, role);
        }
    }
}