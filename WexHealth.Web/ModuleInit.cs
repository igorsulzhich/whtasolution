﻿using System.ComponentModel.Composition;
using Autofac;
using WexHealth.Common.Modules.Common.Interfaces;
using WexHealth.Web.Cache;
using WexHealth.Web.Cache.Common.Interfaces;

namespace WexHealth.Web
{
    [Export(typeof(IModule))]
    public class ModuleInit : IModule
    {
        public void Initialize(ContainerBuilder builder)
        {
            builder.RegisterType<WebCache>().As<ICache>();
        }
    }
}
