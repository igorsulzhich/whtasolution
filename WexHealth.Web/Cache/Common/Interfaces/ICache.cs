﻿namespace WexHealth.Web.Cache.Common.Interfaces
{
    public interface ICache
    {
        string Key { set; }

        object GetCache();

        void SetCache(object data);
    }
}
