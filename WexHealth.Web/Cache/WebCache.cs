﻿using System;
using System.Web;
using System.Web.Caching;
using log4net;
using WexHealth.Web.Cache.Common.Interfaces;

namespace WexHealth.Web.Cache
{
    public class WebCache : ICache
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(WebCache));

        private string _key;

        public string Key
        {
            set
            {
                _key = value;
            }
        }

        public WebCache()
        {

        }

        /// <summary>
        /// Get data from Cache
        /// </summary>
        /// <returns></returns>
        public object GetCache()
        {
            object result = null;

            try
            {
                result = HttpRuntime.Cache[_key];
                log.Info(string.Format($"Method GetCache try to get {_key} from the cache"));
            }
            catch (Exception e)
            {
                log.Error("Cache error", e);
                throw;
            }

            return result;
        }

        /// <summary>
        /// Set data to Cache
        /// </summary>
        /// <param name="data">Input data</param>
        public void SetCache(object data)
        {
            try
            {
                //var sqlDep = new SqlCacheDependency("DefaultConnection", "State");

                if (null != data)
                {
                    HttpRuntime.Cache.Insert(
                        _key,
                        data,
                        null,
                        DateTime.Now.AddMinutes(5),
                        TimeSpan.Zero,
                        CacheItemPriority.High,
                        null);
                }

                log.Info(string.Format($"{_key} successfully set to cache"));
            }
            catch (Exception e)
            {
                log.Error("Cache error", e);
                throw;
            }
        }
    }
}
