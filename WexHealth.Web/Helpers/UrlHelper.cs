﻿using System;
using System.Web;
using System.Collections.Specialized;

namespace WexHealth.Web.Helpers
{
    public static class UrlHelper
    {
        public static string AddParamsUrl(NameValueCollection paramList)
        {
            string result = null;

            if (null == paramList) throw new ArgumentNullException(nameof(paramList));

            string url = HttpContext.Current.Request.Url.AbsolutePath;
            var query = HttpUtility.ParseQueryString(HttpContext.Current.Request.QueryString.ToString());

            foreach (var i in paramList.Keys)
            {
                string key = (string)i;
                string value = (string)paramList[key];

                AddParamUrl(key, value, query);
            }

            result = url;

            if (query.HasKeys())
            {
                result += "?" + query;
            }

            return result;
        }

        private static void AddParamUrl(string paramName, string paramValue, NameValueCollection query)
        {
            if (string.IsNullOrEmpty(paramName)) throw new ArgumentNullException(nameof(paramName));

            if (null == query) throw new ArgumentNullException(nameof(query));

            if (!string.IsNullOrEmpty(paramValue))
            {
                query.Set(paramName, paramValue);
            }
            else
            {
                query.Remove(paramName);
            }
        }

    }
}
